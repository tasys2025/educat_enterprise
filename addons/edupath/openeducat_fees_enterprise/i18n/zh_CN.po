# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* openeducat_fees_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 12.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-26 06:36+0000\n"
"PO-Revision-Date: 2019-09-26 06:36+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_fees_enterprise
#: selection:op.fees.template.line,duration_type:0
msgid "After"
msgstr "后"

#. module: openeducat_fees_enterprise
#: selection:op.fees.template.line,duration_type:0
msgid "Before"
msgstr "之前"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__create_uid
msgid "Created by"
msgstr "由...制作"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__create_date
msgid "Created on"
msgstr "创建于"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__days
msgid "Days"
msgstr "天"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__display_name
msgid "Display Name"
msgstr "显示名称"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__duration_type
msgid "Duration Type"
msgstr "工期类型"

#. module: openeducat_fees_enterprise
#: model:ir.actions.server,name:openeducat_fees_enterprise.ir_cron_send_fees_reminder_ir_actions_server
#: model:ir.cron,cron_name:openeducat_fees_enterprise.ir_cron_send_fees_reminder
#: model:ir.cron,name:openeducat_fees_enterprise.ir_cron_send_fees_reminder
msgid "Fees Collection Reminder"
msgstr "收费催收提醒"

#. module: openeducat_fees_enterprise
#: model:ir.model,name:openeducat_fees_enterprise.model_op_fees_terms_line
msgid "Fees Details Line"
msgstr "收费详细线路"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__line_id
msgid "Fees Line"
msgstr "费行"

#. module: openeducat_fees_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_fees_enterprise.view_fees_term_reminder_form
msgid "Fees Remider Lines"
msgstr "费Remider线"

#. module: openeducat_fees_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_fees_enterprise.view_fees_term_reminder_form
msgid "Fees Reminder Details"
msgstr "费提醒详情"

#. module: openeducat_fees_enterprise
#: model:ir.model,name:openeducat_fees_enterprise.model_op_fees_template_line
msgid "Fees Template Line"
msgstr "费模板线"

#. module: openeducat_fees_enterprise
#: model:ir.model,name:openeducat_fees_enterprise.model_op_fees_terms
msgid "Fees Terms For Course"
msgstr "费条款对于课程"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__id
msgid "ID"
msgstr ""

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line____last_update
msgid "Last Modified on"
msgstr "最后修改"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__write_uid
msgid "Last Updated by"
msgstr "上次更新"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__write_date
msgid "Last Updated on"
msgstr "最后更新于"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_terms_line__line_ids
msgid "Lines"
msgstr "行"

#. module: openeducat_fees_enterprise
#: model:ir.model.fields,field_description:openeducat_fees_enterprise.field_op_fees_template_line__template_id
msgid "Template"
msgstr "模板"

