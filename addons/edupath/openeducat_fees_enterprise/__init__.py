
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from . import models
from . import wizard

from odoo import api, SUPERUSER_ID, _
from odoo.exceptions import ValidationError


def _fees_pre_init(cr):
    env = api.Environment(cr, SUPERUSER_ID, {})
    plan_module = env["ir.module.module"].search(
        [('name', '=', 'openeducat_fees_plan'),
         ('state', '=', 'installed')])
    if plan_module:
        raise ValidationError(
            _("Fees Plan Module is already installed. "
              "So, You are not allowed to install this module."))
