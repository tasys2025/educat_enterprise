from odoo import models
import json
from odoo import api, models
import uuid
import unicodedata
from odoo.tools import ustr
import re


def sanitize_for_xmlid(s):
    s = ustr(s)
    uni = unicodedata.normalize('NFKD', s).encode('ascii', 'ignore').decode('ascii')

    slug_str = re.sub('[\W]', ' ', uni).strip().lower()
    slug_str = re.sub('[-\s]+', '_', slug_str)
    return slug_str[:20]


class IrUiView(models.Model):
    _inherit = 'ir.ui.view'

    TEMPLATE_BLACKLIST = [
        'web.html_container',
        'web.report_layout',
        'web.external_layout',
        'web.internal_layout',
        'web.basic_layout',
        'web.minimal_layout',
        'web.external_layout_background',
        'web.external_layout_boxed',
        'web.external_layout_clean',
        'web.external_layout_standard',
    ]

    def _apply_groups(self, node, name_manager, node_info):
        if self._context.get('editor'):
            if node.get('groups'):
                editor_groups = []
                for xml_id in node.attrib['groups'].split(','):
                    group = self.env.ref(xml_id, raise_if_not_found=False)
                    if group:
                        editor_groups.append({
                            "id": group.id,
                            "name": group.name,
                            "display_name": group.display_name
                        })
                node.attrib['editor_group'] = json.dumps(editor_groups)

        return super(IrUiView, self)._apply_groups(node, name_manager, node_info)


class IrUiMenu(models.Model):
    _inherit = 'ir.ui.menu'

    @api.model_create_multi
    def create(self, vals):
        res = super(IrUiMenu, self).create(vals)
        if self._context.get('editor'):
            res.create_model_field_data_for_editor(res.display_name)
        return res


class Base(models.AbstractModel):
    _inherit = 'base'

    def create_model_field_data_for_editor(self, name):
        IrModelData = self.env['ir.model.data']

        data = IrModelData.search([
            ('model', '=', self._name), ('res_id', '=', self.id)
        ])
        if data:
            data.write({})
        else:
            IrModelData.create({
                'name': '%s_%s' % (sanitize_for_xmlid(name), uuid.uuid4()),
                'model': self._name,
                'res_id': self.id,
            })
