odoo.define('openeducat_web_no_code_studio.ViewsAllAction', function(require){
    'use strict';

    var Widget = require('web.Widget');
    var core = require('web.core');
    var QWeb = core.qweb;
    var SearchRenderer = require('openeducat_web_no_code_studio.SearchRenderer');

    return Widget.extend({
        template: 'openeducat_web_no_code_studio.all_views.action',
        xmlDependencies: ['/openeducat_web_no_code_studio/static/src/xml/all_views_action.xml'],
        ALL_VIEWS:[
            'form',
            'list',
            'calendar',
//            'graph',
            'activity',
            'search',
        ],
        fonts:{
            'form': 'fa-address-book-o',
            'list': 'fa-th-list',
            'calendar': 'fa-calendar-o',
//            'graph': 'fa-pie-chart',
            'activity': 'fa-history',
            'search': 'fa-search',
        },
        events: {
            'click .view_all_component_change': '_onViewClick',
        },
        init: function(parent, action){
            this._super.apply(this, arguments);
            this.action = action;
            this.active_views =  _.map(this.action.views, function (view) {
                return view[1];
//                return view.type;
            });
            this.default_active_view = this.active_views[0];
        },

        start: function(){
            var self = this;

            var ordered_view_types = this.active_views.slice();
            _.each(this.ALL_VIEWS, function (el) {
                if (! _.contains(ordered_view_types, el)) {
                    ordered_view_types.push(el);
                }
            });
//            this.action.search_view ? this.active_views.push('search') : false;
            _.each(this.active_views, function (view_type) {
                var is_default_view = (view_type === self.default_active_view);
                var active = _.contains(self.active_views, view_type);
                if(['form', 'list','calendar','search'].includes(view_type)){
                    var $view = $(QWeb.render('openeducat_web_no_code_studio.all_view.component',{
                        widget: self,
                        active: active || view_type === 'search',
                        default_view: is_default_view,
                        can_default: !_.contains(['form', 'search'], view_type),
                        view_type: view_type,
                        font: self.fonts[view_type],
                        can_be_disabled: view_type !== 'search',
                    }));
                    $view.appendTo(
                        self.$el.find('.web_no_code_studio_view_all_action')
                    );
                }
            });

            return Promise.all([
                this._super.apply(this, arguments),
            ]);
        },

        _onViewClick: function(e){
            var type = $(e.currentTarget).data('type');
            if(_.contains(this.active_views, type)){
                this.trigger_up('editor_edit_view',{type: type})
            }
            else{
                this.trigger_up('editor_edit_view',{type: type})
                //return new SearchRenderer();
            }
        },
    });
});
