/** @odoo-module **/

import { ComponentAdapter } from "web.OwlCompatibility";
import { useService } from "@web/core/utils/hooks";
import { registry } from "@web/core/registry";
import ViewEditorAction from "openeducat_web_no_code_studio.web_no_code_studio_action";
const { Component } = owl;

export class EditorAdapter extends ComponentAdapter {
    constructor(props) {
        props.Component = Component;
        super(...arguments);
    }

    setup() {
        super.setup();

        this.props.Component = ViewEditorAction;
        this.dialog = useService("dialog");
        this.user = useService("user");
        this.dialog = useService("dialog");
        this.viewService = useService("view");
        this.rpc = useService("rpc");
        this.wowlEnv = this.env;
        this.env = Component.env; // use the legacy env
    }

    get widgetArgs() {
        const args = Object.keys(this.props);
        return [
            this.props.action,
        {
            action: this.props.action,
            viewType: this.props.action.context.viewType,
        }];
    }
}

registry.category("actions").add("web_no_code_studio_action", EditorAdapter);