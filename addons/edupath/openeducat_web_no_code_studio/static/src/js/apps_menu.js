/** @odoo-module **/

import { NavBar } from "@web/webclient/navbar/navbar";
import { HomeMenu } from "@openeducat_backend_theme/js/edu/apps_menu";
import { patch } from 'web.utils';
const { hooks } = owl;
const { useState, useRef } = owl;
const { onMounted, useExternalListener } = owl;
import { bus } from 'web.core';
const config = require('web.config');
const { Component, QWeb } = owl;
const core = require("web.core");

patch(NavBar.prototype, 'openeducat_web_no_code_studio/static/src/js/apps_menu.js', {
    setup() {
        this._super();
        this.create_app = false;
        this.env.bus.on("apps_menu_started", this, () => {
             this._appsMenuStarted()
        });
    },

    _appsMenuStarted: function(mode){
        mode = "create_app"
        if(mode == "create_app"){
            this.create_app = true;
            this._searchReset();
        }else{
            this.create_app = false;
        }
    },

    _searchReset: function () {
//        this.$search_container.removeClass("has-results");
//        this.$search_results.empty();
//        this.$search_input.val("");
        $('.o_app_search').css('display', 'none');
        $('.full').css('pointer-events','all');
        this._searchApps = this._apps;
//        var html = core.QWeb.render('AppsSearch', {
//            apps: this._searchApps,
//            widget: this,
//        });
        this.env.bus.trigger('app_menu_toggled', true);
        
    },

    _onAppsMenuItemClicked: function(ev){
        if($(ev.currentTarget).hasClass('create_new_app')){
            new CreateNewApp(this).open();
        }else{
            this._super.apply(this, arguments);
        }
    },

    _onAppsSearch: function (ev) {
        ev.preventDefault();
        var search_txt = this.$search_input.val().trim().toLowerCase();
        if (search_txt.length) {
            this._searchApps = _.filter(this._apps, function (app) {
                return app.name.toLowerCase().indexOf(search_txt) !== -1;
            });
        } else {
            this._searchApps = this._apps;
        }
        var html = Qweb.render('AppsSearch', {
            apps: this._searchApps,
            widget: this,
        });
        this.$el.find('.o_apps_container').replaceWith(html);
        this._search_def = new Promise((resolve) => {
            setTimeout(resolve, 50);
        });
        this._search_def.then(this._searchMenus.bind(this));
    },
});

