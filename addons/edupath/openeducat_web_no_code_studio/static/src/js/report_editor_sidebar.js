odoo.define('openeducat_web_no_code_studio.ReportEditorSideBar', function(require){
    'use strict';

    var Widget = require('web.Widget');
    var Domain = require("web.Domain");
    var ajax = require("web.ajax");
    var ReportElements = require('openeducat_web_no_code_studio.ReportElements');
    var ReportElementsEdit = require('openeducat_web_no_code_studio.ReportElementsEdit').registry;
    var config = require('web.config');
    var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
    var Many2ManyTags = require('web.relational_fields').FieldMany2ManyTags;
    var field_registry = require('web.field_registry');
    var fieldRegistryOwl = require('web.field_registry_owl');
    var DomainSelectorDialog = require('web.DomainSelectorDialog');
    var ModelFieldSelector = require("web.ModelFieldSelector");
    var utils = require('web.utils');
    var NodeTools = require('openeducat_web_no_code_studio.NodeTools');
    var Domain = require("web.Domain");
    var pyUtils = require('web.py_utils');
    var core = require('web.core');
    var _t = core._t;

    var ReportEditorSideBar = Widget.extend(StandaloneFieldManagerMixin, {
        template: 'openeducat_web_no_code_studio.report_side_bar',
        events: {
            'click .remove-element': '_onClickRemoveElement',
            'change input': '_onChangeReportAttribute',
            'change select#paperformat_id': '_onChangePaperFormat',
            'click button[name="background_image"]': '_onClickBackgroundImage',
        },
        init: function(parent, mode, currentNode, params){
            this._super.apply(this, arguments);
            StandaloneFieldManagerMixin.init.call(this);
            var self = this;
            this.models = params.models;
            this.paper_formats = params.paper_formats;
            this.r_name = parent.r_name;
            this.parent = parent;
            this.previous_node = parent.previous_node;
            this.view_id = parent.view_id;
            this.$iframe = parent.$iframe;
            this.debug = config.isDebug();
            this.currentNode = currentNode || null;
            this.previousState = params.previousState || {};
            this.binding_view_id = parent.report_data.read_data.binding_model_id[0];
            this.paperformat_id = parent.report_data.read_data.paperformat_id ? parent.report_data.read_data.paperformat_id[0] : false;
            this.mode = mode
        },

        start: function(){
            this._super.apply(this, arguments);
            this._postStart();
        },

        _postStart: function(){
            if(this.mode == 'add'){
                this._afterRenderAdd();
            } else if( this.mode == 'options' ){
                this._afterRenderOptions();
            }
        },

        _onChangeReportAttribute: function(ev){
            var $input = $(ev.currentTarget);
            var attribute = $input.attr('name');
            if (attribute) {
                var newAttrs = {};
                if ($input.attr('type') === 'checkbox') {
                    newAttrs[attribute] = $input.is(':checked') ? 'True' : '';
                } else {
                    newAttrs[attribute] = $input.val();
                }
                this.trigger_up('view_edit_report', newAttrs);
            }
        },

        _onClickBackgroundImage: function(e){
            var self = this;
            var $image = $('<img/>');
            var dialog = new WysiwygWidgets.MediaDialog(this, {
                onlyImages: true,
            }, $image[0]).open();
            dialog.on("save", self, function (el) {
                var op = [];
                var node = self.$iframe.contents().find('div.page').data('node');
                var current_format =  _.find(self.paper_formats, function(val){
                    return val.id == self.paperformat_id
                });
                var width = current_format ? current_format.print_page_width : 210;
                var height = current_format ? current_format.print_page_height : 297;
                var style= `background: url('${el.attributes.src.value}'); background-size: ${width}mm ${height}mm; width: ${width}mm; height: ${height}mm;`
                op.push({
                    content: '<attribute name="style">' + style + '</attribute>',
                    position: "attributes",
                    view_id: +node.attrs['data-oe-id'],
                    xpath: node.attrs['data-oe-xpath']
                });
                self.trigger_up('update_report', {
                    node: node,
                    operation: {
                        inheritance: op,
                    },
                });
            });
        },

        _afterRenderOptions: function(){
            this.waterfall_node = [];
            var ignorants = [];
            var self = this;
            var componentsAppendedPromise;
            var activeEnabled = false;
            var $accordion = this.$('#accordionElements');
            if (!this.debug) {
                var pageNodeIndex = _.findIndex(this.currentNode[0], function (node) {
                    return node.node.tag === 'div' && _.str.include(node.node.attrs.class, 'page');
                });
                if (pageNodeIndex !== -1) {
                    this.currentNode.splice(pageNodeIndex + 1, this.currentNode.length - (pageNodeIndex + 1));
                }
            }

            for (var index = this.currentNode.length - 1; index >= 0; index--) {
                var node = _.extend({}, this.currentNode[index]);
                if (!this.debug && ignorants.length) {
                    var df = this.currentNode[0].node
                    if (NodeTools._getAssociatedDOMNode($(this.currentNode[0].node)[0]).is(ignorants.join(','))) {
                        continue;
                    }
                }
                var components = NodeTools._getNodeEditableComponents(this.currentNode[0].node);
                node.components = components;
                var blacklist = '';
                _.each(this._getComponentsObject(components), function (Element) {
                    if (Element.prototype.blacklist) {
                        if (blacklist.length) {
                            blacklist += ',';
                        }
                        blacklist += Element.prototype.blacklist;
                    }
                });
                blacklist = blacklist;
                if (blacklist.length) {
                     ignorants.push(blacklist);
                }
                node.widgets = [];
                this.waterfall_node.unshift(node);
            }
            this.waterfall_node.reverse();
            this.waterfall_node.forEach(function (node, key) {
                if(self.parent.currentNode){
                    var isActive = (node.node.attrs['data-oe-id'] == self.parent.currentNode.attrs['data-oe-id'] && node.node.attrs['data-oe-xpath'] == self.parent.currentNode.attrs['data-oe-xpath']);
                    if(isActive){
                        activeEnabled = true;
                    }
                    if(!activeEnabled && self.waterfall_node.length - 1  == key){
                        isActive = true;
                    }
                }
                var $accordionSection = $(core.qweb.render('openeducat_web_no_code_studio.report_option_tag_waterfall', {
                    id: _.uniqueId('id_'),
                    header: _.uniqueId('header_'),
                    nodeName: node.node.tag,
                    node: node.node,
                    isActive: isActive,
                    isLast: key == self.waterfall_node.length - 1 ? true: false,
                    isFirst: key == 0 ? true: false,
                }));
                var renderingProms = self._getComponentsObject(node.components).map(function (Element) {
                    if (!Element) {
                        self.do_warn("Missing component", self.state.directive);
                        return;
                    }
                    var ElName = node.node.attrs["data-oe-id"] + node.node.attrs["data-oe-xpath"].replace(/\[\]\//g, "_");
                    var previousWidgetState = self.previousState[ElName] &&
                        self.previousState[ElName][Element.prototype.name];
                    var directiveWidget = new Element(self, {
                        widgetsOptions: self.widgetsOptions,
                        node: node.node,
                        context: node.context,
                        state: previousWidgetState,
                        models: self.models,
                        componentsList: node.components,
                    });
                    node.widgets.push(directiveWidget);
                    var fragment = document.createDocumentFragment();
                    return directiveWidget.appendTo(fragment);
                });
                componentsAppendedPromise = Promise.all(renderingProms).then(function () {
                    for (var i = 0; i < node.widgets.length; i++) {
                        var widget = node.widgets[i];
                        var selector = '.collapse' + (i > 0 ? '>div:last()' : '');
                        widget.$el.appendTo($accordionSection.find(selector));
                    }
                    var $removeButton = $('<button/>', {
                        text: 'Remove From View',
                        class: 'btn btn-danger remove-element mt16'
                    });
                    $removeButton.data('element', node.node);
                    $accordionSection.find('.collapse')
                        .append($removeButton);
                });
                $accordionSection.appendTo($accordion);
                $accordionSection
                    .on('mouseenter', function () {
                        self.trigger_up('sidebar_node_hover', {
                            node: node.node,
                        });
                    })
                    .on('click', function () {
                        self.trigger_up('sidebar_node_selected', {
                            node: node.node,
                        });
                    })
                    .on('mouseleave', function () {
                        self.trigger_up('sidebar_node_hover', {
                            node: undefined,
                        });
                    })
                    .find('.collapse').on('show.bs.collapse hide.bs.collapse', function (ev) {
                        $(this).parent().toggleClass('web_no_code_studio_active', ev.type === 'show');
                        $(this).parent().find('.arrow_accordion').toggleClass('fa-angle-right fa-angle-down')
                    });
            });

            var $lastCard = $accordion.find('.card:last');
            $lastCard.addClass('web_no_code_studio_active');
            $lastCard.find('.collapse').addClass('show');

            return componentsAppendedPromise;
        },

        _getComponentsObject: function (components) {
            return _.map(components, function (componentName) {
                var Element = _.find(ReportElementsEdit.map, function (Element) {
                    return Element.prototype.name === componentName;
                });
                return Element;
            });
        },

        _onChangePaperFormat: function(e){
            var $target = $(e.currentTarget).val();
            var newAttrs = {
                paperformat_id: $target
            }
            this.trigger_up('view_edit_report', newAttrs);
        },

        _onClickRemoveElement: function(event){
            var element = $(event.currentTarget).data('element');
            this.trigger_up('remove_element_report', {
                node: element,
            });
        },

        _afterRenderAdd: function(){
            this.def = [];
            var self = this;
            this._renderSubSection('blockElements');
            this._renderSubSection('reportElements');
            this._renderSubSection('inlineElements');
            this._renderSubSection('tableElements');
            return Promise.all(this.def).then(() => {
                delete(this.def);
            });
        },

        _renderSubSection: function(type){
            var typeClass = type,
                self = this,
                title = type === 'blockElements' ? 'Block' : type === 'reportElements' ? 'Columns': type === 'inlineElements' ? 'Inline': 'Tables',
                toAddClass = type === 'block' ? 'web_no_code_studio_new_element' : 'web_no_code_studio_new_field',
                self = this,
                classes = ReportElements.get(typeClass);

            var $title = $("<h6 class='text-black'> "+ title + " </h6>");
            var $elementSection = $("<div class='mb-3 web_no_code_studio_field_section " + toAddClass + "' />");
            var widgets = classes.map(Element => new Element(this, {models: self.models}));

            widgets.forEach(function (widget){
                self.def.push(widget.appendTo($elementSection));
            });
            this.$('.side_bar_report_main_data').append($title, $elementSection);
        },

    });

    return ReportEditorSideBar;

});
