odoo.define('openeducat_web_no_code_studio.bus', function (require) {
"use strict";

var Bus = require('web.Bus');

return new Bus();

});
