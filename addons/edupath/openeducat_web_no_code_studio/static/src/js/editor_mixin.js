odoo.define('openeducat_web_no_code_studio.EditorMixin', function(require){
   'use strict';

    var framework = require('web.framework');

    var EditorMixin = {

        displayHook: function ($helper, position) {
            this.$('.web_no_code_studio_nearest_hook').find('td').removeAttr("style");
            this.$('.web_no_code_studio_nearest_hook').removeClass('web_no_code_studio_nearest_hook');
        },

        setSelectable: function ($el) {
            var self = this;

            $el.on('click', function () {
                self.unselectedElements();
                $(this).addClass('web_no_code_studio_clicked');
            }).on('mouseover',function () {
                if (self.$('.ui-draggable-dragging').length) {
                    return;
                }
                $(this).addClass('web_no_code_studio_hovered');
            }).on('mouseout', function () {
                $(this).removeClass('web_no_code_studio_hovered');
            });
        },

        _onHandleDrop: function ($drag, node, position) {
            framework.blockUI();
            var isNew = $drag.hasClass('web_no_code_studio_draggable');
            var values;

            if (isNew) {
                values = {
                    type: 'add',
                    structure: $drag.data('structure'),
                    field_description: $drag.data('field_description'),
                    node: node,
                    new_attrs: $drag.data('new_attrs'),
                    position: position,
                };
            } else {
                var movedFieldName = $drag.data('name');
                if (node.attrs.name === movedFieldName) {
                    return;
                }
                values = {
                    type: 'move',
                    node: node,
                    position: position,
                    structure: 'field',
                    new_attrs: {
                        name: movedFieldName,
                    },
                };
            }
            this.trigger_up('hook_selection');
            this.trigger_up('update_view', values);
            framework.unblockUI();
        },

        unselectedElements: function () {
            this.selected_node_id = false;
            this.$('.web_no_code_studio_clicked').removeClass('web_no_code_studio_clicked');
            if (this.$('.web_no_code_studio_clicked').find('.blockUI')) {
                this.$('.web_no_code_studio_clicked').find('.blockUI').parent().unblock();
            }
        },
    };

    return EditorMixin;
});
