/** @odoo-module **/

import { WebClient } from "@web/webclient/webclient";
const { HomeMenu } = require('@openeducat_backend_theme/js/edu/home_menu');
import  MenuBar  from "@openeducat_web_no_code_studio/js/abstract/menu_bar";
import  MenuWrapper  from "@openeducat_web_no_code_studio/js/edit_menu_adapter";
import bus from "openeducat_web_no_code_studio.bus";
import EditTab from "openeducat_web_no_code_studio.EditTab";
import CreateNewModel from "openeducat_web_no_code_studio.CreateNewModel";
var core = require('web.core');
const { EventBus, useState } = owl;
import framework from 'web.framework';
import { useBus, useEffect, useService } from "@web/core/utils/hooks";
import { ErrorHandler, NotUpdatable } from "@web/core/utils/components";
import { NavBar, MenuDropdown, MenuItem } from "@web/webclient/navbar/navbar";
import { EditMenuItem } from "@openeducat_web_no_code_studio/js/edit_menu_adapter";
import { patch } from 'web.utils';
import Widget from "web.Widget";

export class StudioNavBar extends NavBar {}
patch(NavBar.prototype, 'openeducat_web_no_code_studio/static/src/js/menu.js', {
    setup() {
        this._super();
        const bus = new EventBus();
        this.menuService = useService("menu");
        this.state = useState({
            displayMenu: false
        })
        this.env.bus.on('editor_toggled', this, this.toggleViewEditor);
        core.bus.on('web_no_code_studio_menu', this, this._toggleMenuBar);
        core.bus.on('web_create_new_model', this, this._onClickNewModel);
    },

    toggleViewEditor(state){
        var self = this;
//        this.currentApp = this.menuService.getCurrentApp()
        var menu_data = this.menuService.getMenuAsTree(this.currentApp.id)
        var oldState = this.viewEditorState;
        if( oldState == state){
            return;
        }
        this.viewEditorState = state;
        if(state){

            if(!oldState){
                this.removed_systray = $('.o_menu_systray');
                $('.o_menu_systray').addClass('d-none');
            }
            this.exit = $('<div class="web_no_code_studio_exit" >'+
                '<a href="#" class="web_no_code_studio_exit_button">Close</a>'+
            '</div>');


            this.exit.appendTo('.o_main_navbar');
            this.exit.on('click', function(e){
                e.preventDefault();
                if(this.editor_menu_bar){
                    this.editor_menu_bar.destroy();
                }
                core.bus.trigger('click_web_no_code_studio', false);
            })
            var state = $.bbq.getState();
            var primary_menu = parseInt(state.menu_id);
            this.state.displayMenu = true;
//            this.edit_tab = new MenuWrapper(this, menu_data, primary_menu);
//            this.edit_tab.appendTo('.o_menu_sections');
//            this.edit_tab.on('click', function(e){
//                var state = $.bbq.getState();
//                var primary_menu = parseInt(state.menu_id);
//                e.preventDefault();
//                if($(e.target).hasClass('menu_edit_tab')){
//                    new EditTab(self, menu_data, primary_menu).open();
//                }else{
//                    new CreateNewModel(self, primary_menu).open();
//                }
//            })
            $('.o_menu_sections').append(this.$edit_tab);
            $('.o_main_navbar').append(this.$exit);
        }else{
            $('.o_menu_systray').removeClass('d-none');
            this.state.displayMenu = false;
            $('.web_no_code_studio_menu_bar').remove();
            if(this.exit){
                this.exit.remove();
            }
            if(this.edit_tab){
                $('.edit_tab_menu').remove();
            }
        }
    },

    _onClickEditTab(e){
        e.preventDefault();
        new EditTab(this, this.menu_data, this.current_primary_menu).open();
    },

    _onClickNewModel(){
        var state = $.bbq.getState();
        var primary_menu = parseInt(state.menu_id);
        this.current_primary_menu = 183;
        new CreateNewModel(this, primary_menu).open();
    },

    _onClickViewEditorExit(e){
        // To Prevent From Changing URL
        framework.blockUI();
        e.preventDefault();
        if(this.editor_menu_bar){
            this.editor_menu_bar.destroy();
        }

        bus.trigger('click_web_no_code_studio');
    },

    _toggleMenuBar(action){
        this._renderMenuBar(action);
    },

    _renderMenuBar(action){
        if(this.editor_menu_bar){
            this.editor_menu_bar.destroy();
        }
        this.editor_menu_bar = new MenuBar(this, action);
        return this.editor_menu_bar.insertAfter($('.o_main_navbar'));
    },
});
StudioNavBar.components = { ...NavBar.components, EditMenuItem }
StudioNavBar.template = NavBar.template;