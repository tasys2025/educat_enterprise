/** @odoo-module **/
import { startWebClient } from "@web/start";
import { WebClientStudio } from "./web_client";
startWebClient(WebClientStudio);