odoo.define('openeducat_web_no_code_studio.ReportEditorView', function(require){
    'use strict';

    var Widget = require('web.Widget');
    var core = require('web.core');
    var QWeb = core.qweb;
    var ajax = require('web.ajax');
    var utils = require('web.utils');
    var NodeTools = require('openeducat_web_no_code_studio.NodeTools');
    var SearchRenderer = require('openeducat_web_no_code_studio.SearchRenderer');
    var EditorMixin = require('openeducat_web_no_code_studio.EditorMixin');
    var ReportEditorSideBar = require('openeducat_web_no_code_studio.ReportEditorSideBar');
    var ReportFieldDialog = require('openeducat_web_no_code_studio.ReportFieldDialog');
    const { getBundle, loadBundle } = require('@web/core/assets');

    var ReportEditorView =  Widget.extend(EditorMixin,{
        template: 'ReportEditorViewData',
        xmlDependencies: ['/openeducat_web_no_code_studio/static/src/xml/report.xml'],
        custom_events: {
            drag_started: '_onDragStart',
        },
        events: _.extend({}, Widget.prototype.events, {
            'click .side_bar_report': '_onReportClick',
            'click .side_bar_add': '_onAddClick',
            'click .page':'_onClickReportData',
            //'click': '_onClick'
        }),

        init: function(parent, fieldsView, params){
            this.action = parent.action;
            this.params = params;
            this.models = params.models;
            this.paper_formats = params.paper_formats;
            this.report_data = parent.report_data;
            this.r_url = parent.r_url;
            this.view_id = parent.view_id;
            this.r_name = parent.r_name;
            this.previous_node = parent.previous_node;
            this._super.apply(this, arguments);
            var parser = new DOMParser();
            this.viewEditor = null;
            this.nearest_hook = 500;
            this.dragging = false;
            this.currentNode = null;
            this._onUpdateContentId = _.uniqueId('_processReportPreviewContent');
            this.hook_nodes = {};
            this.$dropZone = $();
            this.$hoverNode = $();
        },

        start: function(){
            var self = this;
            this.$iframe = this.$('iframe');
            this._super.apply(this, arguments);
            $('.container-fluid').empty();
            this.$content = this.$('iframe').contents();
            var reportHTML = this.report_data.report_html;
            var $main = this.$content.find('main:first');
            this.$iframe.ready(this._updateReportContent.bind(this));
            this._afterRender();
            this._adjustPaperFormat();
        },

        sideBarSelectNode: function(node){
            if (this.currentNode) {
                if (this.currentNode === node) { return; }
                var $oldSelectedNodes = this._searchConnectedNodes(this.currentNode);
                $oldSelectedNodes.removeClass('node_element_selected');
            }

            this.currentNode = node;
            var $nodesToHighlight = this._searchConnectedNodes(this.currentNode);
            $nodesToHighlight.addClass('node_element_selected');
        },

        hoverNodeElement: function(node){
            if (!this.$highlight) {
                this.$highlight = $('<span class="web_no_code_studio_report_highlight"/>');
                this.$content.find('body').prepend(this.$highlight);
            }

            if (this.$hoverNode.data('node') !== this.selectedNode) {
                this.$hoverNode.removeClass('node_element_hovered');
            }

            var $nodes = this._searchConnectedNodes(node);
            if ($nodes && $nodes.length) {
                this.$hoverNode = $nodes.addClass('node_element_hovered');
            } else {
                this.$highlight.hide();
            }
        },

        _adjustPaperFormat: function(){
            var self = this;
            if(this.report_data.read_data.paperformat_id){
                var current_format =  _.find(this.paper_formats, function(val){
                    return self.report_data.read_data.paperformat_id[0] == val.id
                });
                if(current_format){

                    this.$iframe.parent().css({
                        'padding-top': Math.max(0, (current_format.margin_top || 0) - (current_format.header_spacing || 0)) + 'mm',
                        'padding-left': (current_format.margin_left || 0) + 'mm',
                        'padding-right': (current_format.margin_right || 0) + 'mm',
                        'width': (current_format.print_page_width || 210) + 'mm',
                        'min-height': (current_format.print_page_height || 297) + 'mm',
                    });

                    this.$iframe.css({
                        'min-height': (current_format.print_page_height || 297) + 'mm',
                    });

                }
            }
        },

        _connectNodes: function () {
            var self = this;
            this.nodesNotInView = [];
            _.each(this.params.nodesArchs, this._connectNodesDirectly.bind(this));

            var children = this.$content.children().get();
            for (var k = 0; k < children.length; k++) {
                NodeTools.connectContextOrder(children[k], []);
            }

            var bodyContext = this.$content.find('html').data('oe-context');
            _.each(this.nodesNotInView, function (node) {
                node.context = node.parent && node.parent.context || bodyContext;
            });
        },

        _connectNodesDirectly: function(node){
            if (!node.attrs) { return; }
            var $nodes = this._searchConnectedNodes(node);
            $nodes.data('node', node);
            node.$nodes = $nodes;
            if ($nodes.length) {
                node.context = $nodes.data('oe-context');
            } else {
                this.nodesNotInView.push(node);
            }

            _.each(node.attrs, function (value, key) {
                if ($nodes.attr(key) === undefined) {
                    $nodes.attr(key, value);
                }
            });
            _.each(node.children, this._connectNodesDirectly.bind(this));
        },

        _onAddClick: function() {
            this._afterRender();
        },

        getNodeContext: function (initialNode) {
            var node = initialNode;
            var $nodes = this._searchConnectedNodes(node);
            while (!$nodes.length && node.parent) {
                var index = node.parent.children.indexOf(node);
                for (index; index > 0; index--) {
                    $nodes = this._searchConnectedNodes(node.parent.children[index]);
                    if ($nodes.length) { break; }
                }
                if (!$nodes.length) { node = node.parent; }
            }
            if (!$nodes.length) {
                $nodes = this.$content.find('*[data-oe-xpath]');
            }

            return $nodes.data('oe-context');
        },

        _onReportElementClick: function(event){
            event.preventDefault();
            event.stopPropagation();

            if ($(event.target).hasClass('o_no_content_helper')) { return; }

            var $active = $(event.target).closest('[data-oe-xpath]');
            if ($active.closest('[t-field], [t-esc]').length) {
                $active = $active.closest('[t-field], [t-esc]');
            }
            this.getNode($active.data('node'));
            this.trigger_up('element_clicked', {
                node: this.currentNode,
            });
        },

        getNode: function(node){
            if (this.currentNode) {
                if (this.currentNode === node) { return; }
                var $oldSelectedNodes = this._searchConnectedNodes(this.currentNode);
                $oldSelectedNodes.removeClass('node_element_selected');
            }
            this.currentNode = node;
            var $nodesToHighlight = this._searchConnectedNodes(this.currentNode);
            $nodesToHighlight.addClass('node_element_selected');
        },

        elementDrop: function(component){
            this.dragging = false;
            var $hooks = this.$dropZone.filter('.web_no_code_studio_nearest_hook');
            var targets = [];

            $hooks.get().forEach(function (nearHook) {
                var $active = $(nearHook);
                var nodeData = $hooks.data('oe-node');
                var is_target = _.find(targets, function(target){
                    return target.node.attr('data-oe-id') === nodeData.attr('data-oe-id') && target.node.attr('data-oe-xpath') === nodeData.attr('data-oe-xpath');
                }) || [];
                if (!is_target.length) {
                    targets.push({
                        node: nodeData,
                        position: $active.data('oe-position'),
                        data: $active.data(),
                    });
                }
            });

            if (targets.length) {
                this.trigger_up('update_report', {
                    component: component,
                    fail: this._removeHooks.bind(this),
                    targets: targets,
                    operation: {
                        type: 'add',
                        position: $hooks.first().data('oe-position'),
                    },
                });
            } else {
                this._removeHooks();
            }
        },

        _searchConnectedNodes: function (node) {
            if (node) {
                return this.$content.find('html').find('[data-oe-id="' + node.attrs['data-oe-id'] + '"][data-oe-xpath="' + node.attrs['data-oe-xpath'] + '"]');
            } else {
                return $();
            }
        },

        _onDragStart: function(el){
            this.dragging = true;
            if (this.$content.find('.web_no_code_studio_nearest_hook').length === 0) {
                this._renderReportHooks(el);
            }
        },

        _processReportPreviewContent: function () {
            this.$content = this.$('iframe').contents();
            this.$content.off('click').on('click', this._onReportElementClick.bind(this));
            this._connectNodes();
            this._resizeIframe();

            this.$content.find('tr').each(function () {
                var lineMax = 0;
                $(this).children().each(function () {
                    var colspan = +$(this).attr('colspan');
                    $(this).data('colspan', colspan || 1);
                    $(this).data('td-position-before', lineMax);
                    lineMax += colspan || 1;
                    $(this).data('td-position-after', lineMax);
                });
            });
            this._setNoContentHelper();
        },

        _getPaperFormatDpi: function(){
            var self = this;

            if(this.report_data.read_data.paperformat_id){
                var current_format =  _.find(this.paper_formats, function(val){
                    return self.report_data.read_data.paperformat_id[0] == val.id
                });
                if(current_format){
                    return current_format.dpi;
                }
            }
            return 96;
        },

        _resizeIframe: function(){
            var self = this;
            var dpi = this._getPaperFormatDpi();
            var zoom = 96 / dpi;
            self.$content.find('main:first').children().each(function () {
                var sectionZoom = Math.min(zoom, $(this).width() / this.scrollWidth);
                $(this).css({zoom: sectionZoom, padding: '1px'});
            });
            self.$iframe[0].style.height = self.$iframe[0].contentWindow.document.body.scrollHeight + 'px';

            this.$content.find('.footer').css({
                'position': 'fixed',
                'bottom': '0',
                'width': this.$content.find('.page').css('width'),
            });

            this.$content.find('html')[0].style.overflow = 'hidden';
            this.$content.find('html')[0].style.padding = '1px';
            $(this.$content).find("img").on("load", function () {
                self.$iframe[0].style.height = self.$iframe[0].contentWindow.document.body.scrollHeight + 'px';
            });
        },

        _setNoContentHelper: function () {
            var self = this;
            setTimeout( function(){
                var $page = $(self.$content).find('div.page');
                if ($page.length && !$page.children().length) {
                    self.$noContentHelper = $('<div/>', {
                        class: 'o_no_content_helper',
                        text: core._t('Drag building block here'),
                    });
                    $page.append(self.$noContentHelper);
                }
            }, 500);
        },

        _updateReportContent: function () {
            var self = this;
            this.$content = this.$iframe.contents();
            var reportHTML = this.report_data.report_html;

            var $main = this.$content.find('main:first');

            if ($main.length) {
                $main.replaceWith($(reportHTML).find('main:first'));
                this._processReportPreviewContent();
                return Promise.resolve();
            }

            return new Promise(function (resolve, reject) {
                window.top[self._onUpdateContentId] = function () {
                    if (!self.$('iframe')[0].contentWindow) { return reject(); }
                    self._processReportPreviewContent();
                    self.trigger_up('iframe_ready');
                    resolve();
                };
                self._processReportPreviewContent();
                if (reportHTML.error) {
                    throw new Error(reportHTML.message || reportHTML.error);
                } else {
                    reportHTML = reportHTML.replace(
                        '</body>',
                        '<script>window.top.' + self._onUpdateContentId + '()</script></body>'
                    );
                }

                var cwindow = self.$iframe[0].contentWindow;
                cwindow.document
                    .open("text/html", "replace")
                    .write(reportHTML);
                if(self.previous_node){
                    if(self.previous_node.attr){
                        self.$prevNodeToActive = self.$content.find('html').find('[data-oe-id="' + self.previous_node.attr('data-oe-id') + '"][data-oe-xpath="' + self.previous_node.attr('data-oe-xpath') + '"]')
                        var interval = setInterval( function(){
                            if($(self.$content.contents()[1]).find('[data-oe-id="' + self.previous_node.attr('data-oe-id') + '"][data-oe-xpath="' + self.previous_node.attr('data-oe-xpath') + '"]').length){
                                clearInterval(interval)
                                $(self.$content.contents()[1]).find('[data-oe-id="' + self.previous_node.attr('data-oe-id') + '"][data-oe-xpath="' + self.previous_node.attr('data-oe-xpath') + '"]').click();
                            }
                        }, 1000);
                    }else{
                        self.$prevNodeToActive = self.$content.find('html').find('[data-oe-id="' + self.previous_node.attrs['data-oe-id'] + '"][data-oe-xpath="' + self.previous_node.attrs['data-oe-xpath'] + '"]')
                        var interval = setInterval( function(){
                            if($(self.$content.contents()[1]).find('[data-oe-id="' + self.previous_node.attrs['data-oe-id'] + '"][data-oe-xpath="' + self.previous_node.attrs['data-oe-xpath'] + '"]').length){
                                clearInterval(interval)
                                $(self.$content.contents()[1]).find('[data-oe-id="' + self.previous_node.attrs['data-oe-id'] + '"][data-oe-xpath="' + self.previous_node.attrs['data-oe-xpath'] + '"]').click();
                            }
                        }, 1000);
                    }
                }
            });
        },

        elementDrag: function(component, x, y){
            this.dragging = true;
            this.$dropZone
                .filter('.web_no_code_studio_nearest_hook')
                .removeClass('web_no_code_studio_nearest_hook')
                .closest(this.$dropZoneStructure).each(function () {
                    $(this).children().css('height', '').children('.web_no_code_studio_hook:only-child').css('height', '');
                });
            this.$dropZoneStructure.removeClass('web_no_code_studio_nearest');

            var bound = this.$iframe[0].getBoundingClientRect();
            var isInIframe = (x >= bound.left && x <= bound.right) && (y >= bound.top && y <= bound.bottom);
            if (!isInIframe) {
                return;
            }

            _.each(this.dropPosition, function (box) {
                box.dist = Math.sqrt(Math.pow(box.centerY - (y - bound.top), 2) + Math.pow(box.centerX - (x - bound.left), 2));
            });
            this.dropPosition.sort(function (a, b) {
                return a.dist - b.dist;
            });

            if (!this.dropPosition[0] || this.dropPosition[0].dist > this.nearest_hook) {
                return;
            }

            var $hook = $(this.dropPosition[0].el);

            $hook
                .addClass('web_no_code_studio_nearest_hook')
                .closest(this.$dropZoneStructure)
                .addClass('web_no_code_studio_nearest');

            if (!$hook.data('oe-node') || !$hook.data('oe-node').data('oe-id')) {
                return;
            }

            var $node = $hook.data('oe-node');
            var id = $node.data('oe-id');
            var xpath = $node.data('oe-xpath');
            var position = $hook.data('oe-position');
            var index = $hook.data('oe-index');

            var td = $node.is('td, th');
            var reg, replace;
            if (td) {
                reg = /^(.*?)\/(thead|tbody|tfoot)(.*?)\/(td|th)(\[[0-9]+\])?/;
                replace = td && position === 'inside' ? '$1/$2/tr/td' : '$1/tr/td';
                xpath = xpath.replace(reg, replace);
            }

            var $nearestHooks = this.$dropZone.filter(function () {
                var $node = $(this).data('oe-node');
                return $(this).data('oe-position') === position &&
                    $(this).data('oe-index') === index &&
                    $node.data('oe-id') === id &&
                    (td ? $node.data('oe-xpath').replace(reg, replace) : $node.data('oe-xpath')) === xpath;
            });

            if (td) {
                var pos = $hook.data('oe-node').data('td-position-' + (position === 'before' ? 'before' : 'after'));
                $nearestHooks = $nearestHooks.filter(function () {
                    var $node = $(this).data('oe-node');
                    return $node.data('td-position-' + (position === 'before' ? 'before' : 'after')) === pos;
                });
            }
            $nearestHooks.addClass('web_no_code_studio_nearest_hook');
        },

        endDragHooks: function (component) {
            this._removeHooks();
        },

        _removeHooks: function () {
            if (this.dragging) {
                return;
            }

            this.$dropZone.filter('th, td').each(function () {
                var $node = $(this).data('oe-node');
                if ($node) {
                    var colspan = $node.data('colspan');
                    if (colspan) {
                        $node.attr('colspan', colspan);
                    }
                }
            });
            this.$content.find('.web_no_code_studio_hook').remove();
            this.$content.find('.web_no_code_studio_structure_hook').remove();

            //this._setNoContentHelper();
        },

        _renderReportHooks: function(component){
            var self = this;
            this._removeHooks();
            //this.$noContentHelper.remove();

            var dropIn = component.dropIn;
            if (component.dropColumns && component.addEmptyRowsTargets) {
                dropIn = (dropIn ? dropIn + ',' : '') + '.page > .row > div:empty';
            }
            if (dropIn) {
                var inSelectors = dropIn.split(component.selectorSeparator || ',');
                _.each(inSelectors, function (selector) {
                    var $target = self.$content.find(selector + "[data-oe-xpath]");
                    _.each($target, function (node) {
//                        if (!$(node).data('node')) { return; }
                        self._createHookOnNodeAndChildren($(node), component);
                    });
                });
            }
            if (component.dropColumns) {
                var $hook = NodeTools._createHook($('<div/>'), component);
                var $gridHooks = $('<div class="row web_no_code_studio_structure_hook"/>');
                _.each(component.dropColumns, function (column, index) {
                    var $col = $('<div class="offset-' + column[0] + ' col-' + column[1] + '"/>');
                    $col.append($hook.clone().attr('data-oe-index', index));
                    $gridHooks.append($col);
                });

                var $page = this.$content.find('.page');
                var $children = $page.children().not('.web_no_code_studio_hook');

                if ($children.length) {
                    $gridHooks.find('.web_no_code_studio_hook').data('oe-node', $children.first()).data('oe-position', 'before');
                    $children.first().before($gridHooks);

                    _.each($children, function (child) {
                        var $child = $(child);
                        var $newHook = $gridHooks.clone();
                        $newHook.find('.web_no_code_studio_hook').data('oe-node', $child).data('oe-position', 'after');
                        $newHook.data('oe-node', $child).data('oe-position', 'after');
                        $child.after($newHook);
                    });
                } else {
                    $gridHooks.find('.web_no_code_studio_hook').data('oe-node', $page).data('oe-position', 'inside');
                    $page.prepend($gridHooks);
                }

                this.$content.find('.web_no_code_studio_structure_hook + .web_no_code_studio_hook').remove();
                this.$content.find('.web_no_code_studio_structure_hook').prev('.web_no_code_studio_hook').remove();
            }
            this.$content.find('.web_no_code_studio_hook + .web_no_code_studio_hook').remove();
            this.$dropZone = this.$content.find('.web_no_code_studio_hook');
            this.$dropZoneStructure = this.$content.find('.web_no_code_studio_structure_hook');
            this.$dropZoneStructure.removeClass('.web_no_code_studio_nearest').each(function () {
                $(this).children().children('.web_no_code_studio_hook:only-child').data('height', $(this).height() + 'px');
            });

            this._computeNearestHookAndShowIt();
            this.$dropZone.filter('th, td').each(function (_, item) {
                var $item = $(item);
                var $node = $item.data('oe-node');
                var colspan = +$node.data('colspan');
                if (colspan > 1) {
                    $node.attr('colspan', colspan * 2 - 1);
                }
            });
        },

         _createHookOnNodeAndChildren: function ($node, component) {
            var $hook = NodeTools._createHook($node, component);
            var $newHook = $hook.clone();
            var $children = $node.children()
                .not('.web_no_code_studio_hook')

            if ($children.length === 1 && $children.is('td[colspan="99"]')) {
                return;
            }
            if ($children.length) {
                if (component.hookAutoHeight) {

                    var height = Math.max.apply(Math, $children.map(function () { return $(this).height(); }));
                    $newHook.data('height', height + 'px');
                    $newHook.css('height', height + 'px');
                }
                $newHook.data('oe-node', $children.first()).data('oe-position', 'before');
                $children.first().before($newHook);

                $children.each(
                    function (_, childNode) {
                        var $childNode = $(childNode);
                        var $newHook = $hook.clone().data('oe-node', $childNode).data('oe-position', 'after');
                        if (component.hookAutoHeight) {
                            $newHook.data('height', height + 'px');
                            $newHook.css('height', height + 'px');
                        }
                        $childNode.after($newHook);
                    });
            } else if ($node.text() &&
                        $node[0].tagName.toLowerCase() !== 'th' &&
                        $node[0].tagName.toLowerCase() !== 'td') {
                if (component.hookAutoHeight) {
                    $newHook.data('height', $node.height() + 'px');
                    $newHook.css('height', $node.height() + 'px');
                }
                $node.before($newHook.clone().data('oe-node', $node).data('oe-position', 'before'));
                $node.after($newHook.clone().data('oe-node', $node).data('oe-position', 'after'));
            }
            else {
                $newHook.data('oe-node', $node).data('oe-position', 'inside');
                $node.append($newHook);
            }
        },

        _computeNearestHookAndShowIt: function () {
            var self = this;
            this.dropPosition = [];
            var dropZone = this.$dropZone.get();
            dropZone.reverse();
            _.each(dropZone, function (node) {
                var $node = $(node);
                var box = node.getBoundingClientRect();
                box.el = node;
                box.centerY = (box.top + box.bottom) / 2;
                box.centerX = (box.left + box.right) / 2;
                self.dropPosition.push(box);
            });
        },

        _afterRender: async function(){
            const xmlids = ['web_editor.assets_wysiwyg'];
            for (const xmlid of xmlids) {
                const assets = await getBundle(xmlid);
                await loadBundle(assets);
            }
//            await ajax.loadLibs({ assetLibs: ['web_editor.compiled_assets_wysiwyg'] });
            var params = {
                models: this.models,
                paper_formats: this.paper_formats,
            }
            if(this.reportEditorSideBar){
                this.reportEditorSideBar.destroy();
            }
            this.reportEditorSideBar = new ReportEditorSideBar(this, 'add', null, params);
            this.reportEditorSideBar.appendTo(this.$('.view_report_editor_sidebar'));
        },

        _onReportClick: function(){
            var params = {
                models: this.models,
                paper_formats: this.paper_formats,
            }
            if(this.reportEditorSideBar){
                this.reportEditorSideBar.destroy();
            }
            this.reportEditorSideBar = new ReportEditorSideBar(this, 'report', null, params);
            this.reportEditorSideBar.appendTo(this.$('.view_report_editor_sidebar'));
        },

        _updateSideBar: function(data){
            if(this.reportEditorSideBar){
                this.reportEditorSideBar.destroy();
            }
            var params = {
                models: this.models,
                paper_formats: this.paper_formats,
            }
            this.reportEditorSideBar = new ReportEditorSideBar(this, data.mode, data.nodes, params);
            this.reportEditorSideBar.appendTo(this.$('.view_report_editor_sidebar'));
        },

    });

    return ReportEditorView;

});