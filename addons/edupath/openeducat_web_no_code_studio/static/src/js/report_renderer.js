odoo.define('openeducat_web_no_code_studio.ViewsReport', function(require){
    'use strict';

    var Widget = require('web.Widget');
    var concurrency = require('web.concurrency');
    var core = require('web.core');
    var QWeb = core.qweb;
    var utils = require('web.utils');
    var ajax = require('web.ajax');
    var session = require('web.session');
    var Dialog = require('web.Dialog');
    var _t = core._t;
    var ReportEditorView = require('openeducat_web_no_code_studio.ReportEditorView');
    const { ComponentWrapper, WidgetAdapterMixin } = require('web.OwlCompatibility');
    var ReportEditorSideBar = require('openeducat_web_no_code_studio.ReportEditorSideBar');
    var ReportRenderer = Widget.extend(WidgetAdapterMixin, {
        template: 'ReportWrapper',
        className: 'web_no_code_studio_report_manager',
        custom_events: {
            drag_started: '_onDragStart',
            update_report: '_onUpdateReport',
            drop_component: '_onElementDrop',
            element_drag_report: '_onElementDrag',
            element_clicked: '_onElementClick',
            sidebar_node_selected: '_onSidebarNodeSelect',
            sidebar_node_hover: '_onSidebarNodeHover',
            remove_element_report: '_onRemoveReportElement',
            view_edit_report: '_onViewEditorReportEdit',
            end_drag_hooks: '_onEndDragHooks',
        },
        events: {
            'click .on_report_click': '_onReportClick',
            'click .remove_report_button': '_onRemoveReportClick',
        },
        init: function(parent, fieldsView,fieldArch){
            this._super.apply(this, arguments);
            this.fieldsView = fieldsView;
            this.action = parent.action;
            this.report = parent.report.selection;
            this.reportEditor = null;
            this._get
            this.steps = [];
            this.mdp = new concurrency.MutexedDropPrevious();
            this.steps_pending = [];
            this.previous_node = null;
        },

        start: function(){
            return Promise.all([
                this._super.apply(this, arguments),
            ]);
        },

        willStart: function(){
            return Promise.all([this._fetchModels(), this._fetchPaperFormats()]);
        },

        _onDragStart: function(e){
            this.reportEditor._onDragStart(e.data.widget);
        },

        _onElementDrop: function(e){
            this.reportEditor.elementDrop(e.data.widget);
        },

        _fetchModels: function(){
            var self = this;
            return this._rpc({
                model: 'ir.model',
                method: 'search_read',
                fields: ['name', 'model'],
                domain: [['transient', '=', false]],
                context: session.user_context,
            }).then(function (models) {
                self.models = {};
                _.each(models, function (model) {
                    self.models[model.model] = model.name;
                });
            });
        },

        _fetchPaperFormats: function(){
            var self = this;
            return this._rpc({
                model: 'report.paperformat',
                method: 'search_read',
                context: session.user_context,
            }).then(function (papers) {
                self.paper_formats = papers;
            });
        },

        _onSidebarNodeHover: function(e){
            this.reportEditor.hoverNodeElement(e.data.node);
        },

        _onEndDragHooks: function (e) {
            this.reportEditor.endDragHooks(e.data.widget);
        },

        _onElementDrag: function(e){
            this.reportEditor.elementDrag(e.data.widget, e.data.position.pageX, e.data.position.pageY);
        },

        _onRemoveReportClick: function(e){
            var self = this;
            e.preventDefault();
            e.stopPropagation();

            var $target = $(e.currentTarget);
            return this._rpc({
                model: 'ir.actions.report',
                method: 'unlink',
                args: [[parseInt($target.data('id'))]],
            }).then(function(){
                self.trigger_up('reset_reports_view');
            });
        },

        _onElementClick: function(e){
            var node = e.data.node;
            var state = {};
            if (node) {
                var currentNode = node;
                var sidebarNodes = [];
                while (currentNode) {
                    sidebarNodes.push({
                        node: currentNode,
                        context: this.reportEditor.getNodeContext(currentNode),
                    });
                    currentNode = currentNode.parent;
                }
                state = {
                    mode: 'options',
                    nodes: sidebarNodes,
                };
            } else {
                state = {
                    mode: 'add',
                };
            }
            this.reportEditor._updateSideBar(state);
        },

        _onReportClick: function(e){
            var self = this;
            var $target = $(e.currentTarget);
            var id = $target.data('id');
            if(['external', 'internal', 'blank'].includes(id)){
                this._createNewReport(this.action.res_model, id);
            } else {
                this.report_id = id;
                this._reportPreOpen();
            }
        },

        _createNewReport: function(model_name, layout){
            var self = this;

            return ajax.jsonRpc('/create/new/report', 'call', {
                model_name,
                layout
            }).then( function(res){
                self.report_name = res.report_name;
                self.report_file = res.report_file;
                self.report_data = res.report_data;
                self.report_id = res.report_id;
                self.view_id = res.report_data.main_view_id;
                self.r_name = res.name;
                var params = {};
                params.report_name = res.report_name;
                params.report_file = res.report_file;
                params.report_data = res.report_data;
                params.r_name = res.name;
                self._openReportEditor(params);
            });
        },

        _onRemoveReportElement: function(e){
            var self = this;
            var node = e.data.node;
            while (
                node.parent &&
                node.parent.children.length === 1 &&
                node.attrs['data-oe-id'] === node.parent.attrs['data-oe-id'] &&
                (!node.attrs.class || node.attrs.class.indexOf('page') !== -1)
            ) {
                node = node.parent;
            }
            var message = _.str.sprintf('Are you sure you want to remove this %s from the view?', e.data.node.tag);

            Dialog.confirm(this, message, {
                confirm_callback: function () {
                    self.trigger_up('update_report', {
                        node: node,
                        operation: {
                            type: 'remove',
                            structure: 'remove',
                        },
                    });
                },
            });

        },

        _onSidebarNodeSelect: function(e){
            this.reportEditor.sideBarSelectNode(e.data.node);
        },

        _reportPreOpen: function(){
            var self = this;
            return ajax.jsonRpc('/report/get/record', 'call', {
                report_id : this.report_id,
            }).then( function(response){
                self.report_name = response.report_name;
                self.report_file = response.report_file;
                self.report_data = response.report_data;
                self.view_id = response.report_data.main_view_id;
                self.r_name = response.name;
                //self.r_url = r_url;
                var params = {};
                params.report_name = response.report_name;
                params.report_file = response.report_file;
                params.report_data = response.report_data;
                params.r_name = response.name;
                //params.r_url = r_url;
                self._openReportEditor(params);
            });
        },

        _computeView: function (views) {
            var nodesArchs = _.mapObject(views, function (view, id) {
                var doc = $.parseXML(view.arch).documentElement;
                // first element child because we don't want <template> node
                if (!doc.hasAttribute('t-name')) {
                    doc = doc.firstElementChild;
                }
                var node = utils.xml_to_json(doc, true);
                node.id = +id;
                node.key = view.key;
                return node;
            });

            this._setParentKey(nodesArchs);

            return nodesArchs;
        },

        _onUpdateReport: function(ev){
            var self = this;
            var def;
            var node = ev.data.node || ev.data.targets[0].node;
            if(node.attr){
                var operation = _.extend(ev.data.operation, {
                    view_id: +node.attr('data-oe-id'),
                    xpath: node.attr('data-oe-xpath'),
                    context: node.context,
                });
                this.previous_node = node;
            }else if(node.attrs){
                var operation = _.extend(ev.data.operation, {
                    view_id: +node.attrs['data-oe-id'],
                    xpath: node.attrs['data-oe-xpath'],
                    context: node.context,
                });
                this.previous_node = node;
            }
             else {
                console.warn("the key 'node' should be present");
                return;
            }
            if (operation.type === 'add') {
                if(node){
                    def = ev.data.component.add({
                        targets: ev.data.targets,
                    }).then(function (result) {
                        _.extend(operation, result);
                    });
                } else {
                    console.warn("the key 'node' should be present");
                }
            } else {
                if (node) {
                    this.reportEditor.currentNode = node;
                } else {
                    console.warn("the key 'node' should be present");
                }
            }
            Promise.resolve(def).then(function () {
                return self._set(operation);
            }).guardedCatch( function(){
                return self._revert(operation, true);
            });
        },

        _onViewEditorReportEdit: function(ev){
            var self = this;
            this._reportChange(ev.data).then(function (result) {
                self.report_file = result.report_file;
                self.report_data = result.report_data;
                self.report_id = result.report_id;
                self.view_id = result.report_data.main_view_id;
                self.r_name = result.name;
                var params = {};
                params.report_name = result.report_name;
                params.report_file = result.report_file;
                params.report_data = result.report_data;
                params.r_name = result.name;
                self._startReportEditor(params);
            });
        },

        _reportChange: function(values){
            return ajax.jsonRpc('/openeducat_web_no_code_studio/edit/report/val', 'call', {
                report_id: this.report_id,
                values: values,
                model: this.action.res_model,
                context: session.user_context,
            });
        },

        _revert: function(que, forget){
            if (!this.steps.length) {
                return Promise.resolve();
            }
            var op;
            if (que) {
                op = _.findWhere(this.steps, {id: que.id});
                this.steps = _.without(this.steps, op);
            } else {
                op = this.steps.pop();
            }

            if (!forget) {
                this.steps_pending.push(op);
            }

            if (op.type === 'replace_arch') {
                var undo_op = jQuery.extend(true, {}, op);
                undo_op.old_arch = op.new_arch;
                undo_op.new_arch = op.old_arch;
                this.steps.push(undo_op);
                return this._doChanges(true, true);
            } else {
                return this._doChanges(false, false);
            }
        },

        _set: function (que) {
            if(this.isRelationField && que.target){
                this._setRelationalSubPath(que);
            }
            que.id = _.uniqueId('que_');
            this.steps.push(que);
            this.steps_pending = [];
            return this._doChanges(false, que.type === 'replace_arch');
        },

        _doChanges: function(delete_que, xml){
            var self = this,
                def,
                previousStep = this.steps.slice(-1)[0],
                previousStepId = previousStep && previousStep.id;

            if(xml){
                def = this.mdp.exec(this._updateActualArch.bind(
                    this,
                    previousStep.view_id,
                    previousStep.new_arch
                )).guardedCatch(function () {
                });
            }else{
                def = this.mdp.exec(function () {
                    var localSteps = [];
                    _.each(self.steps, function (op) {
                        if (op.type !== 'replace_arch') {
                            localSteps.push(_.omit(op, 'id'));
                        }
                    });
                    var prom = self._updateActualView(
                        self.view_id,
                        self.web_no_code_studio_arch,
                        localSteps
                    );
                    prom.then( function(){
                        self.steps = [];
                    }).guardedCatch(function () {
                        //return self._undo(previousStepId, true).then(function () {
                        //    return Promise.reject();
                        //});
                    });
                    return prom;
                });
            }

            return def.then( function(response){
                if(xml){
                    self._cleanSteps(previousStepId);
                }
                return self._renderReport();
                //return self._handleDoChanges(response, xml, previousStepId);
            });
        },

        _renderReport: function(){
            this._reportPreOpen();
        },

        _updateActualView: function(id, arch, steps){
            var self = this;
            core.bus.trigger('clear_cache');
            return ajax.jsonRpc('/openeducat_web_no_code_studio/edit/report', 'call', {
                    report_name: this.report_name,
                    report_views: this.report_data.views,
                    model_name: this.report_data.read_data.model,
                    steps: steps,
                    context: session.user_context,
            }).guardedCatch(error => {
                error.event.preventDefault();
                Dialog.alert(self, _t(error.message.data.message));
            });
        },

        _setParentKey: function (nodesArchs) {
            function setParent(node, parent) {
                if (_.isObject(node)) {
                    node.parent = parent;
                    _.each(node.children, function (child) {
                        setParent(child, node);
                    });
                }
            }
            _.each(nodesArchs, function (node) {
                setParent(node, null);
            });
        },

        _openReportEditor: function(params){
            var def = [];
            var self = this;

            return this._startReportEditor(params);
        },

        _generateReportSideBar: function(){
            return new ReportEditorSideBar(this, 'add');
        },

        _startReportEditor: function(params){
            this.nodesArchs = this._computeView(this.report_data.views);
            params.nodesArchs = this.nodesArchs;
            params.models = this.models;
            params.paper_formats = this.paper_formats;
            if(this.reportEditor){
                this.reportEditor.destroy();
            }
            this.reportEditor = new ReportEditorView(this,this.fieldsView, params);
            this.reportEditor.appendTo(self.$('.o_content'));
        },
    });

//    widgetRegistry.add('report_renderer', ReportRenderer);

    return ReportRenderer;
});
