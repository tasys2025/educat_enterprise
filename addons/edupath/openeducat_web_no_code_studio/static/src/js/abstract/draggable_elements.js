odoo.define('openeducat_web_no_code_studio.DraggableElement', function(require){
    'use strict';

    var Widget = require('web.Widget');
    var config = require('web.config');

    return Widget.extend({
        structure: null,
        label: null,
        description: null,
        start: function () {
            var self = this;
            this.$el.addClass('web_no_code_studio_draggable');
            this.$el.data('structure', this.structure);
            this.$el.text(this.label);
            var img = this.type ? this.type : this.description;
            this.$el.prepend($('<span class="pl-1 pr-1 mr-2 mr4 view_edit_img"><img class="m-1" src="openeducat_web_no_code_studio/static/src/img/icons/'+ img +'.png" width="16" height="16" alt=""/></span>'));
            if (config.isDebug() && this.description) {
                this.$el.addClass('web_no_code_studio_debug_mode');
                this.$el.append($('<div>')
                    .addClass('web_no_code_studio_draggable_description')
                    .text(this.description)
                );
            }
            this.$el.draggable({
                helper: 'clone',
                opacity: 0.4,
                scroll: false,
                revert: 'invalid',
                revertDuration: 200,
                refreshPositions: true,
                start: function (e, ui) {
                    ui.helper.data(self.$el.data());
                    ui.helper.addClass("draggable-help");
                }
            });
            return this._super.apply(this, arguments);
        },

    });
});