odoo.define('openeducat_web_no_code_studio.NodeTools', function(require){
    'use strict';

    var ReportElementsEdit = require('openeducat_web_no_code_studio.ReportElementsEdit').registry;
    var utils = require('web.utils');

    var _getAssociatedDOMNode = (node) => {
        var parser = new DOMParser();
        var xml = utils.json_node_to_xml(node);
        var xmlDoc = parser.parseFromString(xml, "text/xml");
        var xmlNode = xmlDoc.getElementsByTagName(node.tag)[0];
        return $(xmlNode);
    };

    var _getNodeEditableComponents = (node) => {
        var components = [];

        var $node = _getAssociatedDOMNode(node);
        _.each(ReportElementsEdit.map, function (Element) {
            var selector = Element.prototype.selector;
            if ($node.is(selector)) {
                components.push(Element.prototype.name);
            }
        });

        _.each(['layout', 'tif', 'groups'], function (componentName) {
            if (!_.contains(components, componentName)) {
                components.push(componentName);
            }
        });
        return components;
    };

    var connectContextOrder = (dom, contextOrder) => {
        var $node = $(dom);
        var newOrder = contextOrder.slice();
        var node = $node.data('node');

        if (node) {
            if (node.contextOrder) {
                return node.contextOrder;
            }
            newOrder = node.contextOrder = _.uniq(contextOrder.concat(_.keys(node.context)));
        }

        var children = $node.children().get();
        for (var k = 0; k < children.length; k++) {
            newOrder = connectContextOrder(children[k], newOrder);
        }
        return newOrder;
    };

    var _createHook = ($target, component) => {
        var firstChild = $target.children().get(0);
        var hookTag = ((firstChild && firstChild.tagName) || 'div').toLocaleLowerCase();
        if (!$target.is('tr') && component.hookTag) {
            hookTag = component.hookTag;
        }
        if (hookTag === 'table') {
            hookTag = 'div';
        }
        var $hook = $('<' + hookTag + ' class="web_no_code_studio_hook"/>');
        if ($target.hasClass('row')) {
            $hook.addClass('col-3');
        }
        if (component.hookClass) {
            $hook.addClass(component.hookClass);
        }
        return $hook;
    };

    return {
        _getNodeEditableComponents: _getNodeEditableComponents,
        _getAssociatedDOMNode: _getAssociatedDOMNode,
        connectContextOrder: connectContextOrder,
        _createHook: _createHook,
    }

});