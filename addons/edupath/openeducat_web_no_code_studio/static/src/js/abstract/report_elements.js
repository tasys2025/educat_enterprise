odoo.define('openeducat_web_no_code_studio.ReportElements', function(require){
    'use strict';

    var Widget = require('web.Widget');
    var Registry = require('web.Registry');
    var config = require('web.config');
    var Dialog = require('web.Dialog');
    var core = require('web.core');
    var _t = core._t;
//    var WysiwygWidgets = require('wysiwyg.widgets');
    var report_editor_elements_registry = new Registry();
    var CreateFieldDialog = require('openeducat_web_no_code_studio.CreateFieldDialog');
//    var ReportEditorView = require('openeducat_web_no_code_studio.ReportEditorView');
    var TextSelectorTags = 'span, p, h1, h2, h3, h4, h5, h6, blockquote, pre, small, u, i, b, font, strong, ul, li, dl, dt, ol, .page > .row > div:empty';
    var filter = ':not([t-field]):not(:has(t, [t-' + QWeb2.ACTIONS_PRECEDENCE.join('], [t-') + ']))';

    var DraggableElement = Widget.extend({
        init: function(parent, params){
            this._super.apply(this, arguments);
            this.models = params.models;
        },

        start: function () {
            var self = this;
            this.$el.addClass('web_no_code_studio_draggable');
            this.$el.data('structure', this.structure);
            this.$el.text(this.label);
            var img = this.type ? this.type : this.description;
            this.$el.prepend($('<span class="pl-1 pr-1 me-2 mr4 view_edit_img"><img src="openeducat_web_no_code_studio/static/src/img/icons/'+ img +'.png" width="16" height="16" alt=""/></span>'));
            if (config.isDebug() && this.description) {
                this.$el.addClass('web_no_code_studio_debug_mode');
                this.$el.append($('<div>')
                    .addClass('web_no_code_studio_draggable_description')
                    .text(this.description)
                );
            }
            var dragFunction = _.cancellableThrottleRemoveMeSoon(function (e) {
                self.trigger_up('element_drag_report', {
                    position: { pageX: e.pageX, pageY: e.pageY },
                    widget: self,
                });
            }, 100);
            this.$el.draggable({
                helper: 'clone',
                opacity: 0.4,
                scroll: false,
                revertDuration: 200,
                refreshPositions: true,
                iframeFix: true,
                start: function (e, ui) {
                    $(ui.helper).addClass("ui-draggable-helper");
                    self.trigger_up('drag_started', {
                        widget: self,
                    });
                },
                drag: dragFunction,
                stop: function(e){
                    self.trigger_up('drop_component', {
                        position: { pageX: e.pageX, pageY: e.pageY },
                        widget: self,
                    });
                },
            });
            return this._super.apply(this, arguments);
        },

        _findParentWithTForeach: function (node) {
            if (!node || !node.parent || (node.tag === "div" && node.attrs.class === "page")) {
                return;
            }
            if (node.attr("t-foreach")) {
                return node;
            }
            return this._findParentWithTForeach(node.parent);
        },

        _newData: function (options) {
            if (this.dropColumns && typeof this.index === 'number') {
                return this._addStructure({
                    index: this.index,
                    content: options.contentInStructure || options.content,
                    fillStructure: options.fillStructure || false,
                });
            } else {
                return _.map(this.targets, function (target) {
                    var isCol = (target.node.className || '').match(/(^|\s)(col(-[0-9]+)?)(\s|$)/);
                    return {
                        content: isCol ? options.contentInStructure || options.content : options.content,
                        position: target.position,
                        xpath: target.node.attr('data-oe-xpath'),
                        view_id: +target.node.attr('data-oe-id'),
                    };
                });
            }
        },

        _addStructure: function (options) {
            var xml = ['<div class="row'];
            if (this.structureClass) {
                xml.push(' ' + this.structureClass);
            }
            xml.push('">');
            for (var k = 0; k < this.dropColumns.length; k++) {
                var column = this.dropColumns[k];
                xml.push('<div class="col-');
                xml.push(column[1]);
                if (column[0]) {
                    xml.push(' offset-');
                    xml.push(column[0]);
                }
                xml.push('">');
                if (options.content && (k === options.index || options.fillStructure)) {
                    xml.push(options.content);
                }
                xml.push('</div>');
            }
            xml.push('</div>');
            return [{
                content: xml.join(''),
                position: this.position,
                xpath: this.node.attr('data-oe-xpath'),
                view_id: +this.node.attr('data-oe-id'),
            }];
        },

        _getContextKeys: function (node) {
            node = node.data('node')
            var self = this;
            var contextOrder = node.contextOrder || [];

            var keys = _.compact(_.map(node.context, function (relation, key) {
                if (!self.models[relation]) {
                    return {
                        name: key,
                        string: key + ' (' + relation + ')',
                        type: relation,
                        store: true,
                        related: true,
                        searchable: true,
                        order: -contextOrder.indexOf(key),
                    };
                }
                return {
                    name: key,
                    string: key + ' (' + self.models[relation] + ')',
                    relation: relation,
                    type: key[key.length-1] === 's' ? 'one2many' : 'many2one',
                    store: true,
                    related: true,
                    searchable: true,
                    order: -contextOrder.indexOf(key),
                };
            }));
            keys.sort(function (a, b) {
                return a.order - b.order;
            });
            return keys;
        },

        _createReportTableColumn: function (options) {
            var self = this;
            var inheritance = [];
            var updatedNodes = [];

            _.each(this.targets, function (target) {
                var node = target.node;
                var inheritanceItem;
                if (node.tag === 'th' || node.tag === 'td') {
                    var loop = self._findParentWithTForeach(node) ? true : false;
                    var dataName = loop ? 'Loop' : '';
                    var content = '<' + node.tag + '>';
                    if (node.tag === 'th' || node.parent.parent.tag === 'thead') {
                        content += options['head' + dataName] || options.head || '';
                    } else if (node.parent.parent.tag === 'tfoot') {
                        content += options['foot' + dataName] || options.foot || '';
                    } else {
                        content += options['body' + dataName] || options.body || '';
                    }
                    content += '</' + node.tag + '>';

                    updatedNodes.push(node);
                    inheritanceItem = {
                        content: content,
                        position: target.position,
                        xpath: node.attrs['data-oe-xpath'],
                        view_id: +node.attrs['data-oe-id'],
                    };
                } else if (node.tag === 'tr') {
                    updatedNodes.push(node);
                    inheritanceItem = {
                        content: '<td>' + (options.tbody || '') + '</td>',
                        position: target.position,
                        xpath: node.attrs['data-oe-xpath'],
                        view_id: +node.attrs['data-oe-id'],
                    };
                }
                inheritance.push(inheritanceItem);
            });

            var cellsToGrow = [];
            _.each(this.targets, function (target) {
                var node = target.node;
                if (target.position !== 'after') {
                    return;
                }
                var nodeIndex = 0;
                var nodeRow = self._getParentNode(node, function (node) { return node.tag === 'tr'; });
                var cells = self._getChildrenNode(nodeRow, function (node) { return node.tag === 'td' || node.tag === 'th'; });
                for (var k = 0; k < cells.length; k++) {
                    nodeIndex += +(cells[k].attrs.colspan || 1);
                    if (cells[k] === node) {
                        break;
                    }
                }

                var table = self._getParentNode(node, function (node) { return node.tag === 'table'; });
                var rows = self._getChildrenNode(table, function (node) { return node.tag === 'tr'; });
                _.each(rows, function (row) {
                    if (row === nodeRow) {
                        return;
                    }

                    var cells = self._getChildrenNode(row, function (node) { return node.tag === 'td' || node.tag === 'th'; });

                    var cellIndex = 0;
                    for (var k = 0; k < cells.length; k++) {
                        var cell = cells[k];
                        cellIndex += +(cell.attrs.colspan || 1);
                        if (cellIndex >= nodeIndex) {
                            if (((+cell.attrs.colspan) > 1) && cellsToGrow.indexOf(cell) === -1) {
                                cellsToGrow.push(cell);
                            }
                            break;
                        }
                    }
                });
            });
            _.each(cellsToGrow, function (node) {
                inheritance.push({
                    content: '<attribute name="colspan">' + ((+node.attrs.colspan) + 1) + '</attribute>',
                    position: 'attributes',
                    xpath: node.attrs['data-oe-xpath'],
                    view_id: +node.attrs['data-oe-id'],
                });
            });

            return inheritance;
        },

        _getParentNode: function (node, fn) {
            while (node) {
                if (fn(node)) {
                    return node;
                }
                node = node.parent;
            }
        },

        _getChildrenNode: function (parent, fn) {
            var children = [];
            var stack = [parent];
            parent = stack.shift();
            while (parent) {
                if (parent.children) {
                    for (var k = 0; k < parent.children.length; k++) {
                        var node = parent.children[k];
                        if (fn(node)) {
                            children.push(node);
                        }
                    }
                    stack = parent.children.concat(stack);
                }
                parent = stack.shift();
            }
            return children;
        },

        add: function (options) {
            this.targets = options.targets;
            var first = options.targets[0];
            this.index = first.data.oeIndex;
            this.position = first.data.oePosition;
            this.node = first.node;
            return Promise.resolve({
                type: this.type,
                options: {
                    columns: this.dropColumns,
                    index: first.data.oeIndex,
                },
            });
        },
    });

    var FieldDraggableElement = DraggableElement.extend({
        type: 'field',
        add: function () {
            var self = this;
            return self._super.apply(this, arguments).then(function() {
                return new Promise(function (resolve, reject) {
                    var field = {
                        order: 'order',
                        type: 'related',
                        filters: { searchable: false },
                        filter: function (field) {
                            return ! _.contains(['one2many', 'many2many'], field.type);
                        }
                    };

                    var target = self.targets[0];
                    if (self._filterTargets) {
                        target = self._filterTargets() || target;
                    }
                    var availableKeys = _.filter(self._getContextKeys(target.node), function (field) {
                        return !!field.relation && field.name !== 'docs';
                    });

                    var fieldChain = [];
                    if (availableKeys.length) {
                        fieldChain.push(_.first(availableKeys).name);
                    }
                    var dialog = new CreateFieldDialog(self, 'record_fake_model', field, availableKeys, fieldChain).open();
                    dialog.on('field_default_values_saved', self, function (values) {
                        if (values.related.split('.').length < 2) {
                            Dialog.alert(self, _t('The record field name is missing'));
                        } else {
                            resolve({
                                inheritance: self._dataInheritance(values),
                            });
                            dialog.close();
                        }
                    });
                    dialog.on('closed', self, function () {
                        self.trigger_up('end_drag_hooks', {
                            widget: self,
                        });
                        reject();
                    });
                });
            });
        },
    });

    var blockFields = [],
        reportElements = [],
        inlineElements = [],
        tableElements = [];

    reportElements.push(
        DraggableElement.extend({
            structure: 'group',
            label: 'Two Columns',
            type: 'text',
            dropIn: '.page',
            dropColumns: [[0, 6], [0, 6]],
            className: 'web_no_code_studio_block_char',
            addEmptyRowsTargets: false,
            add: function () {
                var self = this;
                return this._super.apply(this, arguments).then(function () {
                    return Promise.resolve({
                        inheritance: self._newData({
                            fillStructure: true,
                            contentInStructure: '<span>New Column</span>',
                        })
                    });
                });
            },
        }),
        DraggableElement.extend({
            structure: 'group',
            label: 'Three Columns',
            type: 'text',
            dropIn: '.page',
            dropColumns: [[0, 4], [0, 4], [0, 4]],
            className: 'web_no_code_studio_block_char',
            addEmptyRowsTargets: false,
            add: function () {
                var self = this;
                return this._super.apply(this, arguments).then(function () {
                    return Promise.resolve({
                        inheritance: self._newData({
                            fillStructure: true,
                            contentInStructure: '<span>New Column</span>',
                        })
                    });
                });
            },
        }),
    );

    blockFields.push(
        DraggableElement.extend({
            type: 'char',
            label: 'Title',
            className: 'web_no_code_studio_field_char',
            dropIn: '.page',
            hookTag: 'div',
            add: function () {
                var self = this;
                return this._super.apply(this, arguments).then(function () {
                    return Promise.resolve({
                        inheritance: [{
                            content: '<div class="row"><div class="col h2"><span>New Title</span></div></div>',
                            position: self.position,
                            xpath: self.node.attr('data-oe-xpath'),
                            view_id: +self.node.attr('data-oe-id'),
                        }],
                    });
                });
            },
        }),
        DraggableElement.extend({
            type: 'text',
            label: 'Text',
            className: 'web_no_code_studio_field_char',
            dropIn: '.page',
            hookTag: 'div',
            add: function () {
                var self = this;
                return this._super.apply(this, arguments).then(function () {
                    return Promise.resolve({
                        inheritance: self._newData({
                            content: '<span>New Text Block</span>',
                        })
                    });
                });
            },
            className: 'web_no_code_studio_field_char',
        }),

        DraggableElement.extend({
            type: 'image',
            label: 'Image',
            className: 'web_no_code_studio_field_picture',
            hookClass: 'web_no_code_studio_hook_picture',
            dropIn: '.page',
            add: function () {
                var self = this;
                return this._super.apply(this, arguments).then(function () {
                    var def = new Promise(function (resolve, reject) {
                        var $image = $("<img/>");
                        var dialog = new WysiwygWidgets.MediaDialog(self, {
                            onlyImages: true,
                        }, $image[0]).open();
                        var value;
                        dialog.on("save", self, function (el) {
                            value = el.attributes.src.value;
                        });
                        dialog.on('closed', self, function () {
                            if (value) {
                                resolve({
                                    inheritance: self._newData({
                                        content: '<img class="img-fluid" src="' + value + '"/>',
                                    })
                                });
                            } else {
                                self.trigger_up('end_drag_hooks', {
                                    widget: self,
                                });
                                reject();
                            }
                        });
                    });
                    return def;
                });
            },
        }),

        DraggableElement.extend({
            type: 'address',
            label: 'Address',
            className: 'web_no_code_studio_field_address',
            hookClass: 'web_no_code_studio_hook_address',
            dropColumns: [[0, 5], [2, 5]],
            add: function () {
                var self = this;
                var callersArguments = arguments;
                return new Promise(function (resolve, reject) {
                    self._super.apply(self, callersArguments).then(function () {
                        var field = {
                            order: 'order',
                            type: 'related',
                            filters: {},
                            filter: function (field) {
                                return field.type === 'many2one';
                            },
                            followRelations: function (field) {
                                return field.type === 'many2one' && field.relation !== 'res.partner';
                            },
                        };
                        var availableKeys = self._getContextKeys(self.node);
                        var fieldChain = [];
                        if (availableKeys.length) {
                            fieldChain.push(_.first(availableKeys).name);
                        }
                        var dialog = new CreateFieldDialog(self, 'record_fake_model', field, availableKeys, fieldChain).open();
                        dialog.on('field_default_values_saved', self, function (values) {
                            if (!_.contains(values.related, '.')) {
                                Dialog.alert(self, _t('Please specify a field name for the selected model.'));
                                return;
                            }
                            if (values.relation === 'res.partner') {
                                resolve({
                                    inheritance: self._newData({
                                        content: '<div t-field="' + values.related + '" t-options-widget="\'contact\'"/>',
                                    })
                                });
                                dialog.close();
                            } else {
                                Dialog.alert(self, _t('You can only display a user or a partner'));
                            }
                        });
                        dialog.on('closed', self, function () {
                            self.trigger_up('end_drag_hooks', {
                                widget: self,
                            });
                            reject();
                        });
                    });
                });
            },
        }),

        FieldDraggableElement.extend({
            type: 'field',
            label: 'Field',
            className: 'web_no_code_studio_field_field',
            hookClass: 'web_no_code_studio_hook_field',
            dropIn: '.page',
            _dataInheritance: function (values) {
                var $field = $('<span/>').attr('t-field', values.related);
                if (values.type === 'binary') {
                    $field.attr('t-options-widget', '"image"');
                }
                var fieldHTML = $field.prop('outerHTML');

                return this._newData({
                    content: "<div class='row'><div class='col'>" + fieldHTML + "</div></div>",
                });
            },
        }),

    );

    inlineElements.push(
        DraggableElement.extend({
            type: 'text',
            label: 'Text',
            className: 'web_no_code_studio_field_char',
            hookClass: 'web_no_code_studio_hook_inline',
            hookAutoHeight: true,
            dropIn: TextSelectorTags.split(',').join(filter + '|') + filter,
            selectorSeparator: '|',
            hookTag: 'span',
            add: function () {
                var self = this;
                return this._super.apply(this, arguments).then(function () {
                    return Promise.resolve({
                        inheritance: self._newData({
                            content: '<span>New Text Block</span>',
                        })
                    });
                });
            },
        }),

        FieldDraggableElement.extend({
            type: 'field',
            label: 'Field',
            className: 'web_no_code_studio_field_many2one',
            hookClass: 'web_no_code_studio_hook_inline',
            hookAutoHeight: true,
            dropIn: TextSelectorTags.split(',').join(filter + '|') + filter,
            selectorSeparator: '|',
            hookTag: 'span',
            _dataInheritance: function (values) {
                var $field = $('<span/>').attr('t-field', values.related);
                if (values.type === 'binary') {
                    $field.attr('t-options-widget', '"image"');
                }
                var fieldHTML = $field.prop('outerHTML');
                if (this.node.tag === 'td' || this.node.tag === 'th') {
                    return this._createReportTableColumn({
                        head: $('<span/>').text(values.string).prop('outerHTML'),
                        bodyLoop: fieldHTML,
                    });
                } else {
                    return this._newData({
                        content: fieldHTML,
                    });
                }
            },
            _filterTargets: function () {
                var self = this;
                var target = this.targets[0];
                if (this.targets.length > 1 && (target.node.tag === 'td' || target.node.tag === 'th')) {
                    target = _.find(this.targets, function (target) {
                        return self._findParentWithTForeach(target.node) ? true : false;
                    });
                }
                return target;
            },
        }),

    );

    tableElements.push(

        DraggableElement.extend({
            type: 'table',
            label: 'Table',
            className: 'web_no_code_studio_field_table',
            hookClass: 'web_no_code_studio_hook_table',
            dropIn: '.page',
            add: function () {
                var self = this;
                var callersArguments = arguments;
                return new Promise(function (resolve, reject) {
                    self._super.apply(self, callersArguments).then(function () {
                        var field = {
                            order: 'order',
                            type: 'related',
                            filters: {},
                            filter: function (field) {
                                return field.type === 'many2one' || field.type === 'one2many' || field.type === 'many2many';
                            },
                            followRelations: function (field) {
                                return field.type === 'many2one';
                            },
                        };
                        var availableKeys = self._getContextKeys(self.node);
                        var fieldChain = [];
                        if (availableKeys.length) {
                            fieldChain.push(_.first(availableKeys).name);
                        }
                        var dialog = new CreateFieldDialog(self, 'record_fake_model', field, availableKeys, fieldChain).open();
                        dialog.on('field_default_values_saved', self, function (values) {
                            if (values.type === 'one2many' || values.type === 'many2many') {
                                resolve({
                                    inheritance: self._dataInheritance(values),
                                });
                                dialog.close();
                            } else {
                                Dialog.alert(self, _t('You need to use a many2many or one2many field to display a list of items'));
                            }
                        });
                        dialog.on('closed', self, function () {
                            self.trigger_up('end_drag_hooks', {
                                widget: self,
                            });
                            reject();
                        });
                    });
                });
            },
            _dataInheritance: function (values) {
                var target = this.targets[0];
                return [{
                    content:
                        '<table class="table o_report_block_table">' +
                        '<thead>' +
                        '<tr>' +
                        '<th><span>Name</span></th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '<tr t-foreach="' + values.related + '" t-as="table_line">' +
                        '<td><span t-field="table_line.display_name"/></td>' +
                        '</tr>' +
                        '</tbody>' +
                        '</table>',
                    position: target.position,
                    xpath: target.node.attr('data-oe-xpath'),
                    view_id: +target.node.attr('data-oe-id'),
                }];
            },
        }),

        FieldDraggableElement.extend({
            label: 'Column',
            className: 'web_no_code_studio_field_fa',
            hookClass: 'web_no_code_studio_hook_table_column',
            dropIn: 'tr',
            _dataInheritance: function (values) {
                var $field = $('<span/>').attr('t-field', values.related);
                if (values.type === 'binary') {
                    $field.attr('t-options-widget', '"image"');
                }
                var fieldHTML = $field.prop('outerHTML');
                if (this.node.tag === 'td' || this.node.tag === 'th') {
                    var targetInLoop = _.find(this.targets, function (target) {
                        return this._findParentWithTForeach(target.node);
                    }.bind(this)) ? true : false;
                    return this._createReportTableColumn({
                        head: $('<span/>').text(values.string).prop('outerHTML'),
                        body: targetInLoop ? undefined : fieldHTML,
                        bodyLoop: targetInLoop ? fieldHTML : undefined,
                    });
                } else {
                    return this._newData({
                        contentInStructure: '<span><strong>' + values.string + ':</strong><br/></span>' + fieldHTML,
                        content: fieldHTML,
                    });
                }
            },
            _filterTargets: function () {
                var self = this;
                var target = this.targets[this.targets.length - 1];
                if (this.targets.length > 1) {
                    target = _.find(this.targets, function (target) {
                        return self._findParentWithTForeach(target.node) ? true : false;
                    });
                }
                return target;
            },
        }),

        DraggableElement.extend({
            type: 'text',
            label: 'Text',
            className: 'web_no_code_studio_field_char',
            hookAutoHeight: false,
            hookClass: 'web_no_code_studio_hook_inline',
            dropIn: 'td, th',
            hookTag: 'span',
            add: function () {
                var self = this;
                return this._super.apply(this, arguments).then(function () {
                    return Promise.resolve({
                        inheritance: self._newData({
                            content: '<span>New Text Block</span>',
                        })
                    });
                });
            },
        }),

        FieldDraggableElement.extend({
            type: 'field',
            label: 'Field',
            className: 'web_no_code_studio_field_field',
            hookAutoHeight: false,
            hookClass: 'web_no_code_studio_hook_inline',
            dropIn: 'td, th',
            _dataInheritance: function (values) {
                var $field = $('<span/>').attr('t-field', values.related);
                if (values.type === 'binary') {
                    $field.attr('t-options-widget', '"image"');
                }
                var fieldHTML = $field.prop('outerHTML');
                if (this.node.tag === 'td' || this.node.tag === 'th') {
                    var targetInLoop = _.find(this.targets, function (target) {
                        return this._findParentWithTForeach(target.node);
                    }.bind(this)) ? true : false;
                    return this._createReportTableColumn({
                        head: $('<span/>').text(values.string).prop('outerHTML'),
                        body: targetInLoop ? undefined : fieldHTML,
                        bodyLoop: targetInLoop ? fieldHTML : undefined,
                    });
                } else {
                    return this._newData({
                        contentInStructure: '<span><strong>' + values.string + ':</strong><br/></span>' + fieldHTML,
                        content: fieldHTML,
                    });
                }
            },
            _filterTargets: function () {
                var self = this;
                var target = this.targets[this.targets.length - 1];
                if (this.targets.length > 1) {
                    target = _.find(this.targets, function (target) {
                        return self._findParentWithTForeach(target.node) ? true : false;
                    });
                }
                return target;
            },
        }),

    );

    report_editor_elements_registry
        .add('blockElements', blockFields)
        .add('reportElements', reportElements)
        .add('inlineElements', inlineElements)
        .add('tableElements',tableElements)

    return report_editor_elements_registry;

});