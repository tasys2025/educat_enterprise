odoo.define('openeducat_web_no_code_studio.NewFieldElement', function(require){
    'use strict';

    var DraggableElement = require('openeducat_web_no_code_studio.DraggableElement');

    return DraggableElement.extend({
        structure: 'field',
        type: null,
        start: function () {
            this.description = this.type;
            this.$el.data('field_description', {
                type: this.type,
                field_description: 'New ' + this.label,
            });
            return this._super.apply(this, arguments);
        },
    });

});