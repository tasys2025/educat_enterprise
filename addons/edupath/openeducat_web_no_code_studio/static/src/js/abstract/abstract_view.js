odoo.define('openeducat_web_no_code_studio.AbstractView', function (require) {
"use strict";

    var ajax = require('web.ajax');
    var AbstractView = require('web.AbstractView');
    var utils = require('web.utils');

    AbstractView.include({

        _generateViewWithEditor: function(parent, renderer, options){
            return this._generateRenderer(parent, renderer, options);
        },

        _generateViewWithRenderer: function(parent, options){
            return this._generateRenderer(parent, this.config.Renderer, options);
        },

        //Todo: Restructure
        _generateRenderer: function (parent, Renderer, options) {
            var self = this;
            var model = this.getModel(parent);
            var loadViewDef = this._loadSubviews ? this._loadSubviews(parent) : Promise.resolve();

            return loadViewDef.then(function () {
                const searchQuery = self.controllerParams.searchModel.get('query');
                if (options.viewType === 'list') {
                    searchQuery.groupBy = [];
                }
                self._updateMVCParams(searchQuery);
                if (options.isRelationField) {
                    self.loadParams.static = true;
                }
                return Promise.all([
                    self._loadData(model, { withSampleData: false }),
//                    ajax.loadLibs(self)
                ]).then(function (results) {
                    var { state } = results[0];
                    if (options.isRelationField) {
                        self.loadParams.static = false;
                    }
                    var params = _.extend({}, self.rendererParams, options, {
                        noContentHelp: undefined,
                    });
                    let viewEditor;
                    if (utils.isComponent(Renderer)) {
                        state = Object.assign({}, state, params);
                        const Component = state.Component;
                        delete state.Component;
                        return new Renderer(parent, Component, state);
                    } else {
                        viewEditor = new Renderer(parent, state, params);
                    }

                    viewEditor.model = model;
                    model.setParent(viewEditor);
                    return viewEditor;
                });
            });
        },
    });
});
