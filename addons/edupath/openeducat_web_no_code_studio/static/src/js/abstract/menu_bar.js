/** @odoo-module **/

import core from 'web.core';
import Widget from 'web.Widget';
//    var Widget = require('web.Widget');
    var ajax = require('web.ajax');
//    var core = require('web.core');
    var bus = require('openeducat_web_no_code_studio.bus');
    var _t = core._t;
//    const { useService } = require("@web/core/utils/hooks");

  var MenuBar = Widget.extend({
        template: 'openeducat_web_no_code_studio.menu_bar',
        events:{
            'click .web_no_code_studio_menu_sections > div': '_onTabClick',
            'click .web_no_code_studio_menu_sections > .web_no_code_studio_menu_icons > a': '_onTabIconView',
        },
        init: function(parent, action){
            this._super.apply(this, arguments);
            this.activeTab = 'Views';
            this.action = action;
            this.my_action = action;
//            this.rpc = useService("rpc");
            this.views_active_editor = this._viewsActiveEditor();
            this.editor_actions = [{action: 'web_no_code_studio_action', title: 'Views'}]
            core.bus.on("editor_mode_started", this, (toggle) => {
                this._viewEditorEnter(toggle)
            });
        },

        renderElement: function() {
            this._super.apply(this, arguments);
            this._activeTabButton();
            this._renderBreadcrumbs();
        },

        _viewsActiveEditor: function(){
            return _.map(this.action.views, function (view) {
                return view.type;
            });
        },

        _renderBreadcrumbs: function(){
            var self = this;
            var $breadcrumb = $('<ol class="breadcrumb" />');
            _.each(this.editor_actions, function (action, index) {
                $breadcrumb.append(
                    self._renderBreadcrumbsSub(action, index, self.editor_actions.length)
                );
            });
            this.$('.web_no_code_studio_breadcrumb').empty().append($breadcrumb);
        },

        _renderBreadcrumbsSub: function(action, index, length){
            var self = this;
            var is_last = (index === length-1);
            var li_content = action.title && _.escape(action.title.trim());
            var $action = $('<li>', {class: 'breadcrumb-item'})
                .append(li_content)
                .toggleClass('active', is_last);
            if((index == 0) && (length-1 == 0)){
                $action.addClass('d-none')
            }else{
                $action.removeClass('d-none')
            }
            if (!is_last) {
                $action.click(function () {
                    var options = {
                        action: self.action,
                        replace_last_action: true,
                        index: index,
                    };
                    if (action.viewType) {
                        options.viewType = action.viewType;
                    }
                    if (action.x2mEditorPath) {
                        options.x2mEditorPath = action.x2mEditorPath.slice();
                    }
                    if (action.viewName) {
                        options.viewName = action.viewName;
                    }
                    self.action.viewType = 'views';
                    self._replaceEditorAction(action.action, action.title, options);
                });
                $action.toggleClass('o_back_button');
            }
            return $action;
        },

        _onTabClick: function(ev){
            var $target = $(ev.currentTarget);
            if (!$target.data('type')) { return; }

            var title = $target.text();
            var type = $target.data('type');
            this.action.viewType = type
            if (type === 'views' || type === 'reports') {
                this._replaceEditorAction('web_no_code_studio_action', title, {
                    action: this.action,
                    editor_clear_breadcrumbs: true,
                });
            } else if (['automation', 'access'].includes(type)) {
                var self = this;
                return ajax.jsonRpc('/openeducat_web_no_code_studio/menu_action', 'call', {
                    name: type,
                    model: this.action.res_model,
                    view_id: this.action.view_id ? this.action.view_id[0] : false ,
                }).then(function (result) {
                    return self._replaceEditorAction(result, title, {
                        editor_clear_breadcrumbs: true,
                    });
                });
            }
        },

        _onTabIconView: function(e){
            e.preventDefault();
            var view = $(e.currentTarget).data('type');
            return this._replaceEditorAction('web_no_code_studio_action', view, {
                action: this.action,
                replace_last_action: true,
                viewType: view,
            });

        },

        _viewEditorEnter: function(viewType){
            this.view = viewType
//            if (this.editor_actions.length === 1) {
                var options = {
                    action: 'web_no_code_studio_action',
                    viewType: viewType,
                    title: viewType ? viewType.charAt(0).toUpperCase() + viewType.slice(1) : false,
                };
                if(options.viewType){
                    this._addAction(options);
                }
//            }
        },

        _addAction: function(options){
            this.editor_actions.push(options);
            if (this.$el) {
                this.$('.o_menu_sections li a.active').removeClass('active');
                this.renderElement();
            }
        },

        _replaceEditorAction: function(action, title, options){
            if (options.viewType) {
                if (options.index > 1) {
                    this.editor_actions.length = options.index + 1;
                } else {
                    this.editor_actions = [
                        {action: 'web_no_code_studio_action', title: _t('Views')},
                        {action: action, title: title, viewType: options.viewType},
                    ];
                }
            } else {
                this.editor_actions = [{action: action, title: title}];
            }
            delete options.index; // to prevent collision with option of doAction

            if (action === 'web_no_code_studio_action') {
                // do not open the default view in this case
                options.noEdit = !options.viewType;
            }
            this.activeMenu = title;
            if (action._originalAction) {
                action = JSON.parse(action._originalAction);
            }
            if (action === 'web_no_code_studio_action') {
                bus.trigger('switch_editor_view', options);
            } else {
                bus.trigger('switch_menu_action',action,options)
            }
            this.renderElement();
        },

        _activeTabButton: function(){
            const $active_menu = this.$(`.o_menu_sections li:contains(${this.activeTab})`);
            const is_action_menu = $active_menu.data('name') === 'views';
            const cur_act = this.editor_actions[this.editor_actions.length - 1];
            const is_action_overview = cur_act.action === 'web_no_code_studio_action' && !cur_act.viewType;
            if (!(is_action_menu && !is_action_overview)) {
                this.$(`.o_menu_sections li:contains(${this.activeMenu})`).addClass('active');
            }
            if (this.editor_actions.length === 0) {
                return;
            }
        },
    });

export default MenuBar;
//});
