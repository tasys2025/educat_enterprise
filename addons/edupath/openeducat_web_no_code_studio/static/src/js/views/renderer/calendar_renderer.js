odoo.define('openeducat_web_no_code_studio.CalendarRenderer', function (require) {
    "use strict";

    var CalendarRenderer = require('web.CalendarRenderer');
    var EditorMixin = require('openeducat_web_no_code_studio.EditorMixin');

    return CalendarRenderer.extend(EditorMixin,{
    });
});