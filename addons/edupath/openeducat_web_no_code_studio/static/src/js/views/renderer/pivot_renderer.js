///** @odoo-module **/
//import { PivotRenderer } from "@web/views/pivot/pivot_renderer";
//var EditorMixin = require('openeducat_web_no_code_studio.EditorMixin');
////import OwlAbstractRenderer from '../abstract_renderer_owl';
//
//export class PivotRendererEditor extends PivotRenderer {
//};
//
//PivotRendererEditor.components = { PivotRenderer };

odoo.define('openeducat_web_no_code_studio.PivotRenderer', function (require) {
    "use strict";

    var PivotRenderer = require('@web/views/pivot/pivot_renderer');
    var EditorMixin = require('openeducat_web_no_code_studio.EditorMixin');
    const OwlAbstractRenderer = require('web.AbstractRendererOwl');

   class PivotRendererEditor extends PivotRenderer {
    };

    return PivotRendererEditor;
});

