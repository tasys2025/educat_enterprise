odoo.define('openeducat_web_no_code_studio.HookFormEditor', function (require) {
"use strict";

var Widget = require('web.Widget');

var HookFormEditor = Widget.extend({
    className: 'web_no_code_studio_hook',
    init: function (parent, position, hook_id, tagName) {
        this._super.apply(this, arguments);
        this.position = position;
        this.hook_id = hook_id;
        this.tagName = tagName || 'div';
    },
    start: function () {
        this.$el.data('hook_id', this.hook_id);

        var $content;
        switch (this.tagName) {
            case 'tr':
                $content = $('<td colspan="2">').append(this._renderSpan());
                break;
            default:
                $content = this._renderSpan();
                break;
        }
        this.$el.append($content);
        return this._super.apply(this, arguments);
    },
    _renderSpan: function () {
        return $('<span>').addClass('web_no_code_studio_hook_separator');
    },
});

return HookFormEditor;

});
