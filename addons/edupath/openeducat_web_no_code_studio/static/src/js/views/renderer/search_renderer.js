odoo.define('openeducat_web_no_code_studio.SearchRenderer', function (require) {
    "use strict";

var Widget = require('web.Widget');
var utils = require('web.utils');
var core = require('web.core');
var EditorMixin = require('openeducat_web_no_code_studio.EditorMixin');
var HookFormEditor = require('openeducat_web_no_code_studio.HookFormEditor');
var DomainSelectorDialog = require("web.DomainSelectorDialog");
var Domain = require('web.Domain');
var _t = core._t;

var SearchRenderer = Widget.extend(EditorMixin,{
    template: 'openeducat_web_no_code_studio.search_view',
    className: 'web_no_code_studio_search',
    events: {
            'click .search_sub_list': '_onSearchSelected',
        },
    init: function (parent, fieldsView) {
        this._super.apply(this, arguments);
        var fieldsViewArch = $.parseXML(fieldsView.arch).documentElement;
        fieldsView.arch = utils.xml_to_json(fieldsViewArch, true);
        this.arch = fieldsView.arch;
        this.fields = fieldsView.fields;
        this.model = fieldsView.model;
        this.arch.slice
        this.search_fields = []
        this.search_filter = []
        this.search_group_by = []
        this.nearest_hook = 200;
        this.hook_nodes = {}

        for (var i in this.arch.children) {
            if(this.arch.children[i].tag === 'field'){
                this.search_fields.push(this.arch.children[i])
            }else if(this.arch.children[i].tag === 'filter'){
                this.search_filter.push(this.arch.children[i])
            }else if(this.arch.children[i].tag === 'group'){
                for(var j in this.arch.children[i].children){
                    this.search_group_by.push(this.arch.children[i].children[j])
                }

            }
        }
    },
    start: function(){
        var self = this;
        this._renderSearchFields();
        this._renderSearchFilter();
        this._renderSearchGroup();
        var res = this._super.apply(this, arguments);
        res.then(function () {
            self.$el.droppable({
                accept: ".web_no_code_studio_draggable",
                drop: self._handleDrop.bind(self),
            });
        });
    },

    _handleDrop: function (ev, ui) {
        var self = this;
        var $hook = this.$('.web_no_code_studio_nearest_hook');

        if ($hook.length) {
            var hook_id = $hook.data('hookId');
            var hook = this.hook_nodes[hook_id];
            var $drag = ui.draggable || $(ev.target);
            if(hook.node.tag === 'filter'){
                $drag.data('structure', 'filter');
                var attrs = $drag.data('new_attrs');
                if(attrs){
                    $drag.data('new_attrs', _.extend(attrs, {
                        'string': attrs.label,
                    }));
                } else {
                    var $domain_div = $("<div><label>Label:</label></div>");
                    self.$domain_label_input = $("<input type='text' id='domain_label' class='o_input mb8'/>");
                    $domain_div.append(self.$domain_label_input);
                    var domainDialog = new DomainSelectorDialog(this, this.model, [['id', '=', 1]], {
                        title: _t("New Filter"),
                        size: 'medium',
                        readonly: false,
                        $content: $domain_div,
                    }).open();
                    domainDialog.on('domain_selected', self, function (event) {
                        var new_attrs = {
                            domain: Domain.prototype.arrayToString(event.data.domain),
                            string: self.$domain_label_input.val(),
                            name: 'editor_' + ui.draggable.data('structure') + '_' + self._generateFieldString(5),
                        };
                        var values = {
                            type: 'add',
                            structure: ui.draggable.data('structure'),
                            node: hook.node,
                            new_attrs: new_attrs,
                            position: hook.position,
                        };
                        this.trigger_up('update_view', values);
                    });
                    return;
                }
            }
            if(hook.node.tag === 'group'){
                $drag.data('structure', 'group');
                var attrs = $drag.data('new_attrs');
                $drag.data('new_attrs',_.extend(attrs, {
                    'string': attrs.label,
                }));
                hook.node.attrs.context = "{'group_by': '" + attrs.name + "'}"
            }

            this._onHandleDrop($drag, hook.node, hook.position);
            ui.helper.removeClass('ui-draggable-helper-ready');

            $hook.removeClass('web_no_code_studio_nearest_hook');
            $hook.find('td').removeAttr("style");
        }
    },

    _generateFieldString: function(range){
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
            string = '';
        for (var i=0; i<range; i++) {
            var num = Math.floor(Math.random() * chars.length);
            string += chars.substring(num,num+1);
        }
        return string;
    },

    _renderSearchFields: function(){
        var self = this;
        var $chatter_hook = $('<table>').addClass('o_list_table table search_autocomplete_table');
        $chatter_hook.append($('<thead>', {class: ''})
                        .append($('<tr>', {
                        }).append($('<th>', {
                            text: _t(' Autocompletion Fields'),
                        }).prepend($('<i>', {
                            class:'fa fa-magic',
                        }))))
                    );

        var $chatter_hook1 = $('<tbody>')
        for(var i in this.search_fields){
            if(this.search_fields[i].attrs.string){
                var node = {
                    attrs: this.search_fields[i].attrs,
                    tag: 'field'
                }
                var hook = this._renderHook(node, 'after', 'tr')
                $chatter_hook1.append($('<tr>', {
                            class:'web_no_code_studio_hook',
                            'data-hook-id': hook.hook_id,
                            }).append($('<td>', {
                            colspan:'2'
                            }).append($('<span>', {
                                class:'web_no_code_studio_hook_separator'
                            })))
                );
                var $tr = $('<tr>', {
                    data_node_id: i,
                    'data-name': this.search_fields[i].attrs.name,
                });
                $chatter_hook1.append($tr.append($('<td>', {
                            }).append($('<span>', {
                                text: _t(this.search_fields[i].attrs.string),
                            })))
                );
                $tr.click( function(e){
                    var $target = $(e.currentTarget);
                    self.trigger_up('clicked_element', {
                        node: {
                            tag: 'field',
                            attrs: self.search_fields[parseInt($target.attr('data_node_id'))].attrs,
                        }
                    });
                });
            }
        }
         var last_hook = this._renderHook(node, 'after', 'tr')
         $chatter_hook1.append($('<tr>', {
                            class:'web_no_code_studio_hook',
                            'data-hook-id': last_hook.hook_id,
                            }).append($('<td>', {
                            colspan:'2'
                            }).append($('<span>', {
                                class:'web_no_code_studio_hook_separator'
                            })))
                );
        $chatter_hook.append($chatter_hook1);
        this.$el.find('.search_autocomplete').append($chatter_hook);
    },
    _renderSearchFilter: function(){
        var $chatter_hook = $('<table>').addClass('table');
        var self = this;
        $chatter_hook.append($('<thead>', {class: ''})
                        .append($('<tr>', {
                        }).append($('<th>', {
                            text: _t('Filter'),
                        }).prepend($('<i>', {
                            class:'fa fa-filter',
                        }))))
                    );
        var $chatter_hook1 = $('<tbody>')
        for(var i in this.search_filter){
            if(this.search_filter[i].attrs.string){
                var node = {
                    attrs: this.search_filter[i].attrs,
                    tag: 'filter'
                }
                var hook = this._renderHook(node, 'after', 'tr')
                $chatter_hook1.append($('<tr>', {
                            class:'web_no_code_studio_hook',
                            'data-hook-id': hook.hook_id,
                            }).append($('<td>', {
                            colspan:'2'
                            }).append($('<span>', {
                                class:'web_no_code_studio_hook_separator'
                            })))
                );
                var $tr = $('<tr>', {
                    data_node_id: i
                });
                $chatter_hook1.append($tr.append($('<td>', {
                            }).append($('<span>', {
                                text: _t(this.search_filter[i].attrs.string),
                            })))
                );
                $tr.click( function(e){
                    var $target = $(e.currentTarget);
                    self.trigger_up('clicked_element', {
                        node: {
                            attrs: self.search_filter[parseInt($target.attr('data_node_id'))].attrs,
                            tag: 'filter',
                        }
                    });
                });
            }
        }
        var last_hook = this._renderHook({
            tag: 'filter',
            new_attrs: {
                label: "Filter"
            }
        }, 'after', 'tr')
        $chatter_hook1.append($('<tr>', {
                            class:'web_no_code_studio_hook',
                            'data-hook-id': last_hook.hook_id,
                            }).append($('<td>', {
                            colspan:'2'
                            }).append($('<span>', {
                                class:'web_no_code_studio_hook_separator'
                            })))
                );
        $chatter_hook.append($chatter_hook1);
        this.$el.find('.search_list_filter').append($chatter_hook);
    },

    _renderSearchGroup: function(){
        var self = this;
        var $chatter_hook = $('<table>').addClass('table');
        $chatter_hook.append($('<thead>', {class: ''})
                        .append($('<tr>', {
                        }).append($('<th>', {
                            text: _t('Group By'),
                        }).prepend($('<i>', {
                            class:'fa fa-bars',
                        }))))
                    );
        var $chatter_hook1 = $('<tbody>')
        for(var i in this.search_group_by){
            if(this.search_group_by[i].attrs.string){
                var node = {
                    attrs: this.search_group_by[i].attrs,
                    tag: 'group'
                }
                var hook = this._renderHook(node, 'before', 'tr')
                $chatter_hook1.append($('<tr>', {
                            class:'web_no_code_studio_hook',
                            'data-hook-id': hook.hook_id,
                            }).append($('<td>', {
                            colspan:'2'
                            }).append($('<span>', {
                                class:'web_no_code_studio_hook_separator'
                            })))
                );
                var $tr =  $('<tr>', {
                    data_node_id: i
                });
                $chatter_hook1.append($tr.append($('<td>', {
                            }).append($('<span>', {
                                text: _t(this.search_group_by[i].attrs.string),
                            })))
                );
                $tr.click( function(e){
                    var $target = $(e.currentTarget);
                    self.trigger_up('clicked_element', {
                        node:  {
                            attrs: self.search_group_by[parseInt($target.attr('data_node_id'))].attrs,
                            tag: '//group//filter'
                        }
                    });
                });
            }
        }
        var last_hook = this._renderHook(node, 'after', 'tr')
        $chatter_hook1.append($('<tr>', {
                            class:'web_no_code_studio_hook',
                            'data-hook-id': last_hook.hook_id,
                            }).append($('<td>', {
                            colspan:'2'
                            }).append($('<span>', {
                                class:'web_no_code_studio_hook_separator'
                            })))
                );

        $chatter_hook.append($chatter_hook1);
        this.$el.find('.search_list_group_by').append($chatter_hook);
    },


    displayHook: function ($helper, position) {
            var self = this;

            EditorMixin.displayHook.apply(this, arguments);
            if($helper.data().new_attrs){
                if(['boolean', 'one2many', 'many2many', 'monetary', 'float', 'integer', 'text', 'binary'].includes($helper.data().new_attrs.type)){
                    $(".search_list_filter").find('table').addClass('text-muted');
                    $(".search_list_group_by").find('table').addClass('text-muted');
                }else if(['char','selection','many2one'].includes($helper.data().new_attrs.type)){
                    $(".search_list_filter").find('table').addClass('text-muted');
                }
            }
//            } else if(!$helper.data().new_attrs && $helper.data().structure == 'filter'){
//
//            }

            var $nearest_form_hooks = this.$('.web_no_code_studio_hook').touching({
                        x: position.pageX - this.nearest_hook,
                        y: position.pageY - this.nearest_hook,
                        w: this.nearest_hook*2,
                        h: this.nearest_hook*2
                    },{
                        container: document.body
                    }
                ).nearest({x: position.pageX, y: position.pageY}, {container: document.body}).eq(0);

            if ($nearest_form_hooks.length) {
                if($nearest_form_hooks.closest('.table').hasClass('text-muted') == false){
                    $nearest_form_hooks.addClass('web_no_code_studio_nearest_hook');
                    $nearest_form_hooks.find('td').css({"border": "2px dashed blue","height":"37px"});
                }
                return true;
            }
            $(".search_list_filter").find('table').removeClass('text-muted');
            $(".search_list_group_by").find('table').removeClass('text-muted');
            return false;
        },

    _renderHook: function (node, position, tagName, type) {
        var hook_id = _.uniqueId();
        this.hook_nodes[hook_id] = {
            node: node,
            position: position,
            type: type,
        };
        return new HookFormEditor(this, position, hook_id, tagName);
    },

    _onSearchSelected: function(e){
    },

});



    return SearchRenderer;
});