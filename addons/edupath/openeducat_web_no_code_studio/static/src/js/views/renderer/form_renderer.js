odoo.define('openeducat_web_no_code_studio.FormRenderer', function (require) {
    "use strict";

    var FormRenderer = require('web.FormRenderer');
    var EditorMixin = require('openeducat_web_no_code_studio.EditorMixin');
    var core = require('web.core');
    var HookFormEditor = require('openeducat_web_no_code_studio.HookFormEditor');
    var pyUtils = require('web.py_utils');
    var _t = core._t;
//    const components = {
//        ChatterContainer: require('mail/static/src/components/chatter_container/chatter_container.js'),
//    };
    const { ChatterContainer } = require('@mail/components/chatter_container/chatter_container');
    const { ComponentWrapper } = require('web.OwlCompatibility');
    class ChatterContainerWrapperComponent extends ComponentWrapper {}

    return FormRenderer.extend(EditorMixin, {
        className: FormRenderer.prototype.className + ' web_no_code_studio_form_editor',
//        events: _.extend({}, FormRenderer.prototype.events, {
//            'click .o_web_no_code_studio_add_chatter': '_onAddChatter',
//        }),
        custom_events: _.extend({}, FormRenderer.prototype.custom_events, {
            'hook_selection': '_onHookSelection',
        }),
        init: function (parent, state, params) {
            this.show_invisible = (params.show_invisible == 'true');
            this._super.apply(this, arguments);
            this.count_note_book = 0;
            this.nearest_hook_tolerance = 50;
            this.renderInvisible = this.show_invisible;
            this.chatter_allowed = params.chatter_allowed;
            this._chatterNode = undefined;
            this._chatterContainerOverview = undefined;
            this.silent = false;
            this.node_id = 1;
            this.hook_nodes = {};
        },

        destroy() {
            this._super(...arguments);
            this._chatterContainerOverview = undefined;
        },

        getLocalState: function () {
            var state = this._super.apply(this, arguments) || {};
            if (this.selected_node_id) {
                state.selected_node_id = this.selected_node_id;
            }
            return state;
        },

        setLocalState: function (state) {
            this.silent = true;
            this._super.apply(this, arguments);
            this.silent = false;
            this.unselectedElements();
            if (state.selected_node_id) {
                var $selected_node = this.$('[data-node-id="' + state.selected_node_id + '"]');
                if ($selected_node) {
                    $selected_node.click();
                }
            }
        },

        displayHook: function ($helper, position) {
            var self = this;
            EditorMixin.displayHook.apply(this, arguments);
            var $nearest_form_hooks = this.$('.web_no_code_studio_hook')
                .touching({
                        x: position.pageX - this.nearest_hook_tolerance,
                        y: position.pageY - this.nearest_hook_tolerance,
                        w: this.nearest_hook_tolerance*2,
                        h: this.nearest_hook_tolerance*2
                    },{
                        container: document.body
                    }
                ).nearest({x: position.pageX, y: position.pageY}, {container: document.body});

            var is_nearest_hook = false;
            $nearest_form_hooks.each(function () {
                var hook_id = $(this).data('hook_id');
                var hook = self.hook_nodes[hook_id];
                if ($helper.data('structure') === 'notebook') {
                    // a notebook cannot be placed inside a page or in a group
                    if (hook.type !== 'page' && !$(this).parents('.o_group').length) {
                        is_nearest_hook = true;
                    }
                } else if ($helper.data('structure') === 'group') {
                    // a group cannot be placed inside a group
                    if (hook.type !== 'insideGroup' && !$(this).parents('.o_group').length) {
                        is_nearest_hook = true;
                    }
                } else {
                    is_nearest_hook = true;
                }

                // Prevent drops outside of groups if not in whitelist
                var whitelist = ['web_no_code_studio_field_binary', 'web_no_code_studio_field_html',
                    'web_no_code_studio_field_many2many', 'web_no_code_studio_field_one2many',
                    'web_no_code_studio_field_notebook', 'web_no_code_studio_field_group'];
                var hookTypeBlacklist = ['genericTag', 'afterGroup', 'afterNotebook', 'insideSheet'];
                var fieldClasses = $helper[0].className.split(' ');
                if (_.intersection(fieldClasses, whitelist).length === 0 && hookTypeBlacklist.indexOf(hook.type) > -1) {
                    is_nearest_hook = false;
                }

                if (is_nearest_hook) {
                    $(this).addClass('web_no_code_studio_nearest_hook');
                    return false;
                }
            });

            return is_nearest_hook;
        },

        selectField: function (fieldName) {
            this.$('[name=' + fieldName + ']').click();
        },

        _addButtonHandler: function (node, $button) {
            this.setSelectable($button);
            const nodeID = this.node_id++;
            $button.attr('data-node-id', nodeID);
            if (node.attrs.effect) {
                node.attrs.effect = _.defaults(pyUtils.py_eval(node.attrs.effect), {
                    fadeout: 'medium'
                });
            }
            $button.click((event) => {
                event.preventDefault();
                event.stopPropagation();
                this.selected_node_id = nodeID;
                this.trigger_up('clicked_element', {node: node});
            });
            return $button;
        },

         _applyModifiers: function (modifiersData, record, element) {
            var def = this._super.apply(this, arguments);

            if (this.show_invisible) {
                var elements = element ? [element] : modifiersData.elements;
                _.each(elements, function (element) {
                    if (element.$el.hasClass('o_invisible_modifier')) {
                        element.$el
                            .removeClass('o_invisible_modifier')
                            .addClass('web_no_code_studio_show_invisible');
                    }
                    if (element.$el.hasClass('web_no_code_studio_show_invisible')) {
                        element.$el.closest("tr")
                            .addClass('web_no_code_studio_invisible');
                    }
                });
            }

            return def;
        },

         _handleDrop: function (ev, ui) {
            var $hook = this.$('.web_no_code_studio_nearest_hook');
            if ($hook.length) {
                var hook_id = $hook.data('hook_id');
                var hook = this.hook_nodes[hook_id];
                // draggable is only set on `droppable` elements, not `draggable`
                var $drag = ui.draggable || $(ev.target);
                this._onHandleDrop($drag, hook.node, hook.position);
                ui.helper.removeClass('ui-draggable-helper-ready');
                $hook.removeClass('web_no_code_studio_nearest_hook');
            }
        },

        _postProcessField: function (widget, node) {
            this._super.apply(this, arguments);
            if (!widget.isSet() && (!node.has_label || node.attrs.nolabel)) {
                widget.$el.removeClass('o_field_empty').addClass('web_no_code_studio_widget_empty');
                // statusbar needs to be rendered normally
                if (node.attrs.widget !== 'statusbar') {
                    widget.$el.text(widget.string);
                }
            }
            // remove all events on the widget as we only want to click for edition
            widget.$el.off();
            this._processField(node, widget.$el);
        },

        _processField: function (node, $el) {
            var self = this;
            // detect presence of mail fields
            if (node.attrs.name === "message_ids") {
                this.has_message_field = true;
            } else if (node.attrs.name === "message_follower_ids") {
                this.has_follower_field = true;
            } else if (node.attrs.name === "activity_ids") {
                this.has_activity_field = true;
            } else {
                var modifiers = self._getEvaluatedModifiers(node, this.state);
                if (modifiers.invisible && !this.show_invisible) {
                    return;
                }
                $el.attr('data-node-id', this.node_id++);
                this.setSelectable($el);
                $el.click(function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    self.selected_node_id = $el.data('node-id');
                    self.trigger_up('clicked_element', {node: node, $node:$el});
                });
            }
        },

        _render: function () {
            var self = this;
            this.has_follower_field = false;
            this.has_message_field = false;
            this.has_activity_field = false;

            this.$el.droppable({
                accept: ".web_no_code_studio_draggable",
                drop: this._handleDrop.bind(this),
            });

            return this._super.apply(this, arguments).then(async function () {
                if (!self.hasChatter && self.chatter_allowed) {
                    var $chatter_hook = $('<div>').addClass('web_no_code_studio_add_chatter o_chatter');
                    $chatter_hook.append($('<span>', {class: 'container'})
                        .append($('<span>', {
                            text: _t('Add Chatter Widget'),
                        }).prepend($('<i>', {
                            class: 'fa fa-comments',
                            style: 'margin-right:10px',
                        })))
                    );
                    const $chatterMain = $('<div>').addClass('web_no_code_studio_ChatterContainer');
                    $chatter_hook.append($chatterMain);
                    if (!self._chatterContainerOverview) {
                        self._chatterContainerOverview = new ChatterContainerWrapperComponent(
                            self,
                            ChatterContainer,
                            {
                                threadModel: self.state.model,
                            },
                        );
                    }
                    await self._chatterContainerOverview.mount($chatterMain[0]);
                    $chatter_hook.insertAfter(self.$('.o_form_sheet'));
                }
                if (!self.$('.oe_button_box').length) {
                    var $buttonbox_hook = $('<button>')
                        .addClass('btn oe_stat_button web_no_code_studio_button_hook')
                        .click(function (event) {
                            event.preventDefault();
                            self.trigger_up('update_view', {
                                type: 'add',
                                add_buttonbox: true,
                                structure: 'button',
                            });
                        });
                    var $buttonbox = $('<div>')
                        .addClass('oe_button_box')
                        .append($buttonbox_hook);
                    self.$('.o_form_sheet').prepend($buttonbox);
                }
                if (!self.$('.o_statusbar_status').length) {
                    var $statusbar = $('<div>', {
                        text: _t("Add a pipeline status bar"),
                        class: 'web_no_code_studio_statusbar_hook',
                    }).click(function () {
                        var values = {
                            add_statusbar: !self.$('.o_form_statusbar').length,
                            type: 'add',
                            structure: 'field',
                            field_description: {
                                field_description: "Pipeline status bar",
                                type: 'selection',
                                selection: [
                                    ['status1', _t('First Status')],
                                    ['status2', _t('Second Status')],
                                    ['status3', _t('Third Status')],
                                ],
                                default_value: true,
                            },
                            target: {
                                tag: 'header',
                            },
                            new_attrs: {
                                widget: 'statusbar',
                                options: "{'clickable': '1'}",
                            },
                            position: 'inside',
                        };
                        self.trigger_up('update_view', values);
                    });
                    self.$('.o_form_sheet_bg').prepend($statusbar);
                }
                self.$('i.o_optional_columns_dropdown_toggle')
                    .addClass('text-muted')
            });
        },

        _renderAddingContentLine: function (node) {
            var formEditorHook = this._renderHook(node, 'after', 'tr');
             // start the widget
            return formEditorHook.appendTo($('<div>')).then(function() {
                return formEditorHook.$el;
            })
        },

        _renderButtonBox: function () {
            var self = this;
            var $buttonbox = this._super.apply(this, arguments);
            var $buttonhook = $('<button>').addClass('btn oe_stat_button web_no_code_studio_button_hook');
            $buttonhook.click(function (event) {
                event.preventDefault();

                self.trigger_up('update_view', {
                    type: 'add',
                    structure: 'button',
                });
            });

            $buttonhook.prependTo($buttonbox);
            return $buttonbox;
        },

        _renderGenericTag: function (node) {
            var $result = this._super.apply(this, arguments);
            if (node.attrs.class === 'oe_title') {
                var formEditorHook = this._renderHook(node, 'after', '', 'genericTag')
                this.defs.push(formEditorHook.appendTo($result));
            }
            return $result;
        },

        _renderHeaderButton: function (node) {
            var self = this;
            var $button = this._super.apply(this, arguments);
            $button = this._addButtonHandler(node, $button);
            return $button;
        },

        _renderInnerGroup: function (node) {
            var self = this;
            var formEditorHook;
            var $result = this._super.apply(this, arguments);
            _.each(node.children, function (child) {
                if (child.tag === 'field') {
                    Promise.all(self.defs).then(function () {
                        var $widget = $result.find('[name="' + child.attrs.name + '"]');
                        var $tr = $widget.closest('tr');
                        if (!$widget.is('.o_invisible_modifier')) {
                            self._renderAddingContentLine(child).then(function(element) {
                                element.insertAfter($tr);
                                // apply to the entire <tr> o_web_no_code_studio_show_invisible
                                // rather then inner label/input
                                if ($widget.hasClass('o_web_no_code_studio_show_invisible')) {
                                    $widget.removeClass('o_web_no_code_studio_show_invisible');
                                    $tr.find('label[for="' + $widget.attr('id') + '"]').removeClass('o_web_no_code_studio_show_invisible');
                                    $tr.addClass('o_web_no_code_studio_show_invisible');
                                }
                            });
                        }
                        if (child.has_label) {
                            // as it's not possible to move the label, we only allow to
                            // move fields with a default label (otherwise the field
                            // will be moved but the label will stay)
                            self._setDraggable(child, $tr);
                        }
                        self._processField(child, $tr);
                    });
                }
            });
            const modifiers = this._getEvaluatedModifiers(node, this.state);
            if (!modifiers.invisible || this.show_invisible) {
                // Add click event to see group properties in sidebar
                $result.attr('data-node-id', this.node_id++);
                this.setSelectable($result);
                $result.click(function (event) {
                    event.stopPropagation();
                    self.selected_node_id = $result.data('node-id');
                    self.trigger_up('clicked_element', { node: node });
                });
            }
            // Add hook for groups that have not yet content.
            if (!node.children.length) {
                formEditorHook = this._renderHook(node, 'inside', 'tr', 'insideGroup');
                this.defs.push(formEditorHook.appendTo($result));
            } else {
                // Add hook before the first node in a group.
                var $firstRow = $result.find('tr:first');
                formEditorHook = this._renderHook(node.children[0], 'before', 'tr');
                if (node.attrs.string) {
                    // the group string is displayed in a tr
                    this.defs.push(formEditorHook.insertAfter($firstRow));
                } else {
                    this.defs.push(formEditorHook.insertBefore($firstRow));
                }
            }
            return $result;
        },

        _renderInnerGroupField: function (node) {
            node.has_label = (node.attrs.nolabel !== "1");
            return this._super.apply(this, arguments);
        },

        _renderNode: function (node) {
            var $el = this._super.apply(this, arguments);
            if (node.tag === 'div' && node.attrs.class === 'oe_chatter') {
                this._chatterNode = node;
                //return;
            }
            return $el;
        },

        _renderStatButton: function (node) {
            var self = this;
            var $button = this._super.apply(this, arguments);
            $button.attr('data-node-id', this.node_id++);
            this.setSelectable($button);
            $button.click(function (ev) {
                if (! $(ev.target).closest('.o_field_widget').length) {
                    self.selected_node_id = $button.data('node-id');
                    self.trigger_up('clicked_element', {node: node});
                }
            });
            return $button;
        },

        _renderTabPage: function (node) {
            var $result = this._super.apply(this, arguments);
            if (!$result.children('.o_group:not(.o_inner_group):last-child').length) {
                var formEditorHook = this._renderHook(node, 'inside', 'div', 'page');
                this.defs.push(formEditorHook.appendTo($result));
            }
            return $result;
        },

        _renderOuterGroup: function (node) {
            var $result = this._super.apply(this, arguments);

            // Add hook after this group
            var formEditorHook = this._renderHook(node, 'after', '', 'afterGroup');
            this.defs.push(formEditorHook.insertAfter($result));
            return $result;
        },

        _renderTagButton: function (node) {
            let $result = this._super.apply(this, arguments);
            $result = this._addButtonHandler(node, $result);
            return $result;
        },

        _renderTagLabel: function (node) {
            var self = this;
            var $result = this._super.apply(this, arguments);

            // only handle label tags, not labels associated to fields (already
            // handled in @_renderInnerGroup with @_processField)
            if (node.tag === 'label') {
                $result.attr('data-node-id', this.node_id++);
                this.setSelectable($result);
                $result.click(function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    self.selected_node_id = $result.data('node-id');
                    self.trigger_up('clicked_element', {node: node});
                });
            }
            return $result;
        },

        _renderTagNotebook: function (node) {
            var self = this;
            var $result = this._super.apply(this, arguments);

            var $addTag = $('<li>', {class: 'nav-item'}).append('<a href="#" class="nav-link"><i class="fa fa-plus-square"/></a>');
            $addTag.click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                self.trigger_up('update_view', {
                    type: 'add',
                    structure: 'page',
                    position: 'inside',
                    node: node,
                });
            });
            $result.find('ul.nav-tabs').append($addTag);

            var formEditorHook = this._renderHook(node, 'after', '', 'afterNotebook');
            this.defs.push(formEditorHook.appendTo($result));

            // add node-id data on all tabs once whole notebook tabs are rendered
            // as registerModifiers of all tabs are called from _renderTagNotebook
            // so we need to evaluate modifiers here else we will not get modifiers
            Object.entries(node.children).forEach(([index, child]) => {
                const modifiers = this._getEvaluatedModifiers(child, this.state);
                if (!modifiers.invisible || this.show_invisible) {
                    const $page = $result.find(`.nav-item:eq(${index})`);
                    $page.attr('data-node-id', this.node_id++);
                    this.setSelectable($page);
                    $page.click(function (event) {
                        event.preventDefault();
                        if (!self.silent) {
                            self.selected_node_id = $page.data('node-id');
                            self.trigger_up('clicked_element', { node: child });
                        }
                    });
                }
            });
            return $result;
        },

        _renderTagSheet: function (node) {
            var $result = this._super.apply(this, arguments);
            var formEditorHook = this._renderHook(node, 'inside', '', 'insideSheet');
            this.defs.push(formEditorHook.prependTo($result));
            return $result;
        },

        async _renderView() {
            await this._super(...arguments);
            this.$( ".o_form_statusbar" ).hide();
            //this.$( ".oe_button_box" ).hide();
            if (this.hasChatter) {
                const $el = $(this._chatterContainerComponent.el);
                this.setSelectable($el);
                // Put a div in overlay preventing all clicks chatter's elements
                $el.append($('<div>', { 'class': 'web_no_code_studio_overlay' }));
                $el.attr('data-node-id', this.node_id++);
                $el.click(() => {
                    this.selected_node_id = $el.data('node-id');
                    this.trigger_up('clicked_element', { node: this._chatterNode });
                });
            }
        },

        _renderHook: function (node, position, tagName, type) {
            var hook_id = _.uniqueId();
            this.hook_nodes[hook_id] = {
                node: node,
                position: position,
                type: type,
            };
            return new HookFormEditor(this, position, hook_id, tagName);
        },

        _setDraggable: function (node, $el) {
            var self = this;

            if ($el.is('tr')) {
                $el = $el.find('td:first');
            }

            $el.draggable({
                revertDuration: 200,
                refreshPositions: true,
                start: function (e, ui) {
                    self.$('.web_no_code_studio_hovered').removeClass('web_no_code_studio_hovered');
                    self.$('.web_no_code_studio_clicked').removeClass('web_no_code_studio_clicked');
                    ui.helper.addClass('ui-draggable-helper');
                    ui.helper.data('name', node.attrs.name);
                },
                revert: function () {
                    // a field cannot be dropped on the same place
                    var $hook = self.$('.web_no_code_studio_nearest_hook');
                    if ($hook.length) {
                        var hook_id = $hook.data('hook_id');
                        var hook = self.hook_nodes[hook_id];
                        if (hook.node.attrs.name !== node.attrs.name) {
                            return false;
                        }
                    }
                    self.$('.ui-draggable-helper').removeClass('ui-draggable-helper');
                    self.$('.ui-draggable-helper-ready').removeClass('ui-draggable-helper-ready');
                    return true;
                },
                stop: this._handleDrop.bind(this),
            });

            // display nearest hook (handled by the ViewEditorManager)
            $el.on('drag', _.throttle(function (event, ui) {
                self.trigger_up('element_drag', {
                    position: {pageX: event.pageX, pageY: event.pageY},
                    $helper: ui.helper,
                });
            }, 200));
        },

        _onAddChatter: function (ev) {
            $(ev.currentTarget).css('pointer-events', 'none');
            this.trigger_up('update_view', {
                structure: 'chatter',
                remove_follower_ids: this.has_follower_field,
                remove_message_ids: this.has_message_field,
                remove_activity_ids: this.has_activity_field,
            });
        },

        _onButtonBoxHook: function () {
            this.trigger_up('update_view', {
                structure: 'buttonbox',
            });
        },

        _onHookSelection: function () {
            this.selected_node_id = false;
        },
    });
});