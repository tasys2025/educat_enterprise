from odoo import http, _
from odoo.http import request
from odoo.exceptions import UserError, ValidationError
from lxml import etree
from .main import ViewEditorMain
import uuid


class ViewEditorReportController(ViewEditorMain):

    @http.route('/report/get/data', type='json', auth='user')
    def report_get_data(self, model):
        views = request.env['ir.actions.report'].search([('model', '=', model)])
        name, selection = None, []
        for select in views:
            selection.append({
                'name': select.name,
                'report_name': select.report_name,
                'id': select.id,
            })
        return {
            "selection": selection,
            "name": name.name if name else None,
        }

    @http.route('/report/get/record', type='json', auth='user')
    def report_get_record(self, report_id):
        views = request.env['ir.actions.report'].search([('id', '=', report_id)])
        rec = request.env[views.model].search([], limit=1)
        data = self.get_report_views(views.report_name, rec.id, views.model)
        values = {
            'report_name': views.report_name,
            'report_file': views.report_file,
            'report_data': data,
            'name': views.name,
        }
        return values

    def get_report_views(self, report_name, record_id, model):
        loaded = set()
        views = {}

        def get_report_view(key):
            view = request.env['ir.ui.view'].search([
                ('key', '=', key),
                ('type', '=', 'qweb'),
                ('mode', '=', 'primary'),
            ], limit=1)
            if not view:
                raise UserError(_("No view found for the given report!"))
            return view

        def load_arch(view_name):
            if view_name in loaded:
                return
            loaded.add(view_name)
            view = get_report_view(view_name)
            editor_view = view.sudo().search([('inherit_id', '=', view.id),
                                              ('name', '=',
                                               'View Editor: %s' % view.name)])
            element = request.env['ir.qweb']._get_template(view.id)[0]
            views[view.id] = {
                'arch': etree.tostring(element),
                'key': view.key,
                'editor_view_arch': editor_view.arch_db or "<data/>",
                'editor_view_id': editor_view.id,
                'view_id': view.id,
            }
            for node in element.getroottree().findall("//*[@t-call]"):
                tcall = node.get("t-call")
                if '{' in tcall:
                    continue
                load_arch(tcall)
            return view.id

        load_arch(report_name)
        main_view_id = get_report_view(report_name).id
        report = request.env['ir.actions.report']._get_report_from_name(report_name)
        report_html = report._render_qweb_html(report, [record_id], {
            'full_branding': True,
            'editor': True,
            'model': model,
        })
        return {
            'report_html': report_html and report_html[0],
            'main_view_id': main_view_id,
            'views': views,
            'read_data': report.read()[0]
        }

    @http.route('/report/editor/bg', type='json', auth='user')
    def report_editor_bg_image_change(self, view_id=None, image=None):
        if view_id:
            view = request.env['ir.ui.view'].sudo().browse(int(view_id))
            editor_view = view.sudo().search([('inherit_id', '=', view.id),
                                              ('name', '=',
                                               'View Editor: %s' % view.name)])
            if not editor_view:
                editor_view = request.env['ir.ui.view'].create({
                    'type': view.type,
                    'model': view.model,
                    'inherit_id': view.id,
                    'mode': 'extension',
                    'priority': 99,
                    'arch': '<data/>',
                    'name': 'View Editor: %s' % view.name,
                })
            parser = etree.XMLParser(remove_blank_text=True)
            arch = etree.fromstring(editor_view.arch, parser=parser)
            node = etree.Element('attribute', {
                'name': 't-value',
            })
            node.text = "'background: url(\\'%s\\');'" % image
            etree.SubElement(arch, 'xpath', {
                'expr': "//t[@t-set='background_image_editor_style_container']",
                'position': 'attributes',
            }).append(node)
            new_arch = etree.tostring(arch, encoding='unicode', pretty_print=True)
            self.web_no_code_studio_initialize(new_arch, view)

    @http.route('/openeducat_web_no_code_studio/edit/report', type='json', auth='user')
    def edit_report_view(self, report_name, report_views, model_name, steps=None):
        groups = {}
        ops = []
        for op in steps:
            ops += op.get('inheritance', [op])
        for op in ops:
            if str(op['view_id']) not in groups:
                groups[str(op['view_id'])] = []
            groups[str(op['view_id'])].append(op)

        parser = etree.XMLParser(remove_blank_text=True)
        for group_view_id in groups:
            view = request.env['ir.ui.view'].browse(int(group_view_id))
            if view.key in request.env['ir.ui.view'].TEMPLATE_BLACKLIST:
                raise ValidationError(_(
                    "You can not modify this view, it is part of the generic layout"))
            arch = etree.fromstring(report_views[group_view_id]['editor_view_arch'],
                                    parser=parser)

            for op in groups[group_view_id]:
                if not op.get('type'):
                    content = etree.fromstring(op['content'], etree.HTMLParser())
                    for node in content[0]:
                        etree.SubElement(arch, 'xpath', {
                            'expr': op['xpath'],
                            'position': op['position'],
                        }).append(node)
                else:
                    op['position'] = op['type']
                    op['target'] = {
                        'xpath_info': [{
                            'tag': g.split('[')[0],
                            'index': g.split('[')[1][:-1] if '[' in g else 1
                        } for g in op['xpath'].split('/')[1:]]
                    }
                    getattr(self, '_operation_%s' % (op['type']))(arch, op)
            new_arch = etree.tostring(arch, encoding='unicode', pretty_print=True)
            self.web_no_code_studio_initialize(new_arch, view)

        data = request.env[model_name].search([], limit=1)
        result = self.get_report_views(
            report_name, data.id if data else None, model_name)

        return result

    @http.route('/create/new/report', type='json', auth='public')
    def create_new_report_web_no_code_studio(self, model_name, layout):
        if layout == 'blank':
            arch_document = etree.fromstring("""
                <t t-name="web_no_code_studio_created_report">
                    <div class="page">
                        <div class="row"/>
                    </div>
                </t>
                """)
        else:
            arch_document = etree.fromstring("""
                <t t-name="web_no_code_studio_created_report">
                    <t t-call="web.%s_layout">
                        <div class="page">
                            <div class="row"/>
                        </div>
                    </t>
                </t>
                """ % layout)

        view_document = request.env['ir.ui.view'].create({
            'name': 'web_no_code_studio_created_report',
            'type': 'qweb',
            'arch': etree.tostring(arch_document, encoding='utf-8', pretty_print=True),
        })

        uid = uuid.uuid4()
        new_view_document_xml_id = 'openeducat_web_no_code_studio' \
                                   '.report_doc_%s_document' % uid
        new_view_xml_id = 'openeducat_web_no_code_studio.report_doc_%s' % uid
        view_document.name = new_view_document_xml_id
        view_document.key = new_view_document_xml_id

        if layout == 'blank':
            arch = etree.fromstring("""
                <t t-name="web_no_code_studio_created_report_main">
                    <t t-foreach="docs" t-as="doc">
                        <t t-call="%(layout)s">
                            <t t-call="%(document)s_document"/>
                            <p style="page-break-after: always;"/>
                        </t>
                    </t>
                </t>
            """ % {'layout': "web.basic_layout", 'document': new_view_xml_id})
        else:
            arch = etree.fromstring("""
                <t t-name="web_no_code_studio_created_report_main">
                    <t t-call="web.html_container">
                        <t t-foreach="docs" t-as="doc">
                            <t t-call="%(document)s_document"/>
                        </t>
                    </t>
                </t>
            """ % {'document': new_view_xml_id})
        view = request.env['ir.ui.view'].create({
            'name': 'web_no_code_studio_created_report_main',
            'type': 'qweb',
            'arch': etree.tostring(arch, encoding='utf-8', pretty_print=True),
        })
        view.name = new_view_xml_id
        view.key = new_view_xml_id

        model = request.env['ir.model'].search([('model', '=', model_name)])
        report = request.env['ir.actions.report'].create({
            'name': _('%s Report', model.name),
            'model': model.model,
            'report_type': 'qweb-pdf',
            'report_name': view.name,
        })
        report.create_action()
        rec = request.env[report.model].search([], limit=1)
        data = self.get_report_views(report.report_name, rec.id, model_name)
        values = {
            'report_name': report.report_name,
            'report_file': report.report_file,
            'report_data': data,
            'name': report.name,
            'report_id': report.id,
        }

        return values

    @http.route('/openeducat_web_no_code_studio/edit/report/val', type='json',
                auth='user')
    def web_no_code_studio_report_edit(self, report_id, values, model_name):
        report = request.env['ir.actions.report'].browse(report_id)
        if report:
            if 'attachment_use' in values:
                if values['attachment_use']:
                    values['attachment'] = "'%s'" % report.name
                else:
                    values['attachment'] = False
            if 'groups_id' in values:
                values['groups_id'] = [(6, 0, values['groups_id'])]
            if 'display_in_print' in values:
                if values['display_in_print']:
                    report.create_action()
                else:
                    report.unlink_action()
                values.pop('display_in_print')
            report.write(values)
        rec = request.env[report.model].search([], limit=1)
        data = self.get_report_views(report.report_name, rec.id, model_name)
        return {
            'report_name': report.report_name,
            'report_file': report.report_file,
            'report_data': data,
            'name': report.name,
            'report_id': report.id,
        }
