/** @odoo-module **/

import { CharField } from "@web/views/fields/char/char_field";
import { registry } from "@web/core/registry";
const { onWillStart, Component, useState, onWillUpdateProps } = owl;

export class AppointmentAccessUrl extends Component {
    setup() {
        super.setup();
        this.state = useState({
            url: this.generateAppointmentUrl(this.props),
        });

        onWillUpdateProps((nextProps) => {
            Object.assign(this.state, {
                url: this.generateAppointmentUrl(nextProps),
            });
        });
    }

    generateAppointmentUrl(props) {
        var context = this.props.record.getFieldContext('id')
        if(!context.parent_id) { return false; }
        return `${window.location.origin}/booking?employee_id=${props.record.data.id}?appointment_id=${context.parent_id}`;
    }

    onClick(e) {
        e.stopPropagation();
    }
}

AppointmentAccessUrl.template = 'appointment_access_url';

registry.category("fields").add("access_url", AppointmentAccessUrl);