# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_assignment_grading_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 11:35+0000\n"
"PO-Revision-Date: 2022-12-13 11:35+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_assignment_grading_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_assignment_grading_enterprise.assignment_marks_view
msgid "<strong>Grade:</strong>"
msgstr "<strong> الصف: </strong>"

#. module: openeducat_assignment_grading_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_assignment_grading_enterprise.assignment_marks_view
msgid "<strong>Marks:</strong>"
msgstr "<strong> علامات: </strong>"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model,name:openeducat_assignment_grading_enterprise.model_op_assignment
msgid "Assignment"
msgstr "مهمة"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.ui.menu,name:openeducat_assignment_grading_enterprise.menu_op_assign_grade_config
msgid "Assignment Grades"
msgstr "درجات المهمة"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model,name:openeducat_assignment_grading_enterprise.model_op_assignment_sub_line
msgid "Assignment Submission"
msgstr "تقديم الطلب"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config__create_uid
msgid "Created by"
msgstr "انشأ من قبل"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config__create_date
msgid "Created on"
msgstr "تم إنشاؤها على"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config__display_name
msgid "Display Name"
msgstr "اسم العرض"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assignment_sub_line__evaluation_boolean
msgid "Evaluation Boolean"
msgstr "تقييم منطقي"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assignment__evaluation_type
msgid "Evolution type"
msgstr "نوع التطور"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config__grade
#: model:ir.model.fields.selection,name:openeducat_assignment_grading_enterprise.selection__op_assignment__evaluation_type__grade
msgid "Grade"
msgstr "صف دراسي"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.actions.act_window,name:openeducat_assignment_grading_enterprise.act_op_assign_grade_config_view
#: model:ir.model,name:openeducat_assignment_grading_enterprise.model_op_assign_grade_config
#: model_terms:ir.ui.view,arch_db:openeducat_assignment_grading_enterprise.view_op_assign_grade_config_form
#: model_terms:ir.ui.view,arch_db:openeducat_assignment_grading_enterprise.view_op_assign_grade_config_tree
msgid "Grade Configuration"
msgstr "تكوين الصف"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assignment_sub_line__grades_id
msgid "Grades"
msgstr "درجات"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config__id
msgid "ID"
msgstr "بطاقة تعريف"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config____last_update
msgid "Last Modified on"
msgstr "آخر تعديل على"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config__write_uid
msgid "Last Updated by"
msgstr "آخر تحديث بواسطة"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields,field_description:openeducat_assignment_grading_enterprise.field_op_assign_grade_config__write_date
msgid "Last Updated on"
msgstr "آخر تحديث على"

#. module: openeducat_assignment_grading_enterprise
#: model:ir.model.fields.selection,name:openeducat_assignment_grading_enterprise.selection__op_assignment__evaluation_type__mark
msgid "Marks"
msgstr "علامات"
