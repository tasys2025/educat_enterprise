# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

{
    'name': "OpenEduCat Biometric Attendance",
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'version': '16.0.1.0',
    'category': 'Human Resources',
    'depends': ['web','hr_attendance'],
    'data': [
        'data/scheduler_data.xml',
        'data/attendance_state_data.xml',
        'data/mail_template_data.xml',
        'security/module_security.xml',
        'security/ir.model.access.csv',
        'views/menu_view.xml',
        'views/attendance_device_views.xml',
        'views/attendance_state_views.xml',
        'views/device_user_views.xml',
        'views/hr_attendance_views.xml',
        'views/hr_employee_views.xml',
        'views/user_attendance_views.xml',
        'views/attendance_activity_views.xml',
        'views/finger_template_views.xml',
        'views/set_date.xml',
        'wizard/attendance_wizard.xml',
        'wizard/employee_upload_wizard.xml',

    ],
     'assets': {
        'web.assets_backend': [
            'openeducat_biometric/static/src/js/confirm.js',
        ],
     },
    'images' : ['static/description/main_screenshot.png'],
    'installable': True,
    'application': True,
    'auto_install': False,
   
    
    'external_dependencies': {'python': ['pyzk']},
    'license': 'Other proprietary',
}
