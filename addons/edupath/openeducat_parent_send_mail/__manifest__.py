
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

{
    'name': "OpenEduCat Parent Mass Mailing",
    'description': "Allows to send email in bulks for parents",
    'author': 'OpenEduCat Inc',
    'version': '16.0.1.0',
    'category': 'Education',
    'depends': ['base', 'openeducat_parent_enterprise'],
    'website': 'http://www.openeducat.org',
    'data': [
        'data/mail_data.xml',
        'views/op_parent_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'Other proprietary',
}
