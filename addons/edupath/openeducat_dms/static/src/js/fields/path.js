/** @odoo-module **/

import { CharField } from "@web/views/fields/char/char_field";
import { registry } from "@web/core/registry";
import { useService } from "@web/core/utils/hooks";


export class FieldPathJson extends CharField {

    setup() {
        super.setup();
        this.max_width = this.props.width || 500;
        this.seperator = this.props.seperator || "/";
        this.prefix = this.props.prefix || false;
        this.suffix = this.props.suffix || false;
        this.action = useService("action");
        this._renderReadonly()
    
    }

    _renderReadonly () {
        setTimeout(() => {
            $('.path_json').empty();
            this._renderPath();
        }, 150);
    }
    _renderPath () {
        var text_width_measure = "";
        var path = JSON.parse(this.props.value || "[]");
        $.each(
            _.clone(path).reverse(),
            function (index, element) {
                text_width_measure += element.name + "/";
                if (text_width_measure.length >= this.max_width) {
                    $('.path_json').prepend($("<span/>").text(".."));
                } else if (index === 0) {
                    if (this.suffix) {
                        $('.path_json').prepend($("<span/>").text(this.seperator));
                    }
                    $('.path_json').prepend($("<span/>").text(element.name));
                    $('.path_json').prepend($("<span/>").text(this.seperator));
                } else {
                    $('.path_json').prepend(
                        $('<a/>', {
                            class: "oe_form_uri",
                            "data-model": element.model,
                            "data-id": element.id,
                            href: "#",
                            text: element.name,
                        }).click((ev) => this._onNodeClicked(ev))
                        
                    );
                    if (index !== path.length - 1) {
                        $('.path_json').prepend($("<span/>").text(this.seperator));
                    } else if (this.prefix) {
                        $('.path_json').prepend($("<span/>").text(this.seperator));
                    }
                }
                return text_width_measure.length < this.max_width;
            }.bind(this)
        );
    }
    _onNodeClicked (event) {
        event.preventDefault();
        this.action.doAction({
            type: "ir.actions.act_window",
            res_model: $(event.currentTarget).data("model"),
            res_id: $(event.currentTarget).data("id"),
            views: [[false, "form"]],
            target: "current",
            context: {},
        });
    }
}

export class FieldPathNames extends CharField {
    setup() {
        super.setup();
        this.max_width = this.props.width || 500;
        this.action = useService("action");
        this._renderReadonly()
    
    }
    _renderReadonly() {
        var show_value = this._formatValue(this.props.value);
        var text_witdh = show_value.length;
        if (text_witdh >= this.max_width) {
            var ratio_start = (1 - this.max_width / text_witdh) * show_value.length;
            show_value = ".." + show_value.substring(ratio_start, show_value.length);
        }
        this.$el.text(show_value);
    }

}
registry.category("fields").add("path_names", FieldPathNames);
registry.category("fields").add("path_json", FieldPathJson);

FieldPathJson.template = "FieldPathJson.path_json"

FieldPathJson.props = {
    ...FieldPathJson.props,
    prefix: { type: Boolean, optional: true },
    suffix: { type: Boolean, optional: true },
    max_width: { type: String, optional: true },
    seperator: { type: String, optional: true },
}
FieldPathJson.extractProps = ({ attrs, field }) => {
    return {
        prefix: attrs.options.prefix,
        suffix: attrs.options.suffix,
        max_width: attrs.options.max_width,
        seperator: attrs.options.seperator,
    }
}
FieldPathNames.props = {
    ...FieldPathNames.props,
    
    max_width: { type: String, optional: true },
    
}
FieldPathNames.extractProps = ({ attrs, field }) => {
    return {
        max_width: attrs.options.max_width,
    }
}

return {
    FieldPathJson:FieldPathJson,
    FieldPathNames:FieldPathNames
};
