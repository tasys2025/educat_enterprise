# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_terminology_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 05:10+0000\n"
"PO-Revision-Date: 2022-12-14 05:10+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.res_config_settings_view_form_inherit
msgid "<span>Change Terminology:</span>"
msgstr ""

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.terminology_settings_form_view
msgid "<span>Name:</span>"
msgstr "<span> Nome: </span>"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.terminology_settings_form_view
msgid "<span>PLURAL</span>"
msgstr ""

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.terminology_settings_form_view
msgid "<span>SINGULAR</span>"
msgstr "<span> Singular </span>"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.res_config_settings_view_form_inherit
msgid "<span>Selected Terminology:</span>"
msgstr ""

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_needaction
msgid "Action Needed"
msgstr "Ação necessária"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_attachment_count
msgid "Attachment Count"
msgstr "Contagem de anexos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_batch_label
msgid "Base Batch Label"
msgstr "Etiqueta em lote base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_batch_label_plural
msgid "Base Batch Labels"
msgstr "Etiquetas de lote base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_course_label
msgid "Base Course Label"
msgstr "Etiqueta do curso base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_course_label_plural
msgid "Base Course Labels"
msgstr "Etiquetas do curso base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_faculty_label
msgid "Base Faculty Label"
msgstr "Rótulo da faculdade de base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_faculty_label_plural
msgid "Base Faculty Labels"
msgstr "Etiquetas da faculdade base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_student_label
msgid "Base Student Label"
msgstr "Etiqueta do aluno base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_student_label_plural
msgid "Base Student Labels"
msgstr "Etiquetas de estudantes base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_subject_label
msgid "Base Subject Label"
msgstr "Etiqueta de assunto base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__base_subject_label_plural
msgid "Base Subject Labels"
msgstr "Etiquetas de assunto base"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__batch_new_label
msgid "Batch New Label"
msgstr "Novo rótulo em lote"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__batch_new_label_plural
msgid "Batch New Labels"
msgstr "Novos rótulos em lote"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__batch_old_label
msgid "Batch Old Label"
msgstr "Etiqueta antiga em lote"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__batch_old_label_plural
msgid "Batch Old Labels"
msgstr "Lote de etiquetas antigas"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.wizard_string_label_change
msgid "Cancel"
msgstr "Cancelar"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.wizard_string_label_change
msgid "Change"
msgstr "Mudar"

#. module: openeducat_terminology_enterprise
#: model:ir.actions.act_window,name:openeducat_terminology_enterprise.action_wizard_string_label_change
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.wizard_string_label_change
msgid "Change Name"
msgstr "Mude o nome"

#. module: openeducat_terminology_enterprise
#: model:ir.model,name:openeducat_terminology_enterprise.model_res_config_settings
msgid "Config Settings"
msgstr "Configurações de configuração"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.res_config_settings_view_form_inherit
msgid "Configure Terminology"
msgstr "Configurar terminologia"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__course_new_label
msgid "Course New Label"
msgstr "Curso novo rótulo"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__course_new_label_plural
msgid "Course New Labels"
msgstr "Curso novos rótulos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__course_old_label
msgid "Course Old Label"
msgstr "Claro rótulo antigo"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__course_old_label_plural
msgid "Course Old Labels"
msgstr "Claro rótulos antigos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__create_uid
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology__create_uid
msgid "Created by"
msgstr "Criado por"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__create_date
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology__create_date
msgid "Created on"
msgstr "Criado em"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__display_name
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology__display_name
msgid "Display Name"
msgstr "Nome em Exibição"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__faculty_new_label
msgid "Faculty New Label"
msgstr "Faculdade nova etiqueta"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__faculty_new_label_plural
msgid "Faculty New Labels"
msgstr "Faculdade de novos rótulos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__faculty_old_label
msgid "Faculty Old Label"
msgstr "Etiqueta antiga do corpo docente"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__faculty_old_label_plural
msgid "Faculty Old Labels"
msgstr "Faculdade antigos rótulos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_follower_ids
msgid "Followers"
msgstr "Seguidores"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_partner_ids
msgid "Followers (Partners)"
msgstr "Seguidores (parceiros)"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__has_message
msgid "Has Message"
msgstr "Tem mensagem"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__id
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology__id
msgid "ID"
msgstr "EU IRIA"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,help:openeducat_terminology_enterprise.field_terminology_configuration__message_needaction
msgid "If checked, new messages require your attention."
msgstr "Se verificado, novas mensagens exigem sua atenção."

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,help:openeducat_terminology_enterprise.field_terminology_configuration__message_has_error
#: model:ir.model.fields,help:openeducat_terminology_enterprise.field_terminology_configuration__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "Se verificado, algumas mensagens têm um erro de entrega."

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_is_follower
msgid "Is Follower"
msgstr "É seguidor"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration____last_update
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology____last_update
msgid "Last Modified on"
msgstr "Último modificado em"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__write_uid
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology__write_uid
msgid "Last Updated by"
msgstr "Última atualização por"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__write_date
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology__write_date
msgid "Last Updated on"
msgstr "Última atualização em"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_main_attachment_id
msgid "Main Attachment"
msgstr "Anexo principal"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_has_error
msgid "Message Delivery error"
msgstr "Erro de entrega da mensagem"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_ids
msgid "Messages"
msgstr "Mensagens"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__name
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.terminology_settings_form_view
msgid "Name"
msgstr "Nome"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_needaction_counter
msgid "Number of Actions"
msgstr "Número de ações"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_has_error_counter
msgid "Number of errors"
msgstr "Número de erros"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,help:openeducat_terminology_enterprise.field_terminology_configuration__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Número de mensagens que requer uma ação"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,help:openeducat_terminology_enterprise.field_terminology_configuration__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Número de mensagens com erro de entrega"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.res_config_settings_view_form_inherit
msgid "OpenEduCat Name Change"
msgstr "Alteração do nome do OpenEducat"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__message_has_sms_error
msgid "SMS Delivery error"
msgstr "Erro de entrega do SMS"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__student_new_label
msgid "Student New Label"
msgstr "Novo rótulo do aluno"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__student_new_label_plural
msgid "Student New Labels"
msgstr "Estudantes novos rótulos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__student_old_label
msgid "Student Old Label"
msgstr "Etiqueta antiga do aluno"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__student_old_label_plural
msgid "Student Old Labels"
msgstr "Estudantes antigos rótulos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__subject_new_label
msgid "Subject New Label"
msgstr "Assunto novo rótulo"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__subject_new_label_plural
msgid "Subject New Labels"
msgstr "Sujeito novos rótulos"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__subject_old_label
msgid "Subject Old Label"
msgstr "Assunto Rótulo antigo"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__subject_old_label_plural
msgid "Subject Old Labels"
msgstr "Sujeito rótulos antigos"

#. module: openeducat_terminology_enterprise
#: model:ir.model,name:openeducat_terminology_enterprise.model_wizard_terminology
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_res_config_settings__terminology_setting_id
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_wizard_terminology__terminology_setting_id
msgid "Terminology"
msgstr "Terminologia"

#. module: openeducat_terminology_enterprise
#: model:ir.model,name:openeducat_terminology_enterprise.model_terminology_configuration
msgid "Terminology Configuration"
msgstr "Configuração de terminologia"

#. module: openeducat_terminology_enterprise
#: model:ir.ui.menu,name:openeducat_terminology_enterprise.menu_terminology_setting
msgid "Terminology Setting"
msgstr "Cenário de terminologia"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.terminology_settings_form_view
msgid "Terminology Setting Form"
msgstr "Formulário de configuração de terminologia"

#. module: openeducat_terminology_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_terminology_enterprise.terminology_settings_list_view
msgid "Terminology Setting List"
msgstr "Lista de configurações de terminologia"

#. module: openeducat_terminology_enterprise
#: model:ir.actions.act_window,name:openeducat_terminology_enterprise.action_terminology_setting
msgid "Terminology Settings"
msgstr "Loading..."

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,field_description:openeducat_terminology_enterprise.field_terminology_configuration__website_message_ids
msgid "Website Messages"
msgstr "Mensagens do site"

#. module: openeducat_terminology_enterprise
#: model:ir.model.fields,help:openeducat_terminology_enterprise.field_terminology_configuration__website_message_ids
msgid "Website communication history"
msgstr "Histórico de comunicação do site"
