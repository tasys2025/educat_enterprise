/** @odoo-module **/

import { registerPatch } from '@mail/model/model_core';
import { one } from '@mail/model/model_field';
import { clear } from '@mail/model/model_field_command';

registerPatch({
    name: 'EmojiView',
    recordMethods: {
        onClick(ev) {
            if (!this.emojiGridItemViewOwner.emojiGridRowViewOwner) {
                return;
            }
            if (this.emojiGridItemViewOwner.emojiGridRowViewOwner.emojiGridViewOwner.emojiPickerViewOwner.popoverViewOwner.knowledgeOwnerAsEmojiPicker) {
                this.emojiGridItemViewOwner.emojiGridRowViewOwner.emojiGridViewOwner.emojiPickerViewOwner.popoverViewOwner.knowledgeOwnerAsEmojiPicker.onClickEmoji(this);
                return;
            }
            return this._super(ev);
        },
    },
});

registerPatch({
    name: 'Emoji',
    fields: {
        emojiAsKnowledgeRandom: one('Knowledge', {
            compute() {
                if (!this.messaging || !this.messaging.knowledge) {
                    return clear();
                }
                if (['💩', '💀', '☠️', '🤮', '🖕', '🤢'].includes(this.codepoints)) {
                    return clear();
                }
                return this.messaging.knowledge;
            },
            inverse: 'randomEmojis',
        }),
    },
});

registerPatch({
    name: 'PopoverView',
    fields: {
        anchorRef: {
            compute() {
                if (this.knowledgeOwnerAsEmojiPicker) {
                    return this.knowledgeOwnerAsEmojiPicker.emojiPickerPopoverAnchorRef;
                }
                return this._super();
            },
        },
        emojiPickerView: {
            compute() {
                if (this.knowledgeOwnerAsEmojiPicker) {
                    return {};
                }
                return this._super();
            },
        },
        knowledgeOwnerAsEmojiPicker: one('Knowledge', {
            identifying: true,
            inverse: 'emojiPickerPopoverView',
        }),
        position: {
            compute() {
                if (this.knowledgeOwnerAsEmojiPicker) {
                    return 'bottom';
                }
                return this._super();
            },
        },
    },
});