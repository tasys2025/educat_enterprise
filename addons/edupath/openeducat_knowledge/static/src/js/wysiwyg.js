/** @odoo-module **/

import { qweb as QWeb } from 'web.core';
import Wysiwyg from 'web_editor.wysiwyg';
import { KnowledgeArticleLinkModal } from './wysiwyg/knowledge_article_link.js';
import { preserveCursor } from '@web_editor/js/editor/odoo-editor/src/OdooEditor';
import { ComponentWrapper } from 'web.OwlCompatibility';
import { ViewDialogWrapper } from '@openeducat_knowledge/components/view_dialog/view_dialog';
import { _t } from "@web/core/l10n/translation";
import { Markup } from 'web.utils';


Wysiwyg.include({
    /**
     * @override
     */
    init: function (parent, options) {
        if (options.knowledgeCommands) {
            /**
             * knowledge_commands is a view option from a field_html that
             * indicates that knowledge-specific commands should be loaded.
             * powerboxFilters is an array of functions used to filter commands
             * displayed in the powerbox.
             */
            options.powerboxFilters = options.powerboxFilters ? options.powerboxFilters : [];
            options.powerboxFilters.push(this._filterKnowledgeCommandGroupInTemplate);
        }
        this._super.apply(this, arguments);
    },
    /**
     * Prevent usage of commands from the group "Knowledge" inside the block
     * inserted by the /template Knowledge command. The content of a /template
     * block is destined to be used in @see odooEditor in modules other than
     * Knowledge, where knowledge-specific commands may not be available.
     * i.e.: prevent usage /template in a /template block
     *
     * @param {Array} commands commands available in this wysiwyg
     * @returns {Array} commands which can be used after the filter was applied
     */
    _filterKnowledgeCommandGroupInTemplate: function (commands) {
        let anchor = document.getSelection().anchorNode;
        if (anchor.nodeType !== Node.ELEMENT_NODE) {
            anchor = anchor.parentElement;
        }
        if (anchor && anchor.closest('.o_knowledge_template')) {
            commands = commands.filter(command => command.groupName != 'Knowledge');
        }
        return commands;
    },
    /**
     * @override
     * @returns {Array[Object]}
     */
    _getPowerboxOptions: function () {
        const options = this._super();
        const {commands, categories} = options;
        commands.push({
            groupName: 'Medias',
            name: 'Article',
            description: 'Link an article.',
            fontawesome: 'fa-file',
            callback: () => {
                this._insertArticleLink();
            },
        });
        if (this.options.knowledgeCommands) {
            categories.push({ name: 'Knowledge', priority: 10 });
            commands.push({
                category: 'Knowledge',
                name: 'File',
                description: 'Embed a file.',
                fontawesome: 'fa-file',
                callback: () => {
                    this.openMediaDialog({
                        noVideos: true,
                        noImages: true,
                        noIcons: true,
                        noDocuments: true,
                        knowledgeDocuments: true,
                    });
                }
            }, {
                category: 'Knowledge',
                name: _t('Item Kanban'),
                priority: 40,
                description: _t('Insert a Kanban view of article items'),
                fontawesome: 'fa-th-large',
                callback: () => {
                    const restoreSelection = preserveCursor(this.odooEditor.document);
                    const viewType = 'kanban';
                    this.openViewDialog(viewType, name => {
                        restoreSelection();
                        this.insertInternalView('openeducat_knowledge.knowledge_article_internal_action', viewType, name, {
                            active_id: this.options.recordInfo.res_id,
                            default_parent_id: this.options.recordInfo.res_id,
                            default_icon: '📄',
                            default_is_article_item: true,
                        });
                    });
                }
            }, {
                category: 'Knowledge',
                name: _t('Item List'),
                priority: 50,
                description: _t('Insert a List view of article items'),
                fontawesome: 'fa-th-list',
                callback: () => {
                    const restoreSelection = preserveCursor(this.odooEditor.document);
                    const viewType = 'list';
                    this.openViewDialog(viewType, name => {
                        restoreSelection();
                        this.insertInternalView('openeducat_knowledge.knowledge_article_internal_action', viewType, name, {
                            active_id: this.options.recordInfo.res_id,
                            default_parent_id: this.options.recordInfo.res_id,
                            default_icon: '📄',
                            default_is_article_item: true,
                        });
                    });
                }
            }, {
                category: _t('Knowledge'),
                name: _t('Table Of Content'),
                priority: 30,
                description: _t('Add a table of content.'),
                fontawesome: 'fa-bookmark',
                callback: () => {
                    this._insertTableOfContent();
                },
            }, {
                category: 'Knowledge',
                name: "Template",
                description: "Add a template section.",
                fontawesome: 'fa-pencil-square',
                callback: () => {
                    this._insertTemplate();
                },
            }, {
                category: _t('Knowledge'),
                name: _t('Index'),
                priority: 60,
                description: _t('Show the first level of nested articles.'),
                fontawesome: 'fa-list',
                callback: () => {
                    this.insertArticle(true);
                }
            }, {
                category: _t('Knowledge'),
                name: _t('Article Structure'),
                priority: 60,
                description: _t('Show all nested articles.'),
                fontawesome: 'fa-list',
                callback: () => {
                    this.insertArticle(false);
                }
            });
        }
        return {...options, commands, categories};
    },

    _insertTableOfContent: function () {
        const tableOfContentBlock = $(QWeb.render('openeducat_knowledge.abstract_behavior', {
            behaviorType: "o_knowledge_behavior_type_toc",
        }))[0];
        const [container] = this.odooEditor.execCommand('insert', tableOfContentBlock);
        this._notifyNewBehaviors(container);
    },

    openViewDialog(viewType, save) {
        const dialog = new ComponentWrapper(this, ViewDialogWrapper, {
            isNew: true,
            viewType: viewType,
            save: save
        });
        dialog.mount(document.body);
    },
    
    insertInternalView: async function (actWindowId, viewType, name, context={}) {
        const restoreSelection = preserveCursor(this.odooEditor.document);
        restoreSelection();
        context.knowledge_embedded_view_framework = 'owl';
        const embeddedViewBlock = $(await this._rpc({
            model: 'knowledge.article',
            method: 'add_internal_view_component',
            args: [[this.options.recordInfo.res_id], actWindowId, viewType, name, context],
        }))[0];
        const [container] = this.odooEditor.execCommand('insert', embeddedViewBlock);
        this._notifyNewBehaviors(container);
    },

    insertArticle: function(child){
        const articlesStructureBlock = $(QWeb.render('openeducat_knowledge.articles_structure_wrapper', {
            childrenOnly: child
        }))[0];
        const [container] = this.odooEditor.execCommand('insert', articlesStructureBlock);
        this._notifyNewBehaviors(container);
    },
    /**
     * Notify @see FieldHtmlInjector that toolbars need to be injected
     * @see KnowledgeToolbar
     *
     * @param {Element} container
     */
    _notifyNewToolbars(container) {
        const toolbarsData = [];
        container.querySelectorAll('.o_knowledge_toolbar_anchor').forEach(function (container, anchor) {
            const type = Array.from(anchor.classList).find(className => className.startsWith('o_knowledge_toolbar_type_'));
            if (type) {
                toolbarsData.push({
                    container: container,
                    anchor: anchor,
                    type: type,
                });
            }
        }.bind(this, container));
        this.$editable.trigger('refresh_toolbars', { toolbarsData: toolbarsData });
    },
    /**
     * Notify @see FieldHtmlInjector that behaviors need to be injected
     * @see KnowledgeBehavior
     *
     * @param {Element} anchor
     */
    _notifyNewBehaviors(anchor, props=null) {
        const behaviorsData = [];
        const type = Array.from(anchor.classList).find(className => className.startsWith('o_knowledge_behavior_type_'));
        if (type) {
            behaviorsData.push({
                anchor: anchor,
                behaviorType: type,
                setCursor: true,
                props: props || {},
            });
        }
        this.$editable.trigger('refresh_behaviors', { behaviorsData: behaviorsData});
    },
    /**
     * Insert a /template block
     */
    _insertTemplate() {
        const templateBlock = $(QWeb.render('openeducat_knowledge.abstract_behavior', {
            behaviorType: "o_knowledge_behavior_type_template",
        }))[0];
        const [container] = this.odooEditor.execCommand('insert', templateBlock);
        this._notifyNewBehaviors(container);
    },
    /**
     * Insert a /article block (through a dialog)
     */
    _insertArticleLink: function () {
        const restoreSelection = preserveCursor(this.odooEditor.document);
        const dialog = new KnowledgeArticleLinkModal(this, {});
        dialog.on('save', this, article => {
            restoreSelection();
            if (article) {
                const articleLinkFragment = new DocumentFragment();
                const articleLinkBlock = $(QWeb.render('openeducat_knowledge.wysiwyg_article_link', {
                    display_name: article.display_name,
                    href: '/knowledge/article/' + article.id,
                    article_id: article.id,
                }))[0];
                articleLinkFragment.append(articleLinkBlock);
                const [anchor] = this.odooEditor.execCommand('insertFragment', articleLinkFragment);
                this._notifyNewBehaviors(anchor);
            }
            dialog.close();
        });
        dialog.on('closed', this, () => {
            restoreSelection();
        });
        dialog.open();
    },
    /**
     * Notify the @see FieldHtmlInjector when a /file block is inserted from a
     * @see MediaDialog
     *
     * @private
     * @override
     */
    _onMediaDialogSave(params, element) {
        if (element.classList.contains('o_is_knowledge_file')) {
            params.restoreSelection();
            element.classList.remove('o_is_knowledge_file');
            element.classList.add('o_image');
            const extension = (element.title && element.title.split('.').pop()) || element.dataset.mimetype;
            const fileBlock = $(QWeb.render('openeducat_knowledge.wysiwyg_file_behave', {
                behaviorType: "o_knowledge_behavior_type_file",
                fileName: element.title,
                fileImage: Markup(element.outerHTML),
                behaviorProps: encodeURIComponent(JSON.stringify({
                    fileName: element.title,
                    fileExtension: extension,
                })),
                fileExtension: extension,
            }))[0];
            const [container] = this.odooEditor.execCommand('insert', fileBlock);
            this._notifyNewBehaviors(container);
        } else {
            return this._super(...arguments);
        }
    },
});
