/** @odoo-module */
'use strict';

import publicWidget from 'web.public.widget';
import KnowledgeTreePanelMixin from './tools/tree_panel_mixin.js'
import { qweb as QWeb } from 'web.core';
import session from 'web.session';
const HEADINGS = [
    'H1',
    'H2',
    'H3',
    'H4',
    'H5',
    'H6',
];

const fetchValidHeadings = (element) => {
    const templateHeadings = Array.from(element.querySelectorAll(
        HEADINGS.map((heading) => `.o_knowledge_behavior_type_template ${heading}`).join(',')
    ));

    return Array.from(element.querySelectorAll(HEADINGS.join(',')))
        .filter((heading) => heading.innerText.trim().replaceAll('\u200B', '').length > 0)
        .filter((heading) => !templateHeadings.includes(heading));
};

publicWidget.registry.KnowledgeWidget = publicWidget.Widget.extend(KnowledgeTreePanelMixin, {
    selector: '.o_knowledge_form_view',
    events: {
        'keyup .knowledge_search_bar': '_searchArticles',
        'click .o_article_caret': '_onFold',
        'click .o_favorites_toggle_button': '_toggleFavorite',
        'click .o_knowledge_toc_link': '_onTableLinkClick',
    },

    /**
     * @override
     * @returns {Promise}
     */
    start: function () {
        return this._super.apply(this, arguments).then(() => {
            const id = this.$el.data('article-id');
            this._renderTree(id, '/knowledge/tree_panel/portal');

            const $placeholder = $(QWeb.render('openeducat_knowledge.embedded_view_placeholder', {
                url: `/knowledge/article/${this.$id}`,
                isLoggedIn: session.user_id !== false
            }));
            const $container = $('.o_knowledge_behavior_type_internal_view');
            $container.empty();
            $container.append($placeholder);
        });
    },

    /**
     * @param {Event} event
     */
    _searchArticles: function (event) {
        const $input = $(event.currentTarget);
        const $tree = $('.o_tree');
        const keyword = $input.val().toLowerCase();
        this._traverse($tree, $li => {
            if ($li.text().toLowerCase().includes(keyword)) {
                $li.removeClass('d-none');
                return true;
            } else {
                $li.addClass('d-none');
                return false;
            }
        });
    },

    _onTableLinkClick: function(e){
        e.preventDefault();
        const headingIndex = parseInt(event.target.getAttribute('data-oe-nodeid'));
        const targetHeading = fetchValidHeadings(this.$el[0])[headingIndex];
        if (targetHeading) {
            targetHeading.scrollIntoView({
                behavior: 'smooth',
            });
            targetHeading.classList.add('o_knowledge_header_highlight');
            setTimeout(() => {
                targetHeading.classList.remove('o_knowledge_header_highlight');
            }, 2000);
        }
    },

    _renderTree: async function (active_article_id, route) {
        const container = this.el.querySelector('.o_knowledge_tree');
        let unfoldedArticlesIds = localStorage.getItem('knowledge.unfolded.ids');
        unfoldedArticlesIds = unfoldedArticlesIds ? unfoldedArticlesIds.split(";").map(Number) : [];
        let unfoldedFavoriteArticlesIds = localStorage.getItem('knowledge.unfolded.favorite.ids');
        unfoldedFavoriteArticlesIds = unfoldedFavoriteArticlesIds ? unfoldedFavoriteArticlesIds.split(";").map(Number) : [];
        const params = new URLSearchParams(document.location.search);
        if (Boolean(params.get('auto_unfold'))) {
            unfoldedArticlesIds.push(active_article_id);
            unfoldedFavoriteArticlesIds.push(active_article_id);
        }
        try {
            const htmlTree = await this._rpc({
                route: route,
                params: {
                    active_article_id: active_article_id,
                    unfolded_articles_ids: unfoldedArticlesIds,
                    unfolded_favorite_articles_ids: unfoldedFavoriteArticlesIds
                }
            });
            container.innerHTML = htmlTree;
            this._setTreeFavoriteListener();
        } catch {
            container.innerHTML = "";
        }
    },

    /**
     * Helper function to traverse the dom hierarchy of the aside tree menu.
     * The function will call the given callback function with the article item
     * beeing visited (i.e: a JQuery dom element). The provided callback function
     * should return a boolean indicating whether the algorithm should explore
     * the children of the current article item.
     * @param {jQuery} $tree
     * @param {Function} callback
     */
    _traverse: function ($tree, callback) {
        const stack = $tree.children('li').toArray();
        while (stack.length > 0) {
            const $li = $(stack.shift());
            if (callback($li)) {
                const $ul = $li.children('ul');
                stack.unshift(...$ul.children('li').toArray());
            }
        }
    },

    /**
     * @param {Event} event
     */
    _toggleFavorite: function (event) {
        const $star = $(event.currentTarget);
        const id = $star.data('article-id');
        return this._rpc({
            model: 'knowledge.article',
            method: 'action_toggle_favorite',
            args: [[id]]
        }).then(result => {
            $star.find('i').toggleClass('fa-star', result).toggleClass('fa-star-o', !result);
            // Add/Remove the article to/from the favorite in the sidebar
            return this._rpc({
                route: '/knowledge/tree_panel/favorites',
                params: {
                    active_article_id: id,
                }
            }).then(template => {
                this.$('.o_favorite_container').replaceWith(template);
                this._setTreeFavoriteListener();
            });
        });
    },
});
