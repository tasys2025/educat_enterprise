/** @odoo-module */

import FieldHtml from 'web_editor.field.html';
import {FieldHtmlInjector} from './knowledge_field_html_injector';
import {KnowledgePlugin} from './knowledge_plugin';
import { BehaviourView } from '@openeducat_knowledge/components/behaviour/behaviour_view';
import { HtmlField } from "@web_editor/js/backend/html_field";
const { App, useEnv, Component } = owl;
import { templates } from "@web/core/assets";
import { useService } from "@web/core/utils/hooks";
import { patch } from "@web/core/utils/patch";

const behaviorTypes = {
    o_knowledge_behavior_type_internal_view: {
        Behavior: BehaviourView
    }
};

patch(HtmlField.prototype, 'knowledge_html_field_action',{
    setup() {
        this._super(...arguments);
        this.actionService = useService('action');
    }
});

FieldHtml.include({
    start: function () {
        return this._super(...arguments).then(() => {
            this.behaviorTypes = behaviorTypes;
            this.behaviorAnchors = new Set();
            this.injectorElement = this._getInjectorElement();
            setTimeout(() => {
                this._setViews();
            }, 1000);
        });
    },

    _getDefaultViews: function(viewData, target){
        const anchors = new Set();
        const types = new Set(Object.getOwnPropertyNames(this.behaviorTypes));
        const anchorNodes = target.querySelectorAll('.o_knowledge_behavior_anchor');
        const anchorNodesSet = new Set(anchorNodes);
        for (const anchorNode of anchorNodes) {
            const anchorSubNodes = anchorNode.querySelectorAll('.o_knowledge_behavior_anchor');
            for (const anchorSubNode of anchorSubNodes) {
                anchorNodesSet.delete(anchorSubNode);
            }
        }
        for (const anchor of Array.from(anchorNodesSet)) {
            const type = Array.from(anchor.classList).find(className => types.has(className));
            if (type) {
                viewData.push({
                    anchor: anchor,
                    behaviorType: type,
                });
                anchors.add(anchor);
            }
        }
        const differenceAnchors = new Set([...this.behaviorAnchors].filter(anchor => !anchors.has(anchor)));
        differenceAnchors.forEach(anchor => {
            if (anchor.oKnowledgeBehavior) {
                anchor.oKnowledgeBehavior.destroy();
                delete anchor.oKnowledgeBehavior;
            }
        });
    },

    _setViews: async function(viewData=[], target=null){
        const injectorElement = target || this.injectorElement;
        if (!injectorElement) {
            return;
        }
        if (!viewData.length) {
            this._getDefaultViews(viewData, injectorElement);
        }
        for (const view of viewData) {
            const anchor = view.anchor;
//            if (!document.body.contains(anchor)) {
//                return;
//            }
            const {Behavior} = this.behaviorTypes[view.behaviorType] || {};
            if (!Behavior) {
                return;
            }
            if (!anchor.oKnowledgeBehavior) {
                if (!this.mode == 'readonly' && this.wysiwyg && this.wysiwyg.odooEditor) {
                    this.wysiwyg.odooEditor.observerUnactive('injectBehavior');
                }
                const props = {
                    readonly: this.mode == 'readonly',
                    anchor: anchor,
                    wysiwyg: this.wysiwyg,
                    ...view.props,
                    record: this.record
                };
                let behaviorProps = {};
                if (anchor.hasAttribute("data-behavior-props")) {
                    try {
                        behaviorProps = JSON.parse(anchor.dataset.behaviorProps);
                    } catch {}
                }
                for (const prop in behaviorProps) {
                    if (prop in Behavior.props) {
                        props[prop] = behaviorProps[prop];
                    }
                }
                const propNodes = anchor.querySelectorAll("[data-prop-name]");
                for (const node of propNodes) {
                    if (node.dataset.propName in Behavior.props) {
                        props[node.dataset.propName] = node.innerHTML;
                    }
                }
                anchor.replaceChildren();
                const env = Component.env;
//                const config = (({env, dev, translatableAttributes, translateFn}) => {
//                    return {env, dev, translatableAttributes, translateFn};
//                })(this.__owl__.app);
                anchor.oKnowledgeBehavior = new App(Behavior, {
                    env: HtmlField.env,
                    dev: env.isDebug(),
                    templates: templates,
                    translatableAttributes: ["data-tooltip"],
                    translateFn: env._t,
                    props,
                });
                await anchor.oKnowledgeBehavior.mount(anchor);
                if (!this.mode == 'readonly' && this.wysiwyg && this.wysiwyg.odooEditor) {
                    this.wysiwyg.odooEditor.idSet(anchor);
                    this.wysiwyg.odooEditor.observerActive('injectBehavior');
                    if (view.setCursor && anchor.oKnowledgeBehavior.root.component.setCursor) {
                        anchor.oKnowledgeBehavior.root.component.setCursor();
                    }
                    this.wysiwyg.odooEditor.historyStep();
                }
                await this._setViews([], anchor);
            }
        }
    },

    _getInjectorElement: function(){
        if(this.mode == 'readonly' && this.$el){
            return this.$el[0];
        } else if(this.wysiwyg && this.wysiwyg.$editable){
            return this.wysiwyg.$editable[0];
        }
        return null;
    },
    /**
     * @override
     */
    _renderReadonly: function () {
        const prom = this._super.apply(this, arguments);
        if (prom) {
            return prom.then(function () {
                return this._addFieldHtmlInjector();
            }.bind(this));
        } else {
            return this._addFieldHtmlInjector();
        }
    },
    /**
     * Add a @see FieldHtmlInjector to this field_html, which will add temporary
     * @see KnowledgeBehavior and/or @see KnowledgeToolbar to @see odooEditor
     * blocks. Delegate control of the editor to the injector, mainly to access
     * history methods. i.e.: @see KnowledgeToolbar insertion in the dom should
     * not be registered in the user history
     *
     * @returns {Promise}
     */
    _addFieldHtmlInjector: function () {
        if (this.$content && this.$content.length) {
            const editor = (this.mode === 'edit' && this.wysiwyg) ? this.wysiwyg.odooEditor : null;
            const fieldHtmlInjector = new FieldHtmlInjector(this, this.mode, this.$content[0], editor);
            return fieldHtmlInjector.appendTo(this.el);
        }
    },
    /**
     * Some events can cause the insertion of new elements which may need a
     * @see KnowledgeBehavior and/or @see KnowledgeToolbar , so the
     * @see FieldHtmlInjector needs to be notified.
     *
     * @override
     */
    _onLoadWysiwyg: function () {
        this._super.apply(this, arguments);
        this._addFieldHtmlInjector();
        /**
         * setTimeout is called so that the refresh_injector event is dispatched
         * at the start of the next event cycle. This is to ensure that other
         * handlers for those events have execution priority over the refresh.
         * In particular, the content to drop in the editor would be inserted
         * after the refreshInjector without setTimeout.
         */
        const refreshInjector = () => setTimeout(this.refreshInjector.bind(this));
        this.wysiwyg.odooEditor.addEventListener('historyUndo', refreshInjector);
        this.wysiwyg.odooEditor.addEventListener('historyRedo', refreshInjector);
        this.$content[0].addEventListener('paste', refreshInjector);
        this.$content[0].addEventListener('drop', refreshInjector);
    },
    /**
     * Add a plugin (@see KnowledgePlugin ) for the @see odooEditor which
     * implements @see cleanForSave in order to remove @see KnowledgeToolbar
     * before saving a record.
     *
     * @override
     */
    _getWysiwygOptions: function () {
        const options = this._super.apply(this, arguments);
        if (Array.isArray(options.editorPlugins)) {
            options.editorPlugins.push(KnowledgePlugin);
        } else {
            options.editorPlugins = [KnowledgePlugin];
        }
        return options;
    },
    /**
     * Notify the @see FieldHtmlInjector to render and apply KnowledgeToolbar
     * and KnowledgeBehavior where it is needed.
     */
    refreshInjector: function () {
        if (this.$content) {
            this.$content.trigger('refresh_injector');
        }
    },
});
