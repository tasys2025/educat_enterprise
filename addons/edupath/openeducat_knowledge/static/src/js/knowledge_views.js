/** @odoo-module */

import { formView } from '@web/views/form/form_view';
import viewRegistry from 'web.view_registry';
import { FormController } from '@web/views/form/form_controller';
import { registry } from "@web/core/registry";
import { KnowledgeArticleFormRenderer } from './knowledge_renderer_component.js';
export class KnowledgeArticleFormController extends FormController {}
import { FormCompiler } from '@web/views/form/form_compiler';
import { createElement } from "@web/core/utils/xml";

class KnowledgeCompiler extends FormCompiler{
    setup() {
        super.setup();
        this.compilers.push({ selector: "PermissionPanel", fn: this.compilePermissionPanel });
    }
    compilePermissionPanel(el) {
        const compiled = createElement(el.nodeName);
        for (const attr of el.attributes) {
            compiled.setAttribute(attr.name, attr.value);
        }
        return compiled;
    }

    validateNode() {}
}

KnowledgeArticleFormController.defaultProps = {
    ...FormController.defaultProps,
    mode: 'edit',
};

export const knowledgeArticleFormView = {
    ...formView,
    Controller: KnowledgeArticleFormController,
    Compiler: KnowledgeCompiler,
    Renderer: KnowledgeArticleFormRenderer,
    display: {controlPanel: false}
};

registry.category('views').add('knowledge_article_view_form', knowledgeArticleFormView);

//import { KnowledgeArticleFormController } from './knowledge_controller.js';
//import { KnowledgeArticleFormRenderer } from './knowledge_renderers.js';
//
//const KnowledgeArticleFormView = FormView.extend({
//    config: _.extend({}, FormView.prototype.config, {
//        Controller: KnowledgeArticleFormController,
//        Renderer: KnowledgeArticleFormRenderer,
//    }),
//    /**
//     * For the knowledge module, we want to display a headless layout.
//     * To get a headless layout, the `init` function will deactivate the
//     * control panel as well as the search bar and the default breadcrumb.
//     * @override
//     * @param {Object} viewInfo
//     * @param {Object} params
//     */
//    init: function (viewInfo, params) {
//        params.withBreadcrumbs = false;
//        params.withControlPanel = false;
//        params.withSearchBar = false;
//        this._super.apply(this, arguments);
//        this.rendererParams.breadcrumbs = params.breadcrumbs;
//    },
//});
//
//viewRegistry.add('knowledge_article_view_form', KnowledgeArticleFormView);
//
//export {
//    KnowledgeArticleFormView,
//};
