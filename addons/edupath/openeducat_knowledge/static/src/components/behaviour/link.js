/** @odoo-module */

import { makeContext } from "@web/core/context";
import { useService } from "@web/core/utils/hooks";
const {
    Component,
    onMounted,
    onPatched,
    onWillPatch,
    onWillStart,
    xml
} = owl;

let observerId = 0;
const { useEffect } = owl;


export class Link extends Component {
    setup () {
        super.setup();
        this.observerId = observerId++;
        if (!this.props.readonly) {
            this.props.anchor.setAttribute('contenteditable', 'false');
            onWillStart(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onWillPatch(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onMounted(() => {
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
            onPatched(() => {
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
        }
        this.actionService = useService('action');
        useEffect(() => {
            const type = this.props.readonly ? 'click' : 'dblclick';
            const onLinkClick = event => {
                this.openViewLink(event);
            };
            this.props.anchor.addEventListener(type, onLinkClick);
            return () => {
                this.props.anchor.removeEventListener(type, onLinkClick);
            };
        });
    }

    async openViewLink (event) {
        const action = await this.actionService.loadAction(
            this.props.act_window,
            makeContext([this.props.context])
        );
        if (action.type !== "ir.actions.act_window") {
            throw new Error('Can not open the view: The action is not an "ir.actions.act_window"');
        }
        action.globalState = {
            searchModel: this.props.context.knowledge_search_model_state
        };
        const props = {};
        if (action.context.orderBy) {
            try {
                props.orderBy = JSON.parse(action.context.orderBy);
            } catch {};
        }
        this.actionService.doAction(action, {
            viewType: this.props.view_type,
            props
        });
    }

    get editor () {
        return this.props.wysiwyg ? this.props.wysiwyg.odooEditor : undefined;
    }
}

Link.template = xml`<t t-esc="props.name"/>`;
Link.components = {};
Link.props = {
    readonly: { type: Boolean },
    anchor: { type: Element },
    wysiwyg: { type: Object, optional: true},
    record: { type: Object },
    root: { type: Element },
    act_window: { type: Object },
    context: { type: Object },
    name: { type: String },
    view_type: { type: String }
};
