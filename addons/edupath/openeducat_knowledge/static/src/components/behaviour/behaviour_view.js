/** @odoo-module */

const { Component, useState, onMounted, onError } = owl;
import { useService } from "@web/core/utils/hooks";
let observerId = 0;
import { makeContext } from "@web/core/context";
import { Markup } from "web.utils";
import { EmbeddedViewManager } from './manager';


export function setIntersectionObserver (element, callback) {
    const options = {
        root: null,
        rootMargin: '0px'
    };
    const observer = new window.IntersectionObserver(entries => {
        const entry = entries[0];
        if (entry.isIntersecting) {
            observer.unobserve(entry.target);
            callback();
        }
    }, options);
    observer.observe(element);
    return observer;
}

export class BehaviourView extends Component{
    setup() {
        super.setup();
        this.actionService = useService('action');
        this.state = useState({
            waiting: true,
            error: false
        });
        if (!this.props.readonly) {
            this.props.anchor.setAttribute('contenteditable', 'false');
        }
        this.observerId = observerId++;

        onMounted(() => {
            const { anchor } = this.props;
            this.observer = setIntersectionObserver(anchor, async () => {
                await this.loadData();
                this.state.waiting = false;
            });
            anchor.addEventListener('keydown', this.stopPreventEvent);
            anchor.addEventListener('keyup', this.stopPreventEvent);
            anchor.addEventListener('input', this.stopPreventEvent);
            anchor.addEventListener('beforeinput', this.stopPreventEvent);
            anchor.addEventListener('paste', this.stopPreventEvent);
            anchor.addEventListener('drop', this.stopPreventEvent);
        });

        onError(error => {
            console.error(error);
            this.state.error = true;
        });
    }

    stopPreventEvent(e) {
        e.stopPropagation();
    }

    async loadData () {
        const context = makeContext([this.props.context]);
        const action = await this.actionService.loadAction(this.props.act_window, context);
        if (action.type !== "ir.actions.act_window") {
            this.state.error = true;
            return;
        }
        if (action.help) {
            action.help = Markup(action.help);
        }
        Object.assign(this.props, {
            act_window: action,
            context: context
        });
    }

    onEmbeddedViewLoadStart () {
        if (!this.props.readonly) {
            const editor = this.props.wysiwyg.odooEditor;
            editor.observerUnactive(`knowledge_embedded_view_id_${this.observerId}`);
        }
    }

    onEmbeddedViewLoadEnd () {
        if (!this.props.readonly) {
            const editor = this.props.wysiwyg.odooEditor;
            editor.idSet(this.props.anchor);
            editor.observerActive(`knowledge_embedded_view_id_${this.observerId}`);
        }
    }

    setTitle (name) {
        const data = JSON.parse(this.props.anchor.getAttribute('data-behavior-props'));
        data.act_window.name = name;
        data.act_window.display_name = name;
        this.props.anchor.setAttribute('data-behavior-props', JSON.stringify(data));
        Object.assign(this.props.act_window, {
            name: name,
            display_name: name
        });
        const title = this.props.anchor.querySelector('.o_control_panel .breadcrumb-item.active');
        if (title) {
            title.textContent = name;
        }
    }

    getTitle () {
        if (this.props.act_window) {
            return this.props.act_window.display_name || this.props.act_window.name;
        }
        return '';
    }
}

BehaviourView.template = 'openeducat_knowledge.InternalViewBehavior';
BehaviourView.components = {
    EmbeddedViewManager,
};
BehaviourView.props = {
    readonly: { type: Boolean },
    anchor: { type: Element },
    wysiwyg: { type: Object, optional: true},
    root: { type: Element },
    record: { type: Object },
    act_window: { type: Object },
    context: { type: Object },
    view_type: { type: String }
}
