/** @odoo-module */

import { useService } from "@web/core/utils/hooks";
import { qweb as QWeb }  from "web.core";

const {
    Component,
    markup,
    useEffect,
    useState,
    onMounted,
    onPatched,
    onWillPatch,
    onWillStart,
 } = owl;

export class Article extends Component {
    setup () {
        super.setup();
        this.rpc = useService('rpc');
        this.actionService = useService('action');
        if (this.props.content) {
            this.state = useState({
                loading: false,
                refreshing: false,
            });
        } else {
            this.state = useState({
                loading: true,
                refreshing: false,
            });
            onMounted(async () => {
                this.props.content = await this._renderArticlesStructure();
                this.state.loading = false;
            });
        }

        useEffect(() => {
            const onClick = this._onArticleLinkClick.bind(this);
            const links = this.props.anchor.querySelectorAll('.o_knowledge_article_structure_link');
            links.forEach(link => link.addEventListener('click', onClick));
            return () => {
                links.forEach(link => link.removeEventListener('click', onClick));
            };
        });

        useEffect(() => {
            const onDrop = event => {
                event.preventDefault();
                event.stopPropagation();
            };
            this.props.anchor.addEventListener('drop', onDrop);
            return () => {
                this.props.anchor.removeEventListener('drop', onDrop);
            };
        });
        if (!this.props.readonly) {
            this.props.anchor.setAttribute('contenteditable', 'false');
            onWillStart(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onWillPatch(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onMounted(() => {
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
            onPatched(() => {
                this.props.record.save();
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
        }

    }

    get editor () {
        return this.props.wysiwyg ? this.props.wysiwyg.odooEditor : undefined;
    }

    async _renderArticlesStructure () {
        const articleId = this.props.record.data.id;
        const allArticles = await this._fetchAllArticles(articleId);
        return markup(QWeb.render('openeducat_knowledge.articles_structure', {
            'articles': this._buildArticlesStructure(articleId, allArticles)
        }));
    }

    async _fetchAllArticles (articleId) {
        const selector = 'o_knowledge_articles_structure_children_only';
        const domain = [
            ['parent_id', this.props.anchor.classList.contains(selector) ? '=' : 'child_of', articleId],
            ['is_article_item', '=', false]
        ];
        const { records } = await this.rpc('/web/dataset/search_read', {
            model: 'knowledge.article',
            fields: ['id', 'display_name', 'parent_id'],
            domain,
            sort: 'sequence',
        });
        return records;
    }

    _buildArticlesStructure (parentId, allArticles) {
        const articles = [];
        for (const article of allArticles) {
            if (article.parent_id && article.parent_id[0] === parentId) {
                articles.push({
                    id: article.id,
                    name: article.display_name,
                    child_ids: this._buildArticlesStructure(article.id, allArticles),
                });
            }
        }
        return articles;
    }


    async _onArticleLinkClick (event) {
        event.preventDefault();
        this.actionService.doAction('openeducat_knowledge.ir_actions_server_knowledge_home_page', {
            additionalContext: {
                res_id: parseInt(event.target.getAttribute('data-oe-nodeid'))
            }
        });
    }

    async _onRefreshBtnClick (event) {
        event.stopPropagation();
        this.state.refreshing = true;
        this.props.content = await this._renderArticlesStructure();
        this.state.refreshing = false;
    }
}

Article.template = "openeducat_knowledge.ArticlesStructureBehavior";
Article.props = {
    readonly: { type: Boolean },
    anchor: { type: Element },
    wysiwyg: { type: Object, optional: true},
    record: { type: Object },
    root: { type: Element },
    content: { type: Object, optional: true },
};
