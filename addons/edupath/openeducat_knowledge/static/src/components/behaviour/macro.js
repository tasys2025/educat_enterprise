/** @odoo-module */

import { _t } from "web.core";
import { AlertDialog } from "@web/core/confirmation_dialog/confirmation_dialog";
import { isVisible } from "@web/core/utils/ui";
import { MacroEngine } from "@web/core/macro";

class KnowledgeMacroPageChangeError extends Error {}
export function pasteElements(dataTransfer, target) {
    const fakePaste = new Event('paste', {
        bubbles: true,
        cancelable: true,
        composed: true,
    });
    fakePaste.clipboardData = dataTransfer;

    const sel = document.getSelection();
    sel.removeAllRanges();
    const range = document.createRange();
    const lastChild = target.lastChild;
    if (!lastChild) {
        range.setStart(target, 0);
        range.setEnd(target, 0);
    } else {
        const subLastChild = lastChild.lastChild;
        if (subLastChild) {
            if (subLastChild.nodeType === Node.ELEMENT_NODE && subLastChild.tagName === 'BR') {
                range.setStartBefore(subLastChild);
                range.setEndBefore(subLastChild);
            } else {
                range.setStartAfter(subLastChild);
                range.setEndAfter(subLastChild);
            }
        } else {
            range.setStartAfter(lastChild);
            range.setEndAfter(lastChild);
        }
    }
    const lastElementChild = target.lastElementChild;
    if (lastElementChild) {
        lastElementChild.scrollIntoView();
    } else {
        target.scrollIntoView();
    }
    sel.addRange(range);
    target.dispatchEvent(fakePaste);
}

export class Macro {
    constructor ({
        targetXmlDoc,
        breadcrumbs,
        data,
        services,
        interval = 16,
    }) {
        this.targetXmlDoc = targetXmlDoc;
        this.breadcrumbsIndex = breadcrumbs.length - 1;
        this.breadcrumbsName = breadcrumbs[this.breadcrumbsIndex].name;
        this.breadcrumbsSelector = ['.breadcrumb > .breadcrumb-item.active', (el) => el.textContent.includes(this.breadcrumbsName)];
        this.interval = interval;
        this.data = data;
        this.engine = new MacroEngine();
        this.services = services;
        this.blockUI = { action: function () {
            if (!this.services.ui.isBlocked) {
                this.services.ui.block();
            }
        }.bind(this) };
        this.unblockUI = { action: function () {
            if (this.services.ui.isBlocked) {
                this.services.ui.unblock();
            }
        }.bind(this) };
        this.onError = this.onError.bind(this);
    }

    start() {
        const macroAction = this.macroAction();
        if (!macroAction || !macroAction.steps || !macroAction.steps.length) {
            return;
        }
        const startMacro = {
            name: "restore_record",
            interval: this.interval,
            onError: this.onError,
            steps: [
                this.blockUI, {
                trigger: function () {
                    const breadcrumbs = document.querySelectorAll(`.breadcrumb-item:not(.active)`);
                    if (breadcrumbs.length > this.breadcrumbsIndex) {
                        const breadcrumb = breadcrumbs[this.breadcrumbsIndex];
                        if (breadcrumb.textContent.includes(this.breadcrumbsName)) {
                            return this.getFirstVisibleElement(breadcrumb.querySelector('a'));
                        }
                    }
                    return null;
                }.bind(this),
                action: 'click',
            }, {
                trigger: this.getFirstVisibleElement.bind(this, ...this.breadcrumbsSelector),
                action: this.engine.activate.bind(this.engine, macroAction),
            }],
        };
        this.engine.activate(macroAction);
    }

    onError(error, step, index) {
        this.unblockUI.action();
        if (error instanceof KnowledgeMacroPageChangeError) {
            this.services.dialog.add(AlertDialog,{
                body: _t('The operation was interrupted because the page or the record changed. Please try again later.'),
                title: _t('Error'),
                cancel: () => {},
                cancelLabel: _t('Close'),
            });
        } else {
            console.error(error);
        }
    }

    getFirstVisibleElement(selector, filter=false, reverse=false) {
        const elementsArray = typeof(selector) === 'string' ? Array.from(document.querySelectorAll(selector)) : [selector];
        const sel = filter ? elementsArray.filter(filter) : elementsArray;
        for (let i = 0; i < sel.length; i++) {
            i = reverse ? sel.length - 1 - i : i;
            if (isVisible(sel[i])) {
                return sel[i];
            }
        }
        return null;
    }
    validatePage() {
//        if (!this.getFirstVisibleElement(...this.breadcrumbsSelector)) {
//            throw new KnowledgeMacroPageChangeError();
//        }
    }

    searchInXmlDocNotebookTab(targetSelector) {
        const searchElement = this.targetXmlDoc.querySelector(targetSelector);
        const page = searchElement ? searchElement.closest('page') : undefined;
        const pageString = page ? page.getAttribute('string') : undefined;
        if (!pageString) {
            return;
        }
        const pageEl = this.getFirstVisibleElement('.o_notebook .nav-link:not(.active)', (el) => el.textContent.includes(pageString));

        if (pageEl) {
            pageEl.click();
        }
    }

    macroAction() {
        const action = {
            name: this.constructor.name,
            interval: this.interval,
            onError: this.onError,
            steps: [],
        };
        action.steps.push({
            trigger: function(){
                const el = this.getFirstVisibleElement('.btn-chatter');
                return el;
            }.bind(this),
            action: 'click'
        }, {
            trigger: function() {
                this.validatePage();
                const el = this.getFirstVisibleElement('.o_ChatterTopbar_buttonSendMessage');
                if (el) {
                    if (el.classList.contains('o-active')) {
                        return el;
                    } else {
                        el.click();
                    }
                } else {
                    this.searchInXmlDocNotebookTab('.oe_chatter');
                }
                return null;
            }.bind(this),
            action: () => {},
        }, {
            trigger: function() {
                this.validatePage();
                return this.getFirstVisibleElement('.o_Composer_buttonFullComposer');
            }.bind(this),
            action: 'click',
        }, {
            trigger: function () {
                this.validatePage();
                const dialog = this.getFirstVisibleElement('.o_dialog_container.modal-open .o_mail_composer_form');
                if (dialog) {
                    return this.getFirstVisibleElement(dialog.querySelector('.o_field_html[name="body"] > .odoo-editor-editable'));
                }
                return null;
            }.bind(this),
            action: pasteElements.bind(this, this.data.dataTransfer),
        }, this.unblockUI);
        return action;
    }
}

export class UseAsDescriptionMacro extends Macro {

    macroAction() {
        const action = {
            name: this.constructor.name,
            interval: this.interval,
            onError: this.onError,
            steps: [],
        };
        action.steps.push({
            trigger: function () {
                const readonly = this.getFirstVisibleElement('.o_form_readonly');
                if (readonly) {
                    const editButton = this.getFirstVisibleElement('.o_form_view .o_form_button_edit');
                    return editButton;
                }
                return this.getFirstVisibleElement('.o_form_editable');
            }.bind(this),
            action: (el) => {
                if (el.classList.contains('o_form_button_edit')) {
                    el.click();
                }
            },
        }, {
            trigger: function () {
                return this.getFirstVisibleElement('.o_form_editable');
            }.bind(this),
        }, {
            trigger: function () {
                this.validatePage();
                const el = this.getFirstVisibleElement(`.o_field_html[name="${this.data.fieldName}"]`);
                if (el) {
                    return el;
                }
                if (this.data.pageName) {
                    this.searchInXmlDocNotebookTab(`page[name="${this.data.pageName}"]`);
                }
                return null;
            }.bind(this),
            action: 'click',
        }, {
            trigger: function () {
                this.validatePage();
                return this.getFirstVisibleElement(`.o_field_html[name="${this.data.fieldName}"] > .odoo-editor-editable`);
            }.bind(this),
            action: function (el) {
                pasteElements(this.data.dataTransfer, el);
            }.bind(this),
        }, this.unblockUI);
        return action;
    }
}
