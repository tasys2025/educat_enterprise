/** @odoo-module */

import { _t } from "web.core";
import { browser } from "@web/core/browser/browser";
import { Macro, UseAsDescriptionMacro } from "./macro";
import { Tooltip } from "@web/core/tooltip/tooltip";
import { useService } from "@web/core/utils/hooks";
import { MacroEngine } from "@web/core/macro";

const {
    useEffect,
    useState,
    Component,
    useRef,
    onWillUnmount,
    onMounted,
    onPatched,
    onWillPatch,
    onWillStart,} = owl;

let observerId = 0;
export class Template extends Component {
    setup() {
        super.setup();
        this.dialogService = useService("dialog");
        this.popoverService = useService("popover");
        this.knowledgeCommandsService = useService('knowledgeCommandsService');
        this.uiService = useService("ui");
        this.copyToClipboardButton = useRef("copyToClipboardButton");
        this.templateContent = useRef("templateContent");
        this.observerId = observerId++;
        if (!this.props.readonly) {
            this.props.anchor.setAttribute('contenteditable', 'false');
            onWillStart(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onWillPatch(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onMounted(() => {
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
            onPatched(() => {
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
        }

        onMounted(() => {
            this.clipboard = new ClipboardJS(
                this.copyToClipboardButton.el,
                {target: () => this.templateContent.el}
            );
            this.clipboard.on('success', () => {
                window.getSelection().removeAllRanges();
                this.showTooltip();
            });
        });
        onWillUnmount(() => {
            if (this.clipboard) {
                this.clipboard.destroy();
            }
        });
        this.targetRecordInfo = this.knowledgeCommandsService.getCommandsRecordInfo();
    }
    get editor () {
        return this.props.wysiwyg ? this.props.wysiwyg.odooEditor : undefined;
    }

    showTooltip() {
        const closeTooltip = this.popoverService.add(this.copyToClipboardButton.el, Tooltip, {
            tooltip: _t("Template copied to clipboard."),
        });
        browser.setTimeout(() => {
            closeTooltip();
        }, 800);
    }
    onClickCopyToClipboard(ev) {
        ev.stopPropagation();
        ev.preventDefault();
    }
    onClickUseAsDescription(ev) {
        const dataTransfer = this._createHtmlDataTransfer();
        const macro = new UseAsDescriptionMacro({
            targetXmlDoc: this.targetRecordInfo.xmlDoc,
            breadcrumbs: this.targetRecordInfo.breadcrumbs,
            data: {
                fieldName: this.targetRecordInfo.fieldInfo.name,
                pageName: this.targetRecordInfo.fieldInfo.pageName,
                dataTransfer: dataTransfer,
            },
            services: {
                ui: this.uiService,
                dialog: this.dialogService,
            }
        });
        macro.start();
    }
    onClickSendAsMessage(ev) {
        const dataTransfer = this._createHtmlDataTransfer();
        const macro = new Macro({
            targetXmlDoc: this.targetRecordInfo.xmlDoc,
            breadcrumbs: this.targetRecordInfo.breadcrumbs,
            data: {
                dataTransfer: dataTransfer,
            },
            services: {
                ui: this.uiService,
                dialog: this.dialogService,
            }
        });
        macro.start();
    }
    _createHtmlDataTransfer() {
        const dataTransfer = new DataTransfer();
        const content = this.props.anchor.querySelector('.o_knowledge_content');
        dataTransfer.setData('text/odoo-editor', `<p></p>${content.innerHTML}<p></p>`);
        return dataTransfer;
    }
}

Template.template = "openeducat_knowledge.TemplateBehavior";
Template.props = {
    readonly: { type: Boolean },
    anchor: { type: Element },
    wysiwyg: { type: Object, optional: true},
    record: { type: Object },
    root: { type: Element },
    content: { type: Object, optional: true },
};
