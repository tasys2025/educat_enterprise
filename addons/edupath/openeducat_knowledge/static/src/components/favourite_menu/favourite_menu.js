/** @odoo-module **/

import { DropdownItem } from "@web/core/dropdown/dropdown_item";
import { registry } from "@web/core/registry";
const favoriteMenuRegistry = registry.category("favoriteMenu");
const { Component } = owl;

export class KnowledgeFavouriteMenu extends Component{
    _onAddViewKnowledge() {
        this.env.searchModel.trigger('insert_knowledge_view');
    }

    _onAddLinkKnowledge() {
        this.env.searchModel.trigger('insert_knowledge_link');
    }
}

KnowledgeFavouriteMenu.props = {};
KnowledgeFavouriteMenu.template = 'openeducat_knowledge.KnowledgeFavouriteMenu';
KnowledgeFavouriteMenu.components = { DropdownItem };

favoriteMenuRegistry.add('knowledge_favourite_menu', {
    Component: KnowledgeFavouriteMenu,
    groupNumber: 1,
    isDisplayed: (env) => {
        return env.config.actionId && !env.searchModel._context.knowledge_embedded_view_framework;
    },
})