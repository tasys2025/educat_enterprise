# Part of odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class HelpdeskTeam(models.Model):
    _inherit = 'helpdesk.team'

    elearning_id = fields.Many2one('op.course', string="E Learning")
