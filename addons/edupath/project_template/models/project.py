# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Project(models.Model):
    _inherit = "project.project"
    _description = "Project inherit"

    project_template_id = fields.Many2one('project.template')

    @api.model_create_multi
    def create(self, vals_list):
        # Prevent double project creation
        self = self.with_context(mail_create_nosubscribe=True)
        projects = super(Project, self).create(vals_list)

        template = projects.project_template_id
        if template:
            print('oke')
            projects.type_ids = [(6, 0, template.task_stage_ids.ids)]
            projects.tag_ids = [(6, 0, template.tag_ids.ids)]
            for rec in template.task_template_ids:
                projects.task_ids.create({
                    'project_id': projects.id,
                    'name': rec.name,
                    'priority': rec.priority,
                    'tag_ids': [(6, 0, rec.tag_ids.ids)],
                    'stage_id': template.task_stage_ids[0].id
                })
        return projects

