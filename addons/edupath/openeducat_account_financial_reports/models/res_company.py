from odoo import models, fields


class ResCompany(models.Model):
    _inherit = 'res.company'

    date_range = fields.Selection(
        [('today', 'Today'),
         ('this_week', 'This Week'),
         ('this_month', 'This Month'),
         ('this_quarter', 'This Quarter'),
         ('this_financial_year', 'This financial Year'),
         ('yesterday', 'Yesterday'),
         ('last_week', 'Last Week'),
         ('last_month', 'Last Month'),
         ('last_quarter', 'Last Quarter'),
         ('last_financial_year', 'Last Financial Year')],
        string='Default Date Range', default='this_financial_year',
        required=True
    )
    financial_year = fields.Selection([
        ('april_march', '1 April to 31 March'),
        ('july_june', '1 july to 30 June'),
        ('january_december', '1 Jan to 31 Dec')
    ], string='Financial Year', default='january_december',
        required=True)

    strict_range = fields.Boolean(string='Use Strict Range', default=True,
                                  help='Use this if you want '
                                       'to show TB with retained earnings section')
