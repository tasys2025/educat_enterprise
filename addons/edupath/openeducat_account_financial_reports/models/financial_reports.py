from odoo import models, fields, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import fiscalyear
import re
import json
from odoo.tools import date_utils
from odoo.tools import safe_eval
import io

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class FinancialReport(models.Model):
    _name = 'financial.reports'
    _description = 'Financial Reports'

    @api.onchange('date_range', 'financial_year')
    def onchange_date_range(self):
        if self.date_range:
            date = datetime.today()
            if self.date_range == 'today':
                self.date_from = date.strftime("%Y-%m-%d")
                self.date_to = date.strftime("%Y-%m-%d")

            if self.date_range == 'this_week':
                self.date_from = (date + relativedelta(days=-date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (self.date_from + relativedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_month':
                self.date_from = (date + relativedelta(day=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_quarter':
                currQuarter = int((date.month - 1) / 3 + 1)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'this_financial_year':
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    cur_year = fiscalyear.FiscalYear.current()
                    self.date_from = cur_year.start.date()
                    self.date_to = cur_year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    if date.month < 4:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    if date.month < 7:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

            if self.date_range == 'yesterday':
                self.date_from = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_week':
                date = (datetime.now() - relativedelta(days=7))
                day_today = date - timedelta(days=date.weekday())
                self.date_from = (day_today - timedelta(days=date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (day_today + timedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_month':
                self.date_from = (date - relativedelta(months=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_quarter':
                currQuarter = int((date.month - 1) / 3)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'last_financial_year':
                date = fiscalyear.FiscalDate.today()
                year = date.prev_fiscal_year
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

    @api.model
    def _get_default_date_range(self):
        return self.env.company.date_range

    name = fields.Char('Name')
    report_line_ids = fields.One2many('financial.reports.line', 'report_id')
    company_id = fields.Many2one('res.company', string='Company',
                                 required=True, default=lambda self: self.env.company)
    journal_ids = fields.Many2many('account.journal',
                                   string='Journals', ondelete='cascade')
    analytic_ids = fields.Many2many(
        'account.analytic.account', string='Analytic Accounts'
    )

    financial_year = fields.Selection(
        [('april_march', '1 April to 31 March'),
         ('july_june', '1 july to 30 June'),
         ('january_december', '1 Jan to 31 Dec')],
        string='Financial Year', default=lambda self: self.env.company.financial_year,
        required=True)

    date_range = fields.Selection(
        [('today', 'Today'),
         ('this_week', 'This Week'),
         ('this_month', 'This Month'),
         ('this_quarter', 'This Quarter'),
         ('this_financial_year', 'This financial Year'),
         ('yesterday', 'Yesterday'),
         ('last_week', 'Last Week'),
         ('last_month', 'Last Month'),
         ('last_quarter', 'Last Quarter'),
         ('last_financial_year', 'Last Financial Year')],
        string='Date Range', default=_get_default_date_range,
    )

    strict_range = fields.Boolean(
        string='Strict Range',
        default=lambda self: self.env.company.strict_range)

    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    target_moves = fields.Selection([('all', 'All'), ('posted', 'Posted')],
                                    string='Target Moves', default='posted',
                                    required=False)

    # analytic_tag_ids = fields.Many2many(
    #     'account.analytic.tag', string='Analytic Tags'
    # )

    def get_filters(self, default_filters={}):
        self.onchange_date_range()

        company_domain = [('company_id', '=', self.env.company.id)]
        journals = self.journal_ids if self.journal_ids \
            else self.env['account.journal'].search(company_domain)
        analytics = self.analytic_ids if self.analytic_ids \
            else self.env['account.analytic.account'].search(company_domain)

        filter_dict = {
            'journal_ids': self.journal_ids.ids,
            'analytic_ids': self.analytic_ids.ids,
            'company_id': self.company_id and self.company_id.id or False,
            'target_moves': self.target_moves,
            'date_from': self.date_from,
            'date_to': self.date_to,
            'journals_list': [(j.id, j.name) for j in journals],
            'analytics_list': [(anl.id, anl.name) for anl in analytics],
            'company_name': self.company_id and self.company_id.name,
        }
        filter_dict.update(default_filters)
        return filter_dict

    def process_filters(self):
        data = self.get_filters(default_filters={})

        filters = {}
        if data.get('journal_ids', []):
            filters['journal_ids'] = self.env['account.journal'] \
                .browse(data.get('journal_ids', [])).mapped('id')
        else:
            filters['journal_ids'] = []

        if data.get('analytic_ids', []):
            filters['analytic_account_ids'] = self.env['account.analytic.account'] \
                .browse(data.get('analytic_ids', []))
        else:
            filters['analytic_account_ids'] = []

        if data.get('target_moves') == 'all':
            filters['state'] = 'all'
        else:
            filters['state'] = 'posted'

        if data.get('date_from'):
            filters['date_from'] = data.get('date_from')
        else:
            filters['date_from'] = False
        if data.get('date_to'):
            filters['date_to'] = data.get('date_to')
        else:
            filters['date_to'] = False

        if data.get('company_id'):
            filters['company_id'] = data.get('company_id')
        else:
            filters['company_id'] = ''

        filters['journals_list'] = data.get('journals_list')
        filters['analytics_list'] = data.get('analytics_list')
        filters['company_name'] = data.get('company_name')

        return filters

    def write(self, vals):

        if vals.get('date_range'):
            vals.update({'date_from': False, 'date_to': False})

        if vals.get('date_from') or vals.get('date_to'):
            vals.update({'date_range': False})

        if vals.get('target_moves'):
            vals.update({'target_moves': vals.get('target_moves')})

        if vals.get('journal_ids'):
            vals.update({'journal_ids': vals.get('journal_ids')})
        if vals.get('journal_ids') == []:
            vals.update({'journal_ids': [(5,)]})

        if vals.get('analytic_ids'):
            vals.update({'analytic_ids': vals.get('analytic_ids')})
        if vals.get('analytic_ids') == []:
            vals.update({'analytic_ids': [(5,)]})

        ret = super(FinancialReport, self).write(vals)
        return ret

    def get_report_values(self):

        data = dict()
        filters = self.process_filters()
        data['context'] = filters
        report_lines = self.report_lines_values(data.get('context'))

        if report_lines:
            self.write({'journal_ids': False})
            self.write({'analytic_ids': False})
            self.write({'date_from': False})
            self.write({'date_to': False})
            self.write({'target_moves': 'posted'})

        filters.update({'report_name': self.name})

        return filters, report_lines

    def get_duplicate_report_values(self):

        context = dict(self._context)
        report_lines = self.report_lines_values(context)
        return report_lines

    def calculate_number_of_days(self, data):

        from_date = ''
        to_date = ''
        if data.get('date_from'):
            from_date = data.get('date_from').strftime("%m/%d/%Y")
        if data.get('date_to'):
            to_date = data.get('date_to').strftime("%m/%d/%Y")
        if from_date and to_date:
            date_format = "%m/%d/%Y"
            date_from = datetime.strptime(from_date, date_format)
            date_to = datetime.strptime(to_date, date_format)
            dates = date_to - date_from
            day = str(dates.days)
        else:
            day = '364'
        return day

    def report_lines_values(self, data):
        report_value_lines = self.env['financial.reports.line']
        view_order_lines = self.env['financial.reports.line']
        report_values = {}
        account_values = {}
        report_lines = []
        line_id = self.id

        days = self.calculate_number_of_days(data)
        dic = {}
        dic['00'] = days
        report_values['NDays'] = dic

        rep_lines = self.env['financial.reports.line'].search([
            ('id', 'in', self.report_line_ids.ids)],
            order='sequence ASC')
        for lines in rep_lines:
            value = lines.financial_lines(lines)
            report_value_lines += value

        for lines in self.report_line_ids:
            line = lines
            order = line.ordered_financial_lines(line)
            view_order_lines += order

        for lines in report_value_lines:
            if lines.domain:
                if lines.formulas == 'sum' or '-sum':
                    domain = lines.domain
                    if data:
                        value, vals = lines.with_context(data).domain_search(domain)
                    else:
                        value, vals = lines.domain_search(domain)
                    if lines.code:
                        report_values[lines.code] = value
                    else:
                        report_values[lines.name] = value
                    if vals and lines.code:
                        account_values[lines.code] = vals
                    elif vals and lines.name:
                        account_values[lines.name] = vals

        for lines in report_value_lines:
            if not lines.domain:
                formula = lines.formulas
                if lines.formulas == 'sum' or lines.formulas == '-sum' or \
                        not lines.formulas:
                    if lines.code:
                        values = {}
                        values['None'] = 0
                        report_values[lines.code] = values
                else:
                    formula = re.split(r'\+ | - | / | * ', formula)
                    if data:
                        vals = lines.with_context(data) \
                            .formula_calculate(formula, report_values, line_id)
                    else:
                        vals = lines.formula_calculate(formula, report_values, line_id)
                    if lines.code:
                        report_values[lines.code] = vals
                    else:
                        report_values[lines.name] = vals
            if lines.domain:
                if lines.formulas != '-sum' and lines.formulas != 'sum':
                    formula = lines.formulas
                    formula = re.split(r'\+ | - | / | * ', formula)
                    if data:
                        vals = lines.with_context(data) \
                            .diff_formula_calculate(formula, report_values, line_id)
                    else:
                        vals = lines. \
                            diff_formula_calculate(formula, report_values, line_id)
                    report_values[lines.code] = vals

        for lines in view_order_lines:

            if not lines.code:
                if lines.name in report_values.keys():
                    for account_id, value in report_values[lines.name].items():
                        vals = {
                            'name': lines.name,
                            'amount': value,
                            'parent': lines.parent_id.id,
                            'code': lines.code,
                            'type': 'report',
                            'self_id': lines.id,
                            'account_type': lines.drill_down or False,
                            'formula': lines.formulas,
                            'val_type': lines.type,
                            'list_len': [a for a in range(0, lines.level)],
                            'level': lines.level,
                            'company_currency_id': self.env.company.currency_id.id,
                        }
                        report_lines.append(vals)
                        if lines.name in account_values.keys():
                            for account_id, value in account_values[lines.name].items():
                                if value['amount'] == 0:
                                    continue
                                else:
                                    vals = {
                                        'account': value['id'],
                                        'name': value['account_name'],
                                        'amount': value['amount'],
                                        'type': 'account',
                                        'account_type': lines.drill_down or False,
                                        'parent': lines.id,
                                        'self_id': 50,
                                        'val_type': lines.type,
                                        'list_len': [a for a in range(0, 4)],
                                        'level': 4,
                                        'company_currency_id':
                                            self.env.company.currency_id.id,
                                    }
                                    report_lines.append(vals)
                        else:
                            if lines.code in account_values.keys():
                                for account_id, value \
                                        in account_values[lines.code].items():
                                    if value['amount'] == 0:
                                        continue
                                    else:
                                        vals = {
                                            'account': value['id'],
                                            'name': value['account_name'],
                                            'amount': value['amount'],
                                            'type': 'account',
                                            'account_type': lines.drill_down or False,
                                            'parent': lines.id,
                                            'self_id': 50,
                                            'val_type': lines.type,
                                            'list_len': [a for a in range(0, 4)],
                                            'level': 4,
                                            'company_currency_id':
                                                self.env.company.currency_id.id,
                                        }
                                        report_lines.append(vals)
                    continue
                else:
                    vals = {
                        'name': lines.name,
                        'level': lines.level,
                        'type': 'report',
                        'amount': None,
                        'account_type': lines.drill_down or False,
                        'parent': lines.parent_id.id,
                        'code': lines.code,
                        'self_id': lines.id,
                        'val_type': lines.type,
                        'list_len': [a for a in range(0, lines.level)],
                    }
                    report_lines.append(vals)
                    continue
            if report_values[lines.code].items():
                for account_id, value in report_values[lines.code].items():
                    vals = {
                        'name': lines.name,
                        'amount': value,
                        'type': 'report',
                        'parent': lines.parent_id.id,
                        'code': lines.code,
                        'account_type': lines.drill_down or False,
                        'self_id': lines.id,
                        'formula': lines.formulas,
                        'val_type': lines.type,
                        'list_len': [a for a in range(0, lines.level)],
                        'level': lines.level,
                        'company_currency_id': self.env.company.currency_id.id,
                    }
                    report_lines.append(vals)
                    if lines.name in account_values.keys():
                        for account_id, value in account_values[lines.name].items():
                            if value['amount'] == 0:
                                continue
                            else:
                                vals = {
                                    'account': value['id'],
                                    'name': value['account_name'],
                                    'amount': value['amount'],
                                    'type': 'account',
                                    'account_type': lines.drill_down or False,
                                    'parent': lines.id,
                                    'self_id': 50,
                                    'val_type': lines.type,
                                    'list_len': [a for a in range(0, 4)],
                                    'level': 4,
                                    'company_currency_id':
                                        self.env.company.currency_id.id,
                                }
                                report_lines.append(vals)
                    else:
                        if lines.code in account_values.keys():
                            for account_id, value in account_values[lines.code].items():
                                if value['amount'] == 0:
                                    continue
                                else:
                                    vals = {
                                        'account': value['id'],
                                        'name': value['account_name'],
                                        'amount': value['amount'],
                                        'type': 'account',
                                        'account_type': lines.drill_down or False,
                                        'parent': lines.id,
                                        'self_id': 50,
                                        'val_type': lines.type,
                                        'list_len': [a for a in range(0, 4)],
                                        'level': 4,
                                        'company_currency_id':
                                            self.env.company.currency_id.id,
                                    }
                                    report_lines.append(vals)
        return report_lines

    def action_xlsx(self):
        data = self.read()
        report = self.name
        name = '%s' % (report)
        return {
            'type': 'ir.actions.report',
            'data': {'model': 'financial.reports',
                     'options': json.dumps(data[0], default=date_utils.json_default),
                     'output_format': 'xlsx',
                     'report_name': name,
                     },
            'report_type': 'xlsx'
        }

    def get_xlsx_report(self, data, response):

        record = self.env['financial.reports'].browse(data.get('id', [])) or False
        filter_val, data = record.get_report_values()
        currency_id = self.env.user.company_id.currency_id
        symbol = currency_id.symbol + '#,##0'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        cell_format = workbook.add_format({'font_size': '12px', 'bold': True})
        head = workbook.add_format({'align': 'center', 'bold': True,
                                    'font_size': '20px'})
        txt = workbook.add_format({'font_size': '10px'})
        txt2 = workbook.add_format({'font_size': '10px', 'bold': True})
        txt1 = workbook.add_format({'font_size': '10px', 'num_format': symbol})

        sheet.merge_range('B2:I3', filter_val['report_name'], head)

        if filter_val['date_from']:
            sheet.write('C6', 'From:', cell_format)
            sheet.write('D6', filter_val['date_from'], txt)
        if filter_val['date_to']:
            sheet.write('F6', 'To:', cell_format)
            sheet.write('G6', filter_val['date_to'], txt)

        sheet.write(7, 1, _('Name'), cell_format)
        sheet.write(7, 7, _('Balance'), cell_format)

        sheet.set_column('B:B', 30)
        sheet.set_column('H:H', 10)

        row_pos = 9
        col_pos = 1

        for value in data:
            if value.get('level') == 0:
                sheet.write(row_pos, col_pos, '   ' *
                            len(value.get('list_len', [])) + value.get('name'), txt2)
            else:
                sheet.write(row_pos, col_pos, '   ' *
                            len(value.get('list_len', [])) + value.get('name'), txt)

            sheet.write(row_pos, col_pos + 6, float(value.get('amount')), txt1)
            row_pos += 1

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()


class FinancialReportLine(models.Model):
    _name = 'financial.reports.line'
    _description = 'Financial Reports Line'

    name = fields.Char('Report Name')
    code = fields.Char('Code')
    level = fields.Integer('Level')
    sequence = fields.Integer('Sequence')
    parent_id = fields.Many2one('financial.reports.line', 'Parent')
    report_id = fields.Many2one('financial.reports', 'Report')
    formulas = fields.Char('Formulas')
    domain = fields.Char('Domain')
    child_ids = fields.One2many('financial.reports.line', 'parent_id')
    type = fields.Selection([
        ('float', 'Float'),
        ('percents', 'Percents'),
        ('no_unit', 'No Unit')],
        string='Type')
    drill_down = fields.Selection([
        ('account', 'Account Values'),
        ('sum', 'Sum Values'),
        ('none', 'None'),
    ], string='Drill Type')

    def financial_lines(self, line):
        res = self.env['financial.reports.line']
        children = self.search([('parent_id', '=', line.id)], order='id ASC')
        for child in children:
            if child.domain:
                res += child
                continue
            elif child.child_ids:
                res += child.financial_lines(child)
            else:
                res += child.financial_lines(child)
        res += self
        return res

    def ordered_financial_lines(self, line):
        res = self
        children = self.search([('parent_id', '=', line.id)], order='id ASC')

        for child in children:
            if child.domain:
                res += child
                continue
            elif child.child_ids:
                res += child.ordered_financial_lines(child)
            else:
                res += child.ordered_financial_lines(child)
        return res

    def parent_search(self, parent, vals):
        for parents in parent:
            if parents.parent_id:
                parent = parents.parent_id
                parent.parent_search(parent, vals)
            else:
                vals.append(parents)
        return vals

    def get_filter_domains(self, context):

        domain = []

        if context.get('journal_ids'):
            domain += [('journal_id', 'in', context['journal_ids'])]

        if context.get('analytic_account_ids'):
            domain += [('analytic_account_id', 'in',
                        context['analytic_account_ids'].ids)]

        if context.get('date_from'):
            domain += [('date', '>=', context['date_from'])]

        if context.get('date_to'):
            domain += [('date', '<=', context['date_to'])]

        state = context.get('state')
        if state and state.lower() != 'all':
            domain += [('move_id.state', '=', state)]

        return domain

    def domain_search(self, domain):
        context = dict(self._context)
        result = {}
        value_dic = {}
        amt = 0
        line_id = self
        domain = safe_eval.safe_eval(domain) or []

        if 'account_id' in self.domain:
            if context:
                domains = self.get_filter_domains(context)
                if domains:
                    domain += domains

            vals_dic = {}
            acc = self.env['account.move.line'].search(domain)
            credit, debit, amt = 0, 0, 0

            for line in acc:
                credit += line.credit
                debit += line.debit
                if line_id.formulas == '-sum':
                    amt = credit - debit
                else:
                    amt = debit - credit

            for line in acc:
                if line.account_id.id in result:
                    continue
                else:
                    domain = [('account_id', '=', line.account_id.id)]

                    if context:
                        domains = self.get_filter_domains(context)
                        if domains:
                            domain += domains

                    account = self.env['account.move.line'].search(domain)
                    credit = 0
                    debit = 0
                    val = 0
                    for acc in account:
                        credit += acc.credit
                        debit += acc.debit
                        if self.formulas == '-sum':
                            val = credit - debit
                        else:
                            val = debit - credit
                    result[line.account_id.id] = {
                        'amount': val,
                        'account_name':
                            line.account_id.code + ' ' + line.account_id.name,
                        'id': line.account_id.id,
                    }
            vals_dic[line_id.id] = amt
            return vals_dic, result

        else:
            accounts = self.env['account.account'].search(domain)

        if accounts:
            cr = self.env.cr
            mapping = {
                'balance': "COALESCE(SUM(debit),0) - COALESCE(SUM(credit), 0) "
                           "as balance",
                'debit': "COALESCE(SUM(debit), 0) as debit",
                'credit': "COALESCE(SUM(credit), 0) as credit",
            }
            tables, where_clause, where_params = self.env['account.move.line'] \
                .with_context(context)._query_get()
            tables = tables.replace('"', '') if tables else "account_move_line"
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            request = "SELECT account_id as id, " + ', '.join(mapping.values()) + \
                      " FROM " + tables + \
                      " WHERE account_id IN %s " \
                      + filters + \
                      " GROUP BY account_id"
            params = (tuple(accounts.ids),) + tuple(where_params)
            cr.execute(request, params)
            for row in cr.dictfetchall():
                result[row['id']] = row
            for account in accounts:
                if account.id in result:
                    result[account.id]['account_name'] = \
                        account.code + ' ' + account.name
                    if self.formulas == '-sum':
                        result[account.id]['amount'] = \
                            result[account.id]['credit'] - result[account.id]['debit']
                    else:
                        result[account.id]['amount'] = \
                            result[account.id]['debit'] - result[account.id]['credit']

        if result:
            if self.formulas == '-sum':
                for key, value in result.items():
                    amt += value['credit'] - value['debit']
            else:
                for key, value in result.items():
                    amt += value['debit'] - value['credit']

        value_dic[line_id.id] = amt
        return value_dic, result

    def formula_calculate(self, formulas, values, report_id):
        temporary = {}
        vals_dic = {}
        line_id = self
        formula_string = ''
        context = dict(self._context)
        for p_id, p_value in values.items():
            for form in formulas:
                if form == p_id:
                    for key in p_value:
                        temporary[p_id] = p_value[key]
                    if formula_string:
                        if form in line_id.formulas:
                            pattern = re.compile(r'\b' + re.escape(form) + r'\b')
                            formula = str(temporary[form])
                            formula_string = pattern.sub(formula, formula_string)
                    else:
                        if form in line_id.formulas:
                            pattern = re.compile(r'\b' + re.escape(form) + r'\b')
                            formula = str(temporary[form])
                            formula_string = pattern.sub(formula, line_id.formulas)
                else:
                    continue

        if form in formula_string:
            vals = line_id.with_context(context) \
                .diff_formula_calculate(formulas, values, report_id)
            vals_dic[line_id.id] = vals
            return vals

        if not formula_string:
            vals = line_id.with_context(context) \
                .diff_formula_calculate(formulas, values, report_id)
            vals_dic[line_id.id] = vals
            return vals

        vals = self.get_calculated_vals(formula_string)
        vals_dic[line_id.id] = vals
        return vals_dic

    def get_calculated_vals(self, f):
        try:
            vals = eval(f)
        except Exception:
            vals = 0
        return vals

    def diff_formula_calculate(self, formulas, values, line_id):
        context = dict(self._context)
        current_report_id = line_id

        for form in formulas:
            if self.env['financial.reports.line'].search([('code', '=', form)]):
                res = self.env['financial.reports.line'].search([('code', '=', form)])

                if res.parent_id:
                    val = []
                    vals = res.parent_search(res, val)

                    for line_id in vals:
                        if line_id.report_id:
                            report_id = line_id.report_id
                        else:
                            continue

                    if current_report_id == report_id.id:
                        continue

                    else:
                        value = report_id.with_context(context) \
                            .get_duplicate_report_values()
                        for val in value:
                            for k, num in val.items():
                                if num == form:
                                    amount = val['amount']
                                    val_id = val['self_id']
                                    dic = {}
                                    dic[val_id] = amount
                                    values[form] = dic
                                else:
                                    continue
                else:
                    value = res.report_id.with_context(context) \
                        .get_duplicate_report_values()
                    if not value:
                        continue
                    else:
                        for val in value:
                            for key, num in val.items():
                                if key == 'code' and num == form:
                                    amount = val['amount']
                                    val_id = val['self_id']
                                    dic = {}
                                    dic[val_id] = amount
                                    values[form] = dic
                                else:
                                    continue
            else:
                if form == 'sum':
                    dic = {}
                    domain = self.domain
                    val, result = self.domain_search(domain)
                    amt = 0
                    if result:
                        if self.formulas == '-sum':
                            for key, value in result.items():
                                amt += value['credit'] - value['debit']
                                dic['0'] = amt
                                values['sum'] = dic
                        else:
                            for key, value in result.items():
                                amt += value['debit'] - value['credit']
                                dic['0'] = amt
                                values['sum'] = dic
                    else:
                        dic['0'] = 0
                        values['sum'] = dic

        vals = self.with_context(context). \
            formula_calculate(formulas, values, line_id)
        return vals
