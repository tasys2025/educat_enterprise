from odoo import models, fields, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import fiscalyear
import json
import re
from odoo.tools import date_utils
import io

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter

taxes_query = """SELECT l.tax_line_id, \
            COALESCE(SUM(l.debit-l.credit), 0)
            FROM account_move as m, account_move_line as l
            WHERE (l.move_id=m.id) AND \
            ((((l.date <= '%s')  AND  \
            ((l.date >= '%s')))  AND  \
            (m.state = 'posted'))  AND  \
            (l.company_id = '%s')) \
            GROUP BY l.tax_line_id \
            """
gst_taxes_query = """SELECT r.account_tax_id,\
                COALESCE(SUM(l.debit-l.credit), 0)
                 FROM account_move as m, account_move_line as l\
                 INNER JOIN account_move_line_account_tax_rel r ON \
                 (l.id = r.account_move_line_id)\
                 INNER JOIN account_tax t ON (r.account_tax_id = t.id)\
                 WHERE (l.move_id=m.id) AND \
                 ((l.date <= '%s')  AND  \
                 ((l.date >= '%s') AND  \
                (m.state = 'posted'))\
                 ) group by r.account_tax_id"""


class ReportTax(models.Model):
    _name = 'financial.tax.report'
    _description = 'Financial Tax Report'
    _inherit = 'report.openeducat_acc_pdf_reports.report_tax'

    @api.onchange('date_range', 'financial_year')
    def onchange_date_range(self):
        if self.date_range:
            date = datetime.today()
            if self.date_range == 'today':
                self.date_from = date.strftime("%Y-%m-%d")
                self.date_to = date.strftime("%Y-%m-%d")

            if self.date_range == 'this_week':
                self.date_from = (date + relativedelta(days=-date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (self.date_from + relativedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_month':
                self.date_from = (date + relativedelta(day=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_quarter':
                currQuarter = int((date.month - 1) / 3 + 1)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'this_financial_year':
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    cur_year = fiscalyear.FiscalYear.current()
                    self.date_from = cur_year.start.date()
                    self.date_to = cur_year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    if date.month < 4:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    if date.month < 7:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

            if self.date_range == 'yesterday':
                self.date_from = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_week':
                date = (datetime.now() - relativedelta(days=7))
                day_today = date - timedelta(days=date.weekday())
                self.date_from = (day_today - timedelta(days=date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (day_today + timedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_month':
                self.date_from = (date - relativedelta(months=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_quarter':
                currQuarter = int((date.month - 1) / 3)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'last_financial_year':
                date = fiscalyear.FiscalDate.today()
                year = date.prev_fiscal_year
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

    @api.model
    def _get_default_date_range(self):
        return self.env.company.date_range

    @api.model
    def _get_default_report_type(self):
        if not self.env['account.tax.report'].search_count([(
                'country_id', '=', self.env.company.country_id.id)]):
            return 'global'
        else:
            return 'tax_report'

    financial_year = fields.Selection(
        [('april_march', '1 April to 31 March'),
         ('july_june', '1 july to 30 June'),
         ('january_december', '1 Jan to 31 Dec')],
        string='Financial Year',
        default=lambda self: self.env.company.financial_year,
        required=True)

    date_range = fields.Selection(
        [('today', 'Today'),
         ('this_week', 'This Week'),
         ('this_month', 'This Month'),
         ('this_quarter', 'This Quarter'),
         ('this_financial_year', 'This financial Year'),
         ('yesterday', 'Yesterday'),
         ('last_week', 'Last Week'),
         ('last_month', 'Last Month'),
         ('last_quarter', 'Last Quarter'),
         ('last_financial_year', 'Last Financial Year')],
        string='Date Range', default=_get_default_date_range)

    date_from = fields.Date(
        string='Start date',
    )
    date_to = fields.Date(
        string='End date',
    )

    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company
    )

    report_type = fields.Selection([('global', 'Global Tax'),
                                    ('tax_report', 'Tax Report')],
                                   string='Report Types',
                                   default=_get_default_report_type, required=True)

    def create(self, vals):
        res = super(ReportTax, self).create(vals)
        return res

    def write(self, vals):

        if vals.get('date_range'):
            vals.update({'date_from': False, 'date_to': False})
        if vals.get('date_from') and vals.get('date_to'):
            vals.update({'date_range': False})

        if not vals.get('date_range') and not vals.get('date_from'):
            vals.update({'date_range': self._get_default_date_range()})

        if vals.get('report_type'):
            vals.update({'report_type': vals.get('report_type')})

        res = super(ReportTax, self).write(vals)
        return res

    def process_filters(self):

        data = self.get_filters(default_filters={})

        filters = {}

        if data.get('date_from', False):
            filters['date_from'] = data.get('date_from')
            filters['strict_range'] = True
        if data.get('date_to', False):
            filters['date_to'] = data.get('date_to')
        if data.get('currency_id'):
            filters['currency_id'] = data.get('currency_id')
        if data.get('report_type'):
            filters['report_type'] = data.get('report_type')
        filters['company_name'] = data.get('company_name')

        return filters

    def get_filters(self, default_filters={}):

        self.onchange_date_range()

        filter_dict = {
            'date_from': self.date_from,
            'date_to': self.date_to,
            'report_type': self.report_type,
            'currency_id': self.company_id.currency_id.id,
            'company_name': self.company_id.name,
        }

        filter_dict.update(default_filters)
        return filter_dict

    def tax_report(self, filters):
        country = self.env.company.country_id.id
        tax_report = self.env['account.tax.report']. \
            search([('country_id', '=', country)])
        filters['report_name'] = tax_report.name
        report_value_lines = self.env['account.tax.report.line']
        view_order_lines = self.env['account.tax.report.line']
        report_lines = []
        code_values = {}

        for lines in tax_report.root_line_ids:
            value = lines.tax_report_financial_lines(lines)
            report_value_lines += value

        for lines in tax_report.root_line_ids:
            order = lines.tax_report_ordered_financial_lines(lines)
            view_order_lines += order

        left_order_lines = self.env['account.tax.report.line']

        for lines in report_value_lines:
            if lines.code or lines.tag_name:
                if lines.tag_name:
                    tag_name = '+' + lines.tag_name
                    value = self.env['account.tax.repartition.line']. \
                        search([('tag_ids', '=', tag_name)])
                    amount = self.get_amount(value, filters, tag_name)
                    if lines.code and not lines.tag_name:
                        code_values[lines.code] = amount
                    elif lines.tag_name and not lines.code:
                        code_values[lines.tag_name] = amount
                    else:
                        code_values[lines.code] = amount
                else:
                    left_order_lines += lines

        if left_order_lines:
            for left in left_order_lines:
                amount = 0
                for child in left.children_line_ids:
                    if child.tag_name in code_values:
                        amount += code_values[child.tag_name]
                    elif child.code in code_values:
                        amount += code_values[child.code]
                    else:
                        continue
                if left.code:
                    code_values[left.code] = amount
                else:
                    code_values[left.tag_name] = amount

        for lines in report_value_lines:
            if lines.formula:
                formula = re.split(r'[\+ \- \* \> \< \( \)]', lines.formula)
                vals = lines.formula_calculate(formula, code_values)
                code_values[lines.id] = vals

        for lines in view_order_lines:
            if not lines.code and not lines.tag_name and not lines.formula:
                if lines.parent_id:
                    vals = {
                        'name': lines.name,
                        'level': 2,
                        'list_len': [a for a in range(0, 2)],
                        'amount': False
                    }
                else:
                    vals = {
                        'name': lines.name,
                        'level': 0,
                        'list_len': [a for a in range(0, 1)],
                        'amount': False
                    }
                report_lines.append(vals)

            if lines.code and lines.tag_name:
                amount = code_values[lines.code]
                if lines.parent_id.parent_id:
                    level = 3
                elif lines.parent_id:
                    level = 3
                else:
                    level = 1
                vals = {
                    'name': lines.name,
                    'level': level,
                    'amount': amount,
                    'formula': lines.code,
                    'list_len': [a for a in range(level)],
                    'query': '+' + lines.tag_name,
                    'parent': lines.parent_id.name
                }
                report_lines.append(vals)

            if lines.code and not lines.tag_name:
                if lines.parent_id:
                    level = 2
                else:
                    level = 1
                amount = code_values[lines.code]
                vals = {
                    'name': lines.name,
                    'level': level,
                    'amount': amount,
                    'formula': lines.code,
                    'list_len': [a for a in range(level)],
                    'query': False,
                    'parent': lines.parent_id.name
                }
                report_lines.append(vals)

            if lines.formula:
                if lines.parent_id.parent_id:
                    level = 4
                elif lines.parent_id:
                    level = 2
                else:
                    level = 1
                for key, value in code_values[lines.id].items():
                    amount = value
                    vals = {
                        'name': lines.name,
                        'level': level,
                        'amount': amount,
                        'formula': lines.formula,
                        'list_len': [a for a in range(level)],
                    }
                    report_lines.append(vals)

            if lines.tag_name and not lines.code and not lines.formula:
                if lines.parent_id.parent_id:
                    level = 4
                elif lines.parent_id:
                    level = 3
                else:
                    level = 2
                amount = code_values[lines.tag_name]
                vals = {
                    'name': lines.name,
                    'level': level,
                    'amount': amount,
                    'formula': lines.tag_name,
                    'list_len': [a for a in range(level)],
                    'query': '+' + lines.tag_name,
                    'parent': lines.parent_id.name
                }
                report_lines.append(vals)

        return report_lines

    def get_amount(self, values, filters, tag_name):
        tax_ids = self.env['account.tax']
        flag = 0

        for value in values:
            tax_ids += value.tax_id

        for repartition in tax_ids.invoice_repartition_line_ids:
            for tag in repartition.tag_ids:
                if tag.name == tag_name and repartition.use_in_tax_closing is True:
                    flag = 1
                else:
                    continue
        total = 0.0
        date_start = filters.get('date_from')
        date_stop = filters.get('date_to')
        self._cr.execute(taxes_query % (date_stop, date_start,
                                        self.env.user.company_id.id))
        taxes_result = self._cr.fetchall()

        self._cr.execute(gst_taxes_query % (date_stop, date_start))
        gst_taxes_results = self._cr.fetchall()

        if flag == 1:
            for i in range(len(taxes_result)):
                for tax in tax_ids:
                    if tax.id == taxes_result[i][0]:
                        total += taxes_result[i][1]

        else:
            for i in range(len(gst_taxes_results)):
                for tax in tax_ids:
                    if tax.id == gst_taxes_results[i][0]:
                        total += gst_taxes_results[i][1]

        return abs(total)

    def get_report_values(self):
        filters = self.process_filters()

        if filters.get('report_type') == 'global':
            values = self.get_lines(filters)
            return filters, values
        else:
            values = self.tax_report(filters)
            return filters, values

    def action_xlsx(self):
        data = self.read()
        report = 'Tax Report'
        name = '%s' % (report)

        return {
            'type': 'ir.actions.report',
            'data': {'model': 'financial.tax.report',
                     'options': json.dumps(data[0], default=date_utils.json_default),
                     'output_format': 'xlsx',
                     'report_name': name,
                     },
            'report_type': 'xlsx'
        }

    def get_xlsx_report(self, data, response):

        record = self.env['financial.tax.report'].browse(data.get('id', [])) or False
        filter_val, data = record.get_report_values()
        currency_id = self.env.user.company_id.currency_id

        symbol = currency_id.symbol + '#,##0'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        cell_format = workbook.add_format({'font_size': '12px', 'bold': True})
        cell_format_date = workbook.add_format({'font_size': '10px', })
        cell_format_tax_date = workbook.add_format({'font_size': '10px',
                                                    'bold': True})
        head = workbook.add_format({'align': 'center', 'bold': True,
                                    'font_size': '20px'})
        txt = workbook.add_format({'font_size': '10px'})
        txt2 = workbook.add_format({'font_size': '10px', 'bold': True})
        txt1 = workbook.add_format({'font_size': '10px', 'num_format': symbol})

        sheet.merge_range('B2:I3', 'Tax Report', head)

        if filter_val['date_from']:
            date_from = filter_val['date_from'].strftime("%Y-%m-%d")
            if filter_val['report_type'] == 'tax_report':
                sheet.write('B7', 'From: ' + date_from, cell_format_tax_date)
            else:
                sheet.write('B7', 'From:', cell_format)
                sheet.write('C7', date_from, cell_format_date)

        if filter_val['date_to']:
            date_to = filter_val['date_to'].strftime("%Y-%m-%d")
            if filter_val['report_type'] == 'tax_report':
                sheet.write('B8', 'To: ' + date_to, cell_format_tax_date)
            else:
                sheet.write('B8', 'To:', cell_format)
                sheet.write('C8', date_to, cell_format_date)

        if filter_val['report_type'] == 'global':
            sheet.set_column('B:B', 25)
            sheet.set_column('G:G', 10)
            sheet.set_column('H:H', 10)
            row_pos = 13
            col_pos = 1

            if data['sale']:
                sheet.write(11, 1, _('Sale'), cell_format)
                sheet.write(10, 6, _('NET'), cell_format)
                sheet.write(10, 7, _('TAX'), cell_format)
                for sale in data['sale']:
                    sheet.write(row_pos, col_pos, '   ' * 2 + sale.get('name'),
                                txt2)
                    sheet.write(row_pos, col_pos + 5, sale.get('net'), txt1)
                    sheet.write(row_pos, col_pos + 6, sale.get('tax'), txt1)
                    row_pos += 1

            if data['purchase']:
                row_pos += 1
                sheet.write(row_pos, 1, _('Purchase'), cell_format)
                row_pos += 2
                for purchase in data['purchase']:
                    sheet.write(row_pos, col_pos, '   ' * 2 + purchase.get('name'),
                                txt2)
                    sheet.write(row_pos, col_pos + 5, purchase.get('net'), txt1)
                    sheet.write(row_pos, col_pos + 6, purchase.get('tax'), txt1)
                    row_pos += 1

        else:
            sheet.write(10, 4, _('Balance'), cell_format)
            sheet.set_column('B:B', 120)
            sheet.set_column('H:H', 15)
            row_pos = 12
            col_pos = 1

            for value in data:
                if value.get('level') == 0:
                    sheet.write(row_pos, col_pos, '   ' *
                                len(value.get('list_len', [])) + value.get('name'),
                                txt2)
                else:
                    sheet.write(row_pos, col_pos, '   ' *
                                len(value.get('list_len', [])) + value.get('name'), txt)

                sheet.write(row_pos, col_pos + 3, float(value.get('amount')), txt1)
                row_pos += 1

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()
