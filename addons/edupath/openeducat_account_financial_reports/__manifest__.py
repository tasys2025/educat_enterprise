{
    'name': "OpenEduCat Accounting Financial Reports",
    'summary': """Drill Down Financial Reports""",
    'description': 'This module contains features of Account Financial Reports.',
    'author': "OpenEduCat Inc",
    'website': "http://www.openeducat.org",
    'category': 'Account',
    'version': '16.0.0.0',
    'license': 'Other proprietary',
    'depends': ['openeducat_core_enterprise', 'base',
                'account', 'web', 'openeducat_acc_pdf_reports'],

    'data': [
        'security/ir.model.access.csv',
        'data/financial_reports_line_data.xml',
        'data/financial_reports_line_data_1.xml',
        'data/financial_reports_data.xml',
        'views/financial_report_templates.xml',
        'views/account_financial_reports.xml',
        'views/report_actions.xml',
        'views/report_views.xml',
    ],
    'assets': {
        'web.assets_backend': [
            'openeducat_account_financial_reports/static/src/js/script.js',
            'openeducat_account_financial_reports/static/src/js/action_manager.js',
            'openeducat_account_financial_reports/static/src/scss/dynamic_common_style.scss',
            'openeducat_account_financial_reports/static/src/scss/reports_design.scss',
            'openeducat_account_financial_reports/static/src/xml/view.xml',
        ],
    },

    'qweb': ['static/src/xml/*.xml'],
}
