odoo.define('openeducat_account_financial_reports.ReportsMain', function (require) {
'use strict';
var AbstractAction = require('web.AbstractAction');
var Dialog = require('web.Dialog');
var FavoriteMenu = require('web.FavoriteMenu');
var web_client = require('web.web_client');
var ajax = require('web.ajax');
var core = require('web.core');
var Widget = require('web.Widget');
var field_utils = require('web.field_utils');
var rpc = require('web.rpc');
var time = require('web.time');
var session = require('web.session');
var utils = require('web.utils');
var round_di = utils.round_decimals;
var QWeb = core.qweb;
var _t = core._t;
var exports = {};
var exports = {};

                //FINANCIAL REPORTS//

    var FinancialReport = AbstractAction.extend({
        template: 'openeducat_account_financial_reports',
        xmlDependencies: ['/openeducat_account_financial_reports/static/src/xml/view.xml'],
        events: {
        'click #filter_apply_button': 'update_with_filter',
        'click .click_on_financial_entry': '_OnClickFinancialEntry',
        'click #pdf': 'print_pdf',
        'click #xlsx': 'print_xlsx',
        'click .dropdown-item': '_setSelection',
        },
        init : function(view, code){
            this._super(view, code);
            var self = this
            this.financial_display_name = code.context.name || null;
            this.model_id = code.context.model || null;
            this.session = session;
        },
        start : function(){
            this._super.apply(this, arguments);
            var self = this;
            self.initial_render = true;
            this._rpc({
                model:'financial.reports',
                method: 'search_read',
                kwargs: {
                    domain: [['name' ,'=', this.financial_display_name]],
                    }
            }).then(function(data){
                self.financial_report_id = data[0].id
            self.plot_data(self.initial_render);
            });
        },

        _setSelection: function(e){

            if($(e.target).hasClass('selected')){
                $(e.target).removeClass('selected');
                this.update_with_filter(e);
            }else{
                if($('span.date_filter-multiple1.selected').length != 0){
                    $('span.date_filter-multiple1.selected').each(function (el) {
                        $(this).removeClass('selected');
                    });
                }
                $(e.target).addClass('selected');
                this.update_with_filter(e);
            }
        },

         _OnClickFinancialEntry: function(e){
            var id = $(e.currentTarget).data('id');
            var date_from = $("input[name='date_from']").val()
            var date_to = $("input[name='date_to']").val()
            var self = this;
            var domain = [];
            if(date_from){
                if(date_from === 'false'){
                    date_from = [];
                } else {
                    domain.push(['date', '>=', date_from])
                }
            }
            if(date_to){
                if(date_to === 'false'){
                    date_to = [];;
                } else {
                    domain.push(['date', '<=', date_to])
                }
            }
            this._rpc({
                model:'account.account',
                method: 'search_read',
                kwargs: {
                    domain: [['id' ,'=', id]],
                    }
            }).then(function(data){
                var move_id = data[0].id;
                domain.push(['account_id', '=', move_id]);
                self.do_action({
                    name: 'Journal Items',
                    type: "ir.actions.act_window",
                    res_model: 'account.move.line',
                    domain: domain,
                    res_id: move_id,
                    views: [[false, "list"],[false, "form"]],
                    target: "current",
                    context: {},
                });
            });
        },

        formatWithSign : function(amount, formatOptions, sign){
        var currency_id = formatOptions.currency_id;
        var type = formatOptions.type;
        currency_id = session.get_currency(currency_id);
        var without_sign = field_utils.format.monetary(Math.abs(amount), {}, formatOptions);
        if(!amount){return '-'};
        if (type === "float") {
            if (currency_id.position === "after") {
            return sign + '  ' + without_sign + '  ' + currency_id.symbol;
            } else {
            return currency_id.symbol + '  ' + sign + '  ' + without_sign;
            }
            return without_sign;
            }
        if (type === "percents") {
                return without_sign * 100.0 + "%";
            } else{
                return without_sign;
            }
         },

        plot_data : function(initial_render = true){
            var self = this;
            var node = self.$('.py-data-container');
            var last;
            while (last = node.lastChild) node.removeChild(last);
            self._rpc({
            model: 'financial.reports',
            method: 'get_report_values',
            args: [[self.financial_report_id]],
            }).then(function (datas) {
            self.data = datas
            self.filter_data = datas[0]
            self.account_data = datas[1]

            _.each(self.account_data, function (k, v){
            var formatOptions = {
            currency_id: k.company_currency_id,
            type: k.val_type,
            noSymbol: true,
            };
            k.amount = self.formatWithSign(k.amount, formatOptions, k.amount < 0 ? '-' : '');
            });

            if(initial_render){
            self.$('.py-control-panel').html(QWeb.render('FinancialReportFS', {
            filter_data : self.filter_data,
            }));
            self.$el.find('#date_from').datepicker({ dateFormat: 'dd-mm-yy' });
            self.$el.find('#date_to').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#ui-datepicker-div').click(function(e){e.stopPropagation();});
            self.$el.find('.date_filter-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Date...',
            });
            self.$el.find('.journal-multiple').select2({
            placeholder:'Select Journal...',
            });
            self.$el.find('.analytic-multiple').select2({
            placeholder:'Select Analytic...',
            });
            self.$el.find('.extra-multiple').select2({
            placeholder:'Extra Options...',
            })
            .val('').trigger('change')
            ;
            }
            self.$('.py-data-container').html(QWeb.render('FinancialReportDS', {
            account_data : self.account_data,
            filter_data : self.filter_data,
            }));
            }
        );},
        update_with_filter : function(event){
        event.preventDefault();
        var self = this;
        self.initial_render = false;
        var output = {date_range:false};
        output.target_moves =  'posted';
        if($("span.date_filter-multiple1.selected").select2('data').length === 1){
        output.date_range = $("span.date_filter-multiple1.selected").select2('data')[0].id
        }
        if ($("#date_from").val()){
        var dateObject = $("#date_from").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_from = dateString;
        output.date_to = false;
        }
        if ($("#date_to").val()){
        var dateObject = $("#date_to").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_to = dateString;
        output.date_from = false;
        }
        if ($("#date_from").val() && $("#date_to").val()) {
        var dateObject = $("#date_from").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_from = dateString;
        var dateObject = $("#date_to").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_to = dateString;
        }

        var journal_ids = [];
        var journal_list = $("span.journal-multiple1.selected").select2('data')
        for (var i=0; i < journal_list.length; i++){
        journal_ids.push(parseInt(journal_list[i].id))
        }
        output.journal_ids = journal_ids
        var analytic_ids = [];
        var analytic_list = $("span.analytic-multiple1.selected").select2('data')
        for (var i=0; i < analytic_list.length; i++){
        analytic_ids.push(parseInt(analytic_list[i].id))
        }
        output.analytic_ids = analytic_ids
        var options_list = $("a.extra-multiple1.selected").select2('data')
        for (var i=0; i < options_list.length; i++){
        if(options_list[i].id === 'all_entry'){
        output.target_moves = 'all';
        }
        }
        self._rpc({
        model: 'financial.reports',
        method: 'write',
        args: [self.financial_report_id, output],
        }).then(function(res){
        self.plot_data(self.initial_render);
        });
        },
        print_pdf : function(e){
        e.preventDefault();
        var self = this;
        var action = {
        'type': 'ir.actions.report',
        'report_type': 'qweb-pdf',
        'report_name': 'openeducat_account_financial_reports.ins_report_financial',
        'report_file': 'openeducat_account_financial_reports.ins_report_financial',
        'data': {'js_data':{'account':self.account_data,'filter':self.filter_data},},
        'context': {'active_model':'financial.reports',
        'landscape':1,
        'from_js': true
        },

        };
        return self.do_action(action);
        },

        print_xlsx : function(){
        var self = this;
        self._rpc({
        model: 'financial.reports',
        method: 'action_xlsx',
        args: [[self.financial_report_id]],
        }).then(function(action){
        return self.do_action(action);
        });
        },

            });

                //GENERAL LEDGER//

    var LedgerReport = AbstractAction.extend({
        template: 'general_ledger',
        events: {
        'click #filter_apply_button': 'update_with_filter',
        'click .click_on_ledger_entry': '_OnClickEntry',
        'click #pdf': 'print_pdf',
        'click #xlsx': 'print_xlsx',
        'click .dropdown-item': '_setSelection',
        },
        init : function(view, code){
            this._super(view, code);
            this.financial_report_id = code.context.id | null;
            this.model_id = code.context.model | null;
            this.session = session;
        },
        start : function(){
            var self = this;
            self.initial_render = true;
            if(! self.financial_report_id){
            self._rpc({
            model: 'financial.general.ledger',
            method: 'create',
            args: [{res_model: this.res_model,},{}]
            }).then(function (record) {
            self.financial_report_id = record;
            self.plot_data(self.initial_render);
            })
            }else{
            self.plot_data(self.initial_render);
            }
            },
        _OnClickEntry: function(e){
            var id = $(e.currentTarget).data('id');
            var self = this;
            this._rpc({
                model:'account.move.line',
                method: 'search_read',
                kwargs: {
                    domain: [['id' ,'=', id]],
                    fields: ['move_id']
                }
            }).then(function(data){
                var move_id = data[0].move_id[0];
                self.do_action({
                    type: "ir.actions.act_window",
                    res_model: 'account.move',
                    res_id: move_id,
                    views: [[false, "form"]],
                    target: "current",
                    context: {},
                });
            });
        },
        _setSelection: function(e){

            if($(e.target).hasClass('selected')){
                $(e.target).removeClass('selected');
                this.update_with_filter(e);
            }else{
                if($('span.date_filter-multiple1.selected').length != 0){
                    $('span.date_filter-multiple1.selected').each(function (el) {
                        $(this).removeClass('selected');
                    });
                }
                $(e.target).addClass('selected');
                this.update_with_filter(e);
            }
        },

        formatWithSign : function(amount, formatOptions, sign){
        var currency_id = formatOptions.currency_id;
        currency_id = session.get_currency(currency_id);
        var without_sign = field_utils.format.monetary(Math.abs(amount), {}, formatOptions);
        if(!amount){return '-'};
        if (currency_id.position === "after") {
        return sign + '  ' + without_sign + '  ' + currency_id.symbol;
        } else {
        return currency_id.symbol + '  ' + sign + '  ' + without_sign;
        }
        return without_sign;
        },

        plot_data : function(initial_render = true){
            var self = this;
            var node = self.$('.py-data-container-orig');
            var last;
            while (last = node.lastChild) node.removeChild(last);
            self._rpc({
            model: 'financial.general.ledger',
            method: 'get_report_values',
            args: [[self.financial_report_id]],
            }).then(function (datas) {
            self.filter_data = datas[0]
            self.account_data = datas[1]

            _.each(self.account_data, function (k, v){
            var formatOptions = {
            currency_id: k.currency_id,
            noSymbol: true,
            };
            k.debit = self.formatWithSign(k.debit, formatOptions, k.debit < 0 ? '-' : '');
            k.credit = self.formatWithSign(k.credit, formatOptions, k.credit < 0 ? '-' : '');
            k.balance = self.formatWithSign(k.balance, formatOptions, k.balance < 0 ? '-' : '');
            k.ldate = field_utils.format.date(field_utils.parse.date(k.ldate, {}, {isUTC: true}));
            _.each(k.move_lines, function (ks, vs){
            ks.debit = self.formatWithSign(ks.debit, formatOptions, ks.debit < 0 ? '-' : '');
            ks.credit = self.formatWithSign(ks.credit, formatOptions, ks.credit < 0 ? '-' : '');
            ks.balance = self.formatWithSign(ks.balance, formatOptions, ks.balance < 0 ? '-' : '');
            ks.ldate = field_utils.format.date(field_utils.parse.date(ks.ldate, {}, {isUTC: true}));
            });
            });


            if(initial_render){
            self.$('.py-control-panel').html(QWeb.render('FilterSection', {
            filter_data : datas[0],
            }));

            self.$el.find('#date_from').datepicker({ dateFormat: 'dd-mm-yy' });
            self.$el.find('#date_to').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#ui-datepicker-div').click(function(e){e.stopPropagation();});
            self.$el.find('.date_filter-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Date...',
            });
            self.$el.find('.extra-multiple').select2({
            placeholder:'Extra Options...',
            })
            .val(['']).trigger('change');
            self.$el.find('.analytic-multiple').select2({
            placeholder:'Select Analytic...',
            });
            self.$el.find('.journal-multiple').select2({
            placeholder:'Select Journal...',
            });
        }
        self.$('.py-data-container-orig').html(QWeb.render('DataSection', {
            account_data : datas[1]
        }));
        }

        );
        },
        update_with_filter : function(event){
        event.preventDefault();
        var self = this;
        self.initial_render = false;
        var output = {date_range:false};
        output.display_accounts = 'all';
        output.target_moves = 'posted';
        var journal_ids = [];
        var journal_list = $("span.journal-multiple1.selected").select2('data')
        for (var i=0; i < journal_list.length; i++){
        journal_ids.push(parseInt(journal_list[i].id))
        }
        output.journal_ids = journal_ids
        var analytic_ids = [];
        var analytic_list = $("span.analytic-multiple1.selected").select2('data')
        for (var i=0; i < analytic_list.length; i++){
        analytic_ids.push(parseInt(analytic_list[i].id))
        }
        output.analytic_ids = analytic_ids
        if($("span.date_filter-multiple1.selected").select2('data').length === 1){
        output.date_range = $("span.date_filter-multiple1.selected").select2('data')[0].id
        }
        var options_list = $("a.extra-multiple1.selected").select2('data')
        for (var i=0; i < options_list.length; i++){
        if(options_list[i].id === 'not_zero'){
        output.display_accounts = 'not_zero';
        }
        if(options_list[i].id === 'all_entry'){
        output.target_moves = 'all';
        }
        }
        if ($("#date_from").val()){
        var dateObject = $("#date_from").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_from = dateString;
        }
        if ($("#date_to").val()){
        var dateObject = $("#date_to").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_to = dateString;
        }
        self._rpc({
        model: 'financial.general.ledger',
        method: 'write',
        args: [[self.financial_report_id], output],
        }).then(function(res){
        self.plot_data(self.initial_render);
        });
        },
        print_pdf : function(e){
        e.preventDefault();
        var self = this;
        var action = {
        'type': 'ir.actions.report',
        'report_type': 'qweb-pdf',
        'report_name': 'openeducat_account_financial_reports.general_ledger_template',
        'report_file': 'openeducat_account_financial_reports.general_ledger_template',
        'data': {'js_data':{'account':self.account_data,'filter':self.filter_data},},
        'context': {'active_model':'financial.general.ledger',
        'landscape':1,
        'from_js': true
        },
        'display_name': 'Finance Report',
        };
        return self.do_action(action);
        },
        print_xlsx : function(){
        var self = this;
        self._rpc({
        model: 'financial.general.ledger',
        method: 'action_xlsx',
        args: [[self.financial_report_id]],
        }).then(function(action){
        return self.do_action(action);
        });
        },

            });

                //PARTNER LEDGER//

    var PartnerReport = AbstractAction.extend({
        template: 'PartnerLedger',
        events: {
        'click #filter_apply_button': 'update_with_filter',
        'click .click_on_partner_entry': '_OnClickPartnerEntry',
        'click #pdf': 'print_pdf',
        'click #xlsx': 'print_xlsx',
        'click .dropdown-item': '_setSelection',
        },
        init : function(view, code){
            this._super(view, code);
            this.financial_report_id = code.context.id | null;
            this.model_id = code.context.model | null;
            this.session = session;
        },
        start : function(){
            var self = this;
            self.initial_render = true;
            if(! self.financial_report_id){
            self._rpc({
            model: 'financial.partner.ledger',
            method: 'create',
            args: [{res_model: this.res_model,},{}]
            }).then(function (record) {
            self.financial_report_id = record;
            self.plot_data(self.initial_render);
            })
            }else{
            self.plot_data(self.initial_render);
            }
        },
        _setSelection: function(e){

            if($(e.target).hasClass('selected')){
                $(e.target).removeClass('selected');
                this.update_with_filter(e);
            }else{
                if($('span.date_filter-multiple1.selected').length != 0){
                    $('span.date_filter-multiple1.selected').each(function (el) {
                        $(this).removeClass('selected');
                    });
                }
                $(e.target).addClass('selected');
                this.update_with_filter(e);
            }
        },
         _OnClickPartnerEntry: function(e){
            var id = $(e.currentTarget).data('id');
            var self = this;
            this._rpc({
                model:'account.move.line',
                method: 'search_read',
                kwargs: {
                    domain: [['id' ,'=', id]],
                    fields: ['move_id']
                }
            }).then(function(data){
                var move_id = data[0].move_id[0];
                self.do_action({
                    type: "ir.actions.act_window",
                    res_model: 'account.move',
                    res_id: move_id,
                    views: [[false, "form"]],
                    target: "current",
                    context: {},
                });
            });
        },

        formatWithSign : function(amount, formatOptions, sign){
        var currency_id = formatOptions.currency_id;
        currency_id = session.get_currency(currency_id);
        var without_sign = field_utils.format.monetary(Math.abs(amount), {}, formatOptions);
        if(!amount){return '-'};
        if (currency_id.position === "after") {
        return sign + '  ' + without_sign + '  ' + currency_id.symbol;
        } else {
        return currency_id.symbol + '  ' + sign + '  ' + without_sign;
        }
        return without_sign;
        },

        plot_data : function(initial_render = true){
            var self = this;
            var node = self.$('.py-data-container');
            var last;
            while (last = node.lastChild) node.removeChild(last);
            self._rpc({
            model: 'financial.partner.ledger',
            method: 'get_report_values',
            args: [[self.financial_report_id]],
            }).then(function (datas) {
            self.filter_data = datas[0]
            self.account_data = datas[1];

            _.each(self.account_data, function (k, v){
            var formatOptions = {
            currency_id: k.currency_id,
            noSymbol: true,
            };
            k.total_balance = self.formatWithSign(k.total_balance, formatOptions, k.total_balance < 0 ? '-' : '');
            k.total_credit = self.formatWithSign(k.total_credit, formatOptions, k.total_credit < 0 ? '-' : '');
            k.total_debit = self.formatWithSign(k.balance, formatOptions, k.balance < 0 ? '-' : '');
            k.ldate = field_utils.format.date(field_utils.parse.date(k.ldate, {}, {isUTC: true}));
            _.each(k.lines, function (ks, vs){
            ks.debit = self.formatWithSign(ks.debit, formatOptions, ks.debit < 0 ? '-' : '');
            ks.credit = self.formatWithSign(ks.credit, formatOptions, ks.credit < 0 ? '-' : '');
            ks.progress = self.formatWithSign(ks.progress, formatOptions, ks.progress < 0 ? '-' : '');
            ks.ldate = field_utils.format.date(field_utils.parse.date(ks.ldate, {}, {isUTC: true}));
            });
            });


            self.$('.py-data-container').html(QWeb.render('PartnerLedgerDS', {
            account_data : self.account_data,
            }));
            if(initial_render){
            self.$('.py-control-panel').html(QWeb.render('PartnerLedgerFS', {
            filter_data : self.filter_data,
            }));
            self.$el.find('#date_from').datepicker({ dateFormat: 'dd-mm-yy' });
            self.$el.find('#date_to').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#ui-datepicker-div').click(function(e){e.stopPropagation();});
            self.$el.find('select.date_filter-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Date...',
            });

            self.$el.find('select.type-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Account Type...',
            });
            self.$el.find('select.extra-multiple').select2({
            placeholder:'Extra Options...',
            })
            .val(['']).trigger('change');
            self.$el.find('select.partner-multiple').select2({
            placeholder:'Select Partner...',
            });
            self.$el.find('select.partner-tag-multiple').select2({
            placeholder:'Select Tag...',
            });
            }
            }
        );},
        update_with_filter : function(event){
        event.preventDefault();
        var self = this;
        self.initial_render = false;
        var output = {date_range:false};
        output.type = false;
        output.target_moves = 'posted';

        var partner_ids = [];
        var partner_list = $("span.partner-multiple1.selected").select2('data')
        for (var i=0; i < partner_list.length; i++){
        partner_ids.push(parseInt(partner_list[i].id))
        }
        output.partner_ids = partner_ids
        var partner_tag_ids = [];
        var partner_tag_list = $("span.partner-tag-multiple1.selected").select2('data')
        for (var i=0; i < partner_tag_list.length; i++){
        partner_tag_ids.push(parseInt(partner_tag_list[i].id))
        }
        output.partner_category_ids = partner_tag_ids

        if($("span.date_filter-multiple1.selected").select2('data').length === 1){
        output.date_range = $("span.date_filter-multiple1.selected").select2('data')[0].id}
        if($("a.type-multiple.selected").select2('data').length === 1){
        output.type = $("a.type-multiple.selected").select2('data')[0].id}

        var options_list = $("a.extra-multiple1.selected").select2('data')
        for (var i=0; i < options_list.length; i++){
        if(options_list[i].id === 'all_entry'){
        output.target_moves = 'all';
        }
        }
        if ($("#date_from").val()){
        var dateObject = $("#date_from").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_from = dateString;
        }
        if ($("#date_to").val()){
        var dateObject = $("#date_to").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_to = dateString;
        }
        self._rpc({
        model: 'financial.partner.ledger',
        method: 'write',
        args: [[self.financial_report_id], output],
        }).then(function(res){
        self.plot_data(self.initial_render);
        });
        },
        print_pdf : function(e){
        e.preventDefault();
        var self = this;
        var action = {
        'type': 'ir.actions.report',
        'report_type': 'qweb-pdf',
        'report_name': 'openeducat_account_financial_reports.partner_ledger_template',
        'report_file': 'openeducat_account_financial_reports.partner_ledger_template',
        'data': {'js_data':{'account':self.account_data,'filter':self.filter_data},},
        'context': {'active_model':'financial.trial.balance',
        'landscape':1,
        'from_js': true
        },
        };
        return self.do_action(action);
        },
        print_xlsx : function(){
        var self = this;
        self._rpc({
        model: 'financial.partner.ledger',
        method: 'action_xlsx',
        args: [[self.financial_report_id]],
        }).then(function(action){
        return self.do_action(action);
        });
        },
    });

             //AGED PARTNER BALANCE//

    var AgedPartner = AbstractAction.extend({
        template: 'AgedPartner',
        events: {
        'click #filter_apply_button': 'update_with_filter',
        'click .click_on_aged_entry': '_OnClickAgedEntry',
        'click #pdf': 'print_pdf',
        'click #xlsx': 'print_xlsx',
        'click .dropdown-item': '_setSelection',
        },
        init : function(view, code){
            this._super(view, code);
            this.financial_report_id = code.context.id | null;
            this.model_id = code.context.model | null;
            this.session = session;
        },
        start : function(){
            var self = this;
            self.initial_render = true;
            if(! self.financial_report_id){
            self._rpc({
            model: 'financial.aged.partner',
            method: 'create',
            args: [{res_model: this.res_model,},{}]
            }).then(function (record) {
            self.financial_report_id = record;
            self.plot_data(self.initial_render);
            })
            }else{
            self.plot_data(self.initial_render);
            }
        },
        _setSelection: function(e){

            if($(e.target).hasClass('selected')){
                $(e.target).removeClass('selected');
                this.update_with_filter(e);
            }else{
                if($('span.date_filter-multiple1.selected').length != 0){
                    $('span.date_filter-multiple1.selected').each(function (el) {
                        $(this).removeClass('selected');
                    });
                }
                $(e.target).addClass('selected');
                this.update_with_filter(e);
            }
        },
        _OnClickAgedEntry: function(e){
            var id = $(e.currentTarget).data('id');
            var self = this;
            this._rpc({
                model:'account.move.line',
                method: 'search_read',
                kwargs: {
                    domain: [['id' ,'=', id]],
                    fields: ['move_id']
                }
            }).then(function(data){
                var move_id = data[0].move_id[0];
                self.do_action({
                    type: "ir.actions.act_window",
                    res_model: 'account.move',
                    res_id: move_id,
                    views: [[false, "form"]],
                    target: "current",
                    context: {},
                });
            });
        },
        formatWithSign : function(amount, formatOptions, sign){
        var currency_id = formatOptions.currency_id;
        currency_id = session.get_currency(currency_id);
        var without_sign = field_utils.format.monetary(Math.abs(amount), {}, formatOptions);
        if(!amount){return '-'};
        if (currency_id.position === "after") {
        return sign + '  ' + without_sign + '  ' + currency_id.symbol;
        } else {
        return currency_id.symbol + '  ' + sign + '  ' + without_sign;
        }
        return without_sign;
        },
        plot_data : function(initial_render = true){
            var self = this;
            var node = self.$('.py-data-container');
            var last;
            while (last = node.lastChild) node.removeChild(last);
            self._rpc({
            model: 'financial.aged.partner',
            method: 'get_report_values',
            args: [[self.financial_report_id]],
            }).then(function (datas) {
            self.data = datas[0];
            self.total = datas[1]
            self.lines = datas[2]
            self.aged = datas[3]
            self.filter_data = datas[4]
            self.currency_id = datas[5]

            _.each(self.data, function (k, v){
            var formatOptions = {
            currency_id: self.currency_id,
            noSymbol: true,
            };
            k[0] = self.formatWithSign(k[0], formatOptions, k[0] < 0 ? '-' : '');
            k[1] = self.formatWithSign(k[1], formatOptions, k[1] < 0 ? '-' : '');
            k[2] = self.formatWithSign(k[2], formatOptions, k[2] < 0 ? '-' : '');
            k[3] = self.formatWithSign(k[3], formatOptions, k[3] < 0 ? '-' : '');
            k[4] = self.formatWithSign(k[4], formatOptions, k[4] < 0 ? '-' : '');
            k.direction = self.formatWithSign(k.direction, formatOptions, k.direction < 0 ? '-' : '');
            k.total = self.formatWithSign(k.total, formatOptions, k.total < 0 ? '-' : '');
            });

            var total_list = self.total
            for (var i=0; i < total_list.length; i++){
            var formatOptions = {
            currency_id: self.currency_id,
            noSymbol: true,
            };
            total_list[i] = self.formatWithSign(total_list[i], formatOptions, total_list[i] < 0 ? '-' : '');
            };

             _.each(self.lines, function (k, v){
            var formatOptions = {
            currency_id: self.currency_id,
            noSymbol: true,
            };
            _.each(k, function (ks, vs){
                  ks.amount = self.formatWithSign(ks.amount, formatOptions, ks.amount < 0 ? '-' : '');
                });
            });

            self.$('.py-data-container').html(QWeb.render('AgedPartnerDS', {
            data : self.data,
            total : self.total,
            lines : self.lines,
            aged : self.aged,
            }));
            if(initial_render){
            self.$('.py-control-panel').html(QWeb.render('AgedLedgerFS', {
            filter_data : self.filter_data,
            }));
            self.$el.find('#date_from').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#ui-datepicker-div').click(function(e){e.stopPropagation();});
            self.$el.find('.date_filter-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Date...',
            });
            self.$el.find('.extra-multiple').select2({
            placeholder:'Extra Options...',
            })
            .val(['']).trigger('change');
            self.$el.find('.type-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Account Type...',
            });
            }
            }
        );},
        update_with_filter : function(event){
        event.preventDefault();
        var self = this;
        self.initial_render = false;
        var output = {date_range:false};
        output.type = false;
        output.target_moves = 'posted';

        if($("span.date_filter-multiple1.selected").select2('data').length === 1){
        output.date_range = $("span.date_filter-multiple1.selected").select2('data')[0].id}
        if($("a.type-multiple1.selected").select2('data').length === 1){
        output.type = $("a.type-multiple1.selected").select2('data')[0].id}
        if ($("#date_from").val()){
        var dateObject = $("#date_from").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_from = dateString;
        }

        var options_list = $("a.extra-multiple1.selected").select2('data')
        for (var i=0; i < options_list.length; i++){
        if(options_list[i].id === 'all_entry'){
        output.target_moves = 'all';
        }
        }
        self._rpc({
        model: 'financial.aged.partner',
        method: 'write',
        args: [[self.financial_report_id], output],
        }).then(function(res){
        self.plot_data(self.initial_render);
        });
        },

        print_pdf : function(e){
        e.preventDefault();
        var self = this;
        var action = {
        'type': 'ir.actions.report',
        'report_type': 'qweb-pdf',
        'report_name': 'openeducat_account_financial_reports.aged_partner_ledger_template',
        'report_file': 'openeducat_account_financial_reports.aged_partner_ledger_template',
        'data': {'js_data':{'account':self.data,'filter':self.filter_data,'aged':self.aged,},'lines':self.lines},
        'context': {'active_model':'financial.aged.partner',
        'landscape':1,
        'from_js': true
        },
        };
        return self.do_action(action);
        },

        print_xlsx : function(){
        console.log("4444")
        var self = this;
        self._rpc({
        model: 'financial.aged.partner',
        method: 'action_xlsx',
        args: [[self.financial_report_id]],
        }).then(function(action){
        return self.do_action(action);
        });
        },
    });



                //TRIAL BALANCE//
    var TrialBalance = AbstractAction.extend({

        template: 'trial_balance',
        events: {
        'click #filter_apply_button': 'update_with_filter',
        'click .click_on_trial_entry': '_OnClickTrialEntry',
        'click #pdf': 'print_pdf',
        'click #xlsx': 'print_xlsx',
        'click .dropdown-item': '_setSelection',
        },
        init : function(view, code){
            this._super(view, code);
            this.financial_report_id = code.context.id | null;
            this.model_id = code.context.model | null;
            this.session = session;
        },
        start : function(){
            var self = this;
            self.initial_render = true;
            if(! self.financial_report_id){
            self._rpc({
            model: 'financial.trial.balance',
            method: 'create',
            args: [{res_model: this.res_model,}]
            }).then(function (record) {
            self.financial_report_id = record;
            self.plot_data(self.initial_render);
            })
            }else{
            self.plot_data(self.initial_render);
            }
        },
           _OnClickTrialEntry: function(e){
            var code = $(e.currentTarget).data('id');
            var self = this;
            this._rpc({
                model:'account.account',
                method: 'search_read',
                kwargs: {
                    domain: [['code' ,'=', code]],
                    }

            }).then(function(data){
                var move_id = data[0].id;
                self.do_action({
                    name: 'Journal Items',
                    type: "ir.actions.act_window",
                    res_model: 'account.move.line',
                    domain: [['account_id', '=', move_id]],
                    res_id: move_id,
                    views: [[false, "list"]],
                    target: "current",
                    context: {},
                });
            });
        },
        _setSelection: function(e){

            if($(e.target).hasClass('selected')){
                $(e.target).removeClass('selected');
                this.update_with_filter(e);
            }else{
                if($('span.date_filter-multiple1.selected').length != 0){
                    $('span.date_filter-multiple1.selected').each(function (el) {
                        $(this).removeClass('selected');
                    });
                }
                $(e.target).addClass('selected');
                this.update_with_filter(e);
            }
        },
        formatWithSign : function(amount, formatOptions, sign){
        var currency_id = formatOptions.currency_id;
        currency_id = session.get_currency(currency_id);
        var without_sign = field_utils.format.monetary(Math.abs(amount), {}, formatOptions);
        if(!amount){return '-'};
        if (currency_id.position === "after") {
        return sign + '  ' + without_sign + '  ' + currency_id.symbol;
        } else {
        return currency_id.symbol + '  ' + sign + '  ' + without_sign;
        }
        return without_sign;
        },
        plot_data : function(initial_render = true){
            var self = this;
            var node = self.$('.py-data-container');
            var last;
            while (last = node.lastChild) node.removeChild(last);
            self._rpc({
            model: 'financial.trial.balance',
            method: 'get_report_values',
            args: [[self.financial_report_id]],
            }).then(function (datas) {
            self.filter_data = datas[0];
            self.account_data = datas[1];
            self.currency_id = datas[2];

            _.each(self.account_data, function (k, v){
            var formatOptions = {
            currency_id: self.currency_id,
            noSymbol: true,
            };
            k.initial_balance = self.formatWithSign(k.initial_balance, formatOptions, k.initial_balance < 0 ? '-' : '');
            k.initial_credit = self.formatWithSign(k.initial_credit, formatOptions, k.initial_credit < 0 ? '-' : '');
            k.initial_debit = self.formatWithSign(k.initial_debit, formatOptions, k.initial_debit < 0 ? '-' : '');
            k.debit = self.formatWithSign(k.debit, formatOptions, k.debit < 0 ? '-' : '');
            k.credit = self.formatWithSign(k.credit, formatOptions, k.credit < 0 ? '-' : '');
            k.balance = self.formatWithSign(k.balance, formatOptions, k.balance < 0 ? '-' : '');
            k.ending_balance = self.formatWithSign(k.ending_balance, formatOptions, k.ending_balance < 0 ? '-' : '');
            k.ending_credit = self.formatWithSign(k.ending_credit, formatOptions, k.ending_credit < 0 ? '-' : '');
            k.ending_debit = self.formatWithSign(k.ending_debit, formatOptions, k.ending_debit < 0 ? '-' : '');
            });

            if(initial_render){
            self.$('.py-control-panel').html(QWeb.render('TrailBalanceFS', {
            filter_data : self.filter_data,
            }));
            self.$el.find('#date_from').datepicker({ dateFormat: 'dd-mm-yy' });
            self.$el.find('#date_to').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#ui-datepicker-div').click(function(e){e.stopPropagation();});
            self.$el.find('.date_filter-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Date...',
            });
            self.$el.find('.extra-multiple').select2({
            placeholder:'Extra Options...',
            }).val('not_zero').trigger('change');
            self.$el.find('.analytic-multiple').select2({
            placeholder:'Select Analytic...',
            });
            self.$el.find('.journal-multiple').select2({
            placeholder:'Select Journal...',
            });
            self.$el.find('.account-multiple').select2({
            placeholder:'Select Account...',
            });
            }
            self.$('.py-data-container').html(QWeb.render('TrialBalanceDS', {
            account_data : self.account_data,
            filter_data : self.filter_data,
            }));
          }
        );},
        update_with_filter : function(event){
        event.preventDefault();
        var self = this;
        self.initial_render = false;
        var output = {date_range:false};
        output.display_accounts = 'all';
        output.target_moves = 'posted';

        var journal_ids = [];
        var journal_list = $("span.journal-multiple1.selected").select2('data')
        for (var i=0; i < journal_list.length; i++){
        journal_ids.push(parseInt(journal_list[i].id))
        }
        output.journal_ids = journal_ids
        var account_ids = [];
        var account_list = $("span.account-multiple1.selected").select2('data')
        for (var i=0; i < account_list.length; i++){
        account_ids.push(parseInt(account_list[i].id))
        }
        output.account_ids = account_ids
        var analytic_ids = [];
        var analytic_list = $("span.analytic-multiple1.selected").select2('data')
        for (var i=0; i < analytic_list.length; i++){
        analytic_ids.push(parseInt(analytic_list[i].id))
        }
        output.analytic_ids = analytic_ids
        if($("span.date_filter-multiple1.selected").select2('data').length === 1){
        output.date_range = $("span.date_filter-multiple1.selected").select2('data')[0].id
        }
        var options_list = $("a.extra-multiple1.selected").select2('data')
        for (var i=0; i < options_list.length; i++){
        if(options_list[i].id === 'not_zero'){
        output.display_accounts = 'not_zero';
        }
        if(options_list[i].id === 'all_entry'){
        output.target_moves = 'all';
        }
        }
        if ($("#date_from").val()){
        var dateObject = $("#date_from").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_from = dateString;
        }
        if ($("#date_to").val()){
        var dateObject = $("#date_to").datepicker("getDate");
        var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
        output.date_to = dateString;
        }
        self._rpc({
        model: 'financial.trial.balance',
        method: 'write',
        args: [self.financial_report_id, output],
        }).then(function(res){
        self.plot_data(self.initial_render);
        });},
        print_pdf : function(e){
        e.preventDefault();
        var self = this;
        var action = {
        'type': 'ir.actions.report',
        'report_type': 'qweb-pdf',
        'report_name': 'openeducat_account_financial_reports.trial_balance_template',
        'report_file': 'openeducat_account_financial_reports.trial_balance_template',
        'data': {'js_data':{'account':self.account_data,'filter':self.filter_data},},
        'context': {'active_model':'financial.trial.balance',
        'landscape':1,
        'from_js': true
        },
        };
        return self.do_action(action);
        },

        print_xlsx : function(){
        var self = this;
        self._rpc({
        model: 'financial.trial.balance',
        method: 'action_xlsx',
        args: [[self.financial_report_id]],
        }).then(function(action){
        return self.do_action(action);
        });
        },

            });

var TaxReport = AbstractAction.extend({

        template: 'tax_report',
        events: {
        'click #filter_apply_button': 'update_with_filter',
        'click .click_on_tax_type': '_OnClickTaxType',
        'click .click_on_generic_tax_type': '_OnClickGenericTaxType',
        'click #pdf': 'print_pdf',
        'click #xlsx': 'print_xlsx',
        'click .dropdown-item': '_setSelection',
        },
        init : function(view, code){
            this._super(view, code);
            this.financial_report_id = code.context.id | null;
            this.model_id = code.context.model | null;
            this.session = session;
        },
        start : function(){
            var self = this;
            self.initial_render = true;
            if(! self.financial_report_id){
            self._rpc({
            model: 'financial.tax.report',
            method: 'create',
            args: [{res_model: this.res_model,},{}]
            }).then(function (record) {
            self.financial_report_id = record;
            self.plot_data(self.initial_render);
            })
            }else{
            self.plot_data(self.initial_render);
            }
        },
        _OnClickTaxType: function(e){
            var tax = $(e.currentTarget).data('id');
            var date_from = $("input[name='date_from']").val()
            var date_to = $("input[name='date_to']").val()
            var self = this;
            var domain = [];
            if(date_from){
                domain.push(['date', '>=', date_from])
            }
            if(date_to){
                domain.push(['date', '<=', date_to])
            }
            this._rpc({
                model:'account.tax',
                method: 'search_read',
                kwargs: {
                    domain: [['name' ,'=', tax]],
                    }
            }).then(function(data){
                var move_id = data[0].id;
                domain.push('|', ['tax_ids', '=', move_id],['tax_line_id','=', move_id]);
                self.do_action({
                    name: 'Journal Items for Tax Audit',
                    type: "ir.actions.act_window",
                    res_model: 'account.move.line',
                    domain: domain,
                    res_id: move_id,
                    views: [[false, "list"],[false, "form"]],
                    target: "current",
                    context: {'tree_view_ref': 'account.view_move_line_tax_audit_tree'},
                });
            });
        },

        _setSelection: function(e){

            if($(e.target).hasClass('selected')){
                $(e.target).removeClass('selected');
                this.update_with_filter(e);
            }else{
                if($('span.date_filter-multiple1.selected').length != 0){
                    $('span.date_filter-multiple1.selected').each(function (el) {
                        $(this).removeClass('selected');
                    });
                }
                $(e.target).addClass('selected');
                this.update_with_filter(e);
            }
        },

         _OnClickGenericTaxType: function(e){
            var query = $(e.currentTarget).data('id');
            var parent = $(e.currentTarget).data('type');
            var date_from = $("input[name='date_from']").val()
            var date_to = $("input[name='date_to']").val()
            var self = this;
            var domain = [];
            var main = [];
            this._rpc({
                model:'account.tax.repartition.line',
                method: 'search_read',
                kwargs: {
                    domain: [['tag_ids', '=', query]]
                    }
            }).then(function(data){
                var move_id = []
                 _.each(data, function (k, v){
                    move_id.push(k.tax_id[0])
                 });
                if(parent == 'Taxes'){
                domain.push(['tax_line_id','in', move_id]);
                }
                else{
                domain.push('|', ['tax_ids', 'in', move_id],['tax_line_id','in', move_id]);
                }
                self.do_action({
                    name: 'Journal Items for Tax Audit',
                    type: "ir.actions.act_window",
                    res_model: 'account.move.line',
                    domain: domain,
                    res_id: move_id,
                    views: [[false, "list"],[false, "form"]],
                    target: "current",
                    context: {'tree_view_ref': 'account.view_move_line_tax_audit_tree'},
                });
            });
        },

        formatWithSign : function(amount, formatOptions, sign){
        var currency_id = formatOptions.currency_id;
        currency_id = session.get_currency(currency_id);
        var without_sign = field_utils.format.monetary(Math.abs(amount), {}, formatOptions);
        if(!amount){

        return without_sign + '  ' + currency_id.symbol};
        if (currency_id.position === "after") {
        return sign + '  ' + without_sign + '  ' + currency_id.symbol;
        } else {
        return currency_id.symbol + '  ' + sign + '  ' + without_sign;
        }
        return without_sign;
        },
        plot_data : function(initial_render = true){
            var self = this;
            var node = self.$('.py-data-container-orig');
            var last;
            while (last = node.lastChild) node.removeChild(last);
            self._rpc({
            model: 'financial.tax.report',
            method: 'get_report_values',
            args: [[self.financial_report_id]],
            }).then(function (datas) {
            self.filter_data = datas[0]
            self.account_data = datas[1]

            if(self.filter_data['report_type'] == 'global'){
             _.each(self.account_data, function (k, v){
            var formatOptions = {
            currency_id: self.filter_data['currency_id'],
            noSymbol: true,
            };
            _.each(k, function (amt, int){
            amt.tax = self.formatWithSign(amt.tax, formatOptions, amt.tax < 0 ? '-' : '');
            amt.net = self.formatWithSign(amt.net, formatOptions, amt.net < 0 ? '-' : '');
            });
            });

            }else{
             _.each(self.account_data, function (k, v){
            var formatOptions = {
            currency_id: self.filter_data['currency_id'],
            noSymbol: true,
            };
                if(k.amount == 0 | k.amount){
                k.amount = self.formatWithSign(k.amount, formatOptions, k.amount <= 0 ? '-' : '');
                }
            });
            }

            if(initial_render){
            self.$('.py-control-panel').html(QWeb.render('TaxReportFS', {
            filter_data : self.filter_data,
            }));
            self.$el.find('#date_from').datepicker({ dateFormat: 'dd-mm-yy' });
            self.$el.find('#date_to').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#ui-datepicker-div').click(function(e){e.stopPropagation();});
            self.$el.find('.date_filter-multiple').select2({
            maximumSelectionSize: 1,
            placeholder:'Select Date...',
            });
            if(self.filter_data['report_type'] == 'global'){
            self.$el.find('.extra-multiple').select2({
            placeholder:'Extra Options...',
            }).val('global').trigger('change');
            }else{
            self.$el.find('.extra-multiple1').select2({
            placeholder:'Extra Options...',
            }).val('tax_report').trigger('change');
            }
            }
            if(self.filter_data['report_type'] == 'global'){
            self.$('.py-data-container').html(QWeb.render('TaxReportDS', {
            account_data : self.account_data,
            filter_data : self.filter_data,
            }));
            }else{
            self.$('.py-data-container').html(QWeb.render('GenericTaxReportDS', {
            account_data : self.account_data,
            filter_data : self.filter_data,
            }));
            }
         }
        );
        },
            update_with_filter : function(event){
            event.preventDefault();
            var self = this;
            self.initial_render = false;
            var output = {date_range:false};

            if($("span.date_filter-multiple1.selected").select2('data').length === 1){
            output.date_range = $("span.date_filter-multiple1.selected").select2('data')[0].id
            }
            if ($("#date_from").val()){
            var dateObject = $("#date_from").datepicker("getDate");
            var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
            output.date_from = dateString;
            }
            if ($("#date_to").val()){
            var dateObject = $("#date_to").datepicker("getDate");
            var dateString = $.datepicker.formatDate("yy-mm-dd", dateObject);
            output.date_to = dateString;
            }
            var options_list = $("a.extra-multiple1.selected").select2('data')
            for (var i=0; i < options_list.length; i++){
            if(options_list[i].id === 'global'){
            output.report_type = 'global';
            }
            if(options_list[i].id === 'tax_report'){
            output.report_type = 'tax_report';
            }
            }

            self._rpc({
            model: 'financial.tax.report',
            method: 'write',
            args: [self.financial_report_id, output],
            }).then(function(res){
            self.plot_data(self.initial_render);
            });},

            print_pdf : function(e){
            e.preventDefault();
            var self = this;
            var action = {
            'type': 'ir.actions.report',
            'report_type': 'qweb-pdf',
            'report_name': 'openeducat_account_financial_reports.generic_tax_template',
            'report_file': 'openeducat_account_financial_reports.generic_tax_template',
            'data': {'js_data':{'account':self.account_data,'filter':self.filter_data},},
            'context': {'active_model':'financial.tax.report',
            'landscape':1,
            'from_js': true
            },
            };
            return self.do_action(action);
            },

            print_xlsx : function(){
            var self = this;
            self._rpc({
            model: 'financial.tax.report',
            method: 'action_xlsx',
            args: [[self.financial_report_id]],
            }).then(function(action){
            return self.do_action(action);
            });
            },
      });

core.action_registry.add('financial.report', FinancialReport);
core.action_registry.add('ledger.report', LedgerReport);
core.action_registry.add('partner.report', PartnerReport);
core.action_registry.add('trial.balance', TrialBalance);
core.action_registry.add('aged.partner', AgedPartner);
core.action_registry.add('tax.report', TaxReport)
});
