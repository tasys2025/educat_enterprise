# Part of odoo See LICENSE file for full copyright and licensing details.

{
    'name': 'OpenEduCat Helpdesk Forum',
    'version': '16.0.1.0',
    'summary': '''Helpdesk Forum will provide you forum per
helpdesk team''',
    'category': 'Human Resources',
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'depends': ['openeducat_core_enterprise',
                'openeducat_helpdesk_basic', 'website_forum'],
    'data': [
        'views/helpdesk_team_view.xml',
        'views/helpdesk_ticket_view.xml',
    ],

    'installable': True,
    'auto_install': False,
    'license': 'Other proprietary',
}
