
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################
from odoo import http
from odoo.http import request, content_disposition
from odoo import _, SUPERUSER_ID
from odoo.tools import consteq
from odoo.exceptions import AccessError, MissingError, UserError
from odoo.addons.portal.controllers.portal import CustomerPortal


class FeesPlan(CustomerPortal):

    def _document_check_access(self, model_name, document_id, access_token=None):
        document = request.env[model_name].browse([document_id])
        document_sudo = document.with_user(SUPERUSER_ID).exists()
        if not document_sudo:
            raise MissingError(_("This document does not exist."))
        try:
            document.check_access_rights('read')
            document.check_access_rule('read')
        except AccessError:
            if not access_token or not document_sudo.access_token or not consteq(
                    document_sudo.access_token, access_token):
                raise
        return document_sudo

    def _show_report(self, model, report_type, report_ref, download=False):
        if report_type not in ('html', 'pdf', 'text'):
            raise UserError(_("Invalid report type: %s", report_type))

        report_sudo = request.env.ref(report_ref).sudo()

        if not isinstance(report_sudo, type(request.env['ir.actions.report'])):
            raise UserError(_("%s is not the reference of a report", report_ref))

        method_name = '_render_qweb_%s' % (report_type)
        report = getattr(report_sudo, method_name)(
            report_ref, [model.id], data={'report_type': report_type})[0]
        reporthttpheaders = [
            ('Content-Type',
             'application/pdf' if report_type == 'pdf' else 'text/html'),
            ('Content-Length', len(report)),
        ]
        if report_type == 'pdf' and download:
            filename = "%s.pdf" % (model.student_id.name + '- Fees Details')
            reporthttpheaders.append(
                ('Content-Disposition', content_disposition(filename)))
        return request.make_response(
            report, headers=reporthttpheaders)

    @http.route(['/fees-plan/report/download/<int:plan_id>'],
                type='http', auth="user", website=True)
    def portal_fees_report_download(self, plan_id, report_type=None, access_token=None,
                                    download=False, **kw):

        try:
            fees_sudo = self._document_check_access('op.fees.plan',
                                                    plan_id, access_token=access_token)
        except (AccessError, MissingError):
            return request.redirect('/student/profile#fees_details')

        if report_type in ('html', 'pdf', 'text'):
            return self._show_report(model=fees_sudo, report_type=report_type,
                                     report_ref='openeducat_fees_plan.'
                                                'action_report_advance_fees_report',
                                     download=download)
        return request.redirect('/student/profile#fees_details')
