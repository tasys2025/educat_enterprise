# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenEduCat Inc
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError


class OpFeesFessPlan(models.Model):
    _name = "op.fees.plan"
    _inherit = "mail.thread"
    _description = "Fees Plan for Student Fees"
    _rec_name = "name"

    name = fields.Char('Fees Plan',
                       index=True, default=lambda self: _('FEES-PLAN'))
    fees_term_id = fields.Many2one('op.fees.terms', 'Fees Terms',
                                   ondelete="cascade", tracking=True)
    student_id = fields.Many2one('op.student', 'Student',
                                 ondelete="cascade", tracking=True)
    fees_plan_line_ids = fields.One2many('op.fees.plan.line', 'fees_plan_id', 'Lines')
    total_amount = fields.Float('Total Amount', readonly=True)
    paid_amount = fields.Float('Paid Amount', compute="_compute_paid_amount")
    remaining_amount = fields.Float('Remaining Amount', readonly=True)
    product_id = fields.Many2one(
        'product.product', 'Course Fees', required=True,
        domain=[('type', '=', 'service')], tracking=True)
    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env.company)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('invoice', 'Invoice Created'),
        ('cancel', 'Cancel')
    ], string='Status', copy=False, default='draft', tracking=True)


    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            if vals.get('name', '/') == '/':
                vals['name'] = self.env['ir.sequence'].next_by_code('op.fees.plan') or '/'
            res = super(OpFeesFessPlan, self).create(vals)
            res.name = "FEES-PLAN" + '-' + str(res.id)
        return res

    def _compute_paid_amount(self):
        for res in self:
            total_amount = sum(res.fees_plan_line_ids.mapped('amount'))
            paid_amount = sum(res.fees_plan_line_ids.mapped('paid_amount'))
            res.paid_amount = paid_amount
            res.remaining_amount = total_amount - paid_amount
            res.total_amount = total_amount

    def send_reminder_mail_student(self):
        template = self.env.ref('openeducat_fees_plan.fees_reminder_template_student')
        for rec in self:
            template.send_mail(rec.id)


class OpFeesPlanLine(models.Model):
    _name = "op.fees.plan.line"
    _description = "Fees Details Line"
    _rec_name = "student_id"

    due_days = fields.Integer('Due Days')
    due_date = fields.Date('Due Date')
    fees_factor = fields.Float('Value (%)')
    fees_plan_id = fields.Many2one('op.fees.plan', 'Fees')
    student_id = fields.Many2one('op.student', 'Student', ondelete="cascade")
    product_id = fields.Many2one('product.product', 'Product')
    invoice_id = fields.Many2one('account.move', 'Invoice')
    invoice_state = fields.Selection(related="invoice_id.state",
                                     string='Invoice Status',
                                     readonly=True)
    op_fees_terms_line_id = fields.Many2one('op.fees.terms.line', 'Invoice ID')
    course_id = fields.Many2one(
        'op.course', 'Course')
    batch_id = fields.Many2one(
        'op.batch', 'Batch')
    invoice_date = fields.Date("Date")
    amount = fields.Float("Amount", required=True)
    paid_amount = fields.Float("Paid Amount", compute="_compute_paid_amount")
    fees_element_line_ids = fields.One2many('op.fees.element.plan', 'fees_plan_line_id',
                                            string='Lines')
    fees_reminder_line_ids = fields.Many2many('op.fees.template.line',
                                              string='Fees Reminder Lines')
    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env.company)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('invoice', 'Invoice Created'),
        ('cancel', 'Cancel')
    ], string='Status', copy=False)

    def get_invoice(self):
        """ Create invoice for fee payment process of student """
        inv_obj = self.env['account.move']
        partner_id = self.fees_plan_id.student_id.partner_id
        account_id = False
        if not self.fees_element_line_ids:
            raise ValidationError(_("Product should be available to create invoice !"))
        product = self.fees_element_line_ids[0].product_id
        if product.property_account_income_id:
            account_id = product.property_account_income_id.id
        if not account_id:
            account_id = product.categ_id.property_account_income_categ_id.id
        if not account_id:
            raise UserError(
                _('There is no income account defined for this product: "%s".'
                  'You may have to install a chart of account from Accounting'
                  ' app, settings menu.') % product.name)
        if self.amount <= 0.00:
            raise UserError(
                _('The value of the deposit amount must be positive.'))
        else:
            amount = self.amount
            name = product.name
        invoice_line_list = []
        if self.fees_element_line_ids:
            for records in self.fees_element_line_ids:
                invoice_line_list.append((0, 0, {
                    'name': records.product_id.name,
                    'account_id': account_id,
                    'price_unit': records.value * self.amount / 100,
                    'quantity': 1.0,
                    # 'discount': self.discount or False,
                    'product_uom_id': records.product_id.uom_id.id,
                    'product_id': records.product_id.id,
                }))
        else:
            invoice_line_list.append((0, 0, {
                'name': name,
                'account_id': account_id,
                'price_unit': amount,
                'quantity': 1.0,
                'product_uom_id': product.uom_id.id,
                'product_id': product.id
            }))
        invoice = inv_obj.create({
            'move_type': 'out_invoice',
            'partner_id': partner_id.id,
            'invoice_line_ids': invoice_line_list,
        })
        # invoice._compute_tax_totals_json()
        self.state = 'invoice'
        self.invoice_id = invoice.id
        return True

    def _compute_paid_amount(self):
        for res in self:
            if res.invoice_id:
                amount = res.invoice_id.amount_total
                residual_amount = res.invoice_id.amount_residual
                res.amount = amount
                res.paid_amount = amount - residual_amount
            else:
                res.paid_amount = 0

    def action_get_invoice(self):
        value = True
        if self.invoice_id:
            form_view = self.env.ref('account.view_move_form')
            tree_view = self.env.ref('account.view_invoice_tree')
            value = {
                'domain': str([('id', '=', self.invoice_id.id)]),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.move',
                'view_id': False,
                'views': [(form_view and form_view.id or False, 'form'),
                          (tree_view and tree_view.id or False, 'tree')],
                'type': 'ir.actions.act_window',
                'res_id': self.invoice_id.id,
                'target': 'current',
                'nodestroy': True
            }
        return value

    def _cron_create_invoice(self):
        fees_ids = self.env['op.fees.plan.line'].search(
            [('invoice_date', '<', datetime.today()), ('invoice_id', '=', False)])
        for fees in fees_ids:
            fees.get_invoice()

    def action_create_invoice(self):
        for fees in self:
            if not fees.invoice_id and fees.state == 'draft':
                fees.get_invoice()


class OpFeesElementPlanLine(models.Model):
    _name = "op.fees.element.plan"
    _description = "Fees Element Plan"

    sequence = fields.Integer('Sequence')
    product_id = fields.Many2one('product.product',
                                 'Product(s)', required=True)
    value = fields.Float('Value (%)')
    fees_plan_line_id = fields.Many2one('op.fees.plan.line',
                                        string='Fees Plan Line')
