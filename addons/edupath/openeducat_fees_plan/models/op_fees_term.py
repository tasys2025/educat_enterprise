# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models, fields, api
from datetime import timedelta, datetime


class OpFeesTermsInherit(models.Model):
    _inherit = "op.fees.terms"

    fees_terms = fields.Selection(
        selection_add=[('duration_based', 'Duration Based Fees'),
                       ('session_based', 'Session Based Fees'),
                       ('faculty_based', 'Faculty Session Based Fees')
                       ],
        string='Term Type', default='fixed_days')
    expires_after = fields.Integer(string='Invoice Cycles',
                                   help="Expires after number of Invoices")
    term_duration = fields.Integer(default=1)
    bill_selection = fields.Selection([('days', 'Day'),
                                       ('weeks', 'Week'),
                                       ('months', 'Month'), ('years', 'Year')],
                                      string="Bill Every",
                                      help='Generate Invoice Every ('
                                           'Day/Week/Month/Year)')
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.user.company_id)

    @api.model
    def run_send_fees_reminder(self):
        fees_line_ids = self.env['op.fees.plan.line'].search(
            [('state', '=', 'draft')])
        for line in fees_line_ids:
            if not line.fees_element_line_ids \
                    or not line.invoice_date \
                    or not line.fees_plan_id.student_id:
                continue
            submit_date = line.invoice_date

            for reminders in line.fees_reminder_line_ids:
                if not reminders.template_id:
                    continue
                days = reminders.days or 0
                if reminders.duration_type == 'before':
                    days = days * (-1)
                ddate = fields.Date.from_string(submit_date)
                mail_date = ddate + timedelta(days)
                current_date = datetime.today().date()
                if mail_date == current_date:
                    reminders.template_id.send_mail(line.id, force_send=True)
        return True


class OpFeesTermsLine(models.Model):
    _inherit = "op.fees.terms.line"

    line_ids = fields.One2many('op.fees.template.line', 'line_id', 'Lines')
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.user.company_id)


class OpFeesElementLine(models.Model):
    _inherit = "op.fees.element"

    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.user.company_id)


class OpFeesTemplateLine(models.Model):
    _name = "op.fees.template.line"
    _description = "Fees Template Line"

    line_id = fields.Many2one('op.fees.terms.line', 'Fees Line')
    duration_type = fields.Selection([
        ('before', 'Before'), ('after', 'After')], 'Duration Type')
    days = fields.Integer('Days')
    template_id = fields.Many2one('mail.template', 'Template')
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.user.company_id)
