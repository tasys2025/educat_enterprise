# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenEduCat Inc
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from . import models
from . import controllers

from odoo import api, SUPERUSER_ID, _
from odoo.exceptions import ValidationError


def _fees_plan_pre_init(cr):
    env = api.Environment(cr, SUPERUSER_ID, {})
    fees_module = env["ir.module.module"].search(
        [('name', '=', 'openeducat_fees_enterprise'),
         ('state', '=', 'installed')])
    if fees_module:
        raise ValidationError(
            _("Fees Enterprise Module is already installed."
              " So, You are not allowed to install this module."))
