odoo.define('openeducat_dynamic_admission.form', function (require) {
'use strict';

var core = require('web.core');
var FormEditorRegistry = require('website.form_editor_registry');

var _t = core._t;

FormEditorRegistry.add('create_admission', {
    formFields: [{
        type: 'char',
        modelRequired: true,
        name: 'name',
        fillWith: 'name',
        string: 'Name',
    }, {
        type: 'char',
        required: true,
        fillWith: 'first_name',
        name: 'first_name',
        string: 'First Name',
    }, {
        type: 'char',
        required: true,
        fillWith: 'last_name',
        name: 'last_name',
        string: 'Last Name',
    }, {
        type: 'date',
        required: true,
        fillWith: 'birth_date',
        name: 'birth_date',
        string: 'Birth Date',
    }, {
        type: 'selection',
        required: true,
        fillWith: 'gender',
        selection: [
                        ['m', _t('Male')],
                        ['f', _t('Female')],
                        ['o', _t('Other')],
                    ],
        name: 'gender',
        string: 'Gender',
    }, {
        type: 'char',
        required: true,
        fillWith: 'phone',
        name: 'phone',
        string: 'Phone',
    }, {
//        type: 'char',
//        required: true,
//        fillWith: 'mobile',
//        name: 'mobile',
//        string: 'Mobile',
//    }, {
        type: 'email',
        required: true,
        fillWith: 'email',
        name: 'email',
        string: 'Email',
    }, {
        name: 'register_id',
        type: 'hidden',
        string: _t('Register'),
    }, {
        name: 'course_id',
        type: 'hidden',
        string: _t('Course'),
    }],
    successPage: '/admission-success',
    className: 'o_we_user_value_widget active',
});

});
