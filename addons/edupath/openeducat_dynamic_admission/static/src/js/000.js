odoo.define('website.s_website_form_inherit', function (require) {
    'use strict';

    require('website.s_website_form');
    var core = require('web.core');
    var time = require('web.time');
    const {ReCaptcha} = require('google_recaptcha.ReCaptchaV3');
    const session = require('web.session');
    var ajax = require('web.ajax');
    var publicWidget = require('web.public.widget');
    const dom = require('web.dom');
    const concurrency = require('web.concurrency');

    var _t = core._t;
    var qweb = core.qweb;

    publicWidget.registry.s_website_form.include({
        start: function () {
            var self = this;
            return this._super.apply(this, arguments).then(()=>{
//                self._updateAdmissionData()
            });
        },

        _updateAdmissionData: async function() {
            var tag_name = $(this.target).find('select')
            var select_value = $(this.target).find('select').val()
            var data = []
            var default_data = {}
            tag_name.each(function (index) {
                data.push($(this).attr('name'))
                default_data[$(this).attr('name')] = $(this).val()
            });
            ajax.jsonRpc("/admission-form/update", 'call', {
                data: data,
            }).then((res) => {
                for (const [key, value] of Object.entries(res)) {
                    $('select[name="' + key + '"]').empty()
                    for (const [opt_key, opt_value] of Object.entries(value)) {
                            var temp = '<option value="'+opt_value.id+'">'+opt_value.name+'</option>'
                            $('select[name="' + key + '"]').append(temp)
                        $('select[name="' + key + '"]').find('option[value='+opt_value.id+']').length
                    }
                }
                for (const [key, value] of Object.entries(default_data)) {
                    $(this.target).find('select[name='+key+']').val(value)
                }
            });
        },
        
         send: async function (e) {
            e.preventDefault(); // Prevent the default submit behavior
             // Prevent users from crazy clicking
            const $button = this.$target.find('.s_website_form_send, .o_website_form_send');
            $button.addClass('disabled') // !compatibility
                   .attr('disabled', 'disabled');
            this.restoreBtnLoading = dom.addButtonLoadingEffect($button[0]);

            var self = this;

            self.$target.find('#s_website_form_result, #o_website_form_result').empty(); // !compatibility
            if (!self.check_error_fields({})) {
                self.update_status('error', _t("Please fill in the form correctly."));
                return false;
            }

            // Prepare form inputs
            this.form_fields = this.$target.serializeArray();
            $.each(this.$target.find('input[type=file]'), function (outer_index, input) {
                $.each($(input).prop('files'), function (index, file) {
                    // Index field name as ajax won't accept arrays of files
                    // when aggregating multiple files into a single field value
                    self.form_fields.push({
                        name: input.name + '[' + outer_index + '][' + index + ']',
                        value: file
                    });
                });
            });

            // Serialize form inputs into a single object
            // Aggregate multiple values into arrays
            var form_values = {};
            _.each(this.form_fields, function (input) {
                if (input.name in form_values) {
                    // If a value already exists for this field,
                    // we are facing a x2many field, so we store
                    // the values in an array.
                    if (Array.isArray(form_values[input.name])) {
                        form_values[input.name].push(input.value);
                    } else {
                        form_values[input.name] = [form_values[input.name], input.value];
                    }
                } else {
                    if (input.value !== '') {
                        form_values[input.name] = input.value;
                    }
                }
            });

            // force server date format usage for existing fields
            this.$target.find('.s_website_form_field:not(.s_website_form_custom)')
            .find('.s_website_form_date, .s_website_form_datetime').each(function () {
                var date = $(this).datetimepicker('viewDate').clone().locale('en');
                var format = 'YYYY-MM-DD';
                if ($(this).hasClass('s_website_form_datetime')) {
                    date = date.utc();
                    format = 'YYYY-MM-DD HH:mm:ss';
                }
                form_values[$(this).find('input').attr('name')] = date.format(format);
            });

            if (this._recaptchaLoaded) {
                const tokenObj = await this._recaptcha.getToken('website_form');
                if (tokenObj.token) {
                    form_values['recaptcha_token_response'] = tokenObj.token;
                } else if (tokenObj.error) {
                    self.update_status('error', tokenObj.error);
                    return false;
                }
            }

            // Post form and handle result
            ajax.post(this.$target.attr('action') + (this.$target.data('force_action') || this.$target.data('model_name')), form_values)
            .then(async function (result_data) {
                // Restore send button behavior
                self.$target.find('.s_website_form_send, .o_website_form_send')
                    .removeAttr('disabled')
                    .removeClass('disabled'); // !compatibility
                result_data = JSON.parse(result_data);
                console.log(result_data,"inherit REsult.................................................")
                if (!result_data.id) {
                    // Failure, the server didn't return the created record ID
                    self.update_status('error', result_data.error ? result_data.error : false);
                    if (result_data.error_fields) {
                        // If the server return a list of bad fields, show these fields for users
                        self.check_error_fields(result_data.error_fields);
                    }
                } else {
                    // Success, redirect or update status
                    let successMode = self.$target[0].dataset.successMode;
                    let successPage = self.$target[0].dataset.successPage;
                    if (!successMode) {
                        successPage = self.$target.attr('data-success_page'); // Compatibility
                        successMode = successPage ? 'redirect' : 'nothing';
                    }
                    switch (successMode) {
                        case 'redirect': {
                            successPage = successPage.startsWith("/#") ? successPage.slice(1) : successPage;
                            if (successPage.charAt(0) === "#") {
                                await dom.scrollTo($(successPage)[0], {
                                    duration: 500,
                                    extraOffset: 0,
                                });
                                break;
                            }
                            if(successPage == "/admission-success"){
                                successPage = "/admission-success/" + result_data.id
//                                successPage = "/admission-success/result" + result_data
//                                ajax.jsonRpc("/admission-success/result",'call',{
//                                    result_data: result_data,
//                                }).then(function(){});
                            }
                            $(window.location).attr('href', successPage);
                            console.log(successPage,"SUccess Page||||||||||")
                            return;
                        }
                        case 'message': {
                            // Prevent double-clicking on the send button and
                            // add a upload loading effect (delay before success
                            // message)
                            await concurrency.delay(dom.DEBOUNCE);

                            self.$target[0].classList.add('d-none');
                            self.$target[0].parentElement.querySelector('.s_website_form_end_message').classList.remove('d-none');
                            return;
                        }
                        default: {
                            // Prevent double-clicking on the send button and
                            // add a upload loading effect (delay before success
                            // message)
                            await concurrency.delay(dom.DEBOUNCE);

                            self.update_status('success');
                            break;
                        }
                    }

                    self.$target[0].reset();
                    self.restoreBtnLoading();
                }
            })
            .guardedCatch(error => {
                this.update_status(
                    'error',
                    error.status && error.status === 413 ? _t("Uploaded file is too large.") : "",
                );
            });
        },
    })
})