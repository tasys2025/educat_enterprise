
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models, fields
from odoo.tools.translate import html_translate


class OpCourse(models.Model):
    _inherit = "op.course"

    reg_fees = fields.Boolean('Registration Fees')
    product_id = fields.Many2one('product.product', 'Fees')
    full_description = fields.Html('Full Description',
                                   translate=html_translate,
                                   sanitize_attributes=False)
    image_1920 = fields.Image('Image', attachment=True)
    category_ids = fields.Many2many('op.course.category',
                                    'course_category_rel',
                                    'course_id',
                                    'category_id', 'Categories')