# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from datetime import datetime
from odoo import http, SUPERUSER_ID, _
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website.controllers.form import WebsiteForm
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
from markupsafe import Markup
from odoo.addons.website.controllers.main import QueryURL
from odoo.tools import groupby as groupbyelem
from operator import itemgetter
from odoo.exceptions import ValidationError
from collections import OrderedDict
from odoo.osv import expression
import base64


class AdmissionNewRegistration(http.Controller):

    @http.route(['/courses-offered',
                '/courses-offered/category/<model("op.course.category"):category>'], type='http',
                auth='public', website=True)
    def admission_new_registration(self, search='', category=False, **post):
        if post.get('type') and post.get('type') not in ['free', 'paid']:
            post.update({
                'type': ''
            })
        keep = QueryURL("/courses-offered", search=search, category=category, type=post.get("type"))

        register = request.env['op.admission.register'].sudo()
        domain = [('state', '!=', 'done'), ('admission_template_id', '!=', False)]
        if search:
            post["search"] = search
            for srch in search.split(" "):
                domain += [('name', 'ilike', srch)]

        if post.get('type'):
            if post.get('type') == 'free':
                domain += [('course_id.reg_fees', '=', False)]
            if post.get('type') == 'paid':
                domain += [('course_id.reg_fees', '=', True)]

        course_category_ref = request.env['op.course.category']
        if category:
            category = request.env['op.course.category'].sudo().browse(int(category))
            domain += [('course_id.category_ids', 'in', [category.id])]
        if category:
            categories = course_category_ref.sudo().search(
                [('parent_id', '=', category.id)])
        else:
            categories = course_category_ref.sudo().search(
                [('parent_id', '=', False)])
        register_ids = register.search(domain)
        # print(register_ids[0].course_id.image_1920, "-------Hello-")
        post.update({
            'register_ids': register_ids,
            'category': category,
            'categories': categories,
            'current_type': post.get('type'),
            'keep': keep,
        })
        return request.render(
            "openeducat_dynamic_admission.admission_new_registration", post)

    @http.route('''/courses-details/<model("op.admission.register"):register>''', type='http',
                auth="public", sitemap=False, website=True)
    def register(self, register, search='', **kw):
        # domain = [course.id]
        if search:
            for srch in search:
                course = request.env['op.course'].sudo().search(
                    [('name', 'ilike', srch)])

        register = request.env['op.admission.register'].sudo().browse([register.id])
        course = register.course_id


        values = {
            'course': course,
            'register': register,
            # 'enrolled': enrollment and True or False,
            # 'completed_percentage': completed_percentage,
            # 'sections': sections,
            # 'user': request.env.user,
            # 'is_public_user': request.env.user == request.website.user_id,
            # 'rating_message_values': rating_message_values,
            # 'rating_course': rating_course
        }

        # values.update(course.get_course_stats())
        return request.render('openeducat_dynamic_admission.offered_course_details', values)

    @http.route(
        ['/courses-offered/register/<model("op.admission.register"):register>'],
        type='http', auth='public', website=True)
    def admission_new_registration_form(self, register, **post):
        if register.sign_in_required is True:
            if request.session.uid:
                request.website.sale_reset()
                order = request.website.sale_get_order()
                if order:
                    for line in order.website_order_line:
                        line.unlink()
                custom_template_id = request.env['op.admission.customtemplate'].sudo()\
                    .search([('admission_register_line', '=', register.id)])
                post.update({
                    'register': register,
                    'custom_template_id': custom_template_id,
                    'course_id': register.course_id
                })
                return request.render(
                    "openeducat_dynamic_admission.apply", post)
            else:
                return request.redirect(
                    '/web/login?redirect=/courses-offered/register/%s' % register.id
                )
        else:
            request.website.sale_reset()
            order = request.website.sale_get_order()
            if order:
                for line in order.website_order_line:
                    line.unlink()
            custom_template_id = request.env['op.admission.customtemplate'].sudo().\
                search([('admission_register_line', '=', register.id)])
            post.update({
                'register': register,
                'custom_template_id': custom_template_id,
                'course_id': register.course_id
            })
            return request.render(
                "openeducat_dynamic_admission.apply", post)

    @http.route('/admission-template/<model("op.admission.customtemplate"):material>',
                type='http', website=True, auth='public')
    def edit_admission_web_page_content_form(self, material):
        return request.render(
            'openeducat_dynamic_admission.edit_admission_web_page_form', {
                'material': material,
                'edit_in_backend': True,
            })

    @http.route('/admission-success/<int:admission_id_number>', type='http',
                website=True, auth='public')
    def admission_success_page(self, admission_id_number):
        post = {}
        admission_id = request.env['op.admission'].sudo().browse(admission_id_number)
        post.update({
            'admission_number': str(admission_id_number),
        })
        if admission_id:
            register = request.env['op.admission.register'].sudo().search(
                [('admission_ids', '=', admission_id_number)])
            product_id = register.product_id
            lst_price = register.product_id.lst_price
            user = request.env.user.partner_id
            admission_id.write({
                'fees': product_id and lst_price or 0.0,
                'fees_term_id': register.course_id.fees_term_id.id,
                'application_date': datetime.today(),
                'partner_id': user,
                'company_id': register.company_id.id,
                'state': 'online',
            })

            if register.course_id.reg_fees:
                prod_id = register.course_id.product_id.id
            else:
                return request.render(
                    "openeducat_dynamic_admission.application_confirmed",
                    {'admission_id': admission_id})
            add_qty = 1
            set_qty = 0
            request.website.sale_get_order(force_create=1)._cart_update(
                product_id=int(prod_id), add_qty=float(add_qty),
                set_qty=float(set_qty))

            order = request.website.sale_get_order()

            if order and admission_id:
                admission_id.write({'order_id': order.id})

            values = WebsiteSale.checkout_values(self, **post)

            values.update({'website_sale_order': order})

            # Avoid useless rendering if called in ajax
            if post.get('xhr'):
                return 'ok'
            render_values = WebsiteSale._get_shop_payment_values(self, order, **post)
            render_values['admission'] = admission_id
            request.session['sale_last_order_id'] = order.id
            return request.render(
                "openeducat_dynamic_admission.admission_checkout", render_values
            )

        return request.redirect("/")

    @http.route('/admission-form/update', type='json',
                website=True, auth='public')
    def admission_update_form(self, data):
        model_name = 'op.admission'
        data_dict = {}
        for record in data:
            model_details = request.env['ir.model.fields'].sudo().\
                search([('model', '=', model_name), ('name', '=', record)])
            related_model = model_details.relation
            if related_model:
                all_data = request.env[related_model].sudo().search_read([], ['name'])
                data_dict[record] = all_data
        return data_dict

    @http.route('/admission/custom/field', type='json',
                website=True, auth='public')
    def create_admission_custom_field(self, field_dict):
        values = {}
        fied_data = []
        fied_names = {}
        if field_dict:
            model_name = 'op.admission'

            model = request.env['ir.model'].sudo().search([('model', '=', model_name)])
            values['model_id'] = model.id

            i = 0
            for data in field_dict.values():
                values['ttype'] = data['ttype']
                if data['name'] :
                    x = data['name'].split()
                    x_name = "_".join(x)
                    values['name'] = 'x_' + x_name
                # if data['options'] :
                if 'options' in data:
                    option_ls = []
                    for j in data['options']:
                        sample_ls = []
                        sample_ls.append(j)
                        sample_ls.append(j)
                        option_ls.append(sample_ls)

                    values['selection'] = option_ls
                new_field = request.env['ir.model.fields'].sudo().create(values)
                fied_data.append(new_field)
                fied_names[data['name']] = values['name']
                i = i + 1

            return fied_names


PPG = 10  # record per page


class StudentNewRegistration(CustomerPortal):

    def get_search_domain_admission_newregistration(self, search, attrib_values):
        domain = []
        if search:
            for srch in search.split(" "):
                domain += [
                    '|', '|', '|', '|', ('application_number', 'ilike', srch),
                    ('admission_date', 'ilike', srch), ('course_id', 'ilike', srch),
                    ('state', 'ilike', srch), ('application_date', 'ilike', srch)
                ]

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]
        return domain

    def _prepare_portal_layout_values(self):
        values = super(StudentNewRegistration, self). \
            _prepare_portal_layout_values()
        user = request.env.user
        student = request.env['op.student'].sudo().search([
            ('partner_id', '=', user.partner_id.id)])
        if student:
            admission_count = request.env['op.admission'].sudo().search_count(
                ['|', ('partner_id', '=', user.partner_id.id),
                 ('student_id', '=', student.id)])
        else:
            admission_count = request.env['op.admission'].sudo().search_count(
                [('partner_id', '=', user.partner_id.id)])
        values['new_admission_count'] = admission_count
        return values

    def _parent_prepare_portal_layout_values(self, student_id=None):

        val = super(StudentNewRegistration, self). \
            _parent_prepare_portal_layout_values(student_id)
        student = request.env['op.student'].sudo().search(
            [('id', '=', student_id)])
        admission_count = request.env['op.admission'].sudo().search_count(
            ['|', ('partner_id', '=', student.user_id.id),
             ('student_id', '=', student_id)])
        val['new_admission_count'] = admission_count
        return val

    @http.route(['/student/newregistration/info/',
                 '/student/newregistration/info/<int:student_id>',
                 '/student/newregistration/info/page/<int:page>',
                 '/student/newregistration/info/<int:student_id>/page/<int:page>'
                 ], type='http',
                auth='public', website=True)
    def student_newregistration_list_data123(
            self, date_begin=None, student_id=None, date_end=None, page=1,
            search='', ppg=False, sortby=None, filterby=None,
            search_in='application_number', groupby='course_id', **post):
        if student_id:
            val = self._parent_prepare_portal_layout_values(student_id)
        else:
            values = self._prepare_portal_layout_values()
        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG

        user = request.env.user
        is_student = request.env['op.student'].sudo().search([
            ('user_id', '=', user.id)])

        if student_id:
            student = request.env['op.student'].sudo().search([
                ('id', '=', student_id)])
            domain = ['|', ('partner_id', '=', student.user_id.id),
                      ('student_id', '=', student_id)]

        elif is_student:
            user = request.env.user.partner_id
            student = request.env['op.student'].sudo().search([
                ('partner_id', '=', user.id)])
            domain = ['|', ('partner_id', '=', user.id),
                      ('student_id', '=', student.id)]

        else:
            user = request.env.user
            domain = [('partner_id', '=', user._uid)]

        searchbar_sortings = {
            'student_id': {'label': _('Student'), 'order': 'student_id'},
            'application_number': {'label': _('Application No'),
                                   'order': 'application_number'},
            'admission_date': {'label': _('Admission Date'),
                               'order': 'admission_date'},
            'application_date': {'label': _('Application Date'),
                                 'order': 'application_date'},
            'state': {'label': _('State'), 'order': 'state'}
        }

        searchbar_filters = {
            'all': {'label': _('All'), 'domain': []},
            'state': {'label': _('Enroll'), 'domain': [('state', '=', 'done')]},
            'state online': {'label': _('Online Admission'),
                             'domain': [('state', '=', 'online')]},
        }
        courses = request.env['op.admission'].sudo().search(domain)
        for course in courses:
            searchbar_filters.update({
                str(course.course_id.id):
                    {'label': course.course_id.name,
                     'domain': [('course_id', '=', course.course_id.id)]},
            })

        if not filterby:
            filterby = 'all'
        domain += searchbar_filters[filterby]['domain']

        if not sortby:
            sortby = 'student_id'
        order = searchbar_sortings[sortby]['order']

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        searchbar_inputs = {
            'application_number': {'input': 'application_number',
                                   'label': Markup(_('Search <span class="nolabel">'
                                                     '(in Application No)</span>'))},
            'admission_date': {'input': 'Admission Date',
                               'label': _('Search in Admission Date')},
            'application_date': {'input': 'Application Date',
                                 'label': _('Search in Application Date')},
            'course_id': {'input': 'Course', 'label': _('Search in Course')},
            'state': {'input': 'State', 'label': _('Search in State')},
            'all': {'input': 'All', 'label': _('Search in All')},
        }

        searchbar_groupby = {
            'none': {'input': 'none', 'label': _('None')},
            'course_id': {'input': 'course_id', 'label': _('Course')},
        }

        domain += self.get_search_domain_admission_newregistration(
            search, attrib_values
        )
        if student_id:
            keep = QueryURL('/student/newregistration/info/%s' % student_id,
                            search=search, amenity=attrib_list,
                            order=post.get('order'))

            total = request.env['op.admission'].sudo().search_count(domain)
            pager = portal_pager(
                url="/student/newregistration/info/%s" % student_id,
                url_args={'date_begin': date_begin, 'date_end': date_end,
                          'sortby': sortby, 'filterby': filterby,
                          'search': search, 'search_in': search_in},
                total=total,
                page=page,
                step=ppg
            )

        else:
            keep = QueryURL('/student/newregistration/info/', search=search,
                            amenity=attrib_list, order=post.get('order'))
            total = request.env['op.admission'].sudo().search_count(domain)

            pager = portal_pager(
                url="/student/newregistration/info/",
                url_args={'date_begin': date_begin, 'date_end': date_end,
                          'sortby': sortby, 'filterby': filterby,
                          'search': search, 'search_in': search_in},
                total=total,
                page=page,
                step=ppg
            )
        if search:
            post["search"] = search
        if attrib_list:
            post['attrib'] = attrib_list

        if search and search_in:
            search_domain = []
            if search_in in ('all', 'application_number'):
                search_domain = expression.OR(
                    [search_domain, [('application_number', 'ilike', search)]])
            if search_in in ('all', 'admission_date'):
                search_domain = expression.OR(
                    [search_domain, [('admission_date', 'ilike', search)]])
            if search_in in ('all', 'course_id'):
                search_domain = expression.OR(
                    [search_domain, [('course_id', 'ilike', search)]])
            if search_in in ('all', 'application_date'):
                search_domain = expression.OR(
                    [search_domain, [('application_date', 'ilike', search)]])
            if search_in in ('all', 'state'):
                search_domain = expression.OR(
                    [search_domain, [('state', 'ilike', search)]])
            domain += search_domain

        if groupby == 'course_id':
            order = "course_id, %s" % order
        register_ids = request.env['op.admission'].sudo().search(
            domain, order=order, limit=ppg, offset=pager['offset'])

        if groupby == 'course_id':
            grouped_tasks = [
                request.env['op.admission'].sudo().concat(*g)
                for k, g in groupbyelem(register_ids, itemgetter('course_id'))]
        else:
            grouped_tasks = [register_ids]
        if student_id:
            student_access = self.get_student(student_id=student_id)
            if student_access is False:
                return request.render('website.404')
            val.update({
                'date': date_begin,
                'registration_id': register_ids,
                'page_name': 'New_Registration',
                'pager': pager,
                'ppg': ppg,
                'keep': keep,
                'stud_id': student_id,
                'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
                'filterby': filterby,
                'default_url': '/student/newregistration/info/%s' % student_id,
                'searchbar_sortings': searchbar_sortings,
                'sortby': sortby,
                'attrib_values': attrib_values,
                'attrib_set': attrib_set,
                'searchbar_inputs': searchbar_inputs,
                'search_in': search_in,
                'grouped_tasks': grouped_tasks,
                'searchbar_groupby': searchbar_groupby,
                'groupby': groupby,
            })
            return request.render(
                "openeducat_dynamic_admission."
                "openeducat_student_newregistration_list_data", val)
        else:
            values.update({
                'date': date_begin,
                'registration_id': register_ids,
                'page_name': 'New_Registration',
                'pager': pager,
                'ppg': ppg,
                'keep': keep,
                'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
                'filterby': filterby,
                'default_url': '/student/newregistration/info/',
                'searchbar_sortings': searchbar_sortings,
                'sortby': sortby,
                'attrib_values': attrib_values,
                'attrib_set': attrib_set,
                'searchbar_inputs': searchbar_inputs,
                'search_in': search_in,
                'grouped_tasks': grouped_tasks,
                'searchbar_groupby': searchbar_groupby,
                'groupby': groupby,
            })
            return \
                request.render(
                    "openeducat_dynamic_admission."
                    "openeducat_student_newregistration_list_data", values)

    @http.route()
    def home(self, **kw):
        """ Add sales documents to main account page """
        response = super(StudentNewRegistration, self).home(**kw)
        user = request.env.user
        student = request.env['op.student'].sudo().search([
            ('partner_id', '=', user.partner_id.id)])
        if student:
            admission_count = request.env['op.admission'].sudo().search_count(
                ['|', ('partner_id', '=', user.partner_id.id),
                 ('student_id', '=', student.id)])
        else:
            admission_count = request.env['op.admission'].sudo().search_count(
                [('partner_id', '=', user.partner_id.id)])
        response.qcontext.update({
            'new_admission_count': admission_count
        })
        return response


class WebsiteAdmissionForm(WebsiteForm):

    def insert_record(self, request, model, values, custom, meta=None):
        model_name = model.sudo().model
        # ir_model_field = request.env['ir.model.fields'].sudo() \
        #     .search([('model', '=', model_name)])
        # for data in ir_model_field:
        #     print(data.ttype, "-------Here")
        #     field_name = data.name
        #     if data.ttype == 'date' and field_name == 'birth_date' and 'birth_date' in values:
        #         # if field_name == 'birth_date' and 'birth_date' in values:
        #         value1 = values[field_name]
        #         print("Here IN BirthDate...")
        #         if len(value1.split('/')) > 2:
        #             value2 = datetime.strptime(
        #                 value1, '%m/%d/%Y').strftime('%Y-%m-%d')
        #             values[field_name] = value2
        record = request.env[model_name].with_user(SUPERUSER_ID).\
            with_context(mail_create_nosubscribe=True).create(values)

        if custom or meta:
            # ir_model_obj = self.env['ir.model.fields']
            new_custom = {}
            for key, value in custom.items():
                ir_model_field = request.env['ir.model.fields'].sudo()\
                    .search([('model', '=', model_name), ('name', '=', key)])
                field_type = ir_model_field.ttype
                if field_type == 'date':
                    value = datetime.strptime(value, '%Y-%m-%d')
                if field_type == 'datetime':
                    value = datetime.strptime(value, '%m/%d/%Y %H:%M:%S').\
                        strftime('%Y-%m-%d %H:%M:%S')
                new_custom[key] = value
            record.update(new_custom)

        return record.id

    def extract_data(self, model, values):
        print(values, "-----New Data Herer...")
        dest_model = request.env[model.sudo().model]
        custom_dict = {}
        data = {
            'record': {},  # Values to create record
            'attachments': [],  # Attached files
            'custom': '',  # Custom fields values
            'meta': '',  # Add metadata if enabled
        }

        authorized_fields = model.with_user(SUPERUSER_ID)._get_form_writable_fields()
        error_fields = []

        for field_name, field_value in values.items():
            # If the value of the field if a file
            if hasattr(field_value, 'filename'):
                # Undo file upload field name indexing
                field_name = field_name.split('[', 1)[0]

                # If it's an actual binary field, convert the input file
                # If it's not, we'll use attachments instead
                if field_name in authorized_fields and \
                        authorized_fields[field_name]['type'] == 'binary':
                    data['record'][field_name] = base64.\
                        b64encode(field_value.read())
                    field_value.stream.seek(0)  # do not consume value forever
                    if authorized_fields[field_name]['manual'] and \
                            field_name + "_filename" in dest_model:
                        data['record'][field_name + "_filename"] = field_value.filename
                else:
                    field_value.field_name = field_name
                    data['attachments'].append(field_value)

            # If it's a known field
            elif field_name in authorized_fields:
                try:
                    input_filter = self._input_filters[
                        authorized_fields[field_name]['type']
                    ]
                    data['record'][field_name] = input_filter(
                        self, field_name, field_value
                    )
                except ValueError:
                    error_fields.append(field_name)

            # If it's a custom field
            elif field_name != 'context':
                custom_dict[field_name] = field_value

        data['custom'] = custom_dict

        # Add metadata if enabled  # ICP for retrocompatibility
        if request.env['ir.config_parameter'].sudo()\
                .get_param('website_form_enable_metadata'):
            environ = request.httprequest.headers.environ
            data['meta'] += "%s : %s\n%s : %s\n%s : %s\n%s : %s\n" % (
                "IP", environ.get("REMOTE_ADDR"),
                "USER_AGENT", environ.get("HTTP_USER_AGENT"),
                "ACCEPT_LANGUAGE", environ.get("HTTP_ACCEPT_LANGUAGE"),
                "REFERER", environ.get("HTTP_REFERER")
            )

        if hasattr(dest_model, "website_form_input_filter"):
            data['record'] = dest_model.\
                website_form_input_filter(request, data['record'])

        missing_required_fields = \
            [
                label for label, field in authorized_fields.items()
                if field['required'] and not label in data['record'] # noqa
            ]
        if any(error_fields):
            raise ValidationError(error_fields + missing_required_fields)

        return data
