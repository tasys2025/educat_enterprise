from odoo import api, fields, models


class MassMailing(models.Model):
    _inherit = 'mailing.mailing'

    @api.model
    def default_get(self, fields):
        res = super(MassMailing, self).default_get(fields)
        if 'subject' in fields and \
                self.env.context.get('default_automated_marketing', False):
            res['subject'] = self.env.context.get('default_name')
        return res

    automated_marketing = fields.Boolean(
        string='Specific mailing used in marketing campaign',
        default=False,)
    marketing_activity_ids = fields.One2many('marketing.activity',
                                             'mass_mailing_id',
                                             string='Marketing Activities', copy=False)
