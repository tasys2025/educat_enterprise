{
    'name': "OpenEduCat Automated Marketing",
    'version': "16.0.1.0",
    'summary': "Automated Marketing",
    'category': "Marketing",
    'author': "OpenEduCat Inc",
    'license': 'Other proprietary',
    'website': "http://www.openeducat.org",
    'depends': ['openeducat_core_enterprise', 'mass_mailing'],
    'data': [
        'security/automated_marketing_security.xml',
        'security/ir.model.access.csv',
        'views/automated_marketing_menu.xml',
        'wizard/marketing_campaign_test_views.xml',
        'views/mailing_mailing_views.xml',
        'views/mailing_trace_views.xml',
        'views/marketing_participant_view.xml',
        'views/marketing_campaign_view.xml',
        'views/marketing_activity_view.xml',
        'views/marketing_trace_view.xml',
        'views/link_tracker_views.xml',
        'views/ir_actions_views.xml',
        'data/ir_cron_data.xml',
    ],
    'assets': {
        'web._assets_primary_variables': [
            'openeducat_automated_marketing/static/src/scss/variables.scss',
        ],
        'web.assets_backend': [
            'openeducat_automated_marketing/static/src/xml/activity_graph_template.xml',
            'openeducat_automated_marketing/static/src/js/automated_marketing_graph.js',
            'openeducat_automated_marketing/static/src/js/automated_marketing.js',
            'openeducat_automated_marketing/static/src/scss/style.scss',
        ],
    },
    'demo': [
        'data/demo.xml'
    ],
}
