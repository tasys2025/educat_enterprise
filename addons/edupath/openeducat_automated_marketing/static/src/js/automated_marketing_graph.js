/** @odoo-module **/

import { CharField } from "@web/views/fields/char/char_field";
import { registry } from "@web/core/registry";
import fieldRegistry from 'web.field_registry';
import { loadJS } from "@web/core/assets";
const { Component, useState, useRef, onWillStart, useEffect, onWillUnmount } = owl;

export class ActivityGraph extends CharField {

    setup() {
        super.setup();
        this.targetEditElement = useRef('activityGraph');
        this._isInDOM = false;
        this.chart = null;
        this.chartId =  _.uniqueId('chart');
        this.data = JSON.parse(this.props.value);

        onWillStart(() => loadJS("/web/static/lib/Chart/Chart.js"));

        useEffect(() => {
            this._isInDOM = true;
            this._render();
            return () => {
                if (this.chart) {
                    this.chart.destroy();
                }
            };
        });
    }

    _render(){
        if (!this._isInDOM) {
            return;
        }
        if(!this.data || !_.isArray(this.data)){
            return;
        }
        var config = this._getLineChartConfig();
//        var $canvasContainer = $('<div/>', {class: 'graph_container'});
//        this.$canvas = $('<canvas/>').attr('id', this.chartId);
//        $canvasContainer.append(this.$canvas);
//        $(this.targetEditElement.el).append($canvasContainer);
//        var context = document.getElementById(this.chartId);
        this.chart = new Chart(this.targetEditElement.el, config);
    }

    _getLineChartConfig(){
        var labels = this.data[0].points.map(function (point) {
            return point.x;
        });
        var datasets = this.data.map(function (group) {
            var borderColor =  group.color;
            var fillColor = group.color;
            return {
                label: group.label,
                data: group.points,
                fill: false,
                backgroundColor: fillColor,
                borderColor: borderColor,
                borderWidth: 2,
                pointBackgroundColor: 'rgba(0, 1, 1, 0)',
                pointBorderColor: borderColor,
            };
        });
        Chart.defaults.global.elements.line.tension = 0;

        return {
            type: 'line',
            data: {
                labels: labels,
                datasets: datasets,
            },
            options: {
                layout: {
                    padding: {left: 25, right: 20, top: 5, bottom: 20}
                },
                legend: {
                    display: false,
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        type: 'linear',
                        display: false,
                        ticks: {
                            beginAtZero: true,
                        },
                    }],
                    xAxes: [{
                        ticks: {
                            maxRotation: 0,
                        },
                    }],
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    bodyFontColor: 'rgba(1,1,1,1)',
                    titleFontSize: 13,
                    titleFontColor: 'rgba(0,0,0,1)',
                    backgroundColor: 'rgba(255,255,255,0.6)',
                    borderColor: 'rgba(0,0,0,0.2)',
                    borderWidth: 2,
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            var dataset = chart.data.datasets[tooltipItem.datasetIndex];
                            var tooltipBorderColor = chart.tooltip._model.backgroundColor;
                            return {
                                borderColor: tooltipBorderColor,
                                backgroundColor: dataset.backgroundColor,
                            };
                        },
                    }
                }
            }
    }
    }

}
ActivityGraph.template = 'activity_graph';
registry.category("fields").add("marketing_activity_graph", ActivityGraph);
return {ActivityGraph:ActivityGraph};
