/** @odoo-module */

import { ConfirmationDialog } from "@web/core/confirmation_dialog/confirmation_dialog";
import { registry } from "@web/core/registry";
import { useService } from "@web/core/utils/hooks";
import { useX2ManyCrud, useOpenX2ManyRecord } from "@web/views/fields/relational_utils";
import { X2ManyField } from "@web/views/fields/x2many/x2many_field";
import { KanbanRecord } from "@web/views/kanban/kanban_record";
import { KanbanRenderer } from "@web/views/kanban/kanban_renderer";
import { KanbanCompiler } from "@web/views/kanban/kanban_compiler";
import { createElement } from "@web/core/utils/xml";

const { onWillRender, useState, useSubEnv } = owl;
const fieldRegistry = registry.category("fields");

class HierarchyKanbanCompiler extends KanbanCompiler {
    setup() {
        super.setup();
        this.compilers.push({ selector: "HierarchyKanbanRecord", fn: this.compileHierarchyKanbanRecord });
    }

    compileHierarchyKanbanRecord(el) {
        const compiled = createElement(el.nodeName);
        for (const attr of el.attributes) {
            compiled.setAttribute(attr.name, attr.value);
        }
        return compiled;
    }
    validateNode() {}
}

export class HierarchyKanbanRecord extends KanbanRecord {
    setup() {
        super.setup();
        this.dialogService = useService("dialog");
        if (this.props.is_readonly !== undefined) {
            this.props.readonly = this.props.is_readonly;
        }
        this.state = useState({activeState: 'graph'});
    }

    triggerAction(params) {
        const { group, list, record } = this.props;
        const listOrGroup = group || list;
        const { type } = params;

        if (type === "delete" && !listOrGroup.deleteRecords &&
            record.data.children && record.data.children.length !== 0) {
            this.dialogService.add(ConfirmationDialog, {
                body: this.env._t("Deleting this activity will delete ALL its children activities. Are you sure?"),
                confirm: () => super.triggerAction(...arguments),
                cancel: () => {},
            });
        } else {
            super.triggerAction(...arguments);
        }
    }

    async addChildActivity(triggerType) {
        await this.props.list.model.root.save({stayInEdition: true});
        const context = {
            default_parent_id: this.props.record.data.id,
            default_trigger_type: triggerType,
        };
        this.env.onAddActivity({ context });
    }

    onClickActivityTab(ev, view) {
        ev.stopPropagation();
        this.state.activeState = view;
    }
}

HierarchyKanbanRecord.Compiler = HierarchyKanbanCompiler;
HierarchyKanbanRecord.components = {
    ...KanbanRecord.components,
    HierarchyKanbanRecord
};

HierarchyKanbanRecord.defaultProps = {
    ...KanbanRecord.defaultProps,
    displayChildren: false,
};

HierarchyKanbanRecord.props = KanbanRecord.props.concat([
    'is_readonly?',
    'displayChildren?',
]);


export class HierarchyKanbanRenderer extends KanbanRenderer {
    setup() {
        super.setup();

        onWillRender(() => {
            if (this.props.list.model.root.data.marketing_activity_ids
                && this.props.list.model.root.data.marketing_activity_ids.records) {
                this.props.list.model.root.data.marketing_activity_ids.records = this._getRecordsWithChildActivity(
                    this.props.list.model.root.data.marketing_activity_ids.records
                );
            }

            if (this.props.list.model.root.data.trace_ids
                && this.props.list.model.root.data.trace_ids.records) {
                this.props.list.model.root.data.trace_ids.records = this._getRecordsWithChildActivity(
                    this.props.list.model.root.data.trace_ids.records
                );
            }
        });
    }

    _getRecordsWithChildActivity(records) {
        const parentMap = {};
        const allChildrenIds = [];
        records.forEach((activityRecord) => {
            const parentId = activityRecord.data.parent_id;
            if (parentId) {
                if (!parentMap[parentId[0]]) {
                    parentMap[parentId[0]] = [];
                }
                parentMap[parentId[0]].push(activityRecord);
                allChildrenIds.push(activityRecord.data.id);
            }
        });
        records.forEach((activityRecord) => {
            activityRecord.data.children = parentMap[activityRecord.data.id] || [];
        });

        return records;
    }
}

HierarchyKanbanRenderer.components.KanbanRecord = HierarchyKanbanRecord;

export class ActivityHierarchyKanban extends X2ManyField {

    setup() {
        super.setup();
        const { saveRecord, updateRecord } = useX2ManyCrud(
            () => this.list,
            this.isMany2Many
        );

        const openRecord = useOpenX2ManyRecord({
            resModel: this.list.resModel,
            activeField: this.activeField,
            activeActions: this.activeActions,
            getList: () => this.list,
            saveRecord: async (record) => {
                await saveRecord(record);
                await this.props.record.save({stayInEdition: true});
            },
            updateRecord: updateRecord,
        });
        this._openRecord = openRecord;

        useSubEnv({
            onAddActivity: this.onAdd.bind(this),
        });
    }

}

fieldRegistry.add("kanban_hierarchy", ActivityHierarchyKanban);

ActivityHierarchyKanban.components = {
    ...X2ManyField.components,
    KanbanRenderer: HierarchyKanbanRenderer
};
