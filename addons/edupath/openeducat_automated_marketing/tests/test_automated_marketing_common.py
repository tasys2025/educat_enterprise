from odoo.tests import common


class TestAutomatedMarketingCommon(common.TransactionCase):
    def setUp(self):
        super(TestAutomatedMarketingCommon, self).setUp()
        self.activity = self.env['marketing.activity']
        self.campaign = self.env['marketing.campaign']
        self.participant = self.env['marketing.participant']
        self.trace = self.env['marketing.trace']
        self.mailing_trace = self.env['mailing.trace']
