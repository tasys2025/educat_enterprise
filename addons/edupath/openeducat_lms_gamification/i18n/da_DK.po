# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_lms_gamification
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-15 04:34+0000\n"
"PO-Revision-Date: 2022-12-15 04:34+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.slides_home_user_profile_small
msgid "32%"
msgstr "Begynd mødet"

#. module: openeducat_lms_gamification
#: model:gamification.goal.definition,name:openeducat_lms_gamification.goal_definition_1
msgid "50% completion"
msgstr "50% afsluttet"

#. module: openeducat_lms_gamification
#: model:gamification.goal.definition,name:openeducat_lms_gamification.goal_definition_2
msgid "80% completion"
msgstr "80% afsluttet"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.slides_home_user_profile_small
msgid "<span class=\"fw-bold text-muted me-2\">Current rank:</span>"
msgstr "ansøge"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.user_quickkarma_card
msgid "<span class=\"text-500 mx-2\">•</span>"
msgstr ""

#. module: openeducat_lms_gamification
#: model:ir.model.fields,field_description:openeducat_lms_gamification.field_op_course__course_attempt_reward
#: model:ir.model.fields,field_description:openeducat_lms_gamification.field_op_material__quiz_attempt_reward
msgid "Attempt Reward"
msgstr "Sidst opdateret om"

#. module: openeducat_lms_gamification
#: model:openeducat.portal.menu,name:openeducat_lms_gamification.website_menu_badge
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.portal_my_badges_menu_badges
msgid "Badge"
msgstr "Udstede medier"

#. module: openeducat_lms_gamification
#: model:ir.ui.menu,name:openeducat_lms_gamification.gamification_badge_menu
msgid "Badges"
msgstr "Studerende"

#. module: openeducat_lms_gamification
#: model:ir.ui.menu,name:openeducat_lms_gamification.gamification_challenge_menu
msgid "Challenges"
msgstr "Lukket"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.slides_home_user_profile_small
msgid "Congratulations, you have reached the last rank!"
msgstr "Møde logfiler"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.slides_home_user_achievements_small
msgid "Done"
msgstr "Medier (er)"

#. module: openeducat_lms_gamification
#: model:ir.ui.menu,name:openeducat_lms_gamification.menu_op_lms_gamification
msgid "Gamification"
msgstr "Lukket"

#. module: openeducat_lms_gamification
#: model:ir.model.fields,field_description:openeducat_lms_gamification.field_op_course__challenge_ids
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.view_course_lms_blog_form
msgid "Gamification Challenge"
msgstr "Sigtbarhed"

#. module: openeducat_lms_gamification
#: model:gamification.challenge,name:openeducat_lms_gamification.challenge_2
msgid "Get Optimized"
msgstr "Få optimeret"

#. module: openeducat_lms_gamification
#: model:ir.ui.menu,name:openeducat_lms_gamification.gamification_definition_menu
msgid "Goal Definitions"
msgstr "Færdig"

#. module: openeducat_lms_gamification
#: model:ir.ui.menu,name:openeducat_lms_gamification.gamification_goal_menu
msgid "Goals"
msgstr "Lige gjort"

#. module: openeducat_lms_gamification
#: model:ir.model,name:openeducat_lms_gamification.model_op_course
msgid "LMS Course"
msgstr "Klik for at tilføje en ny fil."

#. module: openeducat_lms_gamification
#: model:ir.model,name:openeducat_lms_gamification.model_op_material
msgid "LMS Material"
msgstr "Stopper"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.toggle_latest_achievements
msgid "Latest achievements"
msgstr "Møde gæst"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.toggle_leaderboard
msgid "Leaderboard"
msgstr "opmærksomme logfiler"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.my_challenges
msgid "More info"
msgstr "Tag"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.slides_home_user_profile_small
msgid "Next rank:"
msgstr "Forlægger"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.toggle_latest_achievements
msgid "No achievements currently :("
msgstr "Møde KPI"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.toggle_leaderboard
msgid "No leaderboard currently :("
msgstr "Certificerer det"

#. module: openeducat_lms_gamification
#: model:gamification.challenge,name:openeducat_lms_gamification.challenge_1
msgid "Python competition"
msgstr "Python konkurrence"

#. module: openeducat_lms_gamification
#: model:ir.ui.menu,name:openeducat_lms_gamification.gamification_rank_menu
msgid "Ranks"
msgstr "Ikke færdig"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.my_badges
msgid "There are currently no Badges for your account."
msgstr "Medieanalyse"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.my_challenges
msgid "View"
msgstr "ansøge"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.toggle_leaderboard
msgid "View all"
msgstr "opmærksomme logfiler"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.my_badges
msgid "Your Badges"
msgstr "Forny detaljer"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.achievement_card
msgid "achieved"
msgstr "Medier forny detaljer"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.slides_home_user_profile_small
msgid "bg-200"
msgstr "Møde"

#. module: openeducat_lms_gamification
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.reward_point_view
#: model_terms:ir.ui.view,arch_db:openeducat_lms_gamification.user_quickkarma_card
msgid "xp"
msgstr "Returmedier"
