# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models
import qrcode
from io import BytesIO
import base64


class AccountMoveInherit(models.Model):
    _inherit = 'account.move'

    def generate_payment_qr_code(self):
        payment_link_id = self.env['payment.link.wizard']
        vals = {}
        payment_link = payment_link_id.with_context({
            'active_id': self.id,
            'active_model': self._name}).create(vals)
        if payment_link:
            qr = qrcode.QRCode(
                version=1,
                box_size=10,
                border=5)
            qr.add_data(payment_link.link)
            qr.make(fit=True)
            img = qr.make_image()
            temp = BytesIO()
            img.save(temp, format="PNG")
            qr_image = base64.b64encode(temp.getvalue())
            return qr_image
