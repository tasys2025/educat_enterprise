# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* openeducat_lms_sale
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 12.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-09 09:58+0000\n"
"PO-Revision-Date: 2019-12-09 09:58+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_lms_sale
#: model:op.student,first_name:openeducat_lms_sale.demo_student
msgid "Bruce"
msgstr "บรูซ"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course__product_id
msgid "Fees"
msgstr "ค่าเล่าเรียน"

#. module: openeducat_lms_sale
#: selection:op.course,type:0
msgid "Free"
msgstr "ฟรี"

#. module: openeducat_lms_sale
#: model:ir.model,name:openeducat_lms_sale.model_op_course
msgid "LMS Course"
msgstr "LMS หลักสูตร"

#. module: openeducat_lms_sale
#: model:ir.model,name:openeducat_lms_sale.model_op_course_enrollment
msgid "LMS Course Enrollment"
msgstr "LMS หลักสูตรการลงทะเบียน"

#. module: openeducat_lms_sale
#: model:product.product,name:openeducat_lms_sale.demo_product
#: model:product.template,name:openeducat_lms_sale.demo_product_product_template
msgid "Learn to Speak: French"
msgstr "เรียนรู้ที่จะพูด: ฝรั่งเศส"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course_enrollment__order_id
msgid "Order"
msgstr "ใบสั่ง"

#. module: openeducat_lms_sale
#: selection:op.course,type:0
msgid "Paid"
msgstr "ต้องจ่าย"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course__price
msgid "Price"
msgstr "ราคา"

#. module: openeducat_lms_sale
#: model:ir.model,name:openeducat_lms_sale.model_sale_order
msgid "Sale Order"
msgstr "การขายการสั่งซื้อสินค้า"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course__type
msgid "Type"
msgstr "ชนิด"

#. module: openeducat_lms_sale
#: model:product.product,uom_name:openeducat_lms_sale.demo_product
#: model:product.template,uom_name:openeducat_lms_sale.demo_product_product_template
msgid "Unit(s)"
msgstr "หน่วย"

#. module: openeducat_lms_sale
#: model:op.student,last_name:openeducat_lms_sale.demo_student
msgid "Willis"
msgstr "วิลลิส"

#. module: openeducat_lms_sale
#: model:product.product,weight_uom_name:openeducat_lms_sale.demo_product
#: model:product.template,weight_uom_name:openeducat_lms_sale.demo_product_product_template
msgid "kg"
msgstr "กก."

