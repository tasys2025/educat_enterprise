
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
##############################################################################
{
    'name': 'OpenEduCat Fees Parent Bridge',
    'description': """This module adds you feature to
     connect Fees module and Parent module.""",
    'version': '15.0.0.0',
    'category': 'Education',
    "sequence": 5,
    'summary': 'Bridge between Fees and Parent Module',
    'complexity': "easy",
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'depends': [
        'openeducat_fees_plan',
        'openeducat_parent_enterprise',
    ],
    'data': [
        'data/fees_reminder_mail_template.xml',
        'data/server_action_view.xml',
        'views/fees_plan.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'price': 298,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'live_test_url': 'https://www.openeducat.org/plans'
}
