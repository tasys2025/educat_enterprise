# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenEduCat Inc
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models


class OpFeesFessPlan(models.Model):
    _inherit = "op.fees.plan"

    def send_reminder_mail_parent(self):
        template = self.env.\
            ref('openeducat_fees_parent_bridge.fees_reminder_template_parent')
        for rec in self:
            email_values = {}
            parents = rec.student_id.parent_ids
            for parent in parents:
                email_values['email_to'] = parent.user_id.login
                ctx = {
                    'parent_name': parent.name.name
                }
                template.with_context(ctx).send_mail(rec.id, email_values=email_values)
