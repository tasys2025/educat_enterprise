# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_exam_gpa_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 10:27+0000\n"
"PO-Revision-Date: 2022-12-12 10:27+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "<b>Course:</b>"
msgstr "<b> Khóa học: </b>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "<b>Grade: </b>"
msgstr "<b> Lớp: </b>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "<b>Institution:</b>"
msgstr "<b> tổ chức: </b>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "<b>Roll Number:</b>"
msgstr "<b> Số cuộn: </b>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "<b>Student :</b>"
msgstr "<b> Sinh viên: </b>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid ""
"<strong>\n"
"                                                Transcript\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Bảng điểm\n"
"                                            </strong>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "<strong>GPA</strong>"
msgstr "<strong> GPA </strong>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "<strong>Percentage</strong>"
msgstr "<strong> Tỷ lệ phần trăm </strong>"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "Credit"
msgstr "Tín dụng"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model.fields,field_description:openeducat_exam_gpa_enterprise.field_op_subject__subject_credit
msgid "Credit Hours"
msgstr "Giờ tín dụng"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "Exam Code"
msgstr "Mã thi"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model.fields,field_description:openeducat_exam_gpa_enterprise.field_op_marksheet_line__gpa_count
msgid "GPA"
msgstr ""

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.view_op_student_progression_inherited_form
msgid "Get Transcript"
msgstr "Nhận bảng điểm"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model,name:openeducat_exam_gpa_enterprise.model_op_grade_configuration
msgid "Grade Configuration"
msgstr "Cấu hình lớp"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model.fields,field_description:openeducat_exam_gpa_enterprise.field_op_grade_configuration__grade_point
#: model:ir.model.fields,field_description:openeducat_exam_gpa_enterprise.field_op_result_line__grade_point
msgid "Grade Point"
msgstr "Lớp điểm"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "Grades"
msgstr "Điểm"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "Marks"
msgstr "Điểm"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model,name:openeducat_exam_gpa_enterprise.model_op_marksheet_line
msgid "Marksheet Line"
msgstr "Dòng đánh dấu"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "Points"
msgstr "Điểm"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model.fields,field_description:openeducat_exam_gpa_enterprise.field_op_result_line__qp
msgid "Quality Points"
msgstr "Điểm chất lượng"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model,name:openeducat_exam_gpa_enterprise.model_op_result_line
msgid "Result Line"
msgstr "Dòng kết quả"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model,name:openeducat_exam_gpa_enterprise.model_op_student_progression
msgid "Student progression"
msgstr "Sự tiến bộ của sinh viên"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.student_transcript_report
msgid "Subject"
msgstr "Môn học"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model.fields,field_description:openeducat_exam_gpa_enterprise.field_op_result_line__credit
msgid "Subject Credit"
msgstr "Tín dụng chủ đề"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model,name:openeducat_exam_gpa_enterprise.model_op_subject
msgid "Subject Material Allocation"
msgstr "Phân bổ vật chất chủ đề"

#. module: openeducat_exam_gpa_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.view_op_marksheet_line_inherited_tree_gpa
msgid "Total Points"
msgstr "Tổng số điểm"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.actions.report,name:openeducat_exam_gpa_enterprise.action_report_transcript
#: model_terms:ir.ui.view,arch_db:openeducat_exam_gpa_enterprise.view_op_student_progression_inherited_form
msgid "Transcript"
msgstr "Bảng điểm"

#. module: openeducat_exam_gpa_enterprise
#: model:ir.model.fields,field_description:openeducat_exam_gpa_enterprise.field_op_marksheet_line__total_points
msgid "total_points"
msgstr "Total_points"
