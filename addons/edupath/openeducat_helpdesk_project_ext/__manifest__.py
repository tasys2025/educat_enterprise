# Part of odoo See LICENSE file for full copyright and licensing details.

{
    'name': 'OpenEduCat Helpdesk Project Extension',
    'version': '16.0.1.0',
    'summary': '''Helpdesk with project and task feature for generating
issues.''',
    'category': 'Human Resources',
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'depends': ['openeducat_core_enterprise', 'openeducat_helpdesk_basic', 'project'],
    'data': [
        'views/helpdesk_ticket_view.xml',
        'views/helpdesk_team_view.xml',
        'views/project_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'license': 'Other proprietary',
}
