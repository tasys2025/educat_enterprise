/** @odoo-module */

import { registry } from '@web/core/registry';
import { standardWidgetProps } from "@web/views/widgets/standard_widget_props";
import { useService } from "@web/core/utils/hooks";
var core = require("web.core");
var ajax = require('web.ajax');
var Dialog = require("web.Dialog");
var qweb = core.qweb;
var _t = core._t;
const { Component } = owl;

export class OMRCamWidget extends Component {

    setup() {
        super.setup();
        const { action, record } = this.props;
        this.record = record
        this.notificationService = useService("notification");
    }

    sleep (ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    destroy () {
        Webcam.reset();
        this._super.apply(this, arguments);
    }

    remove_image () {
        var self = this;
        return ajax.jsonRpc('/omr/unlink/image', 'call', {
            image_ids : parseInt(self.image_ids),
        })
    }

    upload_image (image) {
        var self = this;
        ajax.jsonRpc('/omr/create/image', 'call', {
            omr_exam_id : parseInt(this.record.data.id),
            image: image
        }).then((res) => {
            this.image_ids = res
            return res
        })
    }

    prev_image () {
        var self = this;
        ajax.jsonRpc('/omr/action/preview/image', 'call', {
            omr_exam_name : this.record.data.name,
        }).then((res) => {
            $("#webcam").addClass("d-none")
            if(this.dialog){
                var $table = $('<table></table>')
                var $h1 = $('<h1 style="align:center;">Last Student Record</h1>')
                Object.entries(res).forEach(([k, v]) => {
                    var $tr = $('<tr/>');
                    $tr.append($('<td/>',
                        {
                        }).append($('<input/>', {
                            'disabled': 'disabled',
                            'value': k,
                        })));
                    $tr.append($('<td/>',
                        {
                        }).append($('<input/>', {
                            'disabled': 'disabled',
                            'value': v,
                        })));
                    $table.append($tr);
                });
                // this.$el.append($h1)
                // this.$el.append($table)

                $('#prev_result').append($h1);
                $('#prev_result').append($table);
            }

        })
    }

    scan_image (image_id) {
        var self = this;
        return ajax.jsonRpc('/omr/action/check/image', 'call', {
            omr_exam_name : this.record.data.name,
        }).then(function (flag) {
            if (flag == false) {
                self.notificationService.add(
                    self.env._t("Not Valid Image..!!!"),
                    {
                        type: "danger",
                    });
                self.remove_image()
            } else if (flag == 2) {
                self.notificationService.add(
                    self.env._t("Paper Key Not Found..!!!"),
                    {
                        type: "danger",
                    });
                self.remove_image()
            } else if (flag == 3) {
                self.notificationService.add(
                    self.env._t("Student Id Not Found..!!!"),
                    {
                        type: "danger",
                    });
                self.remove_image()
            } else {
                self.notificationService.add(
                    self.env._t("Successfully Scanned OMR "+ flag + "..!!!"),
                    {
                        type: "danger",
                    });
            }   
        });
    }

    web_cam () {
        var image_id
        this.stop = false;
        var self = this;
        var CamDialog = $(qweb.render('CamDialog', {
            widget: this
        }));
        var dialog = new Dialog(self, {
            size: 'large',
            dialogClass: 'o_act_window',
            title: _t("OMR Scanner"),
            $content: CamDialog,

            buttons: [{
                text: _t("Prev"), classes: 'btn-secondary prev_record_btn',
                click: function () {
                    Webcam.reset();
                    self.prev_image();
                    $(".prev_record_btn").addClass("d-none")
                }
            },
            {
                text: _t("Next"), classes: 'btn-primary take_snap_btn',
                click: function () {
                    if($("#webcam").hasClass("d-none")){
                    $("#webcam").removeClass("d-none")
                    $("#prev_result").addClass("d-none")
                    dialog.close();
                    self.web_cam()
                    }
                    else{
                    Webcam.snap(function (data) {
                        self.img_data = data;
                        self.stop = true;
                        Webcam.reset();
                        self.img_data_base64 = self.img_data.split(',')[1];

                        $.when(self.upload_image(self.img_data_base64)).then(function () {
                            setTimeout(() => {
                                if (self.img_data_base64) {
                                    self.scan_image();
                                }
                            }, 1000)
                        })
                        dialog.close();

                        self.web_cam()

                    });
                    }
                }
            },
            {
                text: _t("Close"),
                click: function () {
                    Webcam.reset();
                    dialog.close();
                    self.stop = true;
                }
            }]

        }).open();
        this.dialog = dialog;
        Webcam.set({
            width: 320,
            height: 480,
            dest_width: 320,
            dest_height: 480,
            image_format: 'jpeg',
            jpeg_quality: 90,
            force_flash: false,
            fps: 45,
        });

        Webcam.attach(CamDialog.find('#webcam')[0]),
            Webcam.on('live', function (data) {
                $('video').css('width', '100%');
                $('video').css('height', '100%');
                $('#webcam').css('width', '100%');
                $('#webcam').css('height', '100%');

            });
        Dialog.include({
            destroy: function () {
                Webcam.reset();
                this._super.apply(this, arguments);
            },
        });
    }

}
OMRCamWidget.props = {
    ...standardWidgetProps,
};
OMRCamWidget.extractProps = ({ attrs }) => {
    
};

OMRCamWidget.template = 'openeducat_omr.omr_cam_widget';
registry.category("view_widgets").add("omr_cam_widget", OMRCamWidget);
