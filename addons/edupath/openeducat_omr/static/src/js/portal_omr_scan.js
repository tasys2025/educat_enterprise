odoo.define('openeducat_omr.omr_scan', function (require) {
    "use strict";

    var core = require('web.core');
    var Dialog = require("web.Dialog");
    var session = require('web.session');
    var ajax = require('web.ajax');
    var Widget = require('web.Widget');
    var publicWidget = require('web.public.widget');
    var OMRCamWidget = require('openeducat_omr.omr_cam_widget');
    var utils = require('web.utils');
    var _t = core._t;
    var qweb = core.qweb;
    var OMRScanWidget = publicWidget.Widget.extend({
    selector:'.omr-table',
    xmlDependencies: ['/openeducat_omr/static/src/xml/web_widget_image_webcam.xml'],
    jsLibs: [
        '/openeducat_omr/static/src/lib/webcam.js',
    ],

    init: function () {
        this._super.apply(this, arguments);
    },
    start: function () {
        var self = this
        this._super.apply(this, arguments).then(()=>{
            self._onClickopenWebcam()
        })
    },

    _onClickopenWebcam: function(e){
        var self = this
        $('.omr-scan').each(function(e,b){
            var scanwidget = new OMRCamWidget(self)
            scanwidget.record = {'data':$(b).find('input').val()}
            scanwidget.appendTo($(b))
        })
    },

})

publicWidget.registry.OMRScanWidget = OMRScanWidget;
return OMRScanWidget;

});

