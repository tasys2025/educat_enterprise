# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class CRMLead(models.Model):
    _inherit = "crm.lead"
    _description = "Custom crm lead"

    project_id = fields.Many2one('project.project', string='Project')
    project_stage_id = fields.Many2one('project.project.stage', string='Status', related='project_id.stage_id')
    task_count = fields.Integer('Number of task', related='project_id.task_count')
    task_count_with_subtasks = fields.Integer(related='project_id.task_count_with_subtasks')

    def action_view_project_task(self):
        domain = [('project_id', '=', self.project_id.id)]
        context = {'default_project_id': self.project_id.id}
        return {
            'name': _('Tasks'),
            'view_mode': 'kanban,tree,form',
            'res_model': 'project.task',
            'type': 'ir.actions.act_window',
            'domain': domain,
            'context': context
        }



