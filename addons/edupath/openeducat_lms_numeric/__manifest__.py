{
    'name': 'OpenEduCat LMS Quiz numeric',
    'description': """Based on best of class enterprise level architecture,
    OpenEduCat is ready to be used from local infrastructure to
    a highly scalable cloud environment.""",
    'version': '16.0.1.0',
    'category': 'Education',
    'website': 'http://www.openeducat.org',
    'summary': 'OpenEduCat LMS Quiz numeric',
    'author': 'OpenEduCat Inc',
    'depends': ['openeducat_quiz', 'openeducat_quiz_numeric', 'openeducat_lms'],
    'data': [
        'views/website_lms_numeric.xml',
        ],

    'demo': [],
    'installable': True,
    'qweb': [
    ],
    'auto_install': False,
    'application': True,
    'license': 'Other proprietary',
    'live_test_url': 'https://www.openeducat.org/plans'
}
