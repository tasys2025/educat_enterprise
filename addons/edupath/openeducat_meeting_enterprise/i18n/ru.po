# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_meeting_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 09:11+0000\n"
"PO-Revision-Date: 2022-12-14 09:11+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<small class=\"text-muted\">Meeting Data Information</small>"
msgstr "<small class=\"Text-Mated\"> Соблюдение данных данных </small>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "<span>hours</span>"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>All Day:</strong>"
msgstr "<strong> весь день: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Attendees:</strong>"
msgstr "<strong> Участники: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Description:</strong>"
msgstr "<strong> Описание: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Duration:</strong>"
msgstr "<strong> Продолжительность: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Location:</strong>"
msgstr "<strong> Местоположение: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Meeting Subject:</strong>"
msgstr "<strong> Предмет встречи: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Privacy:</strong>"
msgstr "<strong> конфиденциальность: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Reminders:</strong>"
msgstr "<strong> напоминания: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Show Time as:</strong>"
msgstr "<strong> Покажите время как: </strong>"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "<strong>Starting at:</strong>"
msgstr "<strong> Начиная с: </strong>"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__academic_calendar
msgid "Academic Calendar?"
msgstr "Академический календарь?"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Accept"
msgstr "Принимать"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_needaction
msgid "Action Needed"
msgstr "Действие необходимо"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__active
msgid "Active"
msgstr "Активный"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__activity_ids
msgid "Activities"
msgstr "мероприятия"

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "All"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__allday
msgid "All Day"
msgstr "Весь день"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.op_faculty_user_create_view
msgid "Are You Sure You Want To Create Meeting"
msgstr "Вы уверены, что хотите создать встречу"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_attachment_count
msgid "Attachment Count"
msgstr "Подсчет приложений"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__apw
msgid "Attendee Password"
msgstr "Пароль посетителя"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__attendee_status
msgid "Attendee Status"
msgstr "Статус участника"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__partner_ids
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Attendees"
msgstr "Участники"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__automatic_email
msgid "Automatic Email"
msgstr "Автоматическое электронное письмо"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__batch_ids
msgid "Batches"
msgstr "Партии"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__byday
msgid "By day"
msgstr "Днем"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.op_faculty_user_create_view
msgid "Cancel"
msgstr "Отмена"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__is_organizer_alone
msgid ""
"Check if the organizer is alone in the event, i.e. if the organizer is the only one that hasn't declined\n"
"        the event (only if the organizer is not the only attendee)"
msgstr ""
"Проверьте, является ли организатор одиноким в мероприятии, то есть, если организатор является единственным, который не отклонился\n"
"        Событие (только если организатор не единственный участник)"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__recurrence_update
msgid ""
"Choose what to do with other events in the recurrence. Updating All Events "
"is not allowed when dates or time is modified"
msgstr ""
"Выберите, что делать с другими событиями в повторении. Обновление всех "
"событий не допускается, когда даты или время изменяются"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__meeting_enterprise_onboard_panel__closed
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__onboarding_meeting_layout_state__closed
msgid "Closed"
msgstr "Закрыто"

#. module: openeducat_meeting_enterprise
#: model:ir.model,name:openeducat_meeting_enterprise.model_res_company
msgid "Companies"
msgstr "Компании"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.op_faculty_user_create_view
msgid "Confirm"
msgstr "Подтверждать"

#. module: openeducat_meeting_enterprise
#: model:ir.model,name:openeducat_meeting_enterprise.model_res_partner
msgid "Contact"
msgstr "Контакт"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__course_ids
msgid "Courses"
msgstr "Курсы"

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/wizard/op_meeting_wizard.py:0
#, python-format
msgid "Create Event"
msgstr "Создать событие"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.op_faculty_user_create_view
msgid "Create Meeting Event"
msgstr "Создать событие встречи"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__create_uid
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting_wizard__create_uid
msgid "Created by"
msgstr "Сделано"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__create_date
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting_wizard__create_date
msgid "Created on"
msgstr "Создано на"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.onboarding_meeting_layout_step
msgid "Customize"
msgstr "Настраивать"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__day
msgid "Date of month"
msgstr "Дата месяца"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Decline"
msgstr "Отклонить"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__description
msgid "Description"
msgstr "Описание"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__videocall_channel_id
msgid "Discuss Channel"
msgstr "Обсудить канал"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__display_description
msgid "Display Description"
msgstr "Отображение описание"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__display_name
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting_wizard__display_name
msgid "Display Name"
msgstr "Показать имя"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__res_id
msgid "Document ID"
msgstr "Идентификатор документа"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__res_model_id
msgid "Document Model"
msgstr "Модель документа"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__res_model
msgid "Document Model Name"
msgstr "Название модели документа"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__meeting_enterprise_onboard_panel__done
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__onboarding_meeting_layout_state__done
msgid "Done"
msgstr "Сделанный"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__duration
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Duration"
msgstr "Продолжительность"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__stop_date
msgid "End Date"
msgstr "Дата окончания"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Ending at"
msgstr "Заканчивая на"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__display_time
msgid "Event Time"
msgstr "Время события"

#. module: openeducat_meeting_enterprise
#: model:res.partner.category,name:openeducat_meeting_enterprise.res_partner_category_faculty
msgid "Faculty"
msgstr "Факультет"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__follow_recurrence
msgid "Follow Recurrence"
msgstr "Следуйте за повторением"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_follower_ids
msgid "Followers"
msgstr "Последователи"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_partner_ids
msgid "Followers (Partners)"
msgstr "Последователи (партнеры)"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__fri
msgid "Fri"
msgstr "Пт"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__has_message
msgid "Has Message"
msgstr "Имеет сообщение"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__id
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting_wizard__id
msgid "ID"
msgstr "Я БЫ"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__message_needaction
msgid "If checked, new messages require your attention."
msgstr "При проверке новые сообщения требуют вашего внимания."

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__message_has_error
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "При проверке некоторые сообщения имеют ошибку доставки."

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__active
msgid ""
"If the active field is set to false, it will allow you to hide the event "
"alarm information without removing it."
msgstr ""
"Если активное поле установлено на false, оно позволит вам скрыть информацию "
"о тревоге, не удаляя его."

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__show_as
msgid ""
"If the time is shown as 'busy', this event will be visible to other people with either the full         information or simply 'busy' written depending on its privacy. Use this option to let other people know         that you are unavailable during that period of time. \n"
" If the time is shown as 'free', this event won't         be visible to other people at all. Use this option to let other people know that you are available during         that period of time."
msgstr ""
"Если время будет показано как «занято», это событие будет видно для других людей с полной информацией или просто «занятым», написанным в зависимости от его конфиденциальности. Используйте эту опцию, чтобы другие люди знали, что вы недоступны в течение этого периода времени.\n"
" Если время будет показано как «бесплатное», это событие вообще не будет видно для других людей. Используйте эту опцию, чтобы другие люди знали, что вы доступны в течение этого периода времени."

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__invalid_email_partner_ids
msgid "Invalid Email Partner"
msgstr "Недействительный партнер по электронной почте"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__access_token
msgid "Invitation Token"
msgstr "Приглашение токен"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Invitation details"
msgstr "Детали приглашения"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Invitations"
msgstr "Приглашения"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_is_follower
msgid "Is Follower"
msgstr "Это последователь"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__is_highlighted
msgid "Is the Event Highlighted"
msgstr "Выдвинуто на первый план событие"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__is_organizer_alone
msgid "Is the Organizer Alone"
msgstr "Один органайзер"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__jitsi_open
msgid "Jitsi Open"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__jitsi_room
msgid "Jitsi Room"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__meeting_enterprise_onboard_panel__just_done
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__onboarding_meeting_layout_state__just_done
msgid "Just done"
msgstr "Только что сделано"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting____last_update
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting_wizard____last_update
msgid "Last Modified on"
msgstr "Последнее изменено"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__write_uid
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting_wizard__write_uid
msgid "Last Updated by"
msgstr "Последнее обновлено"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__write_date
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting_wizard__write_date
msgid "Last Updated on"
msgstr "Последнее обновлено"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__rrule_type
msgid "Let the event automatically repeat at that interval"
msgstr "Пусть событие автоматически повторяется в этом интервале"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__location
msgid "Location"
msgstr "Расположение"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.onboarding_meeting_layout_step
msgid "Looks great!"
msgstr "Выглядит отлично!"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_main_attachment_id
msgid "Main Attachment"
msgstr "Основное вложение"

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#: model:ir.actions.act_window,name:openeducat_meeting_enterprise.action_meeting_faculty
#: model:ir.actions.act_window,name:openeducat_meeting_enterprise.action_meeting_parent
#: model:ir.actions.act_window,name:openeducat_meeting_enterprise.action_meeting_wizard_student
#: model:ir.model,name:openeducat_meeting_enterprise.model_op_meeting
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__meeting_id
#: model:ir.ui.menu,name:openeducat_meeting_enterprise.menu_meeting
#, python-format
msgid "Meeting"
msgstr "Встреча"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Meeting Details"
msgstr "Подробности встречи"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__meeting_name
msgid "Meeting ID"
msgstr "Идентификатор встречи"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.onboarding_meeting_layout_step
msgid "Meeting Layout Configuration"
msgstr "Конфигурация макета собрания"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal
msgid "Meeting List"
msgstr "Список встреч"

#. module: openeducat_meeting_enterprise
#: model:ir.actions.act_window,name:openeducat_meeting_enterprise.action_onboarding_meeting_layout
msgid "Meeting Record Configuration"
msgstr "Конфигурация записи встречи"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__name
msgid "Meeting Subject"
msgstr "Предмет встречи"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__videocall_location
msgid "Meeting URL"
msgstr "УРЛ"

#. module: openeducat_meeting_enterprise
#: model:ir.model,name:openeducat_meeting_enterprise.model_op_meeting_wizard
msgid "Meeting Wizard"
msgstr "Мастер встречи"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
msgid "Meeting-Deatils"
msgstr "Собрание-деатилс"

#. module: openeducat_meeting_enterprise
#: model:openeducat.portal.menu,name:openeducat_meeting_enterprise.poratl_menu_meeting_details
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.portal_my_home_menu_meeting
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Meetings"
msgstr "Встречи"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_has_error
msgid "Message Delivery error"
msgstr "Ошибка доставки сообщения"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_ids
msgid "Messages"
msgstr "Сообщения"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Misc"
msgstr "Разное"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__mpw
msgid "Moderator Password"
msgstr "Пароль модератора"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__mon
msgid "Mon"
msgstr "Пн"

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "None"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__meeting_enterprise_onboard_panel__not_done
#: model:ir.model.fields.selection,name:openeducat_meeting_enterprise.selection__res_company__onboarding_meeting_layout_state__not_done
msgid "Not done"
msgstr "Не сделано"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__alarm_ids
msgid "Notifications sent to all attendees to remind of the meeting."
msgstr "Уведомления, отправленные всем участникам, чтобы напомнить о встрече."

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_needaction_counter
msgid "Number of Actions"
msgstr "Количество действий"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_has_error_counter
msgid "Number of errors"
msgstr "Количество ошибок"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Количество сообщений, которые требуют действия"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Количество сообщений с ошибкой доставки"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__online_meeting
msgid "Online Meeting"
msgstr "Онлайн встреча"

#. module: openeducat_meeting_enterprise
#: model:ir.model,name:openeducat_meeting_enterprise.model_op_faculty
msgid "OpenEduCat Faculty"
msgstr "Openeducat Faculty"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__opportunity_id
msgid "Opportunity"
msgstr "Возможность"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__month_by
msgid "Option"
msgstr "Вариант"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.openeducat_meeting_portal_data
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Options"
msgstr "Опции"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__user_id
msgid "Organizer"
msgstr "Организатор"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Owner"
msgstr "Владелец"

#. module: openeducat_meeting_enterprise
#: model:ir.model,name:openeducat_meeting_enterprise.model_op_parent
#: model:res.partner.category,name:openeducat_meeting_enterprise.res_partner_category_parent
msgid "Parent"
msgstr "Родительский"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__attendee_ids
msgid "Participant"
msgstr "Участник"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__partner_id
msgid "Partner-related data of the user"
msgstr "Связанные с партнером данные пользователя"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__privacy
msgid "People to whom this event will be visible."
msgstr "Люди, которым будет видно это событие."

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__privacy
msgid "Privacy"
msgstr "Конфиденциальность"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__rrule_type
msgid "Recurrence"
msgstr "Рецидив"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__recurrence_id
msgid "Recurrence Rule"
msgstr "Правило повторения"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__end_type
msgid "Recurrence Termination"
msgstr "Повторное прекращение"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__recurrence_update
msgid "Recurrence Update"
msgstr "Рецидивное обновление"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__recurrency
msgid "Recurrent"
msgstr "Повторяется"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__rrule
msgid "Recurrent Rule"
msgstr "Повторяющееся правило"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__alarm_ids
msgid "Reminders"
msgstr "Напоминания"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__use_jitsi
msgid "Remote Meeting"
msgstr "Отдаленная встреча"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__count
msgid "Repeat"
msgstr "Повторение"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__interval
msgid "Repeat Every"
msgstr "Повторить каждый"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__interval
msgid "Repeat every (Days/Week/Month/Year)"
msgstr "Повторяйте каждый (дни/неделя/месяц/год)"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__count
msgid "Repeat x times"
msgstr "Повторите x раз"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__message_has_sms_error
msgid "SMS Delivery error"
msgstr "СМС ошибка доставки"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__salt
msgid "Salt"
msgstr "Соль"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__sat
msgid "Sat"
msgstr "Сидел"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__partner_id
msgid "Scheduled by"
msgstr "Запланировано"

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "Search <span class=\"nolabel\"> (in Subject)</span>"
msgstr ""

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "Search in All"
msgstr ""

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "Search in Duration"
msgstr ""

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "Search in EndDate"
msgstr ""

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "Search in Location"
msgstr ""

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "Search in StartDate"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.onboarding_meeting_layout_step
msgid "See how to create a Meeting Layout."
msgstr "Посмотрите, как создать макет встречи."

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Select attendees..."
msgstr "Выберите посетителей ..."

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__op_session_id
msgid "Session"
msgstr "Сессия"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__show_as
msgid "Show as"
msgstr "Показать как"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__start
msgid "Start"
msgstr "Начинать"

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__start_date
#, python-format
msgid "Start Date"
msgstr "Дата начала"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__start
msgid "Start date of an event, without time for full days events"
msgstr "Начать дату мероприятия, без времени для полных дней событий"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Starting at"
msgstr "Начинается с"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_res_company__meeting_enterprise_onboard_panel
msgid "State of the meeting onboarding step"
msgstr "Состояние стадии встречи"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_res_company__onboarding_meeting_layout_state
msgid "State of the onboarding meeting layout  step"
msgstr "Состояние шага макета собрания"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__stop
msgid "Stop"
msgstr "Останавливаться"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__stop
msgid "Stop date of an event, without time for full days events"
msgstr "Остановка даты мероприятия, без времени для проведения полных дней"

#. module: openeducat_meeting_enterprise
#: model:ir.model,name:openeducat_meeting_enterprise.model_op_student
#: model:res.partner.category,name:openeducat_meeting_enterprise.res_partner_category_student
msgid "Student"
msgstr "Ученик"

#. module: openeducat_meeting_enterprise
#. odoo-python
#: code:addons/openeducat_meeting_enterprise/controller/main.py:0
#, python-format
msgid "Subject"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__sun
msgid "Sun"
msgstr "Солнце"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__categ_ids
msgid "Tags"
msgstr "Теги"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__jitsi_open
msgid "The Remote Meeting allows to join"
msgstr "Отдаленное собрание позволяет присоединиться"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__thu
msgid "Thu"
msgstr "Ту"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__event_tz
msgid "Timezone"
msgstr "Часовой пояс"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__tue
msgid "Tue"
msgstr "Вторник"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__meet_url
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__meeting_url
msgid "URL"
msgstr ""

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "Uncertain"
msgstr "Неопределенность"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__until
msgid "Until"
msgstr "До того как"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__videocall_source
msgid "Videocall Source"
msgstr "Источник видеокаллов"

#. module: openeducat_meeting_enterprise
#: model:ir.model,name:openeducat_meeting_enterprise.model_ir_ui_view
msgid "View"
msgstr "Вид"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__website_message_ids
msgid "Website Messages"
msgstr "Сообщения веб -сайта"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,help:openeducat_meeting_enterprise.field_op_meeting__website_message_ids
msgid "Website communication history"
msgstr "История общения веб -сайта"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__wed
msgid "Wed"
msgstr "Мы б"

#. module: openeducat_meeting_enterprise
#: model:ir.model.fields,field_description:openeducat_meeting_enterprise.field_op_meeting__weekday
msgid "Weekday"
msgstr "Будний день"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.view_op_meeting_form_onboard
msgid "e.g. Business Lunch"
msgstr "например Бизнес ланч"

#. module: openeducat_meeting_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_meeting_enterprise.op_faculty_user_create_view
msgid "or"
msgstr "или же"
