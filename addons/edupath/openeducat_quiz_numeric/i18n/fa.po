# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_quiz_numeric
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 10:52+0000\n"
"PO-Revision-Date: 2022-12-13 10:52+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_quiz_numeric
#: model:ir.model.fields.selection,name:openeducat_quiz_numeric.selection__op_quiz_line__que_type__numeric
#: model:ir.model.fields.selection,name:openeducat_quiz_numeric.selection__op_quiz_result_line__que_type__numeric
msgid "Numeric"
msgstr "عددی"

#. module: openeducat_quiz_numeric
#: model:ir.model.fields,field_description:openeducat_quiz_numeric.field_op_quiz_line__que_type
#: model:ir.model.fields,field_description:openeducat_quiz_numeric.field_op_quiz_result_line__que_type
msgid "Question Type"
msgstr "نوع پرسش"

#. module: openeducat_quiz_numeric
#: model:ir.model,name:openeducat_quiz_numeric.model_op_quiz_line
msgid "Questions"
msgstr "پرسش"

#. module: openeducat_quiz_numeric
#: model:ir.model,name:openeducat_quiz_numeric.model_op_quiz
msgid "Quiz"
msgstr "کوشش"

#. module: openeducat_quiz_numeric
#: model:ir.model,name:openeducat_quiz_numeric.model_op_quiz_result_line
msgid "Quiz Result Line"
msgstr "خط نتیجه نتیجه"

#. module: openeducat_quiz_numeric
#: model:ir.model,name:openeducat_quiz_numeric.model_op_quiz_result
msgid "Quiz Results"
msgstr "نتایج مسابقه"

#. module: openeducat_quiz_numeric
#: model:ir.model.fields,field_description:openeducat_quiz_numeric.field_op_quiz_line__numeric_answer
msgid "numeric"
msgstr "عددی"
