
msgid ""
msgstr ""
"Project-Id-Version:Odoo Server 15.0"
"Report-Msgid-Bugs-To:"
"POT-Creation-Date:2022-09-23 04:15+0000"
"PO-Revision-Date:2022-09-23 04:15+0000"
"Last-Translator:"
"Language-Team:"
"MIME-Version:1.0"
"Content-Type:text/plain; charset=UTF-8"
"Content-Transfer-Encoding:"
"Plural-Forms:"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.portal_my_home_menu_student_time_off
msgid "/ Leave Request Form"
msgstr "/ İstek Formu Bırakın"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.portal_my_home_menu_student_time_off
msgid "/ Leave Request Submit"
msgstr "/ İstek Gönder Gönder"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid ""
"<i class=\"fa fa-check mr-2\"/>\n"
"                                            Approve"
msgstr ""
"<i class=\"fa fa-kontrol mr-2\"/>\n"
" Onayla"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid ""
"<i class=\"fa fa-fw fa-check\" aria-label=\"Approved\" title=\"Approved\" role=\"img\"/>\n"
"                                            <span class=\"d-none d-md-inline\">Approved</span>"
msgstr ""
"<i class=\"fa-fw fa-check\" aria-label=\"Onaylandı\" title=\"Onaylandı\" role=\"img\"/>\n"
" <span class=\"d- yok d-md-inline\">Onaylandı</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid ""
"<i class=\"fa fa-fw fa-check-square mr-2\"/>\n"
"                                            Validate"
msgstr ""
"<i class=\"fa fa-fw fa-check-square mr-2\"/>\n"
" Doğrula"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid ""
"<i class=\"fa fa-fw fa-check-square\" aria-label=\"Validated\" title=\"Validated\" role=\"img\"/>\n"
"                                            <span class=\"d-none d-md-inline\">Validate</span>"
msgstr ""
"<i class=\"fa-fw fa-check-square\" aria-label=\"Doğrulandı\" title=\"Doğrulandı\" role=\"img\"/>\n"
" <span class=\" d-none d-md-inline\">Doğrula</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid ""
"<i class=\"fa fa-fw fa-clock-o mr-2\"/>\n"
"                                            Confirm"
msgstr ""
"<i class=\"fa fa-fw fa-clock-o mr-2\"/>\n"
" Onayla"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid ""
"<i class=\"fa fa-fw fa-clock-o\" aria-label=\"Requested\" title=\"Requested\" role=\"img\"/>\n"
"                                            <span class=\"d-none d-md-inline\">Requested</span>"
msgstr ""
"<i class=\"fa-fw fa-clock-o\" aria-label=\"İstenen\" title=\"İstenen\" role=\"img\"/>\n"
" <span class=\" d-none d-md-inline\">İstenen</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid ""
"<i class=\"fa fa-fw fa-remove mr-2\"/>\n"
"                                            Cancel"
msgstr ""
"<i class=\"fa fa-fw fa-mr-2'yi kaldır\"/>\n"
" İptal"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid ""
"<i class=\"fa fa-fw fa-remove\" aria-label=\"Cancelled\" title=\"Cancelled\" role=\"img\"/>\n"
"                                            <span class=\"d-none d-md-inline\">Cancelled</span>"
msgstr ""
"<i class=\"fa-fw fa-remove\" aria-label=\"İptal edildi\" title=\"İptal edildi\" role=\"img\"/>\n"
" <span class=\"d- yok d-md-inline\">İptal edildi</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid ""
"<i class=\"fa fa-fw fa-square mr-2\"/>\n"
"                                            Make as Draft"
msgstr ""
"<i class=\"fa fa-fw fa-square mr-2\"/>\n"
" Taslak Yap"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid ""
"<i class=\"fa fa-fw fa-square mr-2\"/>\n"
"                                            Refuse"
msgstr ""
"<i class=\"fa fa-fw fa-kare mr-2\"/>\n"
" Reddet"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid ""
"<i class=\"fa fa-fw fa-square\" aria-label=\"Draft\" title=\"Draft\" role=\"img\"/>\n"
"                                            <span class=\"d-none d-md-inline\">Update</span>"
msgstr ""
"<i class=\"fa-fw fa-kare\" aria-label=\"Taslak\" title=\"Taslak\" role=\"img\"/>\n"
" <span class=\"d- yok d-md-inline\">Güncelle</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid ""
"<i class=\"fa fa-fw fa-square\" aria-label=\"Refused\" title=\"Refused\" role=\"img\"/>\n"
"                                            <span class=\"d-none d-md-inline\">Refused</span>"
msgstr ""
"<i class=\"fa-fw fa-kare\" aria-label=\"Reddedildi\" title=\"Reddedildi\" role=\"img\"/>\n"
" <span class=\"d- hiçbiri d-md-inline\">Reddedildi</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid ""
"<i class=\"fa fa-long-arrow-right mx-2\" aria-label=\"Arrow icon\" "
"title=\"Arrow\"/>"
msgstr "<i class=\"fa fa-uzun-ok-sağ mx-2\" aria-label=\"Ok simgesi\" title=\"Ok\"/>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "<span class=\"font-weight-bold\">Approve Date:</span>"
msgstr "<span class=\"font-weight-bold\">Onaylama Tarihi:</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "<span class=\"font-weight-bold\">Description:</span>"
msgstr "<span class=\"font-weight-bold\">Açıklama:</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "<span class=\"font-weight-bold\">From:</span>"
msgstr "<span class=\"font-weight-bold\">Kimden:</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "<span class=\"font-weight-bold\">Leave Type:</span>"
msgstr "<span class=\"font-weight-bold\">Bırakma Türü:</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "<span class=\"font-weight-bold\">Status:</span>"
msgstr "<span class=\"font-weight-bold\">Durum:</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "<span class=\"font-weight-bold\">to</span>"
msgstr "<span class=\"font-weight-bold\">için</span>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "<strong>Attachment:</strong>"
msgstr "<strong>Ek:</strong>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "<strong>Description:</strong>"
msgstr "<strong>Açıklama:</strong>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "<strong>End Date:</strong>"
msgstr "<strong>Bitiş Tarihi:</strong>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "<strong>Faculty:</strong>"
msgstr "<strong>Fakülte:</strong>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "<strong>Leave Type:</strong>"
msgstr "<strong>Türden Ayrıl:</strong>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "<strong>Start Date:</strong>"
msgstr "<strong>Başlangıç ​​Tarihi:</strong>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "<strong>Student:</strong>"
msgstr "<strong>Öğrenci:</strong>"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_needaction
msgid "Action Needed"
msgstr "Ihtiyaç duyulan eylem"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_ids
msgid "Activities"
msgstr "Aktiviteler"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_exception_decoration
msgid "Activity Exception Decoration"
msgstr "Etkinlik İstisnası Dekorasyonu"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_state
msgid "Activity State"
msgstr "Faaliyet Durumu"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_type_icon
msgid "Activity Type Icon"
msgstr "Etkinlik Türü Simgesi"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#, python-format
msgid "All"
msgstr "Herşey"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid "Approve"
msgstr "Onaylamak"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__approve_date
msgid "Approve Date"
msgstr "Onay Tarihi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_leave_enterprise.selection__student_leave_request__state__approve
msgid "Approved"
msgstr "Onaylı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__approved_by_id
msgid "Approved By"
msgstr "Tarafından onaylandı"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid "Approved/Refused By"
msgstr "Onaylayan/Reddeden"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid "Approved/Refused On"
msgstr "Onaylandı/Reddedildi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_attachment_count
msgid "Attachment Count"
msgstr "Ek Sayısı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__attachment_ids
msgid "Attachments"
msgstr "ekler"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid "Cancel"
msgstr "İptal"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_leave_enterprise.selection__student_leave_request__state__cancel
msgid "Cancelled"
msgstr "İptal edildi"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_type_form_view
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_type_tree_view
msgid "Code"
msgstr "kod"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__color
msgid "Color Index"
msgstr "Renk İndeksi"

#. module: openeducat_student_leave_enterprise
#: model:ir.ui.menu,name:openeducat_student_leave_enterprise.menu_student_leave_configure
msgid "Configuration"
msgstr "Yapılandırma"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid "Confirm"
msgstr "Onaylamak"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid "Create Leave Request"
msgstr "İzin Talebi Oluştur"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__create_uid
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__create_uid
msgid "Created by"
msgstr "Tarafından yaratıldı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__create_date
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__create_date
msgid "Created on"
msgstr "tarihinde oluşturuldu"

#. module: openeducat_student_leave_enterprise
#: model:ir.ui.menu,name:openeducat_student_leave_enterprise.menu_student_leave_request_dashboard
msgid "Dashboard"
msgstr "Gösterge Paneli"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#, python-format
msgid "Date"
msgstr "Tarih"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "Delete"
msgstr "Silmek"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__description
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
msgid "Description"
msgstr "Tanım"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__display_name
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__display_name
msgid "Display Name"
msgstr "Ekran adı"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "Dropdown menu"
msgstr "Aşağıya doğru açılan menü"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__duration
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
msgid "Duration"
msgstr "Süre"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "Edit Time Off"
msgstr "Kapalı Zamanı Düzenle"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__email_cc
msgid "Email cc"
msgstr "e-posta cc"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__end_date
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
msgid "End Date"
msgstr "Bitiş tarihi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__faculty_id
msgid "Faculty"
msgstr "Fakülte"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_follower_ids
msgid "Followers"
msgstr "Takipçiler"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_partner_ids
msgid "Followers (Partners)"
msgstr "Takipçiler (Ortaklar)"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__activity_type_icon
msgid "Font awesome icon e.g. fa-tasks"
msgstr "Yazı tipi harika simgesi ör. fa-görevler"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__has_message
msgid "Has Message"
msgstr "Mesajı Var"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__id
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__id
msgid "ID"
msgstr "İD"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_exception_icon
msgid "Icon"
msgstr "Simge"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__activity_exception_icon
msgid "Icon to indicate an exception activity."
msgstr "Bir istisna etkinliğini gösteren simge."

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__message_needaction
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__message_unread
msgid "If checked, new messages require your attention."
msgstr "İşaretlenirse, yeni mesajlarla ilgilenmeniz gerekir."

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__message_has_error
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "İşaretlenirse, bazı iletilerde teslim hatası olur."

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_is_follower
msgid "Is Follower"
msgstr "takipçi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request____last_update
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type____last_update
msgid "Last Modified on"
msgstr "Son Değiştirilme Tarihi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__write_uid
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__write_uid
msgid "Last Updated by"
msgstr "Son Güncelleyen"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__write_date
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__write_date
msgid "Last Updated on"
msgstr "Son Güncelleme Tarihi"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid "Leave From"
msgstr "Dan ayrılmak"

#. module: openeducat_student_leave_enterprise
#: model:ir.actions.act_window,name:openeducat_student_leave_enterprise.student_leave_request_action
#: model:ir.actions.act_window,name:openeducat_student_leave_enterprise.student_leave_request_action_calendar
msgid "Leave Request"
msgstr "İstekten Çık"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.portal_my_home_menu_student_time_off
msgid "Leave Request List"
msgstr "İstek Listesinden Ayrıl"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#: model:ir.actions.act_window,name:openeducat_student_leave_enterprise.student_leave_type_action
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__leave_type
#: model:ir.ui.menu,name:openeducat_student_leave_enterprise.menu_student_leave_type
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
#, python-format
msgid "Leave Type"
msgstr "Bırak Türü"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__email_cc
msgid "List of cc from incoming emails."
msgstr "Gelen e-postalardan gelen cc listesi."

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "MM/DD/YYYY"
msgstr "AA/GG/YYYY"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_main_attachment_id
msgid "Main Attachment"
msgstr "Ana Ek"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid "Mark as Draft"
msgstr "Taslak Olarak İşaretle"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_has_error
msgid "Message Delivery error"
msgstr "Mesaj Teslim hatası"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_ids
msgid "Messages"
msgstr "Mesajlar"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__my_activity_date_deadline
msgid "My Activity Deadline"
msgstr "Etkinliğimin Son Tarihi"

#. module: openeducat_student_leave_enterprise
#: model:ir.ui.menu,name:openeducat_student_leave_enterprise.menu_leave_request
msgid "My Time Off"
msgstr "Boş Zamanım"

#. module: openeducat_student_leave_enterprise
#: model:ir.ui.menu,name:openeducat_student_leave_enterprise.menu_student_leave_request
msgid "My Time Off Request"
msgstr "İzin İsteğim"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_calendar_event_id
msgid "Next Activity Calendar Event"
msgstr "Sonraki Etkinlik Takvimi Etkinliği"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_date_deadline
msgid "Next Activity Deadline"
msgstr "Sonraki Etkinlik Son Tarihi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_summary
msgid "Next Activity Summary"
msgstr "Sonraki Etkinlik Özeti"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_type_id
msgid "Next Activity Type"
msgstr "Sonraki Etkinlik Türü"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_needaction_counter
msgid "Number of Actions"
msgstr "Eylem Sayısı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_has_error_counter
msgid "Number of errors"
msgstr "Hata sayısı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "İşlem gerektiren mesaj sayısı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Teslim hatası olan mesaj sayısı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__message_unread_counter
msgid "Number of unread messages"
msgstr "Okunmamış mesaj sayısı"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid "Refuse"
msgstr "Reddetmek"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_leave_enterprise.selection__student_leave_request__state__refuse
msgid "Refused"
msgstr "reddedildi"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__request_number
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
#, python-format
msgid "Request Number"
msgstr "Talep Numarası"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__activity_user_id
msgid "Responsible User"
msgstr "Sorumlu Kullanıcı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_has_sms_error
msgid "SMS Delivery error"
msgstr "SMS Teslim hatası"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#, python-format
msgid "Search <span class=\"nolabel\"> (Description)</span>"
msgstr "<span class=\"nolabel\"> (Açıklama)</span>'da ara"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#, python-format
msgid "Search in All"
msgstr "Tümünde Ara"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#, python-format
msgid "Search in Leave Type"
msgstr "İzin Türünde Ara"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#, python-format
msgid "Search in Start Date"
msgstr "Başlangıç ​​Tarihinde Ara"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#, python-format
msgid "Search in Status"
msgstr "Durumda Ara"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "Select Faculty"
msgstr "Fakülte seçin"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "Select your option"
msgstr "Seçeneğinizi seçin"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__start_date
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
msgid "Start Date"
msgstr "Başlangıç ​​tarihi"

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/models/student_leave.py:0
#, python-format
msgid "Start End Date should after Start Date."
msgstr "Başlangıç ​​Bitiş Tarihi, Başlangıç ​​Tarihinden sonra olmalıdır."

#. module: openeducat_student_leave_enterprise
#: code:addons/openeducat_student_leave_enterprise/controller/main.py:0
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__state
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
#, python-format
msgid "Status"
msgstr "Durum"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__activity_state
msgid ""
"Status based on activities\n"
"Overdue: Due date is already passed\n"
"Today: Activity date is today\n"
"Planned: Future activities."
msgstr ""
"Etkinliklere dayalı durum\n"
"Gecikti: Son tarih zaten geçti\n"
"Bugün: Etkinlik tarihi bugün\n"
"Planlanan: Gelecekteki etkinlikler."

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__student_id
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
msgid "Student"
msgstr "Öğrenci"

#. module: openeducat_student_leave_enterprise
#: model:ir.ui.menu,name:openeducat_student_leave_enterprise.menu_main_student_leave
msgid "Student Leave"
msgstr "Öğrenci İzni"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid "Student Leave List"
msgstr "Öğrenci İzin Listesi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model,name:openeducat_student_leave_enterprise.model_student_leave_request
msgid "Student Leave Request"
msgstr "Öğrenci İzin Talebi"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
msgid "Student Leave Request Submit"
msgstr "Öğrenci İzin Talebi Gönder"

#. module: openeducat_student_leave_enterprise
#: model:ir.model,name:openeducat_student_leave_enterprise.model_student_leave_type
msgid "Student Leave Type"
msgstr "Öğrenci İzin Türü"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_search_view
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_tree_view
msgid "Student Name"
msgstr "Öğrenci adı"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid "Student Time Off Request"
msgstr "Öğrenci İzin Talebi"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.openeducat_student_time_off_request
msgid ""
"Submit\n"
"                                <span class=\"fa fa-long-arrow-right\"/>"
msgstr ""
"Gönder\n"
" <span class=\"fa fa-uzun-ok-sağ\"/>"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
msgid "There are no records."
msgstr "Kayıt yok."

#. module: openeducat_student_leave_enterprise
#: model:openeducat.portal.menu,name:openeducat_student_leave_enterprise.website_menu_time_off_request
msgid "Time Off"
msgstr "Kapanma Zamanı"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_calendar_view
msgid "Time Off Request"
msgstr "İzin Talebi"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_portal
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_time_off_submit
msgid "Time Off Type"
msgstr "Kapalı Zaman Türü"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_leave_enterprise.selection__student_leave_request__state__confirm
msgid "To Approve"
msgstr "Onaylamak için"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_leave_enterprise.selection__student_leave_request__state__draft
msgid "To Submit"
msgstr "Göndermek"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__name
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_type_form_view
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_type_tree_view
msgid "Type"
msgstr "Tip"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__activity_exception_decoration
msgid "Type of the exception activity on record."
msgstr "Kayıttaki istisna etkinliğinin türü."

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_unread
msgid "Unread Messages"
msgstr "Okunmamış Mesajlar"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__message_unread_counter
msgid "Unread Messages Counter"
msgstr "Okunmamış Mesaj Sayacı"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_form_view
msgid "Validate"
msgstr "Doğrula"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_leave_enterprise.selection__student_leave_request__state__validate
msgid "Validated"
msgstr "Doğrulandı"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_request__website_message_ids
msgid "Website Messages"
msgstr "Web Sitesi Mesajları"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,help:openeducat_student_leave_enterprise.field_student_leave_request__website_message_ids
msgid "Website communication history"
msgstr "Web sitesi iletişim geçmişi"

#. module: openeducat_student_leave_enterprise
#: model:ir.model.fields,field_description:openeducat_student_leave_enterprise.field_student_leave_type__code
msgid "code"
msgstr "kod"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "refused"
msgstr "reddetti"

#. module: openeducat_student_leave_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_leave_enterprise.student_leave_request_kanban_view
msgid "validated"
msgstr "onaylanmış"

