# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_asset_request_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 10:25+0000\n"
"PO-Revision-Date: 2022-12-13 10:25+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form_edit
msgid "<span class=\"s_website_form_label_content\">Asset:</span>"
msgstr "<span class=\"s_website_form_label_content\"> Asset: </span>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form_edit
msgid "<span class=\"s_website_form_label_content\">Faculty:</span>"
msgstr "<span class=\"s_website_form_label_content\"> Fakultet: </span>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form_edit
msgid "<span class=\"s_website_form_label_content\">Request Reason:</span>"
msgstr "<span class=\"s_website_form_label_content\"> Anmodning om grund: </span>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form
msgid "<span class=\"s_website_form_label_content\">Student:</span>"
msgstr "<span class=\"s_website_form_label_content\"> Student: </span>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form_edit
msgid "<span class=\"s_website_form_label_content\">Students:</span>"
msgstr "<span class=\"s_website_form_label_content\"> Studerende: </span>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.portal_my_home_menu_request_asset
msgid "<span>Requests</span>"
msgstr "<span> anmodninger </span>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid ""
"<strong>\n"
"                                                    Allocated Date:\n"
"                                                </strong>"
msgstr ""
"<strong>\n"
"                                                    Tildelt dato:\n"
"                                                </strong>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid ""
"<strong>\n"
"                                                    Approved Date:\n"
"                                                </strong>"
msgstr ""
"<strong>\n"
"                                                    Godkendt dato:\n"
"                                                </strong>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid ""
"<strong>\n"
"                                                    Asset Category:\n"
"                                                </strong>"
msgstr ""
"<strong>\n"
"                                                    Asset kategori:\n"
"                                                </strong>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid ""
"<strong>\n"
"                                                    Returned Date:\n"
"                                                </strong>"
msgstr ""
"<strong>\n"
"                                                    Returnerede dato:\n"
"                                                </strong>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid ""
"<strong>\n"
"                                                Reason:\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Grund:\n"
"                                            </strong>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid ""
"<strong>\n"
"                                                Requested Asset:\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Anmodet aktiv:\n"
"                                            </strong>"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid ""
"<strong>\n"
"                                                Requested Date:\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Anmodet dato:\n"
"                                            </strong>"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_needaction
msgid "Action Needed"
msgstr "Der kræves handling"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "All"
msgstr "Alle"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_form_view
#, python-format
msgid "Allocate"
msgstr "Tildele"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__allocate_date
msgid "Allocate Date"
msgstr "Tildel dato"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields.selection,name:openeducat_asset_request_enterprise.selection__account_asset_request__state__allocate
msgid "Allocated"
msgstr "Tildelt"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_search_view
msgid "Allocated Assets Request"
msgstr "Anmodning om tildelte aktiver"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_form_view
#, python-format
msgid "Approve"
msgstr "Godkende"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__approve_date
msgid "Approve Date"
msgstr "Godkend dato"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields.selection,name:openeducat_asset_request_enterprise.selection__account_asset_request__state__approved
msgid "Approved"
msgstr "godkendt"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_search_view
msgid "Approved Request"
msgstr "Godkendt anmodning"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid "Asset :"
msgstr "Asset:"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "Asset Name"
msgstr "Aktivets navn"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form_edit
msgid "Asset Name Which you want to request for"
msgstr "Aktivnavn, som du vil anmode om"

#. module: openeducat_asset_request_enterprise
#: model:ir.model,name:openeducat_asset_request_enterprise.model_account_asset_request
#: model:openeducat.portal.menu,name:openeducat_asset_request_enterprise.website_menu_asset_request
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.portal_my_home_request_assets
msgid "Asset Requests"
msgstr "Anmodninger om aktiv"

#. module: openeducat_asset_request_enterprise
#: model:ir.model,name:openeducat_asset_request_enterprise.model_account_asset_asset
msgid "Asset/Revenue Recognition"
msgstr "Asset/indtægtsgenkendelse"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__asset_id
#: model:ir.ui.menu,name:openeducat_asset_request_enterprise.menu_asset
msgid "Assets"
msgstr "Aktiver"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_asset__assign_state
msgid "Assigned ?"
msgstr "Tildelt?"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_attachment_count
msgid "Attachment Count"
msgstr "Vedhæftningstælling"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "By Date"
msgstr "Efter dato"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__color
msgid "Color Index"
msgstr "Farveindeks"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__create_uid
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason__create_uid
msgid "Created by"
msgstr "Lavet af"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__create_date
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason__create_date
msgid "Created on"
msgstr "Oprettet på"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid "Current stage of this ticket"
msgstr "Nuværende fase af denne billet"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid "Delete"
msgstr "Slet"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__display_name
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason__display_name
msgid "Display Name"
msgstr "Visnavn"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields.selection,name:openeducat_asset_request_enterprise.selection__account_asset_request__state__draft
msgid "Draft"
msgstr "Udkast"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.asset_detail
msgid "Edit"
msgstr "Redigere"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__faculty_id
#: model:ir.model.fields.selection,name:openeducat_asset_request_enterprise.selection__account_asset_request__request_for__faculty
msgid "Faculty"
msgstr "Fakultet"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_search_view
msgid "Faculty Name"
msgstr "Fakultetets navn"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_follower_ids
msgid "Followers"
msgstr "Tilhængere"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_partner_ids
msgid "Followers (Partners)"
msgstr "Tilhængere (partnere)"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__has_message
msgid "Has Message"
msgstr "Har besked"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__id
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason__id
msgid "ID"
msgstr "Id"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,help:openeducat_asset_request_enterprise.field_account_asset_request__message_needaction
msgid "If checked, new messages require your attention."
msgstr "Hvis de kontrolleres, kræver nye meddelelser din opmærksomhed."

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,help:openeducat_asset_request_enterprise.field_account_asset_request__message_has_error
#: model:ir.model.fields,help:openeducat_asset_request_enterprise.field_account_asset_request__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "Hvis de kontrolleres, har nogle meddelelser en leveringsfejl."

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_is_follower
msgid "Is Follower"
msgstr "Er tilhænger"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request____last_update
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason____last_update
msgid "Last Modified on"
msgstr "Sidst ændret på"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__write_uid
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason__write_uid
msgid "Last Updated by"
msgstr "Sidst opdateret af"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__write_date
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason__write_date
msgid "Last Updated on"
msgstr "Sidst opdateret om"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_main_attachment_id
msgid "Main Attachment"
msgstr "Hovedknap"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_has_error
msgid "Message Delivery error"
msgstr "Fejl på meddelelseslevering"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_ids
msgid "Messages"
msgstr "Beskeder"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__name
msgid "Name"
msgstr "Navn"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
msgid "New Request"
msgstr "Ny anmodning"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "Newest"
msgstr "Nyeste"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "None"
msgstr ""

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_needaction_counter
msgid "Number of Actions"
msgstr "Antal handlinger"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_has_error_counter
msgid "Number of errors"
msgstr "Antal fejl"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,help:openeducat_asset_request_enterprise.field_account_asset_request__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Antal meddelelser, der kræver en handling"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,help:openeducat_asset_request_enterprise.field_account_asset_request__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Antal meddelelser med leveringsfejl"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/models/request_assets.py:0
#, python-format
msgid "Please select Asset"
msgstr "Vælg aktiv"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__request_reason_id
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_asset_request_reason__name
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_form_view
msgid "Reason"
msgstr "Grund"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_form_view
#, python-format
msgid "Reject"
msgstr "Afvise"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__reject_date
msgid "Reject Date"
msgstr "Afvis dato"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields.selection,name:openeducat_asset_request_enterprise.selection__account_asset_request__state__rejected
msgid "Rejected"
msgstr "Afvist"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_search_view
msgid "Rejected Request"
msgstr "Afviste anmodning"

#. module: openeducat_asset_request_enterprise
#: model:ir.actions.act_window,name:openeducat_asset_request_enterprise.action_request_asset_list
msgid "Request Asset"
msgstr "Anmod om aktiv"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_form_view
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_reason_form_view
msgid "Request Asset Form"
msgstr "Anmod om formular"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_pivot_view
msgid "Request Asset Pivot"
msgstr "Anmod om aktiv pivot"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_search_view
msgid "Request Asset Search"
msgstr "Anmod om aktivsøgning"

#. module: openeducat_asset_request_enterprise
#: model:ir.ui.menu,name:openeducat_asset_request_enterprise.menu_request_asset
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
msgid "Request Assets"
msgstr "Anmod om aktiver"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_list_view
msgid "Request Assets Tree"
msgstr "Anmod om aktiver træ"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__request_for
msgid "Request By"
msgstr "Anmodning fra"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__request_date
msgid "Request Date"
msgstr "Anmodningsdato"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
msgid "Request Number #"
msgstr "Anmodningsnummer #"

#. module: openeducat_asset_request_enterprise
#: model:ir.actions.act_window,name:openeducat_asset_request_enterprise.action_request_reason_list
#: model:ir.model,name:openeducat_asset_request_enterprise.model_asset_request_reason
msgid "Request Reason"
msgstr "Anmod om grund"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_reason_list_view
msgid "Request Reason Tree"
msgstr "Anmod om grund træ"

#. module: openeducat_asset_request_enterprise
#: model:ir.ui.menu,name:openeducat_asset_request_enterprise.menu_request_reason
msgid "Request Reasons"
msgstr "Anmod om årsager"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__requested_asset
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
msgid "Requested Asset"
msgstr "Anmodet aktiv"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
msgid "Requested Date"
msgstr "Anmodet dato"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.portal_my_home_menu_request_asset
msgid "Requests"
msgstr "Anmodninger"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_form_view
#, python-format
msgid "Return"
msgstr "Vend tilbage"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__return_date
msgid "Return Date"
msgstr "Retur dato"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields.selection,name:openeducat_asset_request_enterprise.selection__account_asset_request__state__returned
msgid "Returned"
msgstr "Vendt tilbage"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__message_has_sms_error
msgid "SMS Delivery error"
msgstr "SMS -leveringsfejl"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "Search in All"
msgstr "Søg i alle"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "Search in allocated asset"
msgstr "Søg i tildelt aktiv"

#. module: openeducat_asset_request_enterprise
#. odoo-python
#: code:addons/openeducat_asset_request_enterprise/controllers/main.py:0
#, python-format
msgid "Search in requested asset"
msgstr "Søg i det ønskede aktiv"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__state
msgid "State"
msgstr "Stat"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
msgid "Status"
msgstr ""

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__student_id
#: model:ir.model.fields.selection,name:openeducat_asset_request_enterprise.selection__account_asset_request__request_for__student
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_form_view
msgid "Student"
msgstr "Studerende"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_search_view
msgid "Student Name"
msgstr "Elevnavn"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_assets_form_edit
msgid "Submit"
msgstr "Indsend"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.all_asset_request
msgid "There are no requests for assets"
msgstr "Der er ingen anmodninger om aktiver"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,field_description:openeducat_asset_request_enterprise.field_account_asset_request__website_message_ids
msgid "Website Messages"
msgstr "Webstedsmeddelelser"

#. module: openeducat_asset_request_enterprise
#: model:ir.model.fields,help:openeducat_asset_request_enterprise.field_account_asset_request__website_message_ids
msgid "Website communication history"
msgstr "Webstedskommunikationshistorie"

#. module: openeducat_asset_request_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_asset_request_enterprise.request_asset_search_view
msgid "by Asset"
msgstr "af aktiv"
