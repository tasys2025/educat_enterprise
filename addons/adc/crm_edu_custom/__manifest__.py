# -*- coding: utf-8 -*-

{
    'name': 'CRM Edu Customization',
    'version': '16.0.1.0.0',
    'category': 'Sales/CRM',
    'description': '',
    'summary': 'CRM Edu Customization',
    'sequence': '0',
    'author': 'AnhCT',
    'license': 'LGPL-3',
    'company': '',
    'maintainer': '',
    'support': '',
    'website': '',
    'depends': ['crm', 'project'],
    'data': [
        'views/crm_lead_views.xml',
    ],
    'pre_init_hook': '',
    'installable': True,
    'application': True,
    'auto_install': False,
}
