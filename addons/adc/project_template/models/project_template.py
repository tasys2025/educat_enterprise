# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ProjectTemplate(models.Model):
    _name = "project.template"
    _description = "Create and manage project template"

    name = fields.Char('Name', required=True)
    task_template_ids = fields.Many2many('project.task.template', string='Tasks')
    task_stage_ids = fields.Many2many('project.task.type', string='Stages of tasks')
    tag_ids = fields.Many2many('project.tags', string='Tags')
    active = fields.Boolean('Active', default=True)
