# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ProjectTaskTemplate(models.Model):
    _name = "project.task.template"
    _description = "Template of task"

    name = fields.Char('Name', required=True)
    priority = fields.Selection([
        ('0', 'Low'),
        ('1', 'High'),
    ], default='0', index=True, string="Priority", tracking=True)
    tag_ids = fields.Many2many('project.tags', string='Tags')

