# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_quiz_sort_paragraphs
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 10:55+0000\n"
"PO-Revision-Date: 2022-12-13 10:55+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_quiz_sort_paragraphs
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_sort_paragraphs.quiz_results_form_sort_paragraphs_line_ids_inherit
msgid "<span style=\"visibility: hidden;\">Answer</span>"
msgstr "<span style=\"Visibility: Hidden;\"> Respuesta </span>"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__answer
msgid "Answer"
msgstr "Responder"

#. module: openeducat_quiz_sort_paragraphs
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_sort_paragraphs.view_op_quiz_sort_paragraphs_tree
msgid "Answers"
msgstr "Respuestas"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__company_id
msgid "Company"
msgstr "Compañía"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__create_uid
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__create_uid
msgid "Created by"
msgstr "Creado por"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__create_date
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__create_date
msgid "Created on"
msgstr "Creado en"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__default_answer
msgid "Default Answer"
msgstr "Respuesta predeterminada"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__display_name
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__display_name
msgid "Display Name"
msgstr "Nombre para mostrar"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__given_answer
msgid "Given Answer"
msgstr "Respuesta dada"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__id
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__id
msgid "ID"
msgstr "IDENTIFICACIÓN"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs____last_update
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs____last_update
msgid "Last Modified on"
msgstr "Última modificación en"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__write_uid
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__write_uid
msgid "Last Updated by"
msgstr "Última actualización por"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__write_date
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__write_date
msgid "Last Updated on"
msgstr "Ultima actualización en"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__question
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line_sort_paragraphs__line_id
msgid "Question"
msgstr "Pregunta"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_line__que_type
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line__que_type
msgid "Question Type"
msgstr "tipo de pregunta"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model,name:openeducat_quiz_sort_paragraphs.model_op_quiz_line
msgid "Questions"
msgstr "Preguntas"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model,name:openeducat_quiz_sort_paragraphs.model_op_quiz
msgid "Quiz"
msgstr "Prueba"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model,name:openeducat_quiz_sort_paragraphs.model_op_quiz_answer_sort_paragraphs
msgid "Quiz Answers sort paragraphs"
msgstr "Cuestionario respuestas ordenar párrafos"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model,name:openeducat_quiz_sort_paragraphs.model_op_quiz_result_line
msgid "Quiz Result Line"
msgstr "Línea de resultados del cuestionario"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model,name:openeducat_quiz_sort_paragraphs.model_op_quiz_result
msgid "Quiz Results"
msgstr "Resultados del cuestionario"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model,name:openeducat_quiz_sort_paragraphs.model_op_quiz_result_line_sort_paragraphs
msgid "Quiz result sort paragraphs"
msgstr "Párrafos de clasificación de resultados de la prueba"

#. module: openeducat_quiz_sort_paragraphs
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_sort_paragraphs.view_op_quiz_result_sort_paragraphs
msgid "Results"
msgstr "Resultados"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_result_line__sort_paragraphs_line_ids
msgid "Sort Paragraphs Answer"
msgstr "Respuesta de clasificación de párrafos"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields.selection,name:openeducat_quiz_sort_paragraphs.selection__op_quiz_line__que_type__sort_paragraphs
#: model:ir.model.fields.selection,name:openeducat_quiz_sort_paragraphs.selection__op_quiz_result_line__que_type__sort_paragraphs
msgid "Sort the Paragraphs"
msgstr "Ordenar los párrafos"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_line__sort_paragraphs_line_ids
msgid "Sort the Paragraphs Answers"
msgstr "Ordena las respuestas de los párrafos"

#. module: openeducat_quiz_sort_paragraphs
#: model:ir.model.fields,field_description:openeducat_quiz_sort_paragraphs.field_op_quiz_answer_sort_paragraphs__sort_paragraphs_line_id
msgid "question Line"
msgstr "línea de preguntas"
