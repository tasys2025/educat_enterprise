# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_student_progress_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 12:47+0000\n"
"PO-Revision-Date: 2022-12-13 12:47+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.openeducat_student_progression_portal_data
msgid "<b style=\"font-size: 18px\" class=\"text-secondary\">Progression Details:</b>"
msgstr ""
"<b style=\"font-size: 18px\" class=\"text-seccondary\"> Détails de "
"progression: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.openeducat_student_progression_portal_data
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.student_progression_report
msgid "<b>Created By:</b>"
msgstr "<b> créé par: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.student_progression_report
msgid "<b>Date :</b>"
msgstr "<b> Date: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.openeducat_student_progression_portal_data
msgid "<b>Date:</b>"
msgstr "<b> Date: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.student_progression_report
msgid "<b>Progression No :</b>"
msgstr "<b> Progression Non: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.openeducat_student_progression_portal_data
msgid "<b>Progression No:</b>"
msgstr "<b> Progression Non: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.openeducat_student_progression_portal_data
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.student_progression_report
msgid "<b>Status:</b>"
msgstr "<b> Statut: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.student_progression_report
msgid "<b>Student :</b>"
msgstr "<b> Étudiant: </b>"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.student_progression_report
msgid ""
"<strong>\n"
"                                                Student Progression Report\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Rapport de progression des étudiants\n"
"                                            </strong>"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_needaction
msgid "Action Needed"
msgstr "Action nécessaire"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__active
msgid "Active"
msgstr "Actif"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_student_progress_form
msgid "Archived"
msgstr "Archivé"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_attachment_count
msgid "Attachment Count"
msgstr "Nombre de pièces jointes"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_progress_enterprise.selection__op_student_progression__state__cancel
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_student_progress_form
msgid "Cancel"
msgstr "Annuler"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__company_id
msgid "Company"
msgstr "Compagnie"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__created_by
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "Created By"
msgstr "Créé par"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__create_uid
msgid "Created by"
msgstr "Créé par"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__create_date
msgid "Created on"
msgstr "Créé sur"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__date
msgid "Date"
msgstr ""

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__display_name
msgid "Display Name"
msgstr "Afficher un nom"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_progress_enterprise.selection__op_student_progression__state__done
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_student_progress_form
msgid "Done"
msgstr "Fait"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_progress_enterprise.selection__op_student_progression__state__draft
msgid "Draft"
msgstr "Brouillon"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_follower_ids
msgid "Followers"
msgstr "Suiveurs"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_partner_ids
msgid "Followers (Partners)"
msgstr "Abonnés (partenaires)"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "Group By..."
msgstr "Par groupe..."

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__has_message
msgid "Has Message"
msgstr "A un message"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__id
msgid "ID"
msgstr "IDENTIFIANT"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,help:openeducat_student_progress_enterprise.field_op_student_progression__message_needaction
msgid "If checked, new messages require your attention."
msgstr "S'il est vérifié, les nouveaux messages nécessitent votre attention."

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,help:openeducat_student_progress_enterprise.field_op_student_progression__message_has_error
#: model:ir.model.fields,help:openeducat_student_progress_enterprise.field_op_student_progression__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "S'il est vérifié, certains messages ont une erreur de livraison."

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields.selection,name:openeducat_student_progress_enterprise.selection__op_student_progression__state__open
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_student_progress_form
msgid "In Progress"
msgstr "En cours"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_is_follower
msgid "Is Follower"
msgstr "Est suiveur"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression____last_update
msgid "Last Modified on"
msgstr "Dernier modification sur"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__write_uid
msgid "Last Updated by"
msgstr "Dernière mise à jour par"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__write_date
msgid "Last Updated on"
msgstr "Dernière mise à jour le"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_main_attachment_id
msgid "Main Attachment"
msgstr "Attachement principal"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_has_error
msgid "Message Delivery error"
msgstr "Erreur de livraison de messages"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_ids
msgid "Messages"
msgstr "messages"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "Month"
msgstr "Mois"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_needaction_counter
msgid "Number of Actions"
msgstr "Nombre d'actions"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_has_error_counter
msgid "Number of errors"
msgstr "Nombre d'erreurs"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,help:openeducat_student_progress_enterprise.field_op_student_progression__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Nombre de messages qui nécessitent une action"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,help:openeducat_student_progress_enterprise.field_op_student_progression__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Nombre de messages avec erreur de livraison"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_student_progress_tree
msgid "Progress Line"
msgstr "Ligne de progression"

#. module: openeducat_student_progress_enterprise
#: model:openeducat.portal.menu,name:openeducat_student_progress_enterprise.poratl_menu_student_progression
msgid "Progression"
msgstr ""

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "Progression Records"
msgstr "Enregistrements de progression"

#. module: openeducat_student_progress_enterprise
#: model:ir.actions.report,name:openeducat_student_progress_enterprise.report_student_progression
msgid "Progression Report"
msgstr "Rapport de progression"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_student_progress_form
msgid "Reset To Draft"
msgstr "Réinitialiser avec le projet"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__message_has_sms_error
msgid "SMS Delivery error"
msgstr "Erreur de livraison SMS"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__name
msgid "Sequence"
msgstr "Séquence"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "State"
msgstr "État"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__state
msgid "Status"
msgstr "Statut"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__student_id
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "Student"
msgstr "Étudiant"

#. module: openeducat_student_progress_enterprise
#: model:ir.actions.act_window,name:openeducat_student_progress_enterprise.act_op_student_progress_analysis_pivot_view
#: model:ir.ui.menu,name:openeducat_student_progress_enterprise.menu_op_student_progress_analysis
msgid "Student Progress Analysis"
msgstr "Analyse des progrès des étudiants"

#. module: openeducat_student_progress_enterprise
#: model:ir.actions.act_window,name:openeducat_student_progress_enterprise.action_student_progrss_analysis
#: model:ir.ui.menu,name:openeducat_student_progress_enterprise.student_progrss_analysis_menu
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.openeducat_student_progression_portal_data
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.portal_student_progresssion
msgid "Student Progression"
msgstr "Progression des élèves"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_pivot
msgid "Student Progression Analysis"
msgstr "Analyse de la progression des étudiants"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_graph
msgid "Student Progression graph"
msgstr "Graphique de progression des étudiants"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.constraint,message:openeducat_student_progress_enterprise.constraint_op_student_progression_student_id
msgid "Student already  exist!!!"
msgstr "L'étudiant existe déjà !!!"

#. module: openeducat_student_progress_enterprise
#: model:ir.model,name:openeducat_student_progress_enterprise.model_op_student_progression
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.portal_student_progresssion
msgid "Student progression"
msgstr "Progression des élèves"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.openeducat_student_progression_portal_data
msgid "There are no records."
msgstr "Il n'y a pas de dossiers."

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "Today"
msgstr "Aujourd'hui"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,field_description:openeducat_student_progress_enterprise.field_op_student_progression__website_message_ids
msgid "Website Messages"
msgstr "Messages du site Web"

#. module: openeducat_student_progress_enterprise
#: model:ir.model.fields,help:openeducat_student_progress_enterprise.field_op_student_progression__website_message_ids
msgid "Website communication history"
msgstr "Historique de communication du site Web"

#. module: openeducat_student_progress_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_student_progress_enterprise.view_op_student_progression_search
msgid "Week"
msgstr "La semaine"
