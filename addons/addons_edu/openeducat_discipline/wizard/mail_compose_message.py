# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models


class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    def _action_send_mail(self, auto_commit=False):
        if self.model == 'op.discipline':
            disciplines = self.env['op.discipline'].browse(
                self.env.context.get('active_ids'))
            for discipline in disciplines:
                discipline.state = 'email_sent'
        if self.model == 'suspended.student':
            students = self.env['suspended.student'].browse(
                self.env.context.get('default_res_id'))
            for student in students:
                student.discipline_id.state = 'suspended'
        return super(MailComposeMessage, self)._action_send_mail(
            auto_commit=auto_commit)
