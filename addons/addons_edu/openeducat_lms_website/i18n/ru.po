# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_lms_website
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 09:02+0000\n"
"PO-Revision-Date: 2022-12-14 09:02+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_lms_website
#: model_terms:ir.ui.view,arch_db:openeducat_lms_website.section_material
msgid ""
"<i class=\"fa fa-folder-o me-2\"/>\n"
"                    <span>Add Section</span>"
msgstr ""

#. module: openeducat_lms_website
#: model_terms:ir.ui.view,arch_db:openeducat_lms_website.section_material
msgid ""
"<i class=\"fa fa-plus me-2\"/>\n"
"                    <span>Add Content</span>"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/course_section_add.js:0
#, python-format
msgid "Add a section"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Already installing \"%s\"."
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/course_section.xml:0
#, python-format
msgid "Are you sure you want to archive this slide ?"
msgstr ""

#. module: openeducat_lms_website
#: model:ir.model.fields,field_description:openeducat_lms_website.field_op_course__can_upload
msgid "Can Upload"
msgstr "Может загрузить"

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Cancel"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Category"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Choose a Cover Image"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Choose a PDF or an Image"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Close"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Code"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Content Preview"
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/controllers/main.py:0
#, python-format
msgid ""
"Could not fetch data from url. Document or access right not available.\n"
"Here is the received response: %s"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Course Title"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/lms_course.editor.js:0
#, python-format
msgid "Create"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Create new %s '%s'"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Description"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/course_section_add.js:0
#: code:addons/openeducat_lms_website/static/src/js/lms_course.editor.js:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Discard"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Do you want to install the \"%s\" app?"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Duration"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Estimated slide completion time"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Failed to install \"%s\"."
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#. odoo-python
#: code:addons/openeducat_lms_website/controllers/main.py:0
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "File is too big. File size cannot exceed 25MB"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Free Learning Path"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Go Back"
msgstr ""

#. module: openeducat_lms_website
#: model:ir.model.fields,field_description:openeducat_lms_website.field_website__website_slide_google_app_key
msgid "Google Doc Key"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Image File"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Install"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Installing \"%s\"."
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/controllers/main.py:0
#, python-format
msgid ""
"Internal server error, please try again later or contact administrator.\n"
"Here is the error message: %s"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Invalid file type. Please select pdf or image file"
msgstr ""

#. module: openeducat_lms_website
#: model:ir.model,name:openeducat_lms_website.model_op_course
msgid "LMS Course"
msgstr "LMS -курс"

#. module: openeducat_lms_website
#: model:ir.model,name:openeducat_lms_website.model_op_course_section
msgid "LMS Course Section"
msgstr "Раздел курса LMS"

#. module: openeducat_lms_website
#: model:ir.model,name:openeducat_lms_website.model_op_material
msgid "LMS Material"
msgstr "LMS материал"

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Minutes"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Navigation Policy"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "New Certification"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/lms_course.editor.js:0
#, python-format
msgid "New Course"
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/models/course_material.py:0
#, python-format
msgid "Please enter valid Youtube or Google Doc URL"
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/controllers/main.py:0
#, python-format
msgid "Please enter valid youtube or google doc url"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Please fill in this field"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Presentation"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Publish"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Quiz"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Retry"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/course_section_add.js:0
#, python-format
msgid "Save"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Save as Draft"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Section"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/course_section.xml:0
#, python-format
msgid "Section name"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/course_section.xml:0
#, python-format
msgid "Sequence"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Sequential Learning Path"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Tags"
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/controllers/main.py:0
#, python-format
msgid "This document does not exist."
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/controllers/main.py:0
#, python-format
msgid "This video already exists in this course on the following slide: %s"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Title"
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/models/course_material.py:0
#, python-format
msgid "Unknown document"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Upload"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Upload Image"
msgstr ""

#. module: openeducat_lms_website
#: model_terms:ir.ui.view,arch_db:openeducat_lms_website.section_material
msgid "Upload Presentation"
msgstr "Загрузить презентацию"

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Upload a document"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Uploading Image..."
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Uploading document ..."
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Video"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "Web Page"
msgstr ""

#. module: openeducat_lms_website
#: model:ir.model,name:openeducat_lms_website.model_website
msgid "Website"
msgstr "Веб-сайт"

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/website_lms_course.xml:0
#, python-format
msgid "Write here a short description of your first course"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/js/material_upload.js:0
#, python-format
msgid "You can not upload password protected file."
msgstr ""

#. module: openeducat_lms_website
#. odoo-python
#: code:addons/openeducat_lms_website/controllers/main.py:0
#, python-format
msgid "You cannot upload on this channel."
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Youtube Link"
msgstr ""

#. module: openeducat_lms_website
#. odoo-javascript
#: code:addons/openeducat_lms_website/static/src/xml/material_upload.xml:0
#, python-format
msgid "Youtube Video URL"
msgstr ""

#. module: openeducat_lms_website
#: model:ir.model.fields,field_description:openeducat_lms_website.field_op_material__course_id
msgid "course"
msgstr "курс"

#. module: openeducat_lms_website
#: model:ir.model.fields,field_description:openeducat_lms_website.field_op_course_section__seq
msgid "seq"
msgstr "себ"
