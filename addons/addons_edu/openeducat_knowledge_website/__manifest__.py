# -*- coding: utf-8 -*-
# Part of odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Knowledge Website',
    'summary': 'Publish your articles',
    'version': '1.0',
    'depends': ['openeducat_knowledge', 'website'],
    'category': 'Education/Knowledge',
    'version': '16.0.1.0',
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/knowledge_views.xml',
        'views/knowledge_templates_frontend.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': True,
    'license': 'LGPL-3',
    'assets': {
        'web.assets_backend': [
            'openeducat_knowledge_website/static/src/client_actions/website_preview/website_preview.js',
        ]
    },
}
