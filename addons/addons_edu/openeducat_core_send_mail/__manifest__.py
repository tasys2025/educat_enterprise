
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

{
    'name': "OpenEduCat Student Faculty Mass Mailing",
    'description': "Allows to send email in bulks for faculty and students",
    'author': 'OpenEduCat Inc',
    'version': '16.0.1.0',
    'category': 'Education',
    'depends': ['base', 'openeducat_core_enterprise'],
    'website': 'http://www.openeducat.org',
    'data': [
        'data/mail_data.xml',
        'views/op_student_view.xml',
        'views/op_faculty_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'Other proprietary',
}
