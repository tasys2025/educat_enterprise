# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_quiz_match_images
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 10:15+0000\n"
"PO-Revision-Date: 2022-12-14 10:15+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_quiz_match_images
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_match_images.quiz_results_form_match_following_images_inherit
msgid "<span style=\"visibility: hidden;\">Answer</span>"
msgstr "<span style=\"visibilitas: tersembunyi;\"> jawaban </span>"

#. module: openeducat_quiz_match_images
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_match_images.view_op_quiz_bank_match_following_images_tree
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_match_images.view_op_quiz_following_Images_tree
msgid "Answers"
msgstr "Jawaban"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__company_id
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__company_id
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__company_id
msgid "Company"
msgstr "Perusahaan"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__create_uid
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__create_uid
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__create_uid
msgid "Created by"
msgstr "Dibuat oleh"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__create_date
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__create_date
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__create_date
msgid "Created on"
msgstr "Dibuat pada"

#. module: openeducat_quiz_match_images
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_match_images.view_op_quiz_bank_match_following_images_tree
msgid "Default Answer"
msgstr "Jawaban default"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__display_name
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__display_name
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__display_name
msgid "Display Name"
msgstr "Nama tampilan"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line__following_images_line_ids
msgid "Following Images"
msgstr "Gambar berikut"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_line__following_images_line_ids
msgid "Follwing Answers"
msgstr ""

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_line__following_images_line_ids
msgid "Follwing Images Answers"
msgstr ""

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__given_answer
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__given_answer
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__given_answer
msgid "Given Answer"
msgstr "Jawaban yang diberikan"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__grade_id
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__grade_id
msgid "Grade"
msgstr "Nilai"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__id
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__id
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__id
msgid "ID"
msgstr "PENGENAL"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__image
msgid "Image"
msgstr "Gambar"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images____last_update
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images____last_update
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images____last_update
msgid "Last Modified on"
msgstr "Terakhir dimodifikasi"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__write_uid
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__write_uid
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__write_uid
msgid "Last Updated by"
msgstr "Terakhir diperbarui oleh"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__write_date
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__write_date
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__write_date
msgid "Last Updated on"
msgstr "Terakhir diperbarui saat"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields.selection,name:openeducat_quiz_match_images.selection__op_quiz_result_line__que_type__match_following_images
msgid "Match Following Images"
msgstr "Cocokkan gambar berikut"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields.selection,name:openeducat_quiz_match_images.selection__op_question_bank_line__que_type__match_following_images
#: model:ir.model.fields.selection,name:openeducat_quiz_match_images.selection__op_quiz_line__que_type__match_following_images
msgid "Match the Images"
msgstr "Cocokkan gambar"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__image
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__image
msgid "Question"
msgstr "Pertanyaan"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_line__que_type
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_line__que_type
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line__que_type
msgid "Question Type"
msgstr "tipe pertanyaan"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__following_images_line_id
msgid "Question line"
msgstr "Baris pertanyaan"

#. module: openeducat_quiz_match_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_quiz_line
msgid "Questions"
msgstr "Pertanyaan"

#. module: openeducat_quiz_match_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_quiz
msgid "Quiz"
msgstr "Ulangan"

#. module: openeducat_quiz_match_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_question_bank_answer_following_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_quiz_answer_following_images
msgid "Quiz Answers match the following images"
msgstr "Jawaban kuis cocok dengan gambar berikut"

#. module: openeducat_quiz_match_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_question_bank_line
msgid "Quiz Question Lines"
msgstr "Baris pertanyaan kuis"

#. module: openeducat_quiz_match_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_quiz_result_line
msgid "Quiz Result Line"
msgstr "Baris hasil kuis"

#. module: openeducat_quiz_match_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_quiz_result_line_following_images
msgid "Quiz Result Line Following Images"
msgstr "Baris Hasil Kuis Mengikuti Gambar"

#. module: openeducat_quiz_match_images
#: model:ir.model,name:openeducat_quiz_match_images.model_op_quiz_result
msgid "Quiz Results"
msgstr "Hasil kuis"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__default_answer
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__default_answer
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_result_line_following_images__default_answer
msgid "default Answer"
msgstr "Jawaban default"

#. module: openeducat_quiz_match_images
#: model_terms:ir.ui.view,arch_db:openeducat_quiz_match_images.view_op_quiz_result_match_the_following_images
msgid "match following Images"
msgstr "Cocokkan gambar berikut"

#. module: openeducat_quiz_match_images
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_question_bank_answer_following_images__following_images_line_id
#: model:ir.model.fields,field_description:openeducat_quiz_match_images.field_op_quiz_answer_following_images__following_images_line_id
msgid "question"
msgstr "pertanyaan"
