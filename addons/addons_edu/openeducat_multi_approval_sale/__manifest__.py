{
    'name': "OpenEduCat Approval All in One With Sale Management",
    'summary': """Approval All in One With Sale Management""",
    'description': """ """,
    'author': "OpenEduCat Inc",
    'website': "http://www.openeducat.org",
    'category': 'Tool',
    'version': '16.0.1.0',
    'license': 'Other proprietary',
    'depends': ['openeducat_multi_approval', 'sale_management'],
    'data': [
    ],
    'demo': [
        'demo/multi_approval_type.xml',
        ],
    'auto_install': False,
    'application': True,

}
