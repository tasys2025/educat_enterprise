# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_quiz_drag_into_text
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 10:00+0000\n"
"PO-Revision-Date: 2022-12-14 10:00+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model.fields.selection,name:openeducat_quiz_drag_into_text.selection__op_question_bank_line__que_type__drag_into_text
#: model:ir.model.fields.selection,name:openeducat_quiz_drag_into_text.selection__op_quiz_line__que_type__drag_into_text
msgid "Drag Into Text"
msgstr "Vilkite į tekstą"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model.fields,field_description:openeducat_quiz_drag_into_text.field_op_question_bank_line__que_type
#: model:ir.model.fields,field_description:openeducat_quiz_drag_into_text.field_op_quiz_line__que_type
#: model:ir.model.fields,field_description:openeducat_quiz_drag_into_text.field_op_quiz_result_line__que_type
msgid "Question Type"
msgstr "Klausimo tipas"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model,name:openeducat_quiz_drag_into_text.model_op_quiz_line
msgid "Questions"
msgstr "Klausimai"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model,name:openeducat_quiz_drag_into_text.model_op_quiz
msgid "Quiz"
msgstr "Viktorina"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model,name:openeducat_quiz_drag_into_text.model_op_question_bank_line
msgid "Quiz Question Lines"
msgstr "Viktorinos klausimų eilutės"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model,name:openeducat_quiz_drag_into_text.model_op_quiz_result_line
msgid "Quiz Result Line"
msgstr "Viktorinos rezultatų eilutė"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model,name:openeducat_quiz_drag_into_text.model_op_quiz_result
msgid "Quiz Results"
msgstr "Viktorinos rezultatai"

#. module: openeducat_quiz_drag_into_text
#: model:ir.model.fields.selection,name:openeducat_quiz_drag_into_text.selection__op_quiz_result_line__que_type__drag_into_text
msgid "drag Into Text"
msgstr "Vilkite į tekstą"
