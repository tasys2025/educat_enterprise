{
    'name': 'OpenEduCat Quiz Numeric',
    'description': """Based on best of class enterprise level architecture,
    OpenEduCat is ready to be used from local infrastructure to
    a highly scalable cloud environment.""",
    'version': '16.0.1.0',
    'category': 'Education',
    'website': 'http://www.openeducat.org',
    'summary': 'OpenEduCat Quiz Numeric',
    'author': 'OpenEduCat Inc',
    'depends': ['openeducat_quiz'],
    'data': [
        'views/quiz_view_numeric.xml',
        'views/quiz_website_inherit.xml',
    ],
    'demo': [
        'demo/demo.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'Other proprietary',
    'live_test_url': 'https://www.openeducat.org/plans'
}
