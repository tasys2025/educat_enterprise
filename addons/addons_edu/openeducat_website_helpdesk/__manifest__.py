# Part of odoo See LICENSE file for full copyright and licensing details.

{
    'name': 'OpenEduCat Website Helpdesk',
    'version': '16.0.1.0',
    'summary': '''Website Helpdesk''',
    'category': 'Human Resources',
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'depends': ['openeducat_core_enterprise',
                'openeducat_helpdesk_basic', 'website', 'portal', 'rating'],
    'data': [
        'security/ir.model.access.csv',
        'views/pages/helpdesk_register_website_view.xml',
        'views/pages/helpdesk_website_view.xml',
        'views/views.xml',
    ],
    'assets': {
        'web.assets_frontend': [
            'openeducat_website_helpdesk/static/src/js/helpdesk.js',
        ],
    },
    'installable': True,
    'auto_install': False,
    'license': 'Other proprietary',
}
