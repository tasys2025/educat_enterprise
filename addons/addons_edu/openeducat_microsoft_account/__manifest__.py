# See LICENSE file for full copyright and licensing details.
{
    "name": "Microsoft Azure SSO Integration",
    "version": "16.0.1.0",
    "license": "Other proprietary",
    "category": "Extra Tools",
    "summary": "Odoo provide interactive feature to integrate or synchronized with Microsoft Azure.",
    "description": """
                  	This odoo app helps user to integrate Microsoft Azure with Odoo, user can login into Odoo using Microsoft account using this microsoft sso integration odoo app, user can also see created partner with microsoft account details.
                    """,
    "author": "OpenEduCat Inc",
    "website": "http://www.openeducat.org",
    "depends": ["base","auth_oauth"],
    "data": [
        "security/security.xml",
        "security/ir.model.access.csv",
        "views/res_config.xml",
        "views/oauth_provider.xml",
        "data/auth_oauth_data.xml",
    ],
    "installable": True,
    "auto_install": False,
}
