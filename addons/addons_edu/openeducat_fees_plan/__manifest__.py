
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
##############################################################################
{
    'name': 'OpenEduCat Fees Plan',
    'description': """This module adds you feature to create invoice
    for fees collection.""",
    'version': '15.0.0.0',
    'category': 'Education',
    "sequence": 5,
    'summary': 'Manage Fees Plan',
    'complexity': "easy",
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'depends': [
        'openeducat_fees',
        'openeducat_core_enterprise',
        'openeducat_attendance_enterprise',
        'openeducat_admission_enterprise',
    ],
    'data': [
        'security/op_security.xml',
        'security/ir.model.access.csv',
        'report/fees_analysis_report_view.xml',
        'report/report_menu.xml',
        'data/server_action_view.xml',
        'data/invoice_cron.xml',
        'data/fees_reminder_mail_template.xml',
        'data/before_fees_reminder_mail_template.xml',
        'data/after_fees_reminder_mail_template.xml',
        'views/fees_plan.xml',
        'views/op_fees_term_view.xml',
        'views/op_student_course_view.xml',
        'views/op_faculty_view.xml',
        'views/service_cron.xml',
        'views/openeducat_fees_collection_portal.xml',
        'menus/op_menu.xml',
    ],
    'demo': [
        'demo/product_demo.xml',
        'demo/fees_element_line_demo.xml',
        'demo/fees_terms_line_demo.xml',
        'demo/fees_terms_demo.xml',
        'demo/subject_cost_demo.xml',
        'demo/fees_reminder_demo.xml',
        'demo/fees_plan_demo.xml',
    ],
    'installable': True,
    'auto_install': False,
    'pre_init_hook': '_fees_plan_pre_init',
    'application': True,
    'price': 298,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'live_test_url': 'https://www.openeducat.org/plans'
}
