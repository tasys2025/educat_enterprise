# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models, fields, api
from dateutil.relativedelta import relativedelta
from datetime import timedelta


class OpStudentCourseTermsInherit(models.Model):
    _inherit = "op.student.course"

    fees_plan_id = fields.Many2one('op.fees.plan', 'Fees Term Plan')
    expires_after = fields.Integer(string='Invoice Cycles',
                                   help="Expires after number of Invoices")
    generated_invoices = fields.Integer(string='Generated Invoices',
                                        help="Number of Invoices Generated")
    next_invoice_date = fields.Date(string="Next Invoice Date", readonly=True)
    prev_invoice_date = fields.Date(string="Previous Invoice Date", readonly=True)

    @api.model_create_multi
    def create(self, values):
        res = super(OpStudentCourseTermsInherit, self).create(values)
        if res.fees_term_id and res.fees_term_id.fees_terms \
                not in ['session_based', 'faculty_based']:
            fees_plan_line = res.fees_plan_id.sudo().create({
                'name': res.fees_term_id.name,
                'product_id': res.product_id.id,
            })
            fees_plan_line.sudo().update({
                'student_id': res.student_id,
                'fees_term_id': res.fees_term_id.id,
            })
            res.fees_plan_id = fees_plan_line
            if res.fees_term_id.fees_terms == 'fixed_days':
                res.fixed_days(res)
            if res.fees_term_id.fees_terms == 'fixed_date':
                res.fixed_date(res)
            if res.fees_term_id.fees_terms == 'duration_based':
                res.duration_based(res)
        return res

    def fixed_date(self, vals):
        list_values = {}
        for val in vals.fees_term_id.line_ids:
            vals.next_invoice_date = val.due_date
            prod_list = []
            for prod in val.fees_element_line:
                per_amount = prod.value
                product_id = prod.product_id.id
                prod_list.append({
                    'product_id': product_id,
                    'value': per_amount
                })
            list_values.update({"invoice_date": vals.next_invoice_date})
            list_values.update({
                'fees_factor': val.value,
                'fees_reminder_line_ids': [(6, 0, val.line_ids.ids)],
                'state': 'draft',
                'fees_plan_id': vals.fees_plan_id.id,
                'student_id': vals.student_id.id,
                'op_fees_terms_line_id': val.id,
                'course_id': vals.course_id.id,
                'batch_id': vals.batch_id.id,
                'amount': vals.product_id.lst_price * val.value / 100,
            })
            plan_line = self.env['op.fees.plan.line'].sudo().create(list_values)
            for rec in prod_list:
                plan_line.sudo().write({'fees_element_line_ids': [(0, 0, rec)]})
        fees_plan_total = 0
        for fees_line in vals.fees_plan_id.fees_plan_line_ids:
            fees_plan_total += float(fees_line.amount)
        vals.fees_plan_id.total_amount = fees_plan_total
        vals.fees_plan_id.remaining_amount = fees_plan_total

    def fixed_days(self, vals):
        list_values = {}
        for val in vals.fees_term_id.line_ids:
            if not vals.fees_start_date:
                vals.fees_start_date = fields.Date.today()
            vals.next_invoice_date = \
                (vals.fees_start_date + timedelta(days=val.due_days))
            prod_list = []
            for prod in val.fees_element_line:
                per_amount = prod.value
                product_id = prod.product_id.id
                prod_list.append({
                    'product_id': product_id,
                    'value': per_amount
                })
            list_values.update({"invoice_date": vals.next_invoice_date})
            list_values.update({
                'fees_factor': val.value,
                'fees_reminder_line_ids': [(6, 0, val.line_ids.ids)],
                'state': 'draft',
                'fees_plan_id': vals.fees_plan_id.id,
                'student_id': vals.student_id.id,
                'op_fees_terms_line_id': val.id,
                'course_id': vals.course_id.id,
                'batch_id': vals.batch_id.id,
                'amount': vals.product_id.lst_price * val.value / 100
            })
            plan_line = self.env['op.fees.plan.line'].sudo().create(list_values)
            for rec in prod_list:
                plan_line.sudo().write({'fees_element_line_ids': [(0, 0, rec)]})

        fees_plan_total = 0
        for fees_line in vals.fees_plan_id.fees_plan_line_ids:
            fees_plan_total += float(fees_line.amount)
        vals.fees_plan_id.total_amount = fees_plan_total
        vals.fees_plan_id.remaining_amount = fees_plan_total

    def duration_based(self, vals):
        list_values = {}
        if not vals.fees_start_date:
            vals.fees_start_date = fields.Date.today()
        if vals.fees_start_date != fields.Date.today():
            vals.next_invoice_date = vals.fees_start_date
        else:
            vals.next_invoice_date = vals.fees_start_date
        for stud in range(0, vals.fees_term_id.expires_after):
            if vals.fees_term_id.bill_selection == 'days':
                term_duration = vals.fees_term_id.term_duration
                vals.prev_invoice_date = vals.next_invoice_date
                list_values.update({"invoice_date": vals.next_invoice_date})
                vals.next_invoice_date = \
                    (vals.next_invoice_date + timedelta(days=term_duration))
            elif vals.fees_term_id.bill_selection == 'weeks':
                term_duration = vals.fees_term_id.term_duration
                vals.prev_invoice_date = vals.next_invoice_date
                list_values.update({"invoice_date": vals.next_invoice_date})
                vals.next_invoice_date = \
                    (vals.next_invoice_date + relativedelta(
                        weeks=term_duration))
            elif vals.fees_term_id.bill_selection == 'months':
                term_duration = vals.fees_term_id.term_duration
                vals.prev_invoice_date = vals.next_invoice_date
                list_values.update({"invoice_date": vals.next_invoice_date})
                vals.next_invoice_date = \
                    (vals.next_invoice_date + relativedelta(
                        months=term_duration))
            elif vals.fees_term_id.bill_selection == 'years':
                term_duration = vals.fees_term_id.term_duration
                vals.prev_invoice_date = vals.next_invoice_date
                list_values.update({"invoice_date": vals.next_invoice_date})
                vals.next_invoice_date = \
                    (vals.next_invoice_date + relativedelta(
                        years=term_duration))
            else:
                vals.next_invoice_date = vals.fees_start_date
                list_values.update({"invoice_date": vals.next_invoice_date})
            prod_list = []
            for val in vals.fees_term_id.line_ids:
                for prod in val.fees_element_line:
                    per_amount = prod.value
                    product_id = prod.product_id.id
                    prod_list.append({
                        'product_id': product_id,
                        'value': per_amount
                    })
                list_values.update({"invoice_date": vals.next_invoice_date})
                list_values.update({
                    'fees_factor': val.value,
                    'fees_reminder_line_ids': [(6, 0, val.line_ids.ids)],
                    'state': 'draft',
                    'fees_plan_id': vals.fees_plan_id.id,
                    'student_id': vals.student_id.id,
                    'op_fees_terms_line_id': val.id,
                    'course_id': vals.course_id.id,
                    'batch_id': vals.batch_id.id,
                    'amount': vals.product_id.lst_price * val.value / 100,
                })
                plan_line = self.env['op.fees.plan.line'].sudo().create(list_values)
                for rec in prod_list:
                    plan_line.sudo().write({'fees_element_line_ids': [(0, 0, rec)]})
                prod_list = []

        fees_plan_total = 0
        for fees_line in vals.fees_plan_id.fees_plan_line_ids:
            fees_plan_total += float(fees_line.amount)
        vals.fees_plan_id.total_amount = fees_plan_total
        vals.fees_plan_id.remaining_amount = fees_plan_total


class OpAttendanceSheetInherit(models.Model):
    _inherit = "op.attendance.sheet"

    def attendance_done(self):
        stud_course_obj = self.env['op.student.course']
        attendance_obj = self.env['op.attendance.line'].search(
            [('attendance_id', '=', self.id), '|',
             ('present', '=', True), ('late', '=', True)])
        list_values = {}
        for rec in attendance_obj:
            amount = 0.0
            student_data = stud_course_obj.search(
                [('student_id', '=', rec.student_id.id),
                 ('course_id', '=', rec.session_id.course_id.id)])
            if student_data.fees_term_id.fees_terms in \
                    ['session_based', 'faculty_based']:
                fees_plan_line = student_data.fees_plan_id.sudo().create({
                    'name': student_data.fees_term_id.name,
                    'product_id': student_data.product_id.id,
                })
                fees_plan_line.sudo().update({
                    'student_id': student_data.student_id,
                    'fees_term_id': student_data.fees_term_id.id,
                })
                student_data.write({
                    'fees_plan_id': fees_plan_line.id
                })
                prod_list = []
                plan_line = None
                for line in student_data.fees_term_id.line_ids:
                    product_id = line.fees_element_line[0].product_id.id
                    per_amount = line.value
                    if student_data.fees_term_id.fees_terms == 'session_based':
                        amount += line.fees_element_line[0].product_id.lst_price
                    elif student_data.fees_term_id.fees_terms == 'faculty_based':
                        for sub_cost in self.session_id.faculty_id.subject_cost_ids:
                            if sub_cost.subject_id == self.session_id.subject_id:
                                amount += sub_cost.product_id.lst_price
                    for prod in line.fees_element_line:
                        per_amount = prod.value
                        amount = student_data.product_id.lst_price
                        product_id = prod.product_id.id
                        prod_list.append({
                            'product_id': product_id,
                            'value': per_amount})
                    list_values.update({"invoice_date": self.attendance_date})
                    list_values.update({
                        'fees_factor': line.value,
                        'fees_reminder_line_ids': [(6, 0, line.line_ids.ids)],
                        'state': 'draft',
                        'fees_plan_id': fees_plan_line.id,
                        'student_id': student_data.student_id.id,
                        'op_fees_terms_line_id': line.id,
                        'course_id': student_data.course_id.id,
                        'batch_id': student_data.batch_id.id,
                        'amount': student_data.product_id.lst_price * line.value / 100,

                    })
                    plan_line = self.env['op.fees.plan.line'].sudo().create(list_values)
                    for rec in prod_list:
                        plan_line.sudo().write({'fees_element_line_ids': [(0, 0, rec)]})
                fees_plan_total = 0
                for fees_line in fees_plan_line.fees_plan_line_ids:
                    fees_plan_total += float(fees_line.amount)
                fees_plan_line.total_amount = fees_plan_total
                fees_plan_line.remaining_amount = fees_plan_total
        self.state = 'done'


class OpStudent(models.Model):
    _inherit = "op.student"

    fees_plan_ids = fields.One2many('op.fees.plan', 'student_id',
                                    string='Fees Plan Details',
                                    tracking=True)
    fees_plan_details_count = fields.Integer(compute='_compute_fees_plan_details')

    @api.depends('fees_plan_ids')
    def _compute_fees_plan_details(self):
        for fees in self:
            fees.fees_plan_details_count = self.env['op.fees.plan'].search_count(
                [('student_id', '=', self.id)])

    def count_fees_plan_details(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Fees Plan Details',
            'view_mode': 'tree,form',
            'res_model': 'op.fees.plan',
            'context': {'create': False},
            'domain': [('student_id', '=', self.id)],
            'target': 'current',
        }
