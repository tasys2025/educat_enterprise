# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models, fields


class OpSubjectCost(models.Model):
    _name = "op.subject.cost"
    _description = "Subject Cost"

    faculty_id = fields.Many2one('op.faculty', 'Faculty')
    subject_id = fields.Many2one('op.subject', 'Subject')
    product_id = fields.Many2one('product.product', 'product')
    cost = fields.Float('Cost', related="product_id.lst_price")


class OpFacultyInherit(models.Model):
    _inherit = "op.faculty"

    subject_cost_ids = fields.One2many('op.subject.cost', 'faculty_id', 'Subject Cost')
