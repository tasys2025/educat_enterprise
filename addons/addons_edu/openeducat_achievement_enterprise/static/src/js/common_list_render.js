/** @odoo-module */

import { ListRenderer } from "@web/views/list/list_renderer";
import { X2ManyField } from "@web/views/fields/x2many/x2many_field";
import { registry } from "@web/core/registry";
import { formatDate } from "@web/core/l10n/dates";

export class CommonSkillsListRenderer extends ListRenderer {
    get colspan() {
        if (this.props.activeActions) {
            return 3;
        }
        return 2;
    }

    get groupBy() {
        return '';
    }

    get groupedList() {
        const grouped = {};
        for (const record of this.list.records) {
            const data = record.data;
            const group = data[this.groupBy];
            if (grouped[group] === undefined) {
                grouped[group] = {
                    id: parseInt(group[0]),
                    name: group || this.env._t('Other'),
                    list: {
                        records: [],
                    },
                };
            }

            grouped[group].list.records.push(record);
        }
        return grouped;
    }

    formatDate(date) {
        return formatDate(date);
    }

    setDefaultColumnWidths() {}

    get showTable() {
        return this.props.list.records.length;
    }

    get isEditable() {
        return this.props.editable !== false;
    }

    async onCellClicked(record, column, ev) {
        return await super.onCellClicked(record, column, ev);
    }
}
CommonSkillsListRenderer.rowsTemplate = "openeducat_achievement_enterprise.ResumeListRenderer.Rows";

export class SkillsListRenderer extends CommonSkillsListRenderer {
    get groupBy() {
        return 'display_name';
    }

    calculateColumnWidth(column) {
        if (column.name != 'display_name') {
            return {
                type: 'absolute',
                value: '90px',
            }
        }

        return super.calculateColumnWidth(column);
    }
}
SkillsListRenderer.template = 'openeducat_achievement_enterprise.ResumeListRenderer';
SkillsListRenderer.recordRowTemplate = 'openeducat_achievement_enterprise.ResumeListRenderer.RecordRow';

export class SkillsX2ManyField extends X2ManyField {
    async onAdd({ context, editable } = {}) {
        const employeeId = this.props.record.resId;
        return super.onAdd({
            editable,
            context: {
                ...context,
                default_employee_id: employeeId,
            }
        });
    }
}
SkillsX2ManyField.components = {
    ...X2ManyField.components,
    ListRenderer: SkillsListRenderer,
};

registry.category("fields").add("ach_page", SkillsX2ManyField);
