# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_dynamic_admission
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 06:19+0000\n"
"PO-Revision-Date: 2022-12-14 06:19+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid "<em class=\"font-weight-normal text-muted\">Registration For:</em>"
msgstr "<em class=\"Font-Weight-Normal-Mutut\"> Registro para: </em>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-danger\">\n"
"                                                <i class=\"fa fa-fw fa-remove\" aria-label=\"Rejected\" title=\"Rejected\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Rejected</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge-Pill Badge-Danger\">\n"
"                                                <i class=\"fa fa-fw fa-remove\" aria-label=\"rechazado\" title=\"rechazado\" rol=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> rechazado </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-info\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"Pending\" title=\"Pending\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Pending</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge-Pill Badge-Info\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"pendiendo\" title=\"pendiente\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> pendiente </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-mute\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"Draft\" title=\"Draft\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Draft</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge-Pill Badge-Mute\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"draft\" title=\"draft\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> draft </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"Admission Confirm\" title=\"Admission Confirm\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Admission Confirm</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge Pill Badge-Primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"admisión confirmar\" title=\"admisión confirmar\" rol=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> admisión confirmar </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"Confirmed\" title=\"Confirmed\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Confirmed</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge Pill Badge-Primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"confirmado\" title=\"confirmado\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> confirmado </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"Online Admission\" title=\"Online Admission\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Online Admission</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge Pill Badge-Primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"admisión en línea\" title=\"en línea de admisión\" rol=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> Admisión en línea </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"Submitted\" title=\"Submitted\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Submitted</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge Pill Badge-Primary\">\n"
"                                                <i class=\"fa fa-fw fa-clock-o\" aria-label=\"enviado\" title=\"enviado\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> enviado </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-success\">\n"
"                                                <i class=\"fa fa-fw fa-check\" aria-label=\"Done\" title=\"Done\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Done</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge-Pill Badge-Success\">\n"
"                                                <i class=\"fa fa-fw fa-check\" aria-label=\"ded\" title=\"ded\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> hecho </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid ""
"<span class=\"badge badge-pill badge-warning\">\n"
"                                                <i class=\"fa fa-fw fa-remove\" aria-label=\"Cancelled\" title=\"Cancelled\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\">Cancelled</span>\n"
"                                            </span>"
msgstr ""
"<span class=\"Badge Badge-Pill Badge-Warning\">\n"
"                                                <i class=\"fa fa-fw fa-remove\" aria-label=\"cancelado\" title=\"cancelado\" role=\"img\"/>\n"
"                                                <span class=\"d-md-inline\"> cancelado </span>\n"
"                                            </span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid "<span class=\"s_website_form_label_content\">Birth Date</span>"
msgstr "<span class=\"s_website_form_label_content\"> fecha de nacimiento </span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid ""
"<span class=\"s_website_form_label_content\">Course</span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"
msgstr ""
"<span class=\"s_website_form_label_content\"> curso </span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid ""
"<span class=\"s_website_form_label_content\">Email</span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"
msgstr ""
"<span class=\"s_website_form_label_content\"> correo electrónico </span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid ""
"<span class=\"s_website_form_label_content\">First Name</span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"
msgstr ""
"<span class=\"S_WEBSITE_FORM_LABEL_CONTENT\"> Nombre </span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid "<span class=\"s_website_form_label_content\">Gender</span>"
msgstr "<span class=\"s_website_form_label_content\"> género </span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid ""
"<span class=\"s_website_form_label_content\">Last Name</span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"
msgstr ""
"<span class=\"s_website_form_label_content\"> apellido </span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid ""
"<span class=\"s_website_form_label_content\">Name</span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"
msgstr ""
"<span class=\"s_website_form_label_content\"> nombre </span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid ""
"<span class=\"s_website_form_label_content\">Phone</span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"
msgstr ""
"<span class=\"s_website_form_label_content\"> teléfono </span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid ""
"<span class=\"s_website_form_label_content\">Register</span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"
msgstr ""
"<span class=\"s_website_form_label_content\"> registrar </span>\n"
"                                <span class=\"s_website_form_mark\"> *</span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.portal_my_home_menu_newadmission_register
msgid "<span>Admission New Registration</span>"
msgstr "<span> Admisión Nuevo registro </span>"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid ""
"<strong>No suitable payment option could be found.</strong>\n"
"                            <br/>\n"
"                            If you believe that it is an error, please contact the website administrator."
msgstr ""
"<strong> no se pudo encontrar una opción de pago adecuada. </strong>\n"
"                            <br/>\n"
"                            Si cree que es un error, comuníquese con el administrador del sitio web."

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_needaction
msgid "Action Needed"
msgstr "Accion necesaria"

#. module: openeducat_dynamic_admission
#: model:ir.model,name:openeducat_dynamic_admission.model_op_admission
msgid "Admission"
msgstr "Admisión"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid "Admission - Payment"
msgstr "Admisión - Pago"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__admission
msgid "Admission Confirm"
msgstr "Admisión confirmar"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
#, python-format
msgid "Admission Date"
msgstr "Fecha de admisión"

#. module: openeducat_dynamic_admission
#: model:product.product,name:openeducat_dynamic_admission.op_product_24
#: model:product.template,name:openeducat_dynamic_admission.op_product_24_product_template
msgid "Admission Fees for MCA"
msgstr "Tarifas de admisión para MCA"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.portal_my_home_menu_newadmission_register
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.portal_my_home_newadmission_registration
msgid "Admission New Registration"
msgstr "Admisión nueva registro"

#. module: openeducat_dynamic_admission
#: model:ir.model,name:openeducat_dynamic_admission.model_op_admission_register
msgid "Admission Register"
msgstr "Registro de admisión"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__admission_register_line
msgid "Admission Register Line"
msgstr "Línea de registro de admisión"

#. module: openeducat_dynamic_admission
#: model:ir.model,name:openeducat_dynamic_admission.model_op_admission_customtemplate
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_register__admission_template_id
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_customtemplate_list_view
msgid "Admission Template"
msgstr "Plantilla de admisión"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_customtemplate_form_view
msgid "Admission Template Form"
msgstr "Formulario de plantilla de admisión"

#. module: openeducat_dynamic_admission
#: model:ir.actions.act_window,name:openeducat_dynamic_admission.action_admission_customtemplate_list
#: model:ir.ui.menu,name:openeducat_dynamic_admission.menu_action_op_admission_customtemplate_list
msgid "Admission Templates"
msgstr "Plantillas de admisión"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__admission_url
msgid "Admission URL"
msgstr "URL de admisión"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_customtemplate_form_view
msgid "Admission Web Template"
msgstr "Plantilla web de admisión"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "All"
msgstr ""

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_new_registration
msgid "All Registers"
msgstr "Todos los registros"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__application
msgid "Application"
msgstr "Solicitud"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Application Date"
msgstr ""

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Application No"
msgstr ""

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid "Application Number"
msgstr "Numero de aplicacion"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid "Application date"
msgstr "Fecha de aplicacion"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_new_registration
msgid "Apply"
msgstr "Aplicar"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_attachment_count
msgid "Attachment Count"
msgstr "Recuento de adjuntos"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__birth_date
msgid "Birth Date"
msgstr "Fecha de nacimiento"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__cancel
msgid "Cancelled"
msgstr "Cancelado"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.view_op_admission_inherit_application_form
msgid "Confirm"
msgstr "Confirmar"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__confirm
msgid "Confirmed"
msgstr "Confirmado"

#. module: openeducat_dynamic_admission
#. openerp-web
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#: code:addons/openeducat_dynamic_admission/static/src/js/website_admission_form_editor.js:0
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
#, python-format
msgid "Course"
msgstr "Curso"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_new_registration
msgid "Course Name:"
msgstr "Nombre del curso:"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__create_uid
msgid "Created by"
msgstr "Creado por"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__create_date
msgid "Created on"
msgstr "Creado en"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__display_name
msgid "Display Name"
msgstr "Nombre para mostrar"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__done
msgid "Done"
msgstr "Hecho"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__draft
msgid "Draft"
msgstr "Reclutar"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_customtemplate_form_view
msgid "Edit Admission Form"
msgstr "Editar formulario de admisión"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__email
msgid "Email"
msgstr "Correo electrónico"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid ""
"Email\n"
"                                <span class=\"s_website_form_mark\">*</span>"
msgstr ""
"Correo electrónico\n"
"                                <span class=\"s_website_form_mark\">*</span>"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Enroll"
msgstr ""

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_course__product_id
msgid "Fees"
msgstr "Tarifa"

#. module: openeducat_dynamic_admission
#. openerp-web
#: code:addons/openeducat_dynamic_admission/static/src/js/website_admission_form_editor.js:0
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__gender__f
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
#, python-format
msgid "Female"
msgstr "Femenino"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__first_name
msgid "First Name"
msgstr "Primer nombre"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_follower_ids
msgid "Followers"
msgstr "Seguidores"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_partner_ids
msgid "Followers (Partners)"
msgstr "Seguidores (socios)"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__gender
msgid "Gender"
msgstr "Género"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__has_message
msgid "Has Message"
msgstr "Tiene mensaje"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__id
msgid "ID"
msgstr "IDENTIFICACIÓN"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,help:openeducat_dynamic_admission.field_op_admission_customtemplate__message_needaction
msgid "If checked, new messages require your attention."
msgstr "Si se revisan, los nuevos mensajes requieren su atención."

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,help:openeducat_dynamic_admission.field_op_admission_customtemplate__message_has_error
#: model:ir.model.fields,help:openeducat_dynamic_admission.field_op_admission_customtemplate__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "Si se verifican, algunos mensajes tienen un error de entrega."

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_is_follower
msgid "Is Follower"
msgstr "Es seguidor"

#. module: openeducat_dynamic_admission
#: model:ir.model,name:openeducat_dynamic_admission.model_op_course
msgid "LMS Course"
msgstr "Curso de LMS"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate____last_update
msgid "Last Modified on"
msgstr "Última modificación en"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__last_name
msgid "Last Name"
msgstr "Apellido"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__write_uid
msgid "Last Updated by"
msgstr "Última actualización por"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__write_date
msgid "Last Updated on"
msgstr "Ultima actualización en"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_main_attachment_id
msgid "Main Attachment"
msgstr "Apego principal"

#. module: openeducat_dynamic_admission
#. openerp-web
#: code:addons/openeducat_dynamic_admission/static/src/js/website_admission_form_editor.js:0
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__gender__m
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
#, python-format
msgid "Male"
msgstr "Masculino"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_has_error
msgid "Message Delivery error"
msgstr "Error de entrega de mensajes"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_ids
msgid "Messages"
msgstr "Mensajes"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__name
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__name
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid "Name"
msgstr "Nombre"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid ""
"Name\n"
"                                <span class=\"s_website_form_mark\">*</span>"
msgstr ""
"Nombre\n"
"                                <span class=\"s_website_form_mark\">*</span>"

#. module: openeducat_dynamic_admission
#: model:openeducat.portal.menu,name:openeducat_dynamic_admission.portal_menu_dynamic_admission_registration
#: model:website.menu,name:openeducat_dynamic_admission.menu_admission_new_registration
msgid "New Registration"
msgstr "Nuevo registro"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "None"
msgstr ""

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_needaction_counter
msgid "Number of Actions"
msgstr "Número de acciones"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_has_error_counter
msgid "Number of errors"
msgstr "Número de errores"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,help:openeducat_dynamic_admission.field_op_admission_customtemplate__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Número de mensajes que requieren una acción"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,help:openeducat_dynamic_admission.field_op_admission_customtemplate__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Número de mensajes con error de entrega"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__online
#, python-format
msgid "Online Admission"
msgstr "Admisión en línea"

#. module: openeducat_dynamic_admission
#. openerp-web
#: code:addons/openeducat_dynamic_admission/static/src/js/website_admission_form_editor.js:0
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__gender__o
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
#, python-format
msgid "Other"
msgstr "Otro"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid "Pay Now"
msgstr "Pagar ahora"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid "Pay with"
msgstr "Pagar con"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid "Payment Now"
msgstr "Pago ahora"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__pending
msgid "Pending"
msgstr "Pendiente"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid ""
"Phone\n"
"                                <span class=\"s_website_form_mark\">*</span>"
msgstr ""
"Teléfono\n"
"                                <span class=\"s_website_form_mark\">*</span>"

#. module: openeducat_dynamic_admission
#. openerp-web
#: code:addons/openeducat_dynamic_admission/static/src/js/000.js:0
#, python-format
msgid "Please fill in the form correctly."
msgstr ""

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_checkout
msgid "Product Details"
msgstr "Detalles de producto"

#. module: openeducat_dynamic_admission
#: model:ir.model,name:openeducat_dynamic_admission.model_product_product
msgid "Product Variant"
msgstr "Variante de producto"

#. module: openeducat_dynamic_admission
#. openerp-web
#: code:addons/openeducat_dynamic_admission/static/src/js/website_admission_form_editor.js:0
#, python-format
msgid "Register"
msgstr ""

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_course__reg_fees
msgid "Registration Fees"
msgstr "Cuotas de inscripción"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__order_id
msgid "Registration Fees Ref"
msgstr "Tarifas de inscripción referencia"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_register__sign_in_required
msgid "Registration Required ?"
msgstr "Se requiere registro ?"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__reject
msgid "Rejected"
msgstr "Rechazado"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__message_has_sms_error
msgid "SMS Delivery error"
msgstr "Error de entrega de SMS"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Search <span class=\"nolabel\">(in Application No)</span>"
msgstr ""

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Search in Admission Date"
msgstr ""

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Search in All"
msgstr ""

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Search in Application Date"
msgstr ""

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Search in Course"
msgstr ""

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Search in State"
msgstr ""

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission__state
#, python-format
msgid "State"
msgstr "Estado"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid "Status"
msgstr "Estado"

#. module: openeducat_dynamic_admission
#: code:addons/openeducat_dynamic_admission/controller/admission_new.py:0
#, python-format
msgid "Student"
msgstr ""

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid "Student Registration Details"
msgstr "Detalles de inscripción al estudiante"

#. module: openeducat_dynamic_admission
#: model_terms:op.admission.customtemplate,admission_webpage:openeducat_dynamic_admission.dynamic_template1
msgid "Submit"
msgstr "Enviar"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields.selection,name:openeducat_dynamic_admission.selection__op_admission__state__submit
msgid "Submitted"
msgstr "Presentada"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.admission_new_registration
msgid "There Is No Any Course Register Available."
msgstr "No hay ningún registro de curso disponible."

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.openeducat_student_newregistration_list_data
msgid "There are no records."
msgstr "No hay registros."

#. module: openeducat_dynamic_admission
#: model:product.product,uom_name:openeducat_dynamic_admission.op_product_24
#: model:product.template,uom_name:openeducat_dynamic_admission.op_product_24_product_template
msgid "Units"
msgstr "Unidades"

#. module: openeducat_dynamic_admission
#. openerp-web
#: code:addons/openeducat_dynamic_admission/static/src/js/000.js:0
#, python-format
msgid "Uploaded file is too large."
msgstr ""

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__admission_webpage
msgid "WebPage Content"
msgstr "Contenido de la página web"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,field_description:openeducat_dynamic_admission.field_op_admission_customtemplate__website_message_ids
msgid "Website Messages"
msgstr "Mensajes del sitio web"

#. module: openeducat_dynamic_admission
#: model:ir.model.fields,help:openeducat_dynamic_admission.field_op_admission_customtemplate__website_message_ids
msgid "Website communication history"
msgstr "Historial de comunicación del sitio web"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.application_confirmed
msgid "Your application"
msgstr "Su aplicación"

#. module: openeducat_dynamic_admission
#: model_terms:ir.ui.view,arch_db:openeducat_dynamic_admission.application_confirmed
msgid "has been received."
msgstr "ha sido recibido."
