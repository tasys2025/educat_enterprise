odoo.define('website.form_editor_inherit', function (require) {
'use strict';

const core = require('web.core');
const FormEditorRegistry = require('website.form_editor_registry');
const options = require('web_editor.snippets.options');
const Dialog = require('web.Dialog');
const dom = require('web.dom');
require('website.editor.snippets.options');
//const Formeditoraa = require('website.form_editor');

const qweb = core.qweb;
const _t = core._t;

options.registry.AddFieldForm.include({
    isTopOption: true,
    isTopFirstOption: true,

    //--------------------------------------------------------------------------
    // Options
    //--------------------------------------------------------------------------

    /**
     * Add a char field at the end of the form.
     * New field is set as active
     */
    addField: async function (previewMode, value, params) {
        const field = this._getCustomField('char', 'Custom Text');

        field.formatInfo = this._getDefaultFormat();
        const fieldEl = this._renderField(field);
        console.log(fieldEl)
        $(fieldEl).attr('data-Admission_field', 'custom')
        console.log($(fieldEl))
        this.$target.find('.s_website_form_submit, .s_website_form_recaptcha').first().before(fieldEl);
        this.trigger_up('activate_snippet', {
            $snippet: $(fieldEl),
        });
    },
});
options.registry.AddField.include({
    isTopOption: true,
    isTopFirstOption: true,

    //--------------------------------------------------------------------------
    // Options
    //--------------------------------------------------------------------------

    /**
     * Add a char field with active field properties after the active field.
     * New field is set as active
     */
    addField: async function (previewMode, value, params) {
        this._super(...arguments)
       const field = this._getCustomField('char', 'Custom Text');
       console.log(field,"THIS IS FIELD")

        field.formatInfo = this._getDefaultFormat();
        const fieldEl = this._renderField(field);
//        this.$(fieldEl).addClass('jaymin')
//        $(fieldEl).addClass('jay')
////        this.$target.addClass('jay');
        this.$target.next('.s_website_form_custom').attr('data-Admission_field', 'custom')
        console.log(this.$target,"target field")
    },
});
})