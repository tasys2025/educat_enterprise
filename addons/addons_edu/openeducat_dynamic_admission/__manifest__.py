# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################
{
    'name': "OpenEduCat Dynamic Admission",
    'version': '16.0.1.0',
    'category': 'Education',
    'sequence': 3,
    'summary': "Manage Admissions Form""",
    'complexity': "easy",
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'depends': [
        'website',
        'website_sale',
        'website_payment',
        'openeducat_admission',
        'openeducat_core_enterprise',
        'website_mail',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/admission_register_view.xml',
        'views/admission_website_new_view.xml',
        'views/admission_payment_form.xml',
        'views/admission_application_view.xml',
        'views/course_view.xml',
        'views/student_registration_details_portal.xml',
        'views/admission_customtemplate_view.xml',
        'views/courses_details_page.xml',
        'data/config_data.xml',
        'menu/admission_portal_menu.xml',
    ],
    'demo': [
        'demo/admission_template_demo.xml',
        'demo/product_demo.xml',
        'demo/op_course_demo.xml',
        'demo/subject_demo.xml',
        'demo/admission_register_demo.xml',
        'demo/batch_demo.xml',
        'demo/student_course_demo.xml',
    ],
    'assets': {
        'website.assets_editor': [
            'openeducat_dynamic_admission/'
            'static/src/js/website_admission_form_editor.js',
        ],
        'web.assets_backend': [
            '/openeducat_dynamic_admission/static/src/scss/account_contract_dashboard.scss',
        ],
        'web.assets_frontend': [
            'openeducat_dynamic_admission/static/src/js/000.js',
            '/openeducat_dynamic_admission/static/src/scss/website_slides.scss',
            '/openeducat_dynamic_admission/static/src/scss/owl.carousel.css',
            '/openeducat_dynamic_admission/static/src/scss/admission_common.scss',
        ],
        'website.assets_wysiwyg': [
            # 'openeducat_dynamic_admission/static/src/js/snippets_editor.js',
            'openeducat_dynamic_admission/static/src/js/options.js',
        ],
        'web_editor.assets_wysiwyg': [
            'openeducat_dynamic_admission/static/src/js/snippets_editor.js',
            # 'openeducat_dynamic_admission/static/src/js/options.js',
        ],
        'web._assets_primary_variables': [
            '/openeducat_dynamic_admission/static/src/scss/primary_variables.scss'
        ],
    },
    'installable': True,
    'auto_install': False,
    'application': True,
    'price': 75,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'live_test_url': 'https://www.openeducat.org/plans'
}

