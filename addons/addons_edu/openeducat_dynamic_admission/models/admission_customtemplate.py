
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models, fields
from odoo.addons.http_routing.models.ir_http import slug
from odoo.http import request


class OpAdmissionTemplate(models.Model):
    _name = "op.admission.customtemplate"
    _description = "Admission Template"
    _inherit = ['mail.thread']
    _rec_name = 'name'

    def _default_admission_form(self):
        return """
<section class="s_website_form pt16 pb16 o_colored_level" data-vcss="001" data-snippet="s_website_form" data-name="Form">
            <div class="container">
                <form action="/website/form/" method="post" enctype="multipart/form-data" class="o_mark_required" data-mark="*"
                  data-pre-fill="true" id="op_admission_form" data-success-mode="redirect"
                  data-success-page="/admission-success" data-model_name="op.admission">
                <div class="s_website_form_rows row s_col_no_bgcolor">
                    
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_required" data-type="char"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">First Name</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <input type="text" class="form-control s_website_form_input" name="first_name" required="1"
                                       data-fill-with="first_name"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_required" data-type="char"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">Middle Name</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <input type="text" class="form-control s_website_form_input" name="middle_name" required="1"
                                       data-fill-with="middle_name"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_required" data-type="char"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">Last Name</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <input type="text" class="form-control s_website_form_input" name="last_name" required="1"
                                       data-fill-with="last_name"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_required" data-type="date"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">Birth Date</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <div class="s_website_form_date input-group date" id="datepicker23640899442317997"
                                     data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input s_website_form_input"
                                           data-target="#datepicker23640899442317997" name="birth_date" required="1"/>
                                    <div class="input-group-append" data-target="#datepicker23640899442317997"
                                         data-toggle="datetimepicker">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"> </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_required" data-type="selection"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class=" col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">Gender</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <div class="row s_col_no_resize s_col_no_bgcolor s_website_form_multiple" data-name="gender"
                                     data-display="horizontal">
                                    <div class="radio col-12 col-lg-4 col-md-6">
                                        <div class="form-check">
                                            <input type="radio" class="s_website_form_input form-check-input"
                                                   name="gender" value="m" required="1"/>
                                            <label class="form-check-label s_website_form_check_label">
                                                Male
                                            </label>
                                        </div>
                                    </div>
                                    <div class="radio col-12 col-lg-4 col-md-6">
                                        <div class="form-check">
                                            <input type="radio" class="s_website_form_input form-check-input"
                                                   name="gender" value="f" required="1"/>
                                            <label class="form-check-label s_website_form_check_label">
                                                Female
                                            </label>
                                        </div>
                                    </div>
                                    <div class="radio col-12 col-lg-4 col-md-6">
                                        <div class="form-check">
                                            <input type="radio" class="s_website_form_input form-check-input"
                                                   name="gender" value="o" required="1"/>
                                            <label class="form-check-label s_website_form_check_label">
                                                Other
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_required" data-type="char"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">Phone</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <input type="text" class="form-control s_website_form_input" name="phone" required="1"
                                       data-fill-with="phone"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_required" data-type="email"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label" style="width: 200px">
                                <span class="s_website_form_label_content">Email</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <input type="email" class="form-control s_website_form_input" name="email" required="1"
                                       data-fill-with="email"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_dnone" data-type="hidden"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">Register</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <input type="hidden" class="form-control s_website_form_input" name="register_id" required="1" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group s_website_form_field mb-3 col-12 s_website_form_dnone" data-type="hidden"
                         data-name="Field">
                        <div class="row s_col_no_resize s_col_no_bgcolor">
                            <label class="col-form-label col-sm-auto s_website_form_label " style="width: 200px">
                                <span class="s_website_form_label_content">Course</span>
                                <span class="s_website_form_mark">*</span>
                            </label>
                            <div class="col-sm">
                                <input type="hidden" class="form-control s_website_form_input" name="course_id" required="1" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3 col-12 s_website_form_submit" data-name="Submit Button" data-original-title="">
                        <div style="width: 200px;" class="s_website_form_label"></div>
                        <a href="#" role="button" class="btn btn-primary btn-lg s_website_form_send" contenteditable="true">
                            Submit
                        </a>
                        <span id="s_website_form_result"></span>
                    </div>
                </div>
            </form>
        </div>
        </section>
        """ # noqa

    name = fields.Char("Name", required=True)
    admission_webpage = fields.Html(
        'WebPage Content', translate=True, sanitize_attributes=False,
        sanitize_form=False, default=_default_admission_form)
    admission_url = fields.Char("Admission URL")
    admission_register_line = fields.One2many(
        'op.admission.register', 'admission_template_id'
    )

    def website_edit_admission_page_content(self):
        url = request.httprequest.host_url + 'admission-template/' + slug(self) + '?enable_editor=1' # noqa
        self.admission_url = url

        return {
            'type': 'ir.actions.act_url',
            'target': 'self',
            'url': url,
        }
