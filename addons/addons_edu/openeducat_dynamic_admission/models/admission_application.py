
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models, fields, api


class OpAdmission(models.Model):
    _inherit = "op.admission"

    name = fields.Char(
        'Name', required=False, translate=True)
    first_name = fields.Char(
        'First Name', required=False, translate=True)
    last_name = fields.Char(
        'Last Name', required=False, translate=True,
        states={'done': [('readonly', True)]})
    birth_date = fields.Date(
        'Birth Date', required=False, states={'done': [('readonly', True)]})
    email = fields.Char(
        'Email', required=False,
        states={'done': [('readonly', True)]})
    gender = fields.Selection(
        [('m', 'Male'), ('f', 'Female'), ('o', 'Other')],
        string='Gender',
        required=False,
        states={'done': [('readonly', True)]})
    state = fields.Selection(selection_add=[
        ('draft', 'Draft'), ('online', 'Online Admission'),
        ('submit', 'Submitted'), ('confirm', 'Confirmed'),
        ('admission', 'Admission Confirm'), ('reject', 'Rejected'),
        ('pending', 'Pending'), ('cancel', 'Cancelled'), ('done', 'Done')],
        default='draft', tracking=True)
    order_id = fields.Many2one('sale.order', 'Registration Fees Ref')
    application = fields.Char()
    email2 = fields.Char(string="HEllo")

    @api.constrains('birth_date')
    def _check_birthdate(self):
        return True


class Product(models.Model):
    _inherit = "product.product"

    def _is_add_to_cart_allowed(self):
        self.ensure_one()
        return \
            self.user_has_groups('base.group_user') \
            or self.user_has_groups('base.group_portal') \
            or self.user_has_groups('base.group_public') \
            or (self.sale_ok and self.website_published)
