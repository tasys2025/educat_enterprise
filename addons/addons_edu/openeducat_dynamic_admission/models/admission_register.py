
# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.
#
##############################################################################
#
#    OpenEduCat Inc.
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

from odoo import models, fields


class OpAdmissionRegister(models.Model):
    _inherit = "op.admission.register"

    admission_template_id = fields.Many2one(
        'op.admission.customtemplate', string='Admission Template')
    sign_in_required = fields.Boolean(string="Registration Required ?")
