from odoo import http, _
from odoo.http import request
from odoo.tools import ustr
from lxml import etree
from lxml.builder import E
from odoo.addons.http_routing.models.ir_http import slugify
from ast import literal_eval
import uuid
from odoo.exceptions import ValidationError


class ViewEditorMain(http.Controller):

    def web_no_code_studio_initialize(self, architecture, view):
        editor_view = view.sudo().search([('inherit_id', '=', view.id),
                                          ('name', '=', 'View Editor: %s' % view.name)])
        try:
            if editor_view and len(architecture):
                editor_view.arch_db = architecture
            elif editor_view:
                editor_view.unlink()
            elif len(architecture):
                request.env['ir.ui.view'].create({
                    'type': view.type,
                    'model': view.model,
                    'inherit_id': view.id,
                    'mode': 'extension',
                    'priority': 99,
                    'arch': architecture,
                    'name': 'View Editor: %s' % view.name,
                })
        except ValidationError:
            return False

    def _get_xpath_node(self, arch, operation):
        # Todo
        expr = self._node_to_expr(operation['target'])
        position = operation['position']
        return etree.SubElement(arch, 'xpath', {
            'expr': expr,
            'position': position
        })

    def _operation_attributes(self, arch, que, model=None):
        new_attrs = que['newAttrs']

        if 'groups' in new_attrs:
            groups = []
            for many2many_value in new_attrs['groups']:
                group_xmlid = request.env['ir.model.data'].search([
                    ('model', '=', 'res.groups'),
                    ('res_id', '=', many2many_value)])
                groups.append(group_xmlid.complete_name)
            new_attrs['groups'] = ",".join(groups)

        xpath = self._get_xpath_node(arch, que)

        for key, attr in new_attrs.items():
            xml_node = etree.Element('attribute', {'name': key})
            xml_node.text = str(attr)
            xpath.insert(0, xml_node)

    def _node_to_expr(self, node):
        if node.get('xpath_info') and not node.get('subview_xpath'):
            expr = ''.join(['/%s[%s]' % (parent['tag'], parent['index']) for parent in
                            node.get('xpath_info')])
        else:
            expr = '//' + node['tag']
            for k, v in node.get('attrs', {}).items():
                if k == 'class':
                    expr += '[contains(@%s,\'%s\')]' % (k, v)
                else:
                    expr += '[@%s=\'%s\']' % (k, v)

            if not node.get('subview_xpath'):
                expr = expr + '[not(ancestor::field)]'

        if node.get('subview_xpath'):
            xpath = node.get('subview_xpath')
            if node.get('isSubviewAttr'):
                expr = xpath
            elif len(xpath) - len(expr) != xpath.find(expr):
                expr = xpath + expr
        return expr

    def _operation_move(self, arch, operation, model=None):
        # Todo
        xpath_node = self._get_xpath_node(arch, operation)
        xml_node = etree.Element('xpath', {
            'expr': self._node_to_expr(operation['node']),
            'position': 'move',
        })
        xpath_node.append(xml_node)

    def _operation_add(self, arch, operation, model):
        node = operation['node']
        xpath_node = self._get_xpath_node(arch, operation)

        def add_columns(xml_node):
            name = 'editor_group_' + xml_node.get('name').split('_')[2]

            if xml_node.tag != 'group':
                xml_node_group = etree.SubElement(xml_node, 'group', {'name': name})
            else:
                xml_node_group = xml_node

            etree.SubElement(xml_node_group, 'group', {'name': name + '_left'})
            etree.SubElement(xml_node_group, 'group', {'name': name + '_right'})

        xml_node = etree.Element(node['tag'], node.get('attrs'))
        if node['tag'] == 'notebook':
            name = 'editor_page_' + node['attrs']['name'].split('_')[2]
            xml_node_page = etree.Element('page', {'string': 'New Page', 'name': name})
            add_columns(xml_node_page)
            xml_node.insert(0, xml_node_page)
        elif node['tag'] == 'page':
            add_columns(xml_node)
        elif node['tag'] == 'group':
            if 'empty' not in operation:
                add_columns(xml_node)
        elif node['tag'] == 'button':
            button_field = request.env['ir.model.fields'].browse(node['field'])
            button_count_field, button_action = self._get_or_create_fields_for_button(
                model, button_field, node['string'])

            xml_node_field = etree.Element('field',
                                           {'widget': 'statinfo',
                                            'name': button_count_field.name,
                                            'string': node['string'] or
                                            button_count_field.field_description})
            xml_node.insert(0, xml_node_field)

            xml_node.attrib['type'] = 'action'
            xml_node.attrib['name'] = str(button_action.id)
        else:
            xml_node.text = node.get('text')
        xpath_node.insert(0, xml_node)

    @http.route('/openeducat_web_no_code_studio/get/view_arch', type='json',
                auth='user')
    def get_web_no_code_studio_view_arch(self, **values):
        view_type = 'tree' if values.get('type') == 'list' else values.get('type')
        view_id = values.get('id') if values.get('id') else False
        ir_view = request.env['ir.ui.view']

        if not view_id:
            view_id = request.env['ir.ui.view']. \
                default_view(request.env[values.get('model')]._name, view_type)
        if view_id:
            view = ir_view.browse(view_id)
            arch_view = view.sudo().search([('inherit_id', '=', view.id),
                                            ('name', '=',
                                             'View Editor: %s' % view.name)])
        else:
            view = False
            arch_view = False

        if view_id:
            return {
                'web_no_code_studio_arch_id': arch_view.id or False,
                'web_no_code_studio_arch': arch_view and arch_view.arch_db or "<data/>"
            }
        else:
            return False

    @http.route('/openeducat_web_no_code_studio/update/arch', type='json', auth='user')
    def web_no_code_studio_arch_update_view(self, **post):
        # Todo: Complete This

        return True

    @http.route('/openeducat_web_no_code_studio/default_view', type='json', auth='user')
    def web_no_code_studio_field_default_view(self, model, field):
        value = request.env['ir.default'].get(model, field, company_id=True)
        return {
            'value': value
        }

    @http.route('/openeducat_web_no_code_studio/rename/field', type='json', auth='user')
    def web_no_code_studio_field_label_rename(self, **post):
        openeducat_web_no_code_studio = request.env['ir.ui.view'].browse(
            post.get('web_no_code_studio_arch_id'))
        openeducat_web_no_code_studio.arch_db = post.get('web_no_code_studio_arch')

        field = request.env['ir.model.fields'].search([
            ('model', '=', post.get('model')),
            ('name', '=', post.get('field_name'))])
        # Todo: Does Not Allow TO Change Name From Code
        field.write({'name': post.get('new_label')})

    def create_new_field(self, values):
        # Todo: Change This
        model_name = values.pop('model_name')

        model = request.env['ir.model'].search([('model', '=', model_name)])

        values['model_id'] = model.id

        if values.get('type'):
            values['ttype'] = values.pop('type')

        if values.get('relation_id'):
            values['relation'] = request.env['ir.model'].browse(
                values.pop('relation_id')).model

        if values.get('related') and values.get('ttype') == 'one2many':
            field_name = values.get('related').split('.')[-1]
            field = request.env['ir.model.fields'].search([
                ('name', '=', field_name),
                ('model', '=', values.pop('relational_model')),
            ])
            field.ensure_one()
            values.update(
                relation=field.relation,
                relation_field=field.relation_field,
            )
        if values.get('relation_field_id'):
            field = request.env['ir.model.fields'].browse(
                values.pop('relation_field_id'))
            values.update(
                relation=field.model_id.model,
                relation_field=field.name,
            )
        if values.get('selection'):
            values['selection'] = ustr(values['selection'])

        if values.get('ttype') == 'many2many':
            values['relation_table'] = request.env[
                'ir.model.fields']._get_next_relation(model_name,
                                                      values.get('relation'))

        default_value = values.pop('default_value', False)

        values = {
            k: v
            for k, v in values.items()
            if k in request.env['ir.model.fields']._fields
        }
        new_field = request.env['ir.model.fields'].create(values)

        if default_value:
            if new_field.ttype == 'selection':
                if default_value is True:
                    default_value = new_field.selection_ids[:1].value
            self.set_default_value(new_field.model, new_field.name, default_value)

        return new_field

    def _operation_remove(self, arch, operation, model=None):
        expr = self._node_to_expr(operation['target'])

        etree.SubElement(arch, 'xpath', {
            'expr': expr,
            'position': 'replace'
        })
        if operation['target'].get('extra_nodes'):
            for target in operation['target']['extra_nodes']:
                expr = self._node_to_expr(target)
                etree.SubElement(arch, 'xpath', {
                    'expr': expr,
                    'position': 'replace'
                })

    @http.route('/openeducat_web_no_code_studio/update/view', type='json', auth='user')
    def web_no_code_studio_update_view(self, **post):
        view_id, steps, view_arch = \
            post.get('view_id'), post.get('steps'), post.get('arch'),
        parser = etree.XMLParser(remove_blank_text=True)
        if view_arch == "":
            view_arch = '<data/>'

        view = request.env['ir.ui.view'].sudo().browse(view_id)
        arch = etree.fromstring(view_arch, parser=parser)
        model = view.model

        for op in steps:
            if 'node' in op:
                if op['node'].get('tag') == 'field' and op['node'].get(
                        'field_description'):
                    model = op['node']['field_description']['model_name']
                    # Check if field exists before creation
                    field = request.env['ir.model.fields'].search([
                        ('name', '=', op['node']['field_description']['name']),
                        ('model', '=', model),
                    ], limit=1)
                    if not field:
                        field = self.create_new_field(
                            op['node']['field_description'])
                    op['node']['attrs']['name'] = field.name
                if op['node'].get('tag') == 'filter' and op['target']['tag'] \
                        == 'group' and op['node']['attrs'].get('create_group'):
                    op['node']['attrs'].pop('create_group')
                    create_group_op = {
                        'node': {
                            'tag': 'group',
                            'attrs': {
                                'name': 'editor_group_by',
                            }
                        },
                        'empty': True,
                        'target': {
                            'tag': 'search',
                        },
                        'position': 'inside',
                    }
                    self._operation_add(arch, create_group_op, model)
            # set a more specific xpath (with templates//) for the kanban view
            if view.type == 'kanban':
                if op.get('target') and op['target'].get('tag') == 'field':
                    op['target']['tag'] = 'templates//field'
                    if op['target'].get('extra_nodes'):
                        for target in op['target']['extra_nodes']:
                            target['tag'] = 'templates//' + target['tag']

            # call the right operation handler
            getattr(self, '_operation_%s' % (op['type']))(arch, op, model)

        new_arch = etree.tostring(arch, encoding='unicode', pretty_print=True)
        self.web_no_code_studio_initialize(new_arch, view)

        new_editor_view = view.sudo().search([('inherit_id', '=', view.id),
                                              ('name', '=',
                                               'View Editor: %s' % view.name)])

        # try:
        #     normalized_view = new_editor_view.normalize()
        #     self.web_no_code_studio_initialize(normalized_view, view)
        # except ValueError:
        #     self.web_no_code_studio_initialize(new_arch, view)
        return {
            "fields_views": {
                'list' if view.type == 'tree' else view.type:
                    request.env[view.model].with_context(editor=True).
                    fields_view_get(view.id, view.type)
            },
            "fields": request.env[view.model].fields_get(),
            "web_no_code_studio_arch_id": new_editor_view.id,
        }

    @http.route('/openeducat_web_no_code_studio/default_value/set', type='json',
                auth='user')
    def set_default_value(self, model, field, value):
        request.env['ir.default'].set(model, field, value, company_id=True)

    @http.route('/get/restore/default_view', type='json', auth='user')
    def get_restored_default_view(self, view_id):
        view = request.env['ir.ui.view'].browse(view_id)
        code_view = view.sudo().search([('inherit_id', '=', view.id),
                                        ('name', '=', 'View Editor: %s' % view.name)])

        ViewModel = request.env[view.model]
        fields_view = ViewModel.with_context(editor=True).get_view(view.id, view.type)
        view_type = 'list' if view.type == 'tree' else view.type
        models = fields_view['models']

        return {
            'models': {model: request.env[model].fields_get() for model in models}
        }

    @http.route('/openeducat_web_no_code_studio/menu_action', type='json', auth='user')
    def _get_menu_bar_action(self, name, model, view_id=None, view_type=None):
        view_type = 'tree' if view_type == 'list' else view_type
        model = request.env['ir.model'].search([('model', '=', model)], limit=1)
        action = None
        if hasattr(self, '_get_menu_action_' + name):
            action = getattr(self, '_get_menu_action_' + name)(model, view_id=view_id,
                                                               view_type=view_type)

        return action

    def _get_menu_action_automation(self, model, view_id, view_type):
        return {
            'name': _('Automated Actions'),
            'type': 'ir.actions.act_window',
            'res_model': 'base.automation',
            'views': [[False, 'list'], [False, 'form']],
            'target': 'current',
            'domain': [],
            'context': {
                'default_model_id': model.id,
                'search_default_model_id': model.id,
            },
        }

    def _get_menu_action_access(self, model, view_id, view_type):
        return {
            'name': _('Access'),
            'type': 'ir.actions.act_window',
            'res_model': 'ir.model.access',
            'views': [[False, 'list'], [False, 'form']],
            'target': 'current',
            'domain': [],
            'context': {
                'default_model_id': model.id,
                'search_default_model_id': model.id,
            },
        }

    def _get_menu_action_reports(self, model, view_id, view_type):
        return {
            'name': _('Reports'),
            'type': 'ir.actions.act_window',
            'res_model': 'ir.actions.report',
            'views': [[False, 'kanban']],
            'target': 'current',
            'domain': [('model_id', '=', model.id)],
            'context': {
                'default_model_id': model.id,
                'search_default_model_id': model.id,
            },
        }

    @http.route('/openeducat_web_no_code_studio/edit/field', type='json', auth='user')
    def edit_field(self, model_name, field_name, values, force_edit=False):
        field = request.env['ir.model.fields'].search(
            [('model', '=', model_name), ('name', '=', field_name)])

        if field.ttype == 'selection' and 'selection' in values:
            selection_values = [False] + [x[0] for x in
                                          literal_eval(values['selection'])]
            records = request.env[model_name].search(
                [(field_name, 'not in', selection_values)])
            if records and not force_edit:
                message = _("""There are %s records using selection values
                 not listed in those you are trying to save.
    Are you sure you want to remove the selection values of those records?""") % len(
                    records)
                return {'records_linked': len(records), 'message': message}
            records.write({field_name: False})

        field.write(values)

        if field.ttype == 'selection':
            current_default = request.env['ir.default'].get(model_name, field_name,
                                                            company_id=True)
            if current_default:
                selection_values = literal_eval(field.selection)
                if current_default not in [x[0] for x in selection_values]:
                    request.env['ir.default'].discard_values(model_name, field_name,
                                                             [current_default])

    @http.route('/openeducat_web_no_code_studio/menu/edit', type='json', auth='user')
    def edit_menu_web_no_code_studio(self, can_move, can_delete):
        for menu in can_move:
            menu_id = request.env['ir.ui.menu'].browse(int(menu))
            if 'parent_menu_id' in can_move[menu]:
                menu_id.parent_id = can_move[menu]['parent_menu_id']
            if 'sequence' in can_move[menu]:
                menu_id.sequence = can_move[menu]['sequence']

        request.env['ir.ui.menu'].browse(can_delete).write({'active': False})

        return True

    @http.route('/openeducat_web_no_code_studio/create/app', type='json', auth='user')
    def create_view_new_app(self, app_name=False, object_name=None, model_type=False,
                            model_id=False, new_options=False, attachment=False):
        if model_type == 'existing' and model_id:
            model = request.env['ir.model'].browse(model_id)
        else:
            model, addon_model = self.create_new_model_app(
                object_name, data=new_options)

        action = self.create_web_no_code_studio_default_action(object_name, model)
        action_ref = 'ir.actions.act_window,' + str(action.id)

        menu_values = {
            'name': app_name,
        }
        child_menu_vals = [(0, 0, {'name': object_name, 'action': action_ref})]
        menu_values.update({
            'web_icon_data': request.env['ir.attachment'].browse(attachment).datas
        })
        menu_values['child_id'] = child_menu_vals

        new_context = dict(request.context)
        new_context.update(
            {'ir.ui.menu.full_list': True, 'editor': True})
        new_menu = request.env['ir.ui.menu'].with_context(new_context).create(
            menu_values)

        return {
            'menu_id': new_menu.id,
            'action_id': action.id,
        }

    @http.route('/openeducat_web_no_code_studio/create/model', type='json', auth='user')
    def create_view_new_model(self, object_name=None, menu_id=False, new_options=False):
        model, addon_model = self.create_new_model_app(
            object_name, data=new_options)

        action = self.create_web_no_code_studio_default_action(object_name, model)
        action_ref = 'ir.actions.act_window,' + str(action.id)
        menu_values = {
            'name': object_name,
            'parent_id': menu_id,
            'action': action_ref,
        }
        new_context = dict(request.context)
        new_context.update({
            'editor': True
        })
        new_menu = request.env['ir.ui.menu'].with_context(new_context).create(
            menu_values)

        return {
            'menu_id': new_menu.id,
            'action_id': action.id,
        }

    @http.route('/openeducat_web_no_code_studio/create/menu', type='json', auth='user')
    def create_view_new_menu(self, model_type=None, model_id=None, object_name=None,
                             menu_id=False, new_options=False):
        if model_type == 'existing' and model_id:
            model = request.env['ir.model'].browse(model_id)
        else:
            model, addon_model = self.create_new_model_app(
                object_name, data=new_options)

        action = self.create_web_no_code_studio_default_action(object_name, model)
        action_ref = 'ir.actions.act_window,' + str(action.id)
        menu_values = {
            'name': object_name,
            'parent_id': menu_id,
            'action': action_ref,
        }
        new_context = dict(request.context)
        new_context.update({
            'editor': True
        })
        new_menu = request.env['ir.ui.menu'].with_context(new_context).create(
            menu_values)

        return {
            'menu_id': new_menu.id,
            'action_id': action.id,
        }

    def create_web_no_code_studio_default_action(self, name, model):
        model_views = request.env['ir.ui.view'].search_read(
            [('model', '=', model.model), ('type', '!=', 'search')],
            fields=['type'])
        available_view_types = set(map(lambda v: v['type'], model_views))
        order = {'kanban': 0, 'tree': 1, 'form': 2, 'calendar': 3, 'gantt': 4,
                 'map': 5,
                 'pivot': 6, 'graph': 7, 'qweb': 8, 'activity': 9}
        view_types = list(
            sorted(available_view_types, key=lambda vt: order.get(vt, 10)))
        action = request.env['ir.actions.act_window'].create({
            'name': name,
            'res_model': model.model,
            'view_mode': ','.join(view_types),
        })
        return action

    def create_new_model_app(self, object_name, data=False):
        attrs = ['active', 'contact', 'company', 'user']
        name = 'x_' + slugify(object_name.replace(' ', '_')).replace('-', '_')
        addon_model = request.env['ir.model']
        new_model = request.env['ir.model'].create({
            'name': object_name,
            'model': name,
            'is_mail_thread': True if 'chatter' in data else False,
            'is_mail_activity': True if 'chatter' in data else False,
        })
        for attr in attrs:
            if attr in data:
                getattr(self, 'set_editor_' + attr + '_field')(new_model)
        self.create_selection_field_editor(new_model)
        self.create_views(new_model, object_name)
        self.generate_access_rights(new_model)
        return new_model, addon_model

    def create_selection_field_editor(self, model):
        field = request.env['ir.model.fields'].create({
            'name': 'x_state',
            'field_description': 'State',
            'model_id': model.id,
            'ttype': 'selection'
        })
        selection = {'draft': 'Draft', 'progress': 'In Progress', 'done': 'Done'}
        for key, value in selection.items():
            request.env['ir.model.fields.selection'].create({
                'name': value,
                'value': key,
                'field_id': field.id
            })
        request.env['ir.default'].set(model.model, field.name, 'draft', company_id=True)

    def create_views(self, ir_model, name):
        ids = []
        default_view_to_create = ['tree', 'form', 'search', 'kanban', 'calendar',
                                  'pivot']
        ir_ui_view = request.env['ir.ui.view']
        for v in default_view_to_create:
            meta = {
                'name': name + " " + v.capitalize() + " View",
                'type': v,
                'model': ir_model.model,
                'arch_base': self.get_arch_base(
                    name, v, ir_model.field_id, ir_model)
            }
            ids.append(ir_ui_view.create(meta))
        return ids

    def _get_or_create_fields_for_button(self, model, field, button_name):
        model = request.env['ir.model'].search([('model', '=', model)], limit=1)

        button_count_field_name = 'x_%s__%s_count' % \
                                  (field.name, field.model.replace('.', '_'))[:63]
        button_count_field = request.env['ir.model.fields'].search([
            ('name', '=', button_count_field_name), ('model_id', '=', model.id)])
        if not button_count_field:
            compute_function = """
                    results = self.env['%(model)s'].read_group(
                    [('%(field)s', 'in', self.ids)], ['%(field)s'], ['%(field)s'])
                    dic = {}
                    for x in results: dic[x['%(field)s'][0]] = x['%(field)s_count']
                    for record in self: record[
                    '%(count_field)s'] = dic.get(record.id, 0)
                """ % {
                'model': field.model,
                'field': field.name,
                'count_field': button_count_field_name,
            }
            button_count_field = request.env['ir.model.fields'].create({
                'name': button_count_field_name,
                'field_description': '%s count' % field.field_description,
                'model': model.model,
                'model_id': model.id,
                'ttype': 'integer',
                'store': False,
                'compute': compute_function.replace('    ', ''),
            })

        button_action_domain = "[('%s', '=', active_id)]" % (field.name)
        button_action_context = "{'search_default_%s': active_id," \
                                "'default_%s': active_id}" % (field.name, field.name)
        button_action = request.env['ir.actions.act_window'].search([
            ('name', '=', button_name), ('res_model', '=', field.model),
            ('domain', '=', button_action_domain),
            ('context', '=', button_action_context),
        ])
        if not button_action:
            button_action = request.env['ir.actions.act_window'].create({
                'name': button_name,
                'res_model': field.model,
                'view_mode': 'tree,form',
                'domain': button_action_domain,
                'context': button_action_context,
            })

        return button_count_field, button_action

    def create_button_server_action(self, model, field, value):
        action = request.env['ir.actions.server'].create({
            'name': 'Update Via Button for %s' % model.name,
            'state': 'object_write',
            'xml_id': 'update_via_button_%s' % str(uuid.uuid4()),
            'model_id': model.id,
        })
        request.env['ir.server.object.lines'].create({
            'server_id': action.id,
            'col1': field.id,
            'value': value['value'],
            'evaluation_type': 'value',
        })
        return str(action.id)

    def generate_state_fields_sorted(self, model, field):
        selections = request.env['ir.model.fields.selection'].search([
            ('field_id', '=', field.id)])
        selections_sorted = [{'value': s.value, 'name': s.name} for s in selections]
        return selections_sorted

    @http.route('/openeducat_web_no_code_studio/change/email_alias', type='json',
                auth='public')
    def change_email_alias_value(self, model_name, value):
        model = request.env['ir.model'].search([('model', '=', model_name)], limit=1)
        if model:
            email_alias = request.env['mail.alias'].search(
                [('alias_model_id', '=', model.id)], limit=1)
            if email_alias:
                if value:
                    email_alias.alias_name = value
                else:
                    email_alias.unlink()
            else:
                request.env['mail.alias'].create({
                    'alias_model_id': model.id,
                    'alias_name': value,
                })

    def get_arch_base(self, name, view_type, fields, model):
        if view_type == 'form':
            header = E.header()
            # Element('header')
            selection_field = model.field_id.filtered(lambda f: f.name == 'x_state')
            selection_field.tracking = True
            sorted_selection = self.generate_state_fields_sorted(model, selection_field)
            index = 0
            for record in sorted_selection:
                if index < len(sorted_selection) - 1:
                    action = self.create_button_server_action(model, selection_field,
                                                              sorted_selection[
                                                                  index + 1])
                    header.append(E.button(string=sorted_selection[index + 1]['name'],
                                           name=str(action),
                                           type="action",
                                           domain="[['x_state' ,'!=', '" +
                                                  sorted_selection[index][
                                                      'value'] + "']]",
                                           attrs="{'invisible': [('x_state' ,'!=', '" +
                                                 sorted_selection[index][
                                                     'value'] + "')]}"))
                    index += 1

            header.append(E.field(name='x_state', widget='statusbar'))
            form = E.form(header, string=name)
            sheet = E.sheet()
            div = E.div()
            div_button_box = E.div(name="button_box")
            h1 = E.h1()
            field = E.field(name='display_name')
            group_main = E.group()
            sub_group = E.group()
            form.append(sheet)
            sheet.append(div_button_box)
            sheet.append(div)
            div.append(h1)
            h1.append(field)
            data_group = E.group()
            sheet.append(group_main)
            sheet.append(data_group)
            for i in fields:
                if i.state == 'manual' and i.name != 'x_state':
                    f = E.field(name=i.name)
                    if i.name == 'x_active':
                        f = E.field(name=i.name, invisible="1")
                        sheet.append(f)
                    else:
                        sub_group.append(f)
            group_main.append(sub_group)
            group_main.append(data_group)
            if model.is_mail_thread and model.is_mail_activity:
                chat_div = E.div({'class': 'oe_chatter'})
                model.field_id.filtered(
                    lambda f: f.name == 'x_name').tracking = True
                field_msg_follower = E.field(
                    name="message_follower_ids", widget="mail_followers")
                field_msg_ids = E.field(
                    name="message_ids", widget="mail_thread")
                chat_div.append(field_msg_follower)
                chat_div.append(field_msg_ids)
                form.append(chat_div)
            return etree.tostring(form)

        if view_type == 'tree':
            tree = E.tree(string=name)
            for i in fields:
                if i.state == 'manual' and i.name != 'x_active':
                    f = E.field(name=i.name)
                    tree.append(f)
            return etree.tostring(tree)

        if view_type == 'search':
            search = E.search(string=name)
            for i in fields:
                if i.state == 'manual':
                    f = E.field(name=i.name,
                                filter_domain="[('%s','ilike',self)]" % i.name,
                                string=i.field_description)
                    search.append(f)
            search_group = E.group(string="Group By")
            for i in fields:
                if i.state == 'manual':
                    f = E.filter(name=i.name,
                                 context="{'group_by': '%s'}" % i.name,
                                 string=i.field_description)
                    search_group.append(f)
            search.append(search_group)
            return etree.tostring(search)

        if view_type == 'kanban':
            kanban = E.kanban()
            for i in fields:
                if i.state == 'manual':
                    f = E.field(name=i.name)
                    kanban.append(f)
            templates = E.templates()
            t = E.t()
            t.set('t-name', 'kanban-box')
            templates.append(t)
            div = E.div()
            div.set('class', 'oe_kanban_global_click')
            t.append(div)
            div.append(E.field(name='display_name'))
            kanban.append(templates)
            return etree.tostring(kanban)

        if view_type == 'calendar':
            calendar = E.calendar()
            calendar.set('date_start', 'create_date')
            calendar.set('date_stop', 'create_date')
            calendar.set('string', 'Default calendar view for ' + model.model)
            return etree.tostring(calendar)

        if view_type == 'graph':
            graph = E.graph()
            graph.set('string', 'Default calendar view for ' + model.model)
            for i in fields:
                if i.state == 'manual':
                    f = E.field(name=i.name)
                    graph.append(f)
            return etree.tostring(graph)

        if view_type == 'pivot':
            pivot = E.pivot()
            pivot.set('string', 'Default Pivot View for ' + model.model)
            return etree.tostring(pivot)

    def generate_access_rights(self, new_model):
        request.env['ir.model.access'].create({
            'name': new_model.name + ' System Rights',
            'model_id': new_model.id,
            'group_id': request.env.ref('base.group_system').id,
            'perm_read': True,
            'perm_write': True,
            'perm_create': True,
            'perm_unlink': True,
        })
        request.env['ir.model.access'].create({
            'name': new_model.name + ' User Rights',
            'model_id': new_model.id,
            'group_id': request.env.ref('base.group_user').id,
            'perm_read': True,
            'perm_write': True,
            'perm_create': True,
            'perm_unlink': True,
        })

        return True

    def set_editor_active_field(self, model):
        field = request.env['ir.model.fields'].create({
            'name': 'x_active',
            'field_description': 'Active',
            'model_id': model.id,
            'ttype': 'boolean',
            'state': 'manual'
        })
        request.env['ir.default'].set(model.model, field.name, True)
        return True

    def set_editor_company_field(self, model):
        request.env['ir.model.fields'].create({
            'name': 'x_company_id',
            'field_description': 'Company',
            'model_id': model.id,
            'ttype': 'many2one',
            'relation': 'res.company',
            'state': 'manual'
        })
        return True

    def set_editor_user_field(self, model):
        request.env['ir.model.fields'].create({
            'name': 'x_user_id',
            'field_description': 'User',
            'model_id': model.id,
            'ttype': 'many2one',
            'relation': 'res.users',
            'state': 'manual'
        })
        return True

    def set_editor_contact_field(self, model):
        request.env['ir.model.fields'].create({
            'name': 'x_partner_id',
            'field_description': 'Contact',
            'model_id': model.id,
            'ttype': 'many2one',
            'relation': 'res.partner',
            'state': 'manual'
        })
        return True

    @http.route('/state_flow/get/data', type='json', auth='user')
    def state_flow_get_data(self, model):
        views = request.env['ir.ui.view'].search([('model', '=', model),
                                                  ('type', '=', 'form')])
        name, selection = None, []
        ir_model = request.env['ir.model'].search([('model', '=', model)])
        for view in views:
            parser = etree.XMLParser(remove_blank_text=True)
            arch = etree.fromstring(view.arch_base, parser=parser)
            values = arch.findall('.//field')
            for value in values:
                if value.get('widget') == 'statusbar':
                    name = request.env['ir.model.fields'].search([
                        ('name', '=', value.get('name')),
                        ('model_id', '=', ir_model.id)])
                    for select in name.selection_ids:
                        selection.append({
                            'name': select.name,
                            'value': select.value,
                            'id': select.id,
                        })
                    break

        return {
            "selection": selection,
            "name": name.name if name else None
        }

    @http.route('/openeducat_web_no_code_studio/state/name', type='json', auth='user')
    def state_flow_change_state_name(self, selection_id, name, model, data):
        selection = request.env['ir.model.fields.selection'].search([
            ('id', '=', int(selection_id))])
        new_attr, new_attr_inv = None, None
        res = self.get_state_server_action(model, selection.field_id.name,
                                           selection.value)
        params = {
            'type': 'form',
            'model': model,
        }
        view_arch = self.get_web_no_code_studio_view_arch(**params)
        node = {
            'tag': 'Button',
            'attrs': {
                'name': res['server_id'],
            }
        }
        expr = self._node_to_expr(node)
        parser = etree.XMLParser(remove_blank_text=True)
        current_arch = etree.fromstring(view_arch['web_no_code_studio_arch'],
                                        parser=parser)
        view = request.env['ir.ui.view'].search([('model', '=', model),
                                                 ('mode', '=', 'primary'),
                                                 ('type', '=', 'form')])
        new_arch = etree.SubElement(current_arch, 'xpath', {
            'expr': expr,
            'position': 'attributes'
        })
        if data.get('name'):
            new_attr = etree.Element('attribute', name="string")
            new_attr.text = name
            selection.update({
                'name': name,
            })

        if data.get('domain'):
            new_attr = etree.Element('attribute', name="domain")
            new_attr.text = name

            name = name.replace('[', '(').replace(']', ')')
            name = [char for char in name]
            name[0] = '['
            name[len(name) - 1] = ']'
            name = "".join(name)
            new_attr_inv = etree.Element('attribute', name="attrs")
            new_attr_inv.text = "{'invisible': " + name + "}"

        if new_attr is not None:
            new_arch.append(new_attr)
            if new_attr_inv is not None:
                new_arch.append(new_attr_inv)
            current_arch.append(new_arch)
            self.web_no_code_studio_initialize(etree.tostring(current_arch,
                                                              encoding='unicode',
                                                              pretty_print=True), view)

        return True

    @http.route('/openeducat_web_no_code_studio/state/cu', type='json', auth='user')
    def state_flow_create_or_update(self, model, from_state, to_state, field, attrs):
        ir_model = request.env['ir.model'].search([('model', '=', model)])
        ir_field = request.env['ir.model.fields'].search([('name', '=', field),
                                                          ('model_id', '=', ir_model.id)
                                                          ])
        pre_domain = '[["%s","=","%s"]]' % (ir_field.name, from_state)
        post_domain = '[["%s","=","%s"]]' % (ir_field.name, to_state)
        automations = self.check_base_automations_exist(ir_model, pre_domain,
                                                        post_domain, attrs['action'])
        data = {
            'from_state': from_state,
            'to_state': to_state,
        }
        if automations:
            self.update_automated_action(automations, attrs)
        else:
            self.create_automated_action(ir_model, pre_domain, post_domain, data, attrs)

        return True

    def update_automated_action(self, automation, attrs):
        if attrs['action'] == 'email':
            automation.update({
                'template_id': int(attrs['id'])
            })

    def check_base_automations_exist(self, model, pre_domain, post_domain, action):
        automations = request.env['base.automation'].search(
            [('model_id', '=', model.id),
             ('state', '=', action)])
        auto_exist = None
        for automation in automations:
            if str(pre_domain) in automation.filter_pre_domain and str(post_domain) \
                    == automation.filter_domain:
                auto_exist = automation
                break

        return auto_exist

    def create_automated_action(self, model, pre_domain, post_domain, data, attrs):
        # ir_field = request.env['ir.model.fields'].
        # search([('name', '=', attrs['field_name']),
        #                                                   ('model_id', '=',
        #                                                    model.id)
        #                                                   ])
        action = None
        if attrs['action'] == 'on_write':
            action = request.env["base.automation"].create({
                'name': 'Update From %s To %s In %s ' % (data['from_state'],
                                                         data['to_state'],
                                                         model.name),
                "trigger": "on_write",
                "model_id": model.id,
                "type": "ir.actions.server",
                "filter_pre_domain": pre_domain,
                "filter_domain": post_domain,
                # "fields_lines": [(0, 0, {
                #     "col1": ir_field.id,
                #     "evaluation_type": attrs['type'],
                #     "value": attrs['new_value'],
                # })],
            })
        elif attrs['action'] == 'email':
            action = request.env["base.automation"].create({
                'name': 'Update From %s To %s In %s ' % (data['from_state'],
                                                         data['to_state'],
                                                         model.name),
                "state": "email",
                "trigger": "on_write",
                "model_id": model.id,
                "type": "ir.actions.server",
                "filter_pre_domain": pre_domain,
                "filter_domain": post_domain,
                "template_id": attrs['id'] if 'id' in attrs else False,
            })
        return action

    @http.route('/openeducat_web_no_code_studio/get/template', type='json', auth='user')
    def web_no_code_studio_get_state_template_data(
            self, model, field, from_state, to_state, action):
        data = []
        ir_model = request.env['ir.model'].search([('model', '=', model)])
        ir_field = request.env['ir.model.fields'].search([('name', '=', field),
                                                          ('model_id', '=', ir_model.id)
                                                          ])
        pre_domain = '[["%s","=","%s"]]' % (ir_field.name, from_state)
        post_domain = '[["%s","=","%s"]]' % (ir_field.name, to_state)
        automations = self.check_base_automations_exist(ir_model, pre_domain,
                                                        post_domain, action)

        if not automations:
            state_data = {
                'from_state': from_state,
                'to_state': to_state
            }
            attrs = {
                'action': 'on_write' if action == 'object_write' else 'email'
            }
            automations = self.create_automated_action(ir_model, pre_domain,
                                                       post_domain,
                                                       state_data, attrs)
        if automations:
            for automation in automations:
                data_dict = {
                    'id': automation.id,
                    'name': automation.name,
                    'template_id': automation.template_id.id
                    if automation.template_id else None,
                }
                if automation.state == 'object_write':
                    sub_data = []
                    for fields_line in automation.fields_lines:
                        sub_data.append({
                            'field': {
                                'name': fields_line.col1.name,
                                'label': fields_line.col1.field_description,
                                'id': fields_line.col1.id,
                                'ttype': fields_line.col1.ttype,
                                'relation': fields_line.col1.relation,
                            },
                            'type': 'evaluation_type',
                            'value': fields_line.value,
                            'id': fields_line.id,
                        })
                    data_dict.update({'field_data': sub_data})
                data.append(data_dict)
        return data, automations.id

    @http.route('/openeducat_web_no_code_studio/state/update', auth='user', type='json')
    def web_no_code_studio_state_field_update(
            self, automation_id, new_value, field, line_id, relation):
        line = request.env['ir.server.object.lines'].search([('id', '=', int(line_id))])
        if line:
            line.update({
                'value': new_value
            })
        else:
            automation = request.env['base.automation'].browse(int(automation_id))
            field_id = request.env['ir.model.fields'].browse(int(field))
            evolution = 'resource_ref' if field_id.ttype in ['many2one', 'many2many'] \
                else 'value'
            automation.update({
                'fields_lines': [(0, 0, {
                    'col1': field_id.id,
                    'evaluation_type': 'reference'
                    if field_id.ttype in ['many2one', 'many2many'] else 'value',
                    evolution: '%s,%s' % (relation, int(new_value))
                    if evolution == 'resource_ref' else new_value,
                    'value': int(
                        new_value) if evolution == 'resource_ref' else new_value
                })]
            })
        return True

    @http.route('/state/get/server', auth='user', type='json')
    def get_state_server_action(self, model, field, value, form_arch=None):
        res = None
        field_id = request.env['ir.model.fields'].search([('name', '=', field),
                                                          ('model_id.model', '=',
                                                           model)])
        server = request.env['ir.server.object.lines'].search(
            [('col1', '=', field_id.id),
             ('evaluation_type', '=', 'value'),
             ('value', '=', value)])
        views = request.env['ir.ui.view'].search([('model', '=', model),
                                                  ('type', '=', 'form')])

        # new_view = request.env['ir.ui.view'].default_view(request.env[model]._name,
        #                                                   'form')
        # ir_model = request.env['ir.model'].search([('model', '=', model)])

        if form_arch is None:
            for view in views:
                parser = etree.XMLParser(remove_blank_text=True)
                arch = etree.fromstring(view.arch_base, parser=parser)
                values = arch.findall('.//Button')
                for value in values:
                    if value.get('name') == str(server.server_id.id):
                        res = value.get('domain')
        else:
            parser = etree.XMLParser(remove_blank_text=True)
            arch = etree.fromstring(form_arch, parser=parser)
            values = arch.findall('.//Button')
            for value in values:
                if value.get('name') == str(server.server_id.id):
                    res = value.get('domain')

        return {
            'domain': res,
            'server_id': server.server_id.id
        }

    @http.route(['/create/icon'],
                type='json', auth="user", website=True)
    def create_attachment(self, file, file_name):
        res = file.encode('utf-8')
        attachment = request.env['ir.attachment'].sudo().create({
            'name': file_name,
            'public': True,
            'mimetype': 'image/svg+xml',
            'datas': res,
        })
        return attachment.id
