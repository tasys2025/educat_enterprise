from odoo import models, api


class IrModelField(models.Model):
    _inherit = 'ir.model.fields'

    @api.model
    def _get_next_relation(self, model_name, comodel_name):
        res = super()._custom_many2many_names(model_name, comodel_name)[0]
        base = res
        count = 0
        prev_m2m = self.search([
            ('model', '=', model_name),
            ('relation', '=', comodel_name),
            ('relation_table', '=', res)
        ])
        while prev_m2m:
            count += 1
            res = '%s_%s' % (base, count)
            prev_m2m = self.search([
                ('model', '=', model_name),
                ('relation', '=', comodel_name),
                ('relation_table', '=', res)
            ])
        return res
