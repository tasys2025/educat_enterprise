/** @odoo-module */

import { useService } from "@web/core/utils/hooks";
import { ComponentAdapter } from "web.OwlCompatibility";
const { Component } = owl;
const { xml, onWillUpdateProps, onPatched, onMounted } = owl;
import Widget from "web.Widget";
import EditTab from "openeducat_web_no_code_studio.EditTab";
import bus from "openeducat_web_no_code_studio.bus";
import CreateNewModel from "openeducat_web_no_code_studio.CreateNewModel";
var core = require('web.core');

const MenuWrapper = Widget.extend({
    template: 'studio.menu.wrapper',
    events: {
        'click .menu_edit_tab': '_onEditMenu',
        'click .menu_create_model': '_onCreateNewModel',
    },
    init: function(parent, menu_data, primary_menu){
        this._super.apply(this, arguments);
        this.parent = parent;
        this.menu_data = menu_data;
        this.primary_menu = primary_menu;
    },

    start: function(){
        this._super.apply(this, arguments);
    },

    _onEditMenu: function(){
        new EditTab(this, this.menu_data, this.primary_menu).open();
    },

    _onCreateNewModel: function(){
        core.bus.trigger_up('web_create_new_model')
        //new CreateNewModel(this, this.primary_menu).open();
    }
});

export default MenuWrapper;

class EditMenuItemAdapter extends ComponentAdapter {
    constructor(props) {
        props.Component = MenuWrapper;
        super(...arguments);
    }

    setup() {
        super.setup();
        this.menus = useService("menu");
        this.env = Component.env;
        onMounted(() => {
            if (this.props.keepOpen) {
                this.widget.editMenu(this.props.scrollToBottom);
            }
        });
    }

    get currentMenuId() {
        return this.menus.getCurrentApp().id;
    }

    get legacyMenuData() {
        return this.menus.getMenuAsTree(this.currentMenuId);
    }

    get widgetArgs() {
        return [this.legacyMenuData, this.currentMenuId];
    }
    mounted() {
        super.mounted(...arguments);
        if (this.props.keepOpen) {
            this.widget.editMenu(this.props.scrollToBottom);
        }
    }
    updateWidget() {}
    renderWidget() {}
}


export class EditMenuItem extends Component {
    setup() {
        this.menus = useService("menu");
        this.localId = 0;
        this.editMenuParams = {};

        onWillUpdateProps(() => {
            this.localId++;
        });
        onPatched(() => {
            this.editMenuParams = {};
        });
    }
    reloadMenuData(ev) {
        const { keep_open, scroll_to_bottom } = ev.data;
        this.editMenuParams = { keepOpen: keep_open, scrollToBottom: scroll_to_bottom };
        this.menus.reload();
    }
}
EditMenuItem.components = { EditMenuItemAdapter };
EditMenuItem.template = xml`
  <t>
    <div t-if="!menus.getCurrentApp()"/>
    <EditMenuItemAdapter t-else="" t-props="editMenuParams" t-key="localId" t-on-reload-menu-data="reloadMenuData" />
  </t>
`;