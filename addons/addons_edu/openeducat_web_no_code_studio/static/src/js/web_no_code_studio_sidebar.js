odoo.define('openeducat_web_no_code_studio.ViewEditorSideBar', function(require){
    'use strict';

    var Widget = require('web.Widget');
    var DraggableElements = require('openeducat_web_no_code_studio.Elements');
    var config = require('web.config');
    var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
    var Many2ManyTags = require('web.relational_fields').FieldMany2ManyTags;
    var field_registry = require('web.field_registry');
    var fieldRegistryOwl = require('web.field_registry_owl');
    var DomainSelectorDialog = require('web.DomainSelectorDialog');
    var utils = require('web.utils');
    var Domain = require("web.Domain");
    var pyUtils = require('web.py_utils');
    var core = require('web.core');
    var _t = core._t;

    var ViewEditorSideBar = Widget.extend(StandaloneFieldManagerMixin, {
        template: 'openeducat_web_no_code_studio.side_bar',
        events: {
            'click .web_no_code_studio_top_bar > div': '_onSideBarTab',
            'change .web_no_code_studio_field_attributes [data-type="attributes"]': '_onFieldAttribute',
            'change .web_no_code_studio_field_attributes [data-type="default_value"]': '_defaultValueChange',
            'click .web_no_code_studio_field_remove': '_onRemoveField',
            'change .view_sidebar_editor input[name!="show_invisible"]': '_onViewAttribute',
            'change .view_sidebar_editor select': '_onViewAttribute',
            'change .view_sidebar_editor input[name="show_invisible"]': '_onViewInvisible',
            'click .web_no_code_studio_domain': '_onDomainAttrs',
            'click .web_no_code_studio_edit_selection_values': '_onClickSelection',
            'keyup  #search-box':'_searchBox',
        },
        init: function(parent, params){
            this._super.apply(this, arguments);
            var self = this;
            StandaloneFieldManagerMixin.init.call(this);
            this.view_type = params.view_type;
            this.model_name = params.fields;
            this.renamingAllowedFields = params.state;
            this.isRelationField = params.editorData;
            this.fieldsInfo = params.defaultOrder;
            this.fields = params.fields;
            this.state = params.state;
            this.searchAlive = false;
            this.editorData = params.editorData;
            this.defaultOrder = params.defaultOrder;
            this.fields_included = params.fields_included;
            this.fields_excluded = params.fields_excluded;
            this.state = params.state ? params.state : {};
            if (this.state.node && ['group', 'page', 'field', 'filter'].includes(this.state.node.tag)) {
                this.state.modifiers = this.state.attrs.modifiers || {};
            }

            if (this.state.node && ['field', 'filter'].includes(this.state.node.tag)) {
                var field = jQuery.extend(true, {}, this.fields[this.state.attrs.name]);

                const fieldRegistryMap = Object.assign({}, field_registry.map, fieldRegistryOwl.map);
                field.field_widgets = _.chain(fieldRegistryMap).pairs().filter(function (arr) {
                    const supportedFieldTypes = utils.isComponent(arr[1]) ?
                        arr[1].supportedFieldTypes :
                        arr[1].prototype.supportedFieldTypes;
                    const description = self._getFieldDetails(arr[1], 'description');
                    const isWidgetKeyDescription = arr[0] === self.widgetKey && !description;
                    var isSupported = _.contains(supportedFieldTypes, field.type)
                        && arr[0].indexOf('.') < 0;
                    return config.isDebug() ? isSupported : isSupported && description || isWidgetKeyDescription;
                }).value();

                this.state.field = field;
                this._generateFieldProps();
            }
        },

        start: function(){
            this._super.apply(this, arguments);
            this._afterRender();
        },

        _onSideBarTab: function(e){
            var $target = $(e.currentTarget);
            this.trigger_up('on_change_sidebar_tab', {
                mode: $target.data('mode'),
            });
        },

        _afterRender: function(){
            this.def = [];
            if(this.state.mode === 'field'){
                if(this.searchAlive == false){
                    if(this.view_type == 'search' || this.view_type == 'form'){
                        this._renderSubSection('element');
                    }
                    if(this.view_type == 'form' || this.view_type == 'list'){
                        this._renderSubSection('newField');
                    }
                    this._renderExistingFields();
                    return Promise.all(this.def).then(() => {
                        delete(this.def);
                        this.$('.web_no_code_studio_draggable').on("drag", _.throttle((event, ui) => {
                            this.trigger_up('element_drag', {position: {pageX: event.pageX, pageY: event.pageY}, $helper: ui.helper});
                        }, 200));
                    });
                }
            }else if(this.state.mode === "attributes"){
                if (this.$('.group_web_no_code_studio').length) {
                    this.def.push(this._createGroupsSelection());
                }
                return Promise.all(this.def).then(() => {
                    delete(this.def);
                });
            }
        },

        _generateFieldProps: function(){
            this.state.attrs.invisible = this.state.modifiers.invisible || this.state.modifiers.column_invisible;
            this.state.attrs.readonly = this.state.modifiers.readonly;
            this.state.attrs.string = this.state.attrs.string || this.state.field.string;
            this.state.attrs.help = this.state.attrs.help || this.state.field.help;
            this.state.attrs.placeholder = this.state.attrs.placeholder || this.state.field.placeholder;
            this.state.attrs.required = this.state.field.required || this.state.modifiers.required;
            this.state.attrs.domain = this.state.attrs.domain || this.state.field.domain;
            this.state.attrs.context = this.state.attrs.context || this.state.field.context;
            this.state.attrs.related = this.state.field.related ? this.state.field.related.join('.'): false;
        },

        _onViewInvisible: function(e){
            var $target = $(e.currentTarget),
                newAttrs = {},
                attr = $target.attr('name');

            if(attr){
                if ($target.attr('type') === 'checkbox') {
                    if (($target.is(':checked') && !$target.data('inverse')) || (!$target.is(':checked') && $target.data('inverse'))) {
                        newAttrs[attr] = $target.data('leave-empty') === 'checked' ? '': 'true';
                    } else {
                        newAttrs[attr] = $target.data('leave-empty') === 'unchecked' ? '': 'false';
                    }
                } else {
                    newAttrs[attr] = $target.val();
                }
                this.trigger_up('data_invisible', {
                  'show_invisible': newAttrs[attr],
                });
            }

        },

        _searchBox: function(){
            var input, filter, ul, li, a, i, txtValue;
            input = document.getElementById("search-box");
            filter = input.value.toUpperCase();
            ul = document.getElementsByClassName("web_no_code_studio_existing_field")[0];
            li = ul.getElementsByTagName("div");
            for (i = 0; i < li.length; i++) {
                a = li[i]
                txtValue = a.textContent || a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        },

        _renderSubSection: function(type){
            var typeClass = type === 'element' ? this.view_type + 'Element' : 'newField',
                title = type === 'element' ? 'Elements' : 'New Field',
                toAddClass = type === 'element' ? 'web_no_code_studio_new_element' : 'web_no_code_studio_new_field',
                self = this,
                classes = DraggableElements.get(typeClass);

            var $title = $("<h6 class='text-black'> "+ title + " </h6>");
            var $elementSection = $("<div class='mb-3 web_no_code_studio_field_section " + toAddClass + "' />");
            var widgets = classes.map(Element => new Element(this));

            widgets.forEach(function (widget){
                self.def.push(widget.appendTo($elementSection));
            });
            this.$('.side_bar_main_data').append($title, $elementSection);
        },

        _onFieldChanged: async function (ev) {
            const res = await StandaloneFieldManagerMixin._onFieldChanged.apply(this, arguments);
            this._changeGroups();
            return res;
        },

        _renderExistingFields: function(){
            var $alreadyFields = this.$('.web_no_code_studio_existing_field'),
                fields,
                self = this,
                widgets,
                Element = DraggableElements.get('existingField');
            if ($alreadyFields.length) {
                $alreadyFields.remove();
            }

            //TODO Restructure This
            if(this.view_type != 'search'){
                fields = _.sortBy(this.fields_excluded, function (field) {
                    return field.string.toLowerCase();
                });
                widgets = fields.map( field =>
                    new Element(this, field.name, field.string, field.type)
                );
            }else{
                widgets = Object.values(this.fields).map(field =>
                    new Element(this, field.name, field.string, field.type, field.store)
                );
            }

            var $elementSection = $("<div class='pb-2'><input type='search' id='search-box'  name='field_search' placeholder='search...' class='w-100 form-control' /></div><div class='web_no_code_studio_field_section web_no_code_studio_existing_field' />");
            widgets.forEach(function (widget){
                self.def.push(widget.appendTo($elementSection));
            });

            if($alreadyFields.length){
                this.$('.side_bar_main_data').append($elementSection);
            }else{
                var $title = $("<h6 class='mt-3 text-black'>Existing Field </h6>");
                this.$('.side_bar_main_data').append($title, $elementSection);
            }
        },

        _onClickSelection: function(ev){
            ev.preventDefault();
            this.trigger_up('field_edition', {
                node: this.state.node,
            });
        },

        _getFieldDetails: function(fieldType, propName) {
            return utils.isComponent(fieldType) ?
                (fieldType.hasOwnProperty(propName) && fieldType[propName]) :
                (fieldType.prototype.hasOwnProperty(propName) && fieldType.prototype[propName]);
        },

        _changeGroups: function(){
            var record = this.model.get(this.groupsHandle);
            var newAttrs = {};
            newAttrs.groups = record.data.groups.res_ids;
            this.trigger_up('update_view', {
                type: 'attributes',
                structure: 'attribute_changed',
                node: this.state.node,
                newAttrs: newAttrs,
            });
        },

        _defaultValueChange: function(ev){
            var $target = $(ev.currentTarget);
            if($target.val() !== this.state.field.default_value){
                this.trigger_up('default_val_changed',{
                    field_name: this.state.attrs.name,
                    value: $target.val(),
                });
            }
        },

        _onDomainAttrs: function(e){
            e.preventDefault();
            var property = $(e.currentTarget).data('property');

            var allFields = this.fields_included;
            if (!allFields.id) {
                allFields = _.extend({
                    id: {
                        searchable: true,
                        string: "ID",
                        type: "integer",
                    },
                }, allFields);
            }
            var domainDialog = new DomainSelectorDialog(this, this.model_name, _.isArray(this.state.modifiers[property]) ? this.state.modifiers[property] : [], {
                readonly: false,
                fields: allFields,
                size: 'medium',
                operators: ["=", "!=", "<", ">", "<=", ">=", "in", "not in", "set", "not set"],
                followRelations: false,
                debugMode: config.isDebug(),
            }).open();

            domainDialog.on("domain_selected", this, function (e) {
                var mods = _.extend({}, this.state.modifiers);
                mods[property] = e.data.domain;
                var newAttrs = this._getNewAttrsFromModifiers(mods);
                this.trigger_up('update_view', {
                    type: 'attributes',
                    structure: 'attribute_changed',
                    node: this.state.node,
                    newAttrs: newAttrs,
                });
            });
        },

        _getNewAttrsFromModifiers: function (modifiers) {
            var self = this;
            var newAttributes = {};
            var attrs = [];
            var originNodeAttr = this.state.modifiers;
            var originSubAttrs =  pyUtils.py_eval(this.state.attrs.attrs || '{}', this.editorData);
            _.each(modifiers, function (value, key) {
                    var keyInNodeAndAttrs = _.contains(['readonly', 'invisible', 'required'], key);
                    var keyFromView = key in originSubAttrs;
                    var trueValue = value === true || _.isEqual(value, []);
                    var isOriginNodeAttr = key in originNodeAttr;

                    if (keyInNodeAndAttrs && !isOriginNodeAttr && trueValue) {
                        newAttributes[key] = "1";
                    } else if (keyFromView || !trueValue) {
                        newAttributes[key] = "";
                        if (value !== false) {
                            attrs.push(_.str.sprintf("\"%s\": %s", key, Domain.prototype.arrayToString(value)));
                        }
                    }
            });
            newAttributes.attrs = _.str.sprintf("{%s}", attrs.join(", "));
            return newAttributes;
        },

        _createGroupsSelection: function(){
            var self = this;
            var editor_group = this.state.attrs.editor_group && JSON.parse(this.state.attrs.editor_group);
            return this.model.makeRecord('ir.model.fields', [{
                name: 'groups',
                fields: [{
                    name: 'id',
                    type: 'integer',
                }, {
                    name: 'display_name',
                    type: 'char',
                }],
                relation: 'res.groups',
                type: 'many2many',
                value: editor_group,
            }]).then(function (recordID) {
                self.groupsHandle = recordID;
                var record = self.model.get(self.groupsHandle);
                var options = {
                    idForLabel: 'groups',
                    mode: 'edit',
                    no_quick_create: true,
                };
                var many2many = new Many2ManyTags(self, 'groups', record, options);
                many2many.className = many2many.className + ' col';
                self._registerWidget(self.groupsHandle, 'groups', many2many);
                return many2many.appendTo(self.$('.group_web_no_code_studio'));
            });
        },

        _onRemoveField: function(){
            var self = this;
            var name = this.state.node.tag;
            if(name == 'div' && this.state.node.attrs.class === 'oe_chatter'){
                name = 'chatter';
            }
            this.trigger_up('update_view', {
                type: 'remove',
                structure: 'remove',
                node: this.state.node,
            });
        },

        _onFieldAttribute: function(e){
            var $target = $(e.currentTarget),
                newAttrs = {},
                attr = $target.attr('name');
            if($target.attr('type') == 'checkbox'){
                if(!(['readonly', 'invisible', 'required'].includes(attr))){
                    if ($target.is(':checked')) {
                        newAttrs[attr] = $target.data('leave-empty') === 'checked' ? '': 'True';
                    } else {
                        newAttrs[attr] = $target.data('leave-empty') === 'unchecked' ? '': 'False';
                    }
                }else{
                    var newMods = _.extend({}, this.state.modifiers);
                    newMods[attr] = $target.is(':checked');
                    newAttrs = this._formatAttributes(newMods);
                }
            }else{
                newAttrs[attr] = $target.val();
            }
            this.trigger_up('update_view', {
                type: 'attributes',
                structure: 'attribute_changed',
                node: this.state.node,
                newAttrs: newAttrs,
            });
        },

        _onViewAttribute: function(e){
            var $target = $(e.currentTarget),
                newAttrs = {},
                attr = $target.attr('name');

            if(attr){
                if ($target.attr('type') === 'checkbox') {
                    if (($target.is(':checked') && !$target.data('inverse')) || (!$target.is(':checked') && $target.data('inverse'))) {
                        newAttrs[attr] = $target.data('leave-empty') === 'checked' ? '': 'true';
                    } else {
                        newAttrs[attr] = $target.data('leave-empty') === 'unchecked' ? '': 'false';
                    }
                } else {
                    newAttrs[attr] = $target.val();
                }
                this.trigger_up('update_view', {
                    type: 'attributes',
                    structure: 'view_changed',
                    newAttrs: newAttrs,
                });
            }
        },

        _formatAttributes: function(attrs){
            for(var attr in attrs){
                attrs[attr] = attrs[attr] ? "1" : "0";
            }
            return attrs;
        },

    });

    return ViewEditorSideBar;

});