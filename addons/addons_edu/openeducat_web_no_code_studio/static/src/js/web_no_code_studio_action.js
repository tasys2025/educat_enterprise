odoo.define('openeducat_web_no_code_studio.web_no_code_studio_action', function(require){
    'use state';

    var AbstractAction = require("web.AbstractAction");
    var { action_registry } = require('web.core');
    var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
    var { ComponentAdapter, ComponentWrapper, WidgetAdapterMixin } = require('web.OwlCompatibility');
    var AbstractViewEditorManager = require('openeducat_web_no_code_studio.AbstractViewEditorManager')
    var ViewsAllAction = require('openeducat_web_no_code_studio.ViewsAllAction');
    var ViewsReport = require('openeducat_web_no_code_studio.ViewsReport');
    var core = require('web.core');
    var ajax = require('web.ajax');
    var dom = require('web.dom');
    var session = require('web.session');
    var _t = core._t;
    var _lt = core._lt;
    var bus = require('openeducat_web_no_code_studio.bus');

    var ViewEditorAction = AbstractAction.extend(WidgetAdapterMixin, StandaloneFieldManagerMixin,{
        custom_events: {
            'editor_edit_view': '_onEditView',
            'reset_reports_view': '_onResetReportsView',
        },
        init: function (parent, context, options) {
            this._super.apply(this, arguments);
            this.wowlEnv = parent.wowlEnv;
            context.context.viewEditor = true;
            this.options = context.context;
            this.action = context.context.action;
            this.controllerState = context.context.controller;
            delete context.context.action
            delete context.context.controller
            var records = this.action.views ? this.action.views.slice() : this.action.context.action.views.slice();
            this.views = records;
            var search_view_id = this.action.search_view_id && this.action.search_view_id[0];
            this.views.push([search_view_id || false, 'search']);
            this.viewType = this.options.viewType
            var view = _.find(this.views, function (descr) {
                    return descr[1] === this.viewType;
            }) || [false, this.viewType];
            this.view_id = view[0];
            this.viewType = view[1];
        },

        start: function(){
            var res = this._super.apply(this, arguments),
                def = this.viewType ? this._prepareViewEditor() : this._prepareAllViews(),
                self = this,
                allDef = Promise.all([def, res]);

            allDef.then( function(){
                self._changeStateEditor();
                core.bus.trigger('web_no_code_studio_menu', self.action);
                if (!self.options.noEdit) {
                    core.bus.trigger('editor_mode_started', self.viewType);
                }
            });
        },

        _onEditView: function(e){
            var self = this;
            var type = e.data.type;
            if(type != 'report'){
                this.views = (this.action._views ? this.action._views : this.action.views).slice();
                var search_view_id = this.action.search_view_id && this.action.search_view_id[0];
                this.views.push([search_view_id || false, 'search']);

                var view = _.find(this.views, function (descr) {
                        return descr[1] === type;
                }) || [false, type];
                this.view_id = view[0];
                this.viewType = view[1];
                this._prepareViewEditor().then( function(){
                    core.bus.trigger('editor_mode_started', self.viewType);
                });
            }
            else{
                this._prepareViewEditor().then( function(){
                    core.bus.trigger('editor_mode_started', 'report');
                });
            }

        },

        _prepareAllViews: function(){
            this._getReportArch();
            if (this.action.viewType == 'views'){
                this.all_action = new ViewsAllAction(this, this.action);
                this.all_action.appendTo(this.$('.o_content'));
            }else{
                 var self = this
                 this.trigger_up('update_view', {
                    type: 'attributes',
                    execute: 'store',
                    structure: 'attribute_changed',
                    node: {
                        'attrs': this.attrs,
                        'tag':'field'
                    },
                });
                //this._getReportViewArch();
                 ajax.jsonRpc('/report/get/data', 'call', {
                    model : this.action.res_model,
                    }).then( function(response){
                        self.report = response
                        if(self.all_action){
                            self.all_action.$el.remove();
                            self.all_action.destroy();
                        }
                        self.all_action = new ViewsReport(self, self.action,self.viewEditorArch)
                        self.all_action.appendTo(self.$('.o_content'));
                    });
            }

        },

        _onResetReportsView: function(){
            this._prepareAllViews();
        },

        _getReportViewArch: function(){
            return ajax.jsonRpc('/get/report/arch', 'call', {
                record_id: this.env.currentId,
                report_name: this.reportName,
                context: session.user_context,
            }).then( function(res){
            });
        },

        _getReportArch: function (){
            var self = this;
            var model = this.action.res_model;
            var type = 'qweb'
            var id = false
            var context = {}
            core.bus.trigger('clear_cache');
            context = _.extend({}, session.user_context, {lang: false});
            return ajax.jsonRpc('/openeducat_web_no_code_studio/get/view_arch', 'call',{
                model, type, id, context
            }).then( function(response){
                self.viewEditorArch = response;
            });
        },


        on_attach_callback: function () {
            this._super.apply(this, arguments);
            this.isInDOM = true;
            if (this.openeducat_web_no_code_studio) {
                this.openeducat_web_no_code_studio.on_attach_callback();
            }
        },

        _changeStateEditor: function(){
            var newState = {
                action: this.action.id,
                model: this.action.res_model,
                view_type: this.viewType,
            };
            this.trigger_up('push_state', {
                state: newState,
            });
        },

        on_detach_callback: function () {
            this._super.apply(this, arguments);
            this.isInDOM = false;
            if (this.openeducat_web_no_code_studio) {
                this.openeducat_web_no_code_studio.on_detach_callback();
            }
        },

        _prepareViewEditor: function(){
            var context = _.extend({}, this.action.context);
            return this._loadSubViews(context);
        },

        _getViewEditorArch: function(model, type, id){
            var self = this;
            core.bus.trigger('clear_cache');
            context = _.extend({}, session.user_context, {lang: false});
            return ajax.jsonRpc('/openeducat_web_no_code_studio/get/view_arch', 'call',{
                model, type, id, context
            }).then( function(response){
                self.viewEditorArch = response;
            });
        },

        _loadSubViews: function(context){
            var def,
                self = this;
            var model_data =  this.action.res_model ? this.action.res_model : this.action.context.action.res_model;
            def = [this._getViewEditorArch(model_data, this.viewType, this.view_id)];
            var context = _.extend({}, self.action.context, {editor: true, lang: false});
//            const loadViewDef = self.wowlEnv.services.view.loadViews(
//                { context, resModel, views },
//                { actionId, loadActionMenus, loadIrFilters }
//            );
            return Promise.all(def).then(function () {
                return self.loadViews(self.action.res_model, context, self.views).then( function(data){
                    var options = {
                        action: self.action,
                        fieldsView: data[self.viewType],
                        viewType: self.viewType,
                        //chatter_allowed: self.chatter_allowed,
                        web_no_code_studio_arch_id: self.viewEditorArch.web_no_code_studio_arch_id,
                        web_no_code_studio_arch: self.viewEditorArch.web_no_code_studio_arch,
                        //x2mEditorPath: self.x2mEditorPath,
                        x2mEditorPath: self.x2mEditorPath,
                        controllerState: self.controllerState,
                        wowlEnv: self.wowlEnv,
                    };
                    self.openeducat_web_no_code_studio = new AbstractViewEditorManager(self, options);

                    var fragment = document.createDocumentFragment();
                    return self.openeducat_web_no_code_studio.appendTo(fragment).then(function () {
                        // TODO:??? check action editor
                        if (self.all_action) {
                            dom.detach([{widget: self.all_action}]);
                        }
                        dom.append(self.$('.o_content'), [fragment], {
                            in_DOM: self.isInDOM,
                            callbacks: [{widget: self.openeducat_web_no_code_studio}],
                        });
                    });
                });
            });
        },
    });

    //action_registry.add('web_no_code_studio_action', ViewEditorAction);
    return ViewEditorAction;

});
