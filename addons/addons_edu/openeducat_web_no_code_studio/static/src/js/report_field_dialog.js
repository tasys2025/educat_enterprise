odoo.define('openeducat_web_no_code_studio.ReportFieldDialog', function(require){
    'use strict';

    var config = require('web.config');
    var core = require('web.core');
    var Dialog = require('web.Dialog');
    var relational_fields = require('web.relational_fields');
    var ModelFieldSelector = require('web.ModelFieldSelector');
    var Domain = require("web.Domain");
    var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');

    var _t = core._t;
    var qweb = core.qweb;
    var Many2one = relational_fields.FieldMany2One;

    var ReportFieldDialog = Dialog.extend(StandaloneFieldManagerMixin, {
        template: 'openeducat_web_no_code_studio.ReportFieldDialog',
        events: {
            'keyup .web_no_code_studio_selection_new_value > input': '_onAddValue',
            'click .web_no_code_studio_add_selection_value': '_onAddValue',
            'click .web_no_code_studio_remove_selection_value': '_onRemoveValue',
            'click .web_no_code_studio_clear_selection_value': '_onClearValue',
        },
        init: function(parent, model_name, field, fields, field_chain){
            this.model_name = model_name;
            this.type = 'many2one';
            var currentDomain = Domain.prototype.stringToArray(parent.action.domain);
            this.chain = currentDomain;
//            this.field = field;
//            this.fieldChain = field_chain || [];
//            this.order = field.order;
//            this.followRelations = field.followRelations || function (field) {return true;};
//            this.filter = field.filter || function (field) {return true;};
//            this.filters = field.filters;

//            if (this.type === 'selection') {
//                this.selection = this.field.selection && this.field.selection.slice() || [];
//            }

            this.fields = fields;
            var options = _.extend({
                title: _t('Field Properties'),
                size: 'small',
                buttons: [{
                    text: _t("Confirm"),
                    classes: 'btn-primary',
                    click: this._onSave.bind(this),
                }, {
                    text: _t("Cancel"),
                    close: true,
                }],
            }, options);
            this._super(parent, options);
            StandaloneFieldManagerMixin.init.call(this);
        },

        start: function(){
            var self = this,
                def = [this._super.apply(this, arguments)],
                record,
                options = {
                    mode: 'edit',
                };
            if ((['many2many', 'many2one']).includes(this.type)) {
                def.push(this.model.makeRecord('ir.model', [{
                    name: 'model',
                    relation: 'ir.model',
                    type: 'many2one',
                    domain: [['transient', '=', false]]
                }]).then(function (recordID) {
                    record = self.model.get(recordID);
                    self.many2one_model = new Many2one(self, 'model', record, options);
                    self._registerWidget(recordID, 'model', self.many2one_model);
                    self.many2one_model.nodeOptions.no_create_edit = !config.isDebug();
                    self.many2one_model.appendTo(self.$('.o_many2one_model'));
                }));
            } else if (this.type === 'one2many') {
                def.push(this.model.makeRecord('ir.model.fields', [{
                    name: 'field',
                    relation: 'ir.model.fields',
                    type: 'many2one',
                    domain: [['relation', '=', this.model_name], ['ttype', '=', 'many2one']],
                }], {
                    'field': {
                        can_create: false,
                    }
                }).then(function (recordID) {
                    record = self.model.get(recordID);
                    self.many2one_field = new Many2one(self, 'field', record, options);
                    self._registerWidget(recordID, 'field', self.many2one_field);
                    self.many2one_field.nodeOptions.no_create_edit = !config.isDebug();
                    self.many2one_field.appendTo(self.$('.o_many2one_field'));
                }));
            } else if(this.type === 'selection'){
                this.opened().then(function () {
                    self.$('.web_no_code_studio_selection_new_value > input').focus();
                });
            } else if(this.type === 'related'){
                this.$el.css("overflow", "visible").closest(".modal-dialog").css("height", "auto");
                var field_options = {
                    order: this.order,
                    filter: this.filter,
                    followRelations: this.followRelations,
                    fields: this.fields,
                    readonly: false,
                    filters: _.extend({}, this.filters, {searchable: false}),
                };
                this.fieldSelector = new ModelFieldSelector(
                    this,
                    'sale.order',
                    this.chain !== undefined ? this.chain.toString().split(".") : [],
                    field_options
                );
                def.push(this.fieldSelector.appendTo(this.$('.o_many2one_field')));
            }
            return Promise.all(def);
        },

        _onAddValue: function(ev){
            if (ev.type === "keyup" && ev.which !== $.ui.keyCode.ENTER) { return; }

            var $input = this.$(".web_no_code_studio_selection_new_value input");
            var string = $input.val().trim();

            if (string && !_.find(this.selection, function(el) {return el[1] === string; })) {
                this.selection.push([string, string]);
            }
            this.renderElement();
            this.$('.web_no_code_studio_selection_new_value > input').focus();
        },

        _onRemoveValue: function(e){
            var val = $(e.target).closest('li')[0].dataset.value;
            var element = _.find(this.selection, function(el) {return el[0] === val; });
            var index = this.selection.indexOf(element);
            if (index >= 0) {
                this.selection.splice(index, 1);
            }
            this.renderElement();
        },

        _onClearValue: function(){
            this.$('.web_no_code_studio_selection_input').val("").focus();
        },

        _onSave: function () {
            var values = {};
            if (this.type === 'one2many') {
                if (!this.many2one_field.value) {
                    this.trigger_up('warning', {title: _t('You must select a related field')});
                    return;
                }
                values.relation_field_id = this.many2one_field.value.res_id;
            } else if (_.contains(['many2many', 'many2one'], this.type)) {
                if (!this.many2one_model.value) {
                    this.trigger_up('warning', {title: _t('You must select a relation')});
                    return;
                }
                values.relation_id = this.many2one_model.value.res_id;
                values.field_description = this.many2one_model.m2o_value;
            } else if (this.type === 'selection') {
                var newSelection = this.$('.web_no_code_studio_selection_new_value > input').val();
                if (newSelection) {
                    this.selection.push([newSelection, newSelection]);
                }
                values.selection = JSON.stringify(this.selection);
            } else if (this.type === 'related') {
                var selectedField = this.fieldSelector.getSelectedField();
                if (!selectedField) {
                    this.trigger_up('warning', {title: _t('You must select a related field')});
                    return;
                }
                values.string = selectedField.string;
                values.model = selectedField.model;
                values.related = this.fieldSelector.chain.join('.');
                values.type = selectedField.type;
                values.readonly = true;
                values.copy = false;
                values.store = selectedField.store;
                if (_.contains(['many2one', 'many2many'], selectedField.type)) {
                    values.relation = selectedField.relation;
                } else if (selectedField.type === 'one2many') {
                    values.relational_model = selectedField.model;
                } else if (selectedField.type === 'selection') {
                    values.selection = selectedField.selection;
                } else if (selectedField.type === 'monetary') {
                    var currencyField = _.find(_.last(this.fieldSelector.pages), function (el) {
                        return el.name === 'currency_id' || el.name === 'x_currency_id';
                    });
                    if (currencyField) {
                        var chain = this.fieldSelector.chain.slice();
                        chain.splice(chain.length - 1, 1, currencyField.name);
                        values._currency = chain.join('.');
                    }
                }

                if (_.contains(['one2many', 'many2many'], selectedField.type)) {
                    values.store = false;
                }
            }
            this.trigger('field_default_values_saved', values);
        },
    });
    return ReportFieldDialog;
});