odoo.define('openeducat_web_no_code_studio.systray_menu', function(require){
    'use strict';
    var SystrayMenu = require('web.SystrayMenu');
    var Widget = require('web.Widget');
    var session = require('web.session');
    var bus = require('openeducat_web_no_code_studio.bus');
    var core = require('web.core');

    var ViewEditorSystrayMenu = Widget.extend({
        template: 'web_no_code_studio_systray_menu',

        events: {
            'click': '_onViewEditorToggle'
        },

        init: function(){
            this._super.apply(this, arguments);
            var state = $.bbq.getState();
            if((state.hasOwnProperty('home') && state.home == 'apps') || (Object.keys(state).length === 1 && Object.keys(state)[0] === "cids")){
                this.activate = 'create_app';
            }else if(state.model == 'res.config.settings'){
                this.activate = false;
            }else{
                this.activate = true;
            }
        },

        _activateToggle: function(active){
            this.activate = active;
            this.renderElement();
        },

        // On Clicking The Toggle For View Editor And Triggers In Web Client
        _onViewEditorToggle: function(e){
            core.bus.trigger('click_web_no_code_studio', true);
        },
    });

//    session.user_has_group('openeducat_web_no_code_studio.group_editor_web_no_code_studio').then( function(res){
//        if(res){
            SystrayMenu.Items.push(ViewEditorSystrayMenu);
//            ViewEditorSystrayMenu.template = 'web_no_code_studio_systray_menu';
//        }
//    });

    return ViewEditorSystrayMenu;
})