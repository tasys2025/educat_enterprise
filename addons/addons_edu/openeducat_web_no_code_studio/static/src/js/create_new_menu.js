odoo.define('openeducat_web_no_code_studio.CreateNewMenu', function(require){
    'use strict';

    var AbstractAction = require('web.AbstractAction');
    var ajax = require('web.ajax');
    var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
    var core = require('web.core');
    var { FieldMany2One } = require('web.relational_fields');
    const { useAutofocus, useListener } = require("@web/core/utils/hooks");
    var framework = require('web.framework');
    var Dialog = require('web.Dialog');
    var _t = core._t;

    var CreateNewMenu = Dialog.extend(StandaloneFieldManagerMixin,{
        template: 'CreateNewMenu',
        events: {
            'change #model_type': '_onChangeModelType',
        },
        init: function(parent, current_primary_menu){
            this.current_primary_menu = current_primary_menu
            this.state = {
                step: 'started',
                appName: "",
                menuName: "",
                modelChoice: "new",
                modelOptions: [],
                modelId: false,
                iconData: {
                    type: 'custom_icon',
                },
            };
            var options = {
                title: _t('Create Menu'),
                size: 'medium',
                buttons: [{
                    text: _t("Create Menu"),
                    classes: 'btn-primary',
                    click: this._onClickNext.bind(this),
                }, {
                    text: _t("Cancel"),
                    close: true,
                }],
            };
            this._super(parent, options);
            StandaloneFieldManagerMixin.init.call(this);
        },

        start: async function(){
            var self = this;
            return this._super.apply(this, arguments).then( function(){
                self._createMenus();
            });
        },

        _createMenus: async function(){
            var self = this;
            return await this.model.makeRecord('ir.actions.act_window', [{
                name: 'model',
                relation: 'ir.model',
                type: 'many2one',
                domain: [['transient', '=', false]]
            }]).then( function(recordId){
                self.recordId = recordId;
                self.record = self.model.get(self.recordId);
                self.many2oneModel = new FieldMany2One(self, 'model', self.record, {
                    mode: 'edit',
                });
                self._registerWidget(self.recordId, 'model', self.many2oneModel);
                self.many2oneModel.appendTo(self.$el.find('.model_selection'));
            });
        },

        _onChangeModelType: function(e){
            var val = $(e.currentTarget).val();
            this.$el.find('.o_field_many2one.o_field_widget').addClass('w-100')
            if(val == 'new'){
                $('.model_selection_wrapper').addClass('d-none');
                $('.config').removeClass('d-none');
            } else if(val == 'existing'){
                $('.model_selection_wrapper').removeClass('d-none');
                $('.config').addClass('d-none');
            }
        },

        _onClickNext: async function(){
            var self = this;
            framework.blockUI();
            await this._onChangeModel();
            await this.configData();

        },

        configData: function(){
            _.extend(this.state, {
                object_name: $('#menu_name').val(),
                model_type: $('#model_type').val(),
                model_id:  this.modelId,
                new_options: this.new_options,
            });
            this._createNewMenu();
        },
        _onChangeModel: function(){
            var self = this;
            if($('#model_type').val() == 'existing'){
                this.modelId = this.many2oneModel.value.res_id;
            }else{
                this.modelId = false;
                this.new_options = {};
                var values = ['chatter', 'user', 'active', 'contact', 'company'];
                values.forEach( function(value, idx){
                    if($(`#${value}`).is(':checked')){
                        self.new_options[$(`#${value}`).attr('name')] = $(`#${value}`).is(':checked');
                    }
                });
            }
        },
        _createNewMenu: function(){
            var self = this;
            return ajax.jsonRpc('/openeducat_web_no_code_studio/create/menu','call',{
                'object_name': self.state.object_name,
                'menu_id': self.current_primary_menu,
                'model_id': self.state.model_id,
                'model_type': self.state.model_type,
                'new_options': self.state.new_options,
            }).then( function(response){
                self.close();
                window.location.reload();
                framework.unblockUI();
            }).guardedCatch(error => {
                error.event.preventDefault();
                Dialog.alert(self, _t(error.message.data.message));
                $('body').unblock();
            });
        },
    });
    return CreateNewMenu;
});