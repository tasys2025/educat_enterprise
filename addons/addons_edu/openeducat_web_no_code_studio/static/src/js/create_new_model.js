odoo.define('openeducat_web_no_code_studio.CreateNewModel', function(require){
    'use strict';
    var AbstractAction = require('web.AbstractAction');
    var ajax = require('web.ajax');
    var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
    var core = require('web.core');
    var { FieldMany2One } = require('web.relational_fields');
    const { useAutofocus, useListener } = require("@web/core/utils/hooks");
    var framework = require('web.framework');
    var Dialog = require('web.Dialog');
    var _t = core._t;
    var CreateNewModel = Dialog.extend(StandaloneFieldManagerMixin,{
        template: 'CreateNewModel',
        events: {
            'change #model_type': '_onChangeModelType',
            'click .next_step': '_onClickNext',
        },
        init: function(parent, current_primary_menu){
            this.menu_id = parent.editor_menu_bar.id
            this.current_primary_menu = current_primary_menu
            this.state = {
                step: 'started',
                appName: "",
                menuName: "",
                modelChoice: "new",
                modelOptions: [],
                modelId: false,
                iconData: {
                    type: 'custom_icon',
                },
            };
            var options = {
                title: _t('Create Model'),
                size: 'medium',
                buttons: [{
                    text: _t("Create Model"),
                    classes: 'btn-primary',
                    click: this._onClickNext.bind(this),
                }, {
                    text: _t("Cancel"),
                    close: true,
                }],
            };
            this._super(parent, options);
            StandaloneFieldManagerMixin.init.call(this);
        },
        start: async function(){
            var self = this;
            return this._super.apply(this, arguments).then( function(){
                self._createModels();
            });
        },
        _createModels: async function(){
            var self = this;
            return await this.model.makeRecord('ir.actions.act_window', [{
                name: 'model',
                relation: 'ir.model',
                type: 'many2one',
                domain: [['transient', '=', false]]
            }]).then( function(recordId){
                self.recordId = recordId;
                self.record = self.model.get(self.recordId);
                self.many2oneModel = new FieldMany2One(self, 'model', self.record, {
                    mode: 'edit',
                });
                self._registerWidget(self.recordId, 'model', self.many2oneModel);
                self.many2oneModel.appendTo(self.$el.find('.model_selection'));
            });
        },
        _onChangeModelType: function(e){
            var val = $(e.currentTarget).val();
            this.$el.find('.o_field_many2one.o_field_widget').addClass('w-100')
            if(val == 'new'){
                $('.model_selection_wrapper').addClass('d-none');
                $('.config').removeClass('d-none');
            } else if(val == 'existing'){
                $('.model_selection_wrapper').removeClass('d-none');
                $('.config').addClass('d-none');
            }
        },
        _onClickNext: async function(){
            var self = this;
            framework.blockUI();
            await this._onChangeModel();
            await this.configData();

        },
        _createIconAttachment: async function(attachment){
            var self = this;
            var fileReader = new FileReader();
            fileReader.onload = async function(){
                return await self._rpc({
                    model: "ir.attachment",
                    method: "create",
                    args: [{
                        name: attachment.name,
                        datas: base64js.fromByteArray(new Uint8Array(fileReader.result)),
                    }],
                }).then( function(res){
                    self.attachment_id = res
                    self.configData();
                });
            };
            fileReader.readAsArrayBuffer(attachment);
        },
        configData: function(){
            _.extend(this.state, {
                object_name: $('#model_name').val(),
                model_id:  this.menu_id,
                new_options: this.new_options,
                context: this.context || {},
            });
            this._createNewModel();
        },
        _onChangeModel: function(){
            var self = this;
            if($('#model_type').val() == 'existing'){
                this.modelId = this.many2oneModel.value.res_id;
            }else{
                this.modelId = false;
                this.new_options = {};
                var values = ['chatter', 'user', 'active', 'contact', 'company'];
                values.forEach( function(value, idx){
                    if($(`#${value}`).is(':checked')){
                        self.new_options[$(`#${value}`).attr('name')] = $(`#${value}`).is(':checked');
                    }
                });
            }
        },
        _createNewModel: function(){
            var self = this;
            return ajax.jsonRpc('/openeducat_web_no_code_studio/create/model','call',{
                'object_name': self.state.object_name,
                'menu_id': self.current_primary_menu,
                'new_options': self.state.new_options,
            }).then( function(response){
                self.close();
                window.location.reload();
                framework.unblockUI();
            }).guardedCatch( function(){
                framework.unblockUI();
            });
        },
    });
    return CreateNewModel;
});