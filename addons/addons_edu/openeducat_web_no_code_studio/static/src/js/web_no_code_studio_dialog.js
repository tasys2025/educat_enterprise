odoo.define('openeducat_web_no_code_studio.ViewEditorDialog', function(require){
    'use strict';

    var config = require('web.config');
    var core = require('web.core');
    var Dialog = require('web.Dialog');
    var relational_fields = require('web.relational_fields');
    var ModelFieldSelector = require('web.ModelFieldSelector');
    var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
    var field_registry = require('web.field_registry');
    var Many2ManyTags = require('web.relational_fields').FieldMany2ManyTags;
    var fieldRegistryOwl = require('web.field_registry_owl');
    var DomainSelectorDialog = require('web.DomainSelectorDialog');
    var utils = require('web.utils');
    var bus = require('openeducat_web_no_code_studio.bus');
    var Domain = require("web.Domain");
    var pyUtils = require('web.py_utils');
    var _t = core._t;
    var qweb = core.qweb;
    var Many2one = relational_fields.FieldMany2One;

    var ViewEditorDialog = Dialog.extend(StandaloneFieldManagerMixin, {
        template: 'openeducat_web_no_code_studio.attribute_dialog',
        events: _.extend({}, Dialog.prototype.events, {
            'click .web_no_code_studio_top_bar > div': '_onSideBarTab',
            'change input[data-type="attribute"]': '_onDialogAttribute',
            'change #widget': '_onDialogAttribute',
            'click .web_no_code_studio_domain': '_onDomainAttrs',
            'change #default_value': '_defaultValueChange',
            'click .web_no_code_studio_edit_selection_values': '_onClickSelection',
        }),
        init: function(parent, params){
            this._super.apply(this, arguments);
            var self = this;

            this.attrs = params.attrs;
            this.field = parent.fields[params.attrs.name];
            this.view_type = parent.view_type;
            this.state = params;
            this.fields = parent.fields;
            this.tag = params.tag;
            this.editorData = parent.editorData;
            this.model_name = parent.model_name;
            this.fields_included = params.fields_included;
            if(this.tag == 'label'){
                var options = _.extend({
                    title: _t('Attributes'),
                    size: 'medium',
                    buttons: [{
                        text: _t('Save'),
                        classes: 'btn-primary',
                        click: this._onClickSave.bind(this),
                    }],
                }, options);
            } else if(this.tag == 'div' && this.attrs.class == 'oe_chatter'){
                var options = _.extend({
                    title: _t('Attributes'),
                    size: 'medium',
                    buttons: [{
                         text: _t('Save'),
                        classes: 'btn-primary',
                        click: this._onEmailAliasSave.bind(this),
                    }],
                }, options);
            }else{
                var options = _.extend({
                    title: _t('Attributes'),
                    size: 'medium',
                    buttons: [{
                         text: _t('Save'),
                        classes: 'btn-primary',
                        click: this._onClickSave.bind(this),
                    },{
                        text: _t("Remove"),
                        classes: 'btn-danger',
                        click: this._onSideBarTab.bind(this),
                    }],
                }, options);
            }
            if (this.state && ['group', 'page', 'field', 'filter'].includes(this.tag)) {
                this.state.modifiers = this.state.modifiers || {};
            }
            if (['field'].includes(this.tag)) {
                var field =  this.field
                const fieldRegistryMap = Object.assign({}, field_registry.map, fieldRegistryOwl.map);
                field.field_widgets = _.chain(fieldRegistryMap).pairs().filter(function (arr) {
                    const supportedFieldTypes = utils.isComponent(arr[1]) ?
                        arr[1].supportedFieldTypes :
                        arr[1].prototype.supportedFieldTypes;
                    const description = self._getFieldDetails(arr[1], 'description');
                    const isWidgetKeyDescription = arr[0] === self.attrs.widget && !description;
                    var isSupported = _.contains(supportedFieldTypes, field.type)
                        && arr[0].indexOf('.') < 0;
                    return isSupported && description || isWidgetKeyDescription;
                }).value();
                this.field = field;
            }
            this._super(parent, options);
            StandaloneFieldManagerMixin.init.call(this);

        },

        start: function(){
            this._afterRender();
            this._super.apply(this, arguments);
        },

        _onClickSave: function() {
            this.trigger_up('execute_activities');
            this.close();
        },

        _onClickSelection: function(ev){
            ev.preventDefault();
            this.trigger_up('field_edition', {
                node: this.state,
            });
        },

        _afterRender: function(){
            this.def = [];
            if(this.tag === 'field'){
                if(this.searchAlive == false){
                    if(this.view_type == 'search' || this.view_type == 'form'){
                        this._renderSubSection('element');
                    }
                    if(this.view_type == 'form' || this.view_type == 'list'){
                        this._renderSubSection('newField');
                    }
                    //this._renderExistingFields();
                    return Promise.all(this.def).then(() => {
                        delete(this.def);
                        this.$('.web_no_code_studio_draggable').on("drag", _.throttle((event, ui) => {
                            this.trigger_up('element_drag', {position: {pageX: event.pageX, pageY: event.pageY}, $helper: ui.helper});
                        }, 200));
                    });
                }
            }

                if (this.$('.group_web_no_code_studio_dialog').length) {
                    this.def.push(this._createGroupsSelection());
                }
                return Promise.all(this.def).then(() => {
                    delete(this.def);
                });

        },

        _onEmailAliasSave: function(){
            var value = this.$el.find('#email_alias').val();
            if( this.email_alias != value ){
                this.trigger_up('change_email_alias', {
                    value: value
                });
            }
        },

        _onFieldChanged: async function (ev) {
            const res = await StandaloneFieldManagerMixin._onFieldChanged.apply(this, arguments);
            this._changeGroups();
            return res;
        },

        _changeGroups: function(){
            var record = this.model.get(this.groupsHandle);
            var newAttrs = {};
            newAttrs.groups = record.data.groups.res_ids;
            this.trigger_up('update_view', {
                type: 'attributes',
                execute: 'store',
                structure: 'attribute_changed',
                node: {
                'attrs': this.attrs,
                'tag': this.tag,
                },
                newAttrs: newAttrs,
            });
        },


        _createGroupsSelection: function(){
            var self = this;
            var editor_group = this.attrs.editor_group && JSON.parse(this.attrs.editor_group);
            return this.model.makeRecord('ir.model.fields', [{
                name: 'groups',
                fields: [{
                    name: 'id',
                    type: 'integer',
                }, {
                    name: 'display_name',
                    type: 'char',
                }],
                relation: 'res.groups',
                type: 'many2many',
                value: editor_group,
            }]).then(function (recordID) {
                self.groupsHandle = recordID;
                var record = self.model.get(self.groupsHandle);
                var options = {
                    idForLabel: 'groups',
                    mode: 'edit',
                    no_quick_create: true,
                };
                var many2many = new Many2ManyTags(self, 'groups', record, options);
                many2many.className = many2many.className + ' col';
                self._registerWidget(self.groupsHandle, 'groups', many2many);
                return many2many.appendTo(self.$('.group_web_no_code_studio_dialog'));
            });
        },

        _onDomainAttrs: function(e){
            e.preventDefault();
            var property = $(e.currentTarget).data('property');

            var allFields = this.fields_included;

            var domainDialog = new DomainSelectorDialog(this, this.model_name, _.isArray(this.state.modifiers[property]) ? this.state.modifiers[property] : [], {
                readonly: false,
                fields: allFields,
                size: 'medium',
                operators: ["=", "!=", "<", ">", "<=", ">=", "in", "not in", "set", "not set"],
                followRelations: false,
                debugMode: config.isDebug(),
            }).open();

            domainDialog.on("domain_selected", this, function (e) {
                var mods = _.extend({}, this.state.modifiers);
                mods[property] = e.data.domain;
                var newAttrs = this._getNewAttrsFromModifiers(mods);
                this.trigger_up('update_view', {
                    type: 'attributes',
                    execute: 'store',
                    structure: 'attribute_changed',
                    node: {
                        'attrs': this.attrs,
                        'tag':'field'
                    },
                    newAttrs: newAttrs,
                });
            });
        },

        destroy: function(){
            this._super.apply(this, arguments);
            bus.trigger('clear_set_activities');
        },

        _getNewAttrsFromModifiers: function (modifiers) {
            var self = this;
            var newAttributes = {};
            var attrs = [];
            var originNodeAttr = this.state.modifiers;
            var originSubAttrs =  pyUtils.py_eval(this.state.attrs.attrs || '{}', this.editorData);
            _.each(modifiers, function (value, key) {
                    var keyInNodeAndAttrs = _.contains(['readonly', 'invisible', 'required'], key);
                    var keyFromView = key in originSubAttrs;
                    var trueValue = value === true || _.isEqual(value, []);
                    var isOriginNodeAttr = key in originNodeAttr;

                    if (keyInNodeAndAttrs && !isOriginNodeAttr && trueValue) {
                        newAttributes[key] = "1";
                    } else if (keyFromView || !trueValue) {
                        newAttributes[key] = "";
                        if (value !== false) {
                            attrs.push(_.str.sprintf("\"%s\": %s", key, Domain.prototype.arrayToString(value)));
                        }
                    }
            });
            newAttributes.attrs = _.str.sprintf("{%s}", attrs.join(", "));
            return newAttributes;
        },

        _defaultValueChange: function(ev){
            var $target = $(ev.currentTarget);
            if($target.val() !== this.field.default_value){
                this.trigger_up('default_val_changed',{
                    field_name: this.attrs.name,
                    execute: 'store',
                    value: $target.val(),
                });
            }
        },

        _onDialogAttribute: function(e){
            var $target = $(e.currentTarget),
                newAttrs = {},
                attr = $target.attr('name');
            if($target.attr('type') == 'checkbox'){
                if(!(['readonly', 'invisible', 'required'].includes(attr))){
                    if ($target.is(':checked')) {
                        newAttrs[attr] = $target.data('leave-empty') === 'checked' ? '': 'True';
                    } else {
                        newAttrs[attr] = $target.data('leave-empty') === 'unchecked' ? '': 'False';
                    }
                }else{
                    var newMods = _.extend({}, this.attrs.invisible);
                    newMods[attr] = $target.is(':checked');
                    newAttrs = this._formatAttributes(newMods);
                }
            } else if($target.attr('type') == 'select'){
                newAttrs[attr] = $target.find('option:selected').val();
            } else{
                newAttrs[attr] = $target.val();
                if (!newAttrs.string){
                    newAttrs[attr] = " "
                }else{
                    newAttrs[attr] = newAttrs[attr].trim()
                }
            }
            this.trigger_up('update_view', {
                type: 'attributes',
                execute: 'store',
                structure: 'attribute_changed',
                node: {
                    'tag':this.tag,
                    'attrs':this.attrs,
                },
                newAttrs: newAttrs,
            });
        },

        _getFieldDetails: function(fieldType, propName) {
            return utils.isComponent(fieldType) ?
                (fieldType.hasOwnProperty(propName) && fieldType[propName]) :
                (fieldType.prototype.hasOwnProperty(propName) && fieldType.prototype[propName]);
        },

        _onSideBarTab: function(e){
            var self = this;
            this.trigger_up('update_view', {
                type: 'remove',
                structure: 'remove',
                node: {
                    'attrs' : this.attrs,
                    'tag': this.tag,
                },
            });
//            $(".close").click();
            this.close();
        },

        _formatAttributes: function(attrs){
            for(var attr in attrs){
                attrs[attr] = attrs[attr] ? "1" : "0";
            }
            return attrs;
        },

    });
    return ViewEditorDialog;
});
