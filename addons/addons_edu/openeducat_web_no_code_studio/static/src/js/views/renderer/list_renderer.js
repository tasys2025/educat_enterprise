odoo.define('openeducat_web_no_code_studio.ListRenderer', function(require){
    'use strict';

    var ListRenderer = require('web.ListRenderer');
    var EditorMixin = require('openeducat_web_no_code_studio.EditorMixin');

    return ListRenderer.extend(EditorMixin, {
        className: ListRenderer.prototype.className + ' web_no_code_studio_list_editor',
        events: _.extend({}, ListRenderer.prototype.events, {
            'click th:not(.web_no_code_studio_hook), td:not(.web_no_code_studio_hook)': '_onColumnSelected',
        }),
        custom_events: _.extend({}, ListRenderer.prototype.custom_events, {
            'hook_selection': '_onHookSelection',
        }),
        init: function (parent, state, params) {
            this.show_invisible = (params.show_invisible === 'true');
            this._super.apply(this, arguments);
            this.nearest_hook = 200;
        },

         _processColumns: function (columnInvisibleFields) {
            if(this.show_invisible){
                var self = this;
                this.handleField = null;
                this.columns = [];
                this.optionalColumns = [];
                this.optionalColumnsEnabled = [];
                var storedOptionalColumns;
                this.trigger_up('load_optional_fields', {
                    keyParts: this._getOptionalColumnsStorageKeyParts(),
                    callback: function (res) {
                        storedOptionalColumns = res;
                    },
                });
                _.each(this.arch.children, function (c) {
                    if (c.tag !== 'control' && c.tag !== 'groupby' && c.tag !== 'header') {
                        self.columns.push(c);
                    }
                });
            } else {
                this._super.apply(this, arguments);
            }
        },

         _renderBodyCell: function (record, node, colIndex, options) {
            options = _.extend(options, {
                renderInvisible: true,
            });
            var $cell = this._super.apply(this, arguments);
            if(node.attrs.modifiers.column_invisible){
                if($cell.hasClass('o_invisible_modifier')){
                    $cell.addClass('web_no_code_studio_invisible');
                }
            }
            return $cell;
        },

        getLocalState: function() {
            var state = this._super.apply(this, arguments) || {};
            if (this.selected_node_id) {
                state.selected_node_id = this.selected_node_id;
            }
            return state;
        },

        displayHook: function ($helper, position) {
            var self = this;
            EditorMixin.displayHook.apply(this, arguments);
            var $hook = this.$('.web_no_code_studio_hook').touching({
                        x: position.pageX - this.nearest_hook,
                        y: position.pageY - this.nearest_hook,
                        w: this.nearest_hook*2,
                        h: this.nearest_hook*2
                    },{
                        container: document.body
                    }
                ).nearest({x: position.pageX, y: position.pageY}, {container: document.body}).eq(0);
            if ($hook.length) {
                var $elements = this._getColumnElements($hook);
                $elements.addClass('web_no_code_studio_nearest_hook');
                return true;
            }
            return false;
        },

        _onColumnSelected: function(e){
            var $el = $(e.currentTarget);
            var $selected_column = $el.closest('table').find('th').eq($el.index());

            var field_name = $selected_column.data('name');
            var node = _.find(this.columns, function (column) {
                return column.attrs.name === field_name;
            });
            this.selected_node_id = $selected_column.data('node-id');
            //Todo: Clicked Element
            this.trigger_up('clicked_element', {node: node});
        },

        _renderFooter: function () {
            var $footer = this._super.apply(this, arguments);
            var $td = $footer.find('td');

            $td.each( function(){
               $(this).after($('<td class="web_no_code_studio_hook"/>'));
            });
            $footer.find('tr').prepend($('<td class="web_no_code_studio_hook"/>'));

            return $footer;
        },

        _getColumnElements: function ($hook) {
            return $hook.closest('table')
                .find('tr')
                .children(':nth-child(' + ($hook.index() + 1) + ')');
        },

        _onHookSelection: function(){
            this.selected_node_id = false;
        },

        _addHookRow: function ($row) {
            var $element = $row.find('td, th');
            $row.find('td.o_list_record_selector').remove();
            $element.each( function(){
                $(this).after($('<td class="web_no_code_studio_hook"/>'));
            });
            $row.prepend($('<td class="web_no_code_studio_hook"/>'));
        },

        selectField: function (fieldName) {
            this.$('th[data-name=' + fieldName + ']').click();
        },

        _render: function () {
            var self = this;
            var res = this._super.apply(this, arguments);
            res.then(function () {
                self.$el.droppable({
                    accept: ".web_no_code_studio_draggable",
                    drop: self._handleDrop.bind(self),
                });

                self.setSelectable(self.$('th, td').not('.web_no_code_studio_hook'));
                self.$('i.o_optional_columns_dropdown_toggle')
                    .addClass('text-muted')
            });
            return res;
        },

        _handleDrop: function (ev, ui) {
            var $hook = this.$('.web_no_code_studio_nearest_hook');
            if ($hook.length) {
                var position = $hook.closest('table').find('th').eq($hook.index()).data('position') || 'after';
                var hookedFieldIndex = position === 'before' && $hook.index() + 1 || $hook.index() - 1;
                var fieldName = $hook.closest('table').find('th').eq(hookedFieldIndex).data('name');
                var node = _.find(this.columns, function (column) {
                    return column.attrs.name === fieldName;
                });

                if (!this.columns.length) {
                    node = {
                       tag: 'tree',
                   };
                   position = 'inside';
                }

                var $drag = ui.draggable || $(ev.target);
                this._onHandleDrop($drag, node, position);
                ui.helper.removeClass('ui-draggable-helper-ready');
                $hook.removeClass('web_no_code_studio_nearest_hook');
            }
        },

        _renderHeader: function () {
            var $header = this._super.apply(this, arguments);
            $header.find('th.o_list_record_selector').remove();
            var self = this;
            _.each($header.find('th'), function (th) {
                var $new_th = $('<th>')
                    .addClass('web_no_code_studio_hook')
                    .css('font-size','12px')
                    .append(
                        $('<i>').addClass('fa fa-arrow-down ')
                );
                $new_th.insertAfter($(th));
                $(th).attr('data-node-id', self.node_id++);

                self._draggableColumn($(th));
            });

            var $new_th_before = $('<th>')
                .addClass('web_no_code_studio_hook')
                .data('position', 'before')
                .css('font-size','12px')
                .append(
                    $('<i>').addClass('fa fa-arrow-down ')
            );
            $new_th_before.prependTo($header.find('tr'));
            return $header;
        },

        _renderHeaderCell: function (node) {

            var $th = this._super.apply(this, arguments);
            if (_.contains(this.invisible_columns, node)) {
                $th.addClass('web_no_code_studio_show_invisible');
            }
            return $th;
        },

        setSelectable: function ($el) {
            EditorMixin.setSelectable.apply(this, arguments);

            var self = this;
            $el.click(function (ev) {
                var $hook = $(ev.currentTarget);
                self.$('.web_no_code_studio_clicked').removeClass('web_no_code_studio_clicked');
                var $elements = self._getColumnElements($hook);
                $elements.addClass('web_no_code_studio_clicked');
            })
            .mouseover(function (ev) {
                if (self.$('.ui-draggable-dragging').length) {
                    return;
                }
                var $hook = $(ev.currentTarget);
                var $elements = self._getColumnElements($hook);
                $elements.addClass('web_no_code_studio_hovered');
            })
            .mouseout(function () {
                self.$('.web_no_code_studio_hovered').removeClass('web_no_code_studio_hovered');
            });
        },

        _draggableColumn: function ($el) {
            var self = this;

            $el.draggable({
                axis: 'x',
                scroll: false,
                revertDuration: 200,
                refreshPositions: true,
                start: function (e, ui) {
                    self.$('.web_no_code_studio_hovered').removeClass('web_no_code_studio_hovered');
                    self.$('.web_no_code_studio_clicked').removeClass('web_no_code_studio_clicked');
                    ui.helper.addClass('ui-draggable-helper');
                },
                stop: this._handleDrop.bind(this),
                revert: function () {

                    var $hook = self.$('.web_no_code_studio_nearest_hook');
                    if ($hook.length) {
                        var position = $hook.closest('table').find('th').eq($hook.index()).data('position') || 'after';
                        var hookedFieldIndex = position === 'before' && $hook.index() + 1 || $hook.index() - 1;
                        var fieldName = $hook.closest('table').find('th').eq(hookedFieldIndex).data('name');
                        if (fieldName !== self.$('.ui-draggable-helper').data('name')) {
                            return false;
                        }
                    }
                    self.$('.ui-draggable-helper').removeClass('ui-draggable-helper');
                    self.$('.ui-draggable-helper-ready').removeClass('ui-draggable-helper-ready');
                    return true;
                },
            });

            $el.on('drag', _.throttle(function (event, ui) {
                self.trigger_up('element_drag', {
                    position: {pageX: event.pageX, pageY: event.pageY},
                    $helper: ui.helper,
                });
            }, 200));
        },

        _renderRow: function () {
            var $row = this._super.apply(this, arguments);
            this._addHookRow($row);
            return $row;
        },

        _getColumnsTotalWidth() {
            const thElementsLength = this.el.querySelectorAll('thead th').length + 1;
            return this._super(...arguments) + thElementsLength;
        },

    });

});