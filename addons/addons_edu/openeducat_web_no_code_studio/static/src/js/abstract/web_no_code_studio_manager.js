odoo.define('openeducat_web_no_code_studio.AbstractViewEditorManager', function (require) {
"use strict";
    const { ComponentWrapper, WidgetAdapterMixin } = require('web.OwlCompatibility');
    var concurrency = require('web.concurrency');
    var core = require('web.core');
    var _t = core._t;
    var Widget = require('web.Widget');
    var bus = require('openeducat_web_no_code_studio.bus');
    var view_registry = require('web.view_registry');
    var dom = require('web.dom');
    var session = require('web.session');
    var ajax = require('web.ajax');
    const { registry } = require("@web/core/registry");
    const viewUtils = require("web.viewUtils");
    const wrapperRegistry = registry.category("wowl_editors_wrappers");
    const { StudioView } = require("@openeducat_web_no_code_studio/js/studio_view");
    const { SearchModel } = require("@web/search/search_model");
    const { useService } = require("@web/core/utils/hooks");
    var FormRenderer = require('openeducat_web_no_code_studio.FormRenderer');
    const { extendEnv } = require("@openeducat_web_no_code_studio/js/abstract/utils");
    var ListRenderer = require('openeducat_web_no_code_studio.ListRenderer');
    var KanbanRenderer = require('openeducat_web_no_code_studio.KanbanRenderer');
    var SearchRenderer = require('openeducat_web_no_code_studio.SearchRenderer');
    var Dialog = require('web.Dialog');
    var CreateFieldDialog = require("openeducat_web_no_code_studio.CreateFieldDialog");
    var FormButtonDialog = require("openeducat_web_no_code_studio.FormButtonDialog");
    var ViewEditorSideBarWidget = require('openeducat_web_no_code_studio.ViewEditorSideBar');
    var ViewEditorDialog = require('openeducat_web_no_code_studio.ViewEditorDialog');
    var framework = require('web.framework');
    const { resetViewCompilerCache } = require("@web/views/view_compiler");
    const {  useEnv } = owl;
    const CONVERTED_VIEWS = [
        "calendar",
        "cohort",
        "dashboard",
        "graph",
        "map",
        "pivot",
        "form",
    ];

    class EditorWrapper extends ComponentWrapper {
    handleDrop() {
        return this.componentRef.comp &&
            this.componentRef.comp.handleDrop(...arguments);
    }
    highlightNearestHook() {
        return this.componentRef.comp &&
            this.componentRef.comp.highlightNearestHook(...arguments);
    }
    setSelectable() {
        return this.componentRef.comp &&
            this.componentRef.comp.setSelectable(...arguments);
    }
    unselectedElements() {
        return this.componentRef.comp &&
            this.componentRef.comp.unselectedElements(...arguments);
    }
}
class GenericWowlEditor extends EditorWrapper {
    setup() {
        super.setup();
        this.state = {
            getFieldNames: () => [],
        }
    }
    getLocalState() {}
    setLocalState() {}
    unselectedElements() {}
    handleDrop() {}
    highlightNearestHook() {}
    setSelectable() {}
}
GenericWowlEditor.services = ["user"];

    var AbstractViewEditorManager = Widget.extend({
        className: 'web_no_code_studio_manager',
        custom_events:{
            on_change_sidebar_tab: '_onChangeSidebarTab',
            clicked_element: '_onElementClick',
            element_drag: '_onElementDrag',
            update_view: '_onUpdateView',
            default_val_changed: '_onDefaultValueChange',
            field_edition: '_onEditionField',
            execute_activities: '_executeSetActivities',
            clear_set_activities: '_clearActivities',
            data_invisible: '_dataInvisible',
            change_email_alias: '_onChangeEmailAlias',
        },
        init: function(parent, params){
            this._super.apply(this, arguments);
            this.viewEditor = null;
            this.viewEditorSideBar = null;
            this.steps = [];
            this.steps_pending = [];
            this.renamingAllowedFields = [];
            this.action = params.action;
            this.fieldsView = params.fieldsView;
            this.wowlEnv = params.wowlEnv;
            this.view_id = this.fieldsView.view_id;
            //this._defaultModelsSet();
            this.web_no_code_studio_arch = params.web_no_code_studio_arch;
            this.web_no_code_studio_arch_id = params.web_no_code_studio_arch_id;
            this.model_name = this.fieldsView.model;
            this.view_type = params.viewType;
            this.execute = true;
            this.show_invisible = false;
            this.controllerState = params.controllerState;
            this.mdp = new concurrency.MutexedDropPrevious();
            this.relationalPath = params.relationalPath || [];
            this.fields = this._calculateFields(this.fieldsView.fields);
            bus.on('clear_set_activities', this, this._cleanSteps)
        },

        start: function(){
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                return self._startViewEditor().then( function(viewEditor){
                    var def = [];
                    var $editor = $('<div class="web_no_code_studio_renderer">');

                    self.viewEditor = viewEditor;
                    self.viewEditor instanceof ComponentWrapper ?
                        def.push(self.viewEditor.mount($editor[0])) :
                        def.push(self.viewEditor.appendTo($editor));

                    $editor.appendTo(self.$el);

                    self.viewEditorSideBar = self._generateEditorSideBar();
                    def.push(self.viewEditorSideBar.appendTo(self.$el));
                    return Promise.all(def);
                });
            });
        },

        _defaultModelsSet: async function(){
            var result = await this._rpc({
                route: '/get/restore/default_view',
                params: {
                    view_id: this.view_id,
                },
            });
            this.fieldsView.relatedModels = result.models;
        },

        on_detach_callback: function () {
            if (this.viewEditor && this.viewEditor.on_detach_callback) {
                this.viewEditor.on_detach_callback();
            }
            this.isInDOM = false;
        },

        on_attach_callback: function () {
            if (this.viewEditor && this.viewEditor.on_attach_callback) {
                this.viewEditor.on_attach_callback();
            }
            this.isInDOM = true;
        },

        _set: function (que) {
            if(this.isRelationField && que.target){
                this._setRelationalSubPath(que);
            }
            que.id = _.uniqueId('que_');
            this.steps.push(que);
            this.steps_pending = [];
            if(this.execute === true){
                return this._doChanges(false, que.type === 'replace_arch');
            }else{
                return;
            }
        },


        _executeSetActivities: function(){
            return this._doChanges(false, false);
        },

        _setRelationalSubPath: function(que){
            var subviewXpath = this._getSubviewXpath(this.relationalPath);
            if (que.target.xpath_info && que.target.xpath_info[0].tag === this.relationViewType) {
                que.target.xpath_info.shift();
            }
            que.target.subview_xpath = subviewXpath;

            if (que.type === 'move') {
                que.node.subview_xpath = subviewXpath;
            }
        },

        _getSubviewXpath: function (relationalPath){
            var subviewXpath = "";
            _.each(relationalPath, function (path) {
                var relationViewType = path.relationViewType === 'list' ? 'tree' : path.relationViewType;
                subviewXpath += "//field[@name='" + path.isRelationField + "']/" + relationViewType;
            });
            return subviewXpath;
        },

        _doChanges: function(delete_que, xml){
            var self = this,
                def,
                previousStep = this.steps.slice(-1)[0],
                previousStepId = previousStep && previousStep.id;

            if(xml){
                def = this.mdp.exec(this._updateActualArch.bind(
                    this,
                    previousStep.view_id,
                    previousStep.new_arch
                )).guardedCatch(function () {
                });
            }else{
                def = this.mdp.exec(function () {
                    var localSteps = [];
                    _.each(self.steps, function (op) {
                        if (op.type !== 'replace_arch') {
                            localSteps.push(_.omit(op, 'id'));
                        }
                    });
                    var prom = self._updateActualView(
                        self.view_id,
                        self.web_no_code_studio_arch,
                        localSteps
                    );
                    prom.guardedCatch(function () {
                        //return self._undo(previousStepId, true).then(function () {
                        //    return Promise.reject();
                        //});
                    });
                    return prom;
                });
            }

            return def.then( function(response){
                if(xml){
                    self._cleanSteps(previousStepId);
                }
                return self._handleDoChanges(response, xml, previousStepId);
            }).then( function(){
                if(self.viewEditorSideBar.state.mode != "attributes"){
                    self._onChangeUpdateSideBar(self.viewEditorSideBar.state.mode);
                }
            });
        },

        _onChangeEmailAlias: function(e){
            var value = e.data.value;
            var model_name = this.relationalModel ? this.relationalModel : this.model_name;
            return this._rpc({
                route: '/openeducat_web_no_code_studio/change/email_alias',
                params: {
                    model_name: model_name,
                    value: value,
                },
            });
        },

        _handleDoChanges: function(response, xml, stepId){
            var self = this;
            var def = Promise.resolve();
            if(!response.fields_views){
                //Todo: Display Error
                return Promise.reject();
            }
            if(response.web_no_code_studio_arch_id){
                this.web_no_code_studio_arch_id = response.web_no_code_studio_arch_id;
            }
            if (this.isRelationField) {
                this.view_type = this.mainViewType;
            }
            this.fields = this._calculateFields(response.fields);
            this.fieldsView = response.fields_views[this.view_type];
            this.fieldsView.viewFields = this.fieldsView.fields;
            this.fieldsView.fields = response.fields;
            if(this.isRelationField){
                def = this._setRelationViewParams();
            }
            return def.then(self._updateViewEditor.bind(self));
        },

        _cleanSteps: function(que){
            this.step = [];
            this.steps_pending = [];
        },

        _onElementClick: function(data){
            var self = this;
            var node = data.data.node;
            var $node = data.data.$node;

            if(this.view_type == 'form' && (node.tag === 'field')){
                var field = this.fields[node.attrs.name];
                var attrs = this.viewEditor.state.fieldsInfo[this.viewEditor.state.viewType][node.attrs.name];
                var isRelational = ['one2many','many2many'].includes(field.type);
                if( isRelational && !(['many2many_tags', 'hr_org_chart'].includes(attrs.widget))){
                    var block = $('<div>' +
                        '<button type="button" class="btn btn-secondary btn-primary web_no_code_studio_relational_field_edit" data-type="list">Edit List View</button>' +
                        '<button type="button" class="btn btn-secondary btn-primary web_no_code_studio_relational_field_edit" data-type="form">Edit Form View</button>' +
                    '</div>');
                    var options = {
                        baseZ: 1000,
                        message: block,
                        css: {
                            cursor: 'auto',
                        },
                        overlayCSS: {
                            cursor: 'auto',
                        }
                    };
                    if ($node.hasClass('o_field_one2many') || $node.hasClass('o_field_many2many')) {
                        $node.block(options);
                    } else {
                        $node.find('div.o_field_one2many, div.o_field_many2many').block(options);
                    }

                    $node.find('.web_no_code_studio_relational_field_edit').click(function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        self._openRelationalEditor(
                            node.attrs.name,
                            $(e.currentTarget).data('type')
                        );
                    });
                }
            }
//            this._onChangeUpdateSideBar('attributes',data.data);
            if(node.tag =='page'){
                if($('li.web_no_code_studio_clicked').hasClass('page_clicked')){
                    this._onChangeUpdateAttribute('attributes',data.data);
                }else{
                    $('li.web_no_code_studio_clicked').addClass('page_clicked');
                }
            }else{
                this._onChangeUpdateAttribute('attributes',data.data);
            }

        },

        _onChangeUpdateAttribute: function(mode, options){
            var self = this,
                state,
                def = Promise.resolve();
            $('li.web_no_code_studio_clicked').removeClass('page_clicked');
            if (mode) {
                state = _.extend({}, options, {
                    renamingAllowedFields: this.renamingAllowedFields,
                    mode: mode,
                    //show_invisible: this._getShowInvisible(),
                });
            } else {
                state = this.ViewEditorSideBar.state;
            }

            //Todo Extend More Modes
            if(mode == 'view'){
                state = _.extend(state, {
                    attrs: this.view.arch.attrs,
                });
                var dialog = new ViewEditorSideBar(this,node).open();
            }else if(mode == 'attributes'){
                var node = options.node,
                    attrs = ((node.tag === 'field' || node.tag === 'group' ) && this.view_type !== 'search') ?
                            this.viewEditor.state.fieldsInfo[this.viewEditor.state.viewType][node.attrs.name] :
                            node.attrs;
                state = _.extend(state, {
                    attrs: attrs,
                    //string: this.fieldsView.fields[node.attrs.name]['string'],
                });
                var modelName = this.relationalModel ? this.relationalModel : this.model_name;
                if (node.tag === 'field') {
                    def = this._getDefaultView(modelName, node.attrs.name);
                }
            }
            var dialog = new ViewEditorDialog(this,node).open();
        },

        _openRelationalEditor: function(name, type, fromBreadcrumb, view, data){
            var self = this;
            this.viewEditor.unselectedElements();
            this.isRelationField = name;
            this.relationViewType = type;
            var fields = this.fields;
            var fieldsInfo = this.viewEditor.state.fieldsInfo;
            if (view) {
                fields = view.fields;
                fieldsInfo = view.fieldsInfo;
            }
            this.relationalModel = fields[this.isRelationField].relation;

            var data = data || this.viewEditor.state.data[this.isRelationField];
            if (type === 'form' && data.count) {
                data = data.data[0];
            }
            var context = _.omit(data.getContext(), function (val, key) {
                return _.str.startsWith(key, 'default_');
            });
            this.relationViewParams = {
                currentId: data.res_id,
                context: context,
                ids: data.res_ids,
                viewType: this.relationViewType,
                model: this.viewEditor.model,
                modelName: this.relationalModel,
                parentID: this.viewEditor.state.id,
            };
            this.renamingAllowedFields = [];
            this.relationalPath.push({
                view_type: this.view_type,
                isRelationField: this.isRelationField,
                relationViewType: this.relationViewType,
                relationalModel: this.relationalModel,
                data: data,
            });
            var field = fieldsInfo[this.view_type][this.isRelationField];
            var def;
            if (!(type in field.views) || field.views[type].name) {
                def = this._createInlineView(type, field.name);
            } else {
                def = this._startRelationalEditor();
            }
            return def.then(function () {
                if (!fromBreadcrumb) {
                    bus.trigger('edition_x2m_entered', type, self.relationalPath.slice());
                }
                self._onChangeUpdateSideBar('new');
            });
        },

        _startRelationalEditor: function(){
            var self = this;
            this.mainViewType = this.view_type;
            return this._setRelationViewParams().then(function () {
                return self._updateViewEditor();
            });
        },

        _setRelationViewParams: function () {
            var self = this;
            this.view_type = this.relationViewType;
            return this._rpc({
                model: this.relationalModel,
                method: 'fields_get',
            }).then(function (fields) {
                self.fields = self._calculateFields(fields);
            });
        },

        _onEditionField: function(e){
            var self = this;
            var node = e.data.node;
            var field = this.fields[node.attrs.name];
            var dialog = new CreateFieldDialog(this, this.model_name, field, this.fields).open();
            var modelName = this.relationalModel ? this.relationalModel : this.model_name;
            dialog.on('field_default_values_saved', this, function (values) {
                self._editField(modelName, field.name, values).then(function (result) {
                    const _closeDialog = function () {
                        dialog.close();
                        self._doChanges(false, false);
                    };
                    if (result && result.records_linked) {
                        const message = result.message || _t("Are you sure you want to remove the selection values?");
                        Dialog.confirm(self, message, {
                            confirm_callback: async function () {
                                await self._editField(modelName, field.name, values, true);
                                _closeDialog();
                            },
                            dialogClass: 'o_web_no_code_studio_preserve_space'
                        });
                    } else {
                        _closeDialog();
                    }
                });
            });
        },

        _editField: function(modelName, fieldName, values, forceEdit) {
            return this._rpc({
                route: '/openeducat_web_no_code_studio/edit/field',
                params: {
                    model_name: modelName,
                    field_name: fieldName,
                    values: values,
                    force_edit: forceEdit,
                }
            });
        },

        _updateViewEditor: function(options){
            var self = this,
                currentViewEditor = this.viewEditor,
                local = (this.viewEditor && this.viewEditor.getLocalState) ? this.viewEditor.getLocalState() : false;

            return this._startViewEditor(options).then( function(newEditor){
                var fragment = document.createDocumentFragment(),
                    def = newEditor instanceof ComponentWrapper ? newEditor.mount(fragment) : newEditor.appendTo(fragment);
                return def.then(function(){
                    dom.append(self.$('.web_no_code_studio_renderer'), [fragment], {
                        in_DOM: self.isInDOM,
                        callbacks: [{ widget: newEditor }],
                    });
                    self.viewEditor = newEditor;
                    currentViewEditor.destroy();

                    //self.$el.scrollTop(rendererScrollTop);
//                    if (local) {
//                        self.viewEditor.setLocalState(local);
//                    }
                });
            });
        },

        _updateActualArch: function(id, arch){
            core.bus.trigger('clear_cache');
            return ajax.jsonRpc('/openeducat_web_no_code_studio/update/arch','call',{
                view_id: id,
                arch: arch,
                context: _.extend({}, session.user_context , {lang: false}),
            });
        },

        _updateActualView: function(id, arch, steps){
            core.bus.trigger('clear_cache');
            return ajax.jsonRpc('/openeducat_web_no_code_studio/update/view','call',{
                view_id: id,
                arch: arch,
                steps: steps,
                context: _.extend({}, session.user_context, {lang: false}),
            });
        },

        _generateFieldString: function(range){
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
                string = '';
            for (var i=0; i<range; i++) {
                var num = Math.floor(Math.random() * chars.length);
                string += chars.substring(num,num+1);
            }
            return string;
        },

        _addElementField: function(data){
            var def,
                self = this,
                dialog,
                field_description = data.field_description;

            //Todo For Field Description
            if(field_description){
                var modelName = this.relationalModel ? this.relationalModel : this.model_name;
                field_description = _.extend({}, field_description, {
                    name: `x_${field_description.type}_field_${self._generateFieldString(5)}`,
                    model_name: modelName,
                });
                if (_.contains(['selection', 'one2many', 'many2one', 'many2many', 'related'], field_description.type)) {
                    def = new Promise(function (resolve, reject) {
                        var prom;
                        if (field_description.type === 'one2many') {
                            var modelName = self.relationalModel ? self.relationalModel : self.model_name;
                            prom = self._rpc({
                                model:"ir.model.fields",
                                method: "search_count",
                                args: [[['relation', '=', modelName], ['ttype', '=', 'many2one']]],
                            });
                        } else {
                            prom = Promise.resolve(true);
                        }
                        prom.then(function (openFieldDialog) {
                            if (!openFieldDialog) {
                                dialog = Dialog.alert(self, '', {
                                    $content: $('<main/>', {
                                        role: 'alert',
                                        html: $('<div> No Many2one Available To Create One2Many Field </div>'),
                                    }),
                                    title: _t("No related many2one fields found"),
                                });
                                dialog.on('closed', self, function () {
                                    reject();
                                });
                            } else {
                                dialog = new CreateFieldDialog(self, modelName, field_description, _.filter(self.fields, {type: 'many2one'})).open();
                                dialog.on('field_default_values_saved', self, function (values) {
                                    if (values.related && values.type === 'monetary') {
                                        if (self._hasCurrencyField()) {
                                            resolve(values);
                                            dialog.close();
                                        } else {
                                            var relatedCurrency = values._currency;
                                            delete values._currency;
                                            var currencyDialog = openCurrencyCreationDialog(relatedCurrency, resolve);
                                            currencyDialog.on('closed', self, function () {
                                                dialog.close();
                                            });
                                        }
                                    } else {
                                        resolve(values);
                                        dialog.close();
                                    }
                                });
                                dialog.on('closed', self, function () {
                                    reject();
                                });
                            }
                        });
                    });
                } else if(field_description.type == 'monetary'){
                    def = new Promise(function (resolve, reject) {
                        if (self._isCurrencyAvailable()) {
                            resolve();
                        } else {
                            field_description = {
                                default_value: session.company_currency_id,
                                field_description: 'Currency',
                                model_name: modelName,
                                name: 'x_currency_id',
                                relation: 'res.currency',
                                type: 'many2one',
                            };
                            resolve();
                        }
                    });
                }
            }
            Promise.resolve(def).then(function(result){
                if (field_description) {
                    self.renamingAllowedFields.push(field_description.name);
                }
                if(data.add_statusbar){
                    self.steps.push({type: 'statusbar'});
                }
                //Todo: Renaming And Status Bar
                var target = data.data.target || {
                    tag: data.node.tag,
                    attrs: _.pick(data.node.attrs, data.node.tag == 'label' ? ['for'] : ['name']),
                    xpath_info: data.expr,
                };
                self._set({
                    type: data.type,
                    target: target,
                    position: data.position,
                    node: {
                        tag: 'field',
                        attrs: data.attributes,
                        field_description: _.extend(field_description, result),
                    },
                }).then( function(){
                    if (self.viewEditor.selectField && field_description) {
                        self.viewEditor.selectField(field_description.name);
                    }
                });
            }).guardedCatch( function(){
                self._updateViewEditor();
            });
            framework.unblockUI();
        },

        _onChangeSidebarTab: function(mode){
            this._onChangeUpdateSideBar(mode.data.mode);
        },

        _isCurrencyAvailable: function () {
            var isCurrency = _.find(this.fields, function (field) {
                return field.type === 'many2one' && field.relation === 'res.currency' &&
                    (field.name === 'currency_id' || field.name === 'x_currency_id');
            });
            return !!isCurrency;
        },

        _getRenderer: function(view){
            if(view == 'form'){
                return FormRenderer;
            }else if(view == "list"){
                return ListRenderer;
            }else if(view == "kanban"){
                return KanbanRenderer;
            }else if(view == "calendar"){
                return CalendarRenderer;
            }else{
                return false;
            }
        },

        _onElementDrag: function(e){
            var hook = this.viewEditor.displayHook(e.data.$helper, e.data.position);
            e.data.$helper.toggleClass('ui-draggable-helper-ready', hook);
        },

        _getDefaultView: function(model, field){
            return ajax.jsonRpc('/openeducat_web_no_code_studio/default_view','call',{
                model,
                field,
            });
        },

        _onChangeUpdateSideBar: function(mode, options){
            var self = this,
                state,
                def = Promise.resolve();

            if (mode) {
                state = _.extend({}, options, {
                    renamingAllowedFields: this.renamingAllowedFields,
                    mode: mode,
                    //show_invisible: this._getShowInvisible(),
                });
            } else {
                state = this.viewEditorSideBar.state;
            }

            //Todo Extend More Modes
            if(mode == 'view'){
                state = _.extend(state, {
                    attrs: this.view.arch.attrs,
                });
            }else if(mode == 'attributes'){
                var node = options.node,
                    attrs = ((node.tag === 'field' || node.tag === 'group' ) && this.view_type !== 'search') ?
                            this.viewEditor.state.fieldsInfo[this.viewEditor.state.viewType][node.attrs.name] :
                            node.attrs;
                state = _.extend(state, {
                    attrs: attrs,
                    //string: this.fieldsView.fields[node.attrs.name]['string'],
                });
                var modelName = this.relationalModel ? this.relationalModel : this.model_name;
                if (node.tag === 'field') {
                    def = this._getDefaultView(modelName, node.attrs.name);
                }
            }
            return def.then( function(extendedState){
                var newState = _.extend(state, extendedState),
                    currentViewEditorSidebar = self.viewEditorSideBar,
                    fragment = document.createDocumentFragment(),
                    previousState = currentViewEditorSidebar.getLocalState ? currentViewEditorSidebar.getLocalState() : undefined;
                self.viewEditorSideBar = self._generateEditorSideBar(newState, previousState);

                return self.viewEditorSideBar.appendTo(fragment).then(function () {
                    currentViewEditorSidebar.destroy();
                    if (!self.viewEditorSideBar.isDestroyed()) {
                        self.viewEditorSideBar.$el.appendTo(self.$el);
                        if (self.viewEditorSideBar.on_attach_callback) {
                            self.viewEditorSideBar.on_attach_callback();
                        }

                        if (self.mode === 'rendering') {
                            self.viewEditorSideBar.$el.detach();
                        }
                    }
                });
            });
        },

        async externalizeWowlController(viewParams) {
            await this._defaultModelsSet();
            const mainViewType = this.view_type;
            const x2ManyInfo = this.relationalPath.length ? this.relationalPath[this.relationalPath.length-1].wowlX2ManyInfo  : null;
            const nextViewType = x2ManyInfo ? x2ManyInfo.viewType : mainViewType;

            const chatterAllowed = x2ManyInfo ? false : this.chatter_allowed;
            const resModel = x2ManyInfo ? x2ManyInfo.resModel : viewParams.action.res_model;

            const fullXpath = x2ManyInfo ? getX2MFullXpath(this.x2mEditorPath.map(infos => infos.wowlX2ManyInfo)) : "";

            let shouldLoadViews = false; //!!this.wowlEditor;
            if (x2ManyInfo && !x2ManyInfo.hasArch) {
                shouldLoadViews = true;
                const { viewType, fieldName, resModel } = x2ManyInfo;
                const viewId = this.viewDescriptions.views[mainViewType].id;
                const subViewRef = null;
                const studioArch = await wowlCreateInlineView(this.wowlEnv, { subViewType: viewType, viewId, fullXpath, subViewRef, resModel, fieldName })
                this.studio_view_arch = studioArch;
            }
            if (shouldLoadViews) {
                const context = Object.assign({}, this.action.context, { studio: true, lang: false });
                const resModel = this.action.res_model;
                const views = this.action.views;
                const actionId = this.action.id;
                const loadActionMenus = false;
                const loadIrFilters = true;
                this.viewDescriptions = await this.wowlEnv.services.view.loadViews(
                    { context, resModel, views },
                    { actionId, loadActionMenus, loadIrFilters }
                );
            }
            let { arch, custom_view_id } = this.fieldsView;
            if (x2ManyInfo) {
                arch = getSubArch(arch, `${fullXpath}/${nextViewType}`);
            }

            const view = registry.category("views").get(nextViewType);

            if (view && view.type === "form") {
                const newModel = class newModel extends view.Model {};
                newModel.Record = class newRecord extends view.Model.Record {
                    get isInEdition() {
                        return false;
                    }
                };
                view.Model = newModel;
            }
            if (this.mode !== "edition") {
                resetViewCompilerCache();
            }
            let resId, resIds = [];
            if (x2ManyInfo) {
                resIds = x2ManyInfo.resIds;
                resId = x2ManyInfo.resId;
            } else if (viewParams.controllerState) {
                resId = viewParams.controllerState.resId || viewParams.controllerState.currentId;
                resIds = viewParams.controllerState.resIds || viewParams.controllerState.res_ids;
            } else {
                resId = this.resId;
                resIds = this.resIds;
            }
            const fields = this.fieldsView.fields;

            let controllerProps = {
                info: {},
                arch,
                fields,
                relatedModels: this.fieldsView.relatedModels,
                resModel,
                useSampleModel: false,
                searchMenuTypes: [],
                className: `o_view_controller o_${nextViewType}_view`,
                resId,
                resIds,
            };
            if (["list", "tree", "form"].includes(nextViewType) && this.mode === "edition" && x2ManyInfo) {
                controllerProps.parentRecord = x2ManyInfo.parentRecord;
            }

            if (nextViewType === "form") {
                controllerProps.preventEdit = true;
            }

            if (custom_view_id) {
                controllerProps.info.customViewId = custom_view_id;
            }

            const editorCallbacks = {};
            const config = {
                executeCallback: (name, ...args) => editorCallbacks[name](...args),
                registerCallback: (name, fn) => editorCallbacks[name] = fn,
                views: [],
                getDisplayName: () => {},
                setDisplayName: () => {},
                mode: 'readonly',
                chatterAllowed,
                x2mField: this.x2mField,
                type: nextViewType,
            };

            config.onNodeClicked = (params) => {
                this.wowlEditor.setLastClickedXpath(params.xpath);
                const legacyNode = getLegacyNode(params.xpath, controllerProps.archInfo.xmlDoc)
                this._onNodeClicked({data: {
                    node: legacyNode,
                    isWowl: true,
                }})
            }
            config.onViewChange = (data) => {
                resetViewCompilerCache();
                return this.__onViewChange(data)
            };

            config.onEditX2ManyView = ({viewType, fieldName, record, xpath}) => {
                let data = record.data[fieldName];
                if ("__bm__" in record.model) {
                    data = record.model.__bm__.get(record.__bm_handle__).data[fieldName];
                }
                const legacyX2MPath = this._computeX2mPath(fieldName, viewType, null, data);
                legacyX2MPath.x2mViewParams.model = record.model.__bm__;
                legacyX2MPath.x2mViewParams.parentID = record.__bm_handle__;
                const activeField = record.activeFields[fieldName];
                const staticList = record.data[fieldName];

                const resIds = staticList.records.map((r) => r.resId);
                const wowlX2ManyInfo = {
                    hasArch: viewType in activeField.views,
                    resModel: staticList.resModel,
                    resId: resIds[0],
                    resIds,
                    viewType,
                    parentRecord: record,
                    xpath,
                    fieldName
                }

                legacyX2MPath.wowlX2ManyInfo = wowlX2ManyInfo;
            }

            config.structureChange = (params) => {
                const legacyNode = getLegacyNode(params.xpath, controllerProps.archInfo.xmlDoc)
                const xpathInfo = xpathToLegacyXpathInfo(params.xpath);
                const data = {...params, node: legacyNode, xpathInfo }
                resetViewCompilerCache();
                this._onViewChange({data});
            }
            controllerProps = view.props ? view.props(controllerProps, view, config) : controllerProps;

            const Controller = view.Controller;
            const SearchModelClass = view.SearchModel || SearchModel;

            const env = this.wowlEnv;
            const studioViewProps = {
                Controller,
                SearchModelClass,
                context: viewParams.context,
                domain: viewParams.domain || [], // bug in cohort domain = false???
                env,// deleted by ComponentWrapper (see owl_compatibility)
                controllerProps,
                setOverlay: !["form", "list", "tree", "kanban"].includes(nextViewType),
                resetSidebar: () => {
                    this._generateEditorSideBar();
                }
            };

            const Wrapper = wrapperRegistry.get(nextViewType, GenericWowlEditor);
            this.wowlEditor = new Wrapper(this, StudioView, studioViewProps);

            const parser = new DOMParser();
            const xml = parser.parseFromString(controllerProps.arch, "text/xml");
            const rootNode = xml.documentElement;

            const attrs = {};
            for (const { name, value } of rootNode.attributes) {
                attrs[name] = value;
            }
            if (attrs.sample) {
                controllerProps.useSampleModel = Boolean(evaluateExpr(attrs.sample));
            }

            this.view = {
                //  in case we pass line: const arch = Editors[this.view_type].prototype.preprocessArch(this.view.arch);
                arch: Object.assign({}, viewUtils.parseArch(controllerProps.arch), { mode: "view"}),
                controllerProps,
                loadParams: {},
            };
            return this.wowlEditor;
        },
        _startViewEditor: function(options){
            options = options ? options : {};
            var fieldsView,
                viewOptions,
                Renderer,
                View = view_registry.get(this.view_type),
                def;
            if(this.isRelationField){
                fieldsView = this._generateRelationalFieldView();
                viewOptions = this.relationViewParams;
            }else{
                fieldsView = this.fieldsView;
                viewOptions = {
                    action: this.action,
                    context: this.action.context,
                    controllerState: this.controllerState,
                    withSearchPanel: false,
                    domain: this.action.domain,
                };
            }
            if(!this.view_type){
                this.view = fieldsView;
                this.view = new ViewsReport(this, fieldsView);
                def = Promise.resolve(this.view);
            }else if(this.view_type == 'calendar'){
                return this.externalizeWowlController(viewOptions);
            }else if(this.view_type !== 'search'){
                this.view = new View(fieldsView, _.extend({}, viewOptions));
                Renderer = this._getRenderer(this.view_type);

                //TODO: If Renderer Is Not Available.

                var viewEditorOptions = _.defaults(options, {
                        mode: 'readonly',
                        //chatter_allowed: chatterAllowed,
                        show_invisible: this.show_invisible,
                        arch: this.view.arch,
                        isRelationField: this.isRelationField,
                        viewType: this.view_type,
                });
                if(Renderer){
                    def = this.view._generateViewWithEditor(this, Renderer, viewEditorOptions);
                }else if(this.view_type === 'graph'){
                    def = this.view._generateViewWithRenderer(this, viewEditorOptions);
                }else{
                    //def = this.view._generateViewWithRenderer(this,viewEditorOptions);
                     def = Promise.resolve(this.view);
                }
            }else{
                this.view = fieldsView;
                this.view = new SearchRenderer(this, fieldsView);
                def = Promise.resolve(this.view);
            }
            
            return def;
        },

        _changeFieldAttribute: function(type, node, expr, newAttrs){
            var que = {
                type: type,
                target: {
                    tag: node.tag,
                    attrs: _.pick(node.attrs, node.tag == 'label' ? 'for' : 'name'),
                    xpath_info: expr,
                },
                position: 'attributes',
                node: node,
                newAttrs: newAttrs,
            };
//            if((newAttrs.string == node.attrs.string) && node.tag == 'field'){
//            //Todo: Check If Renaming Can Be Done
//                if(this.isRelationField){
//                    //Todo Set Sub path
//                }
//                this.steps.push(que);
//
//                var baseName = 'x_' + this._slugify(newAttrs.string);
//                var newName = baseName;
//                var index = 1;
//                while (newName in this.fields) {
//                    newName = baseName + '_' + index;
//                    index++;
//                }
//                this._renameFieldName(newName, node.attrs.name);
//            }else{
                this._set(que);
//            }
        },

        _renameFieldName: function(newLabel, fieldName){
            var self = this;
            return ajax.jsonRpc('/openeducat_web_no_code_studio/rename/field','call',{
                model: this.relationalModel ? this.relationalModel : this.model_name,
                web_no_code_studio_arch_id: this.web_no_code_studio_arch_id,
                web_no_code_studio_arch: this.web_no_code_studio_arch,
                new_label: newLabel,
                field_name: fieldName,
            }).then( function(){
                return self._doChanges();
            });
        },

        _calculateFields: function(fields){
            fields = $.extend(true, {}, fields);
            for (const [name, field] of Object.entries(fields)) {
                field.name = name;
            }
            return fields;
        },

        _slugify: function (text) {
            return text.toString().toLowerCase().trim()
                .replace(/[^\w\s-]/g, '')
                .replace(/[\s_-]+/g, '_')
                .replace(/^-+|-+$/g, '');
        },

        _onUpdateView: function(e){
            var attributes = e.data.new_attrs ? e.data.new_attrs : {},
                node = e.data.node,
                expr = e.data.node ? this._calculateExprParent(this.view.arch, e.data.node) : undefined ,
                position = e.data.position,
                newAttrs = e.data.newAttrs ? e.data.newAttrs: e.data.new_attrs,
                field_description = e.data.field_description,
                structure = e.data.structure,
                data = e.data,
                type = e.data.type;
            this.execute = e.data.execute ? e.data.execute : true;
            if(structure == 'field'){
                attributes = _.pick(attributes, ['name', 'widget', 'options', 'display']);
                if(this.view_type == 'search'){
                    Object.assign( attributes, {
                        'string': data.new_attrs.label,
                    });
                }
                this._addElementField({node, expr, position, type, attributes, data, field_description});
            } else if(structure == 'attribute_changed'){
                attributes = _.pick(attributes, ['name', 'widget', 'options', 'display']);
                if (!newAttrs.string){
                    Object.assign( newAttrs, {
                        'string': attributes.name,
                    });
                }
                this._changeFieldAttribute(type, node, expr, newAttrs);
            } else if(structure == 'view_changed'){
                this._onViewChange(type, newAttrs);
            } else if( structure == 'remove'){
                this._onRemoveField(type, node, expr);
            } else if( structure == 'page'){
                this._OnAddNewPage(type, node, expr, position);
            } else if( ['notebook'].includes(structure) ){
                this._onAddFormElement(type, node, expr, position, structure);
            } else if(structure == 'filter'){
                newAttrs = _.pick(newAttrs, ['name', 'string', 'domain', 'context', 'date'])
                this._onAddFilterElement(type, node, expr, position, structure, newAttrs);
            }else if(structure == 'group'){
                newAttrs = _.pick(newAttrs, ['name', 'string', 'domain', 'context', 'date'])
                this._onAddGroupElement(type, node, expr, position, structure,newAttrs);
            } else if(structure == 'button'){
                this._addNewButton(e.data);
            }
        },

        _onAddGroupElement: function(type, node, expr, position, structure, newAttrs){
            var self = this;
            newAttrs.context = node.attrs.context
            if(this.view_type == 'search'){
                this._set({
                    type: type,
                    position: position,
                    target: {
                        tag: 'group//filter',
                        attrs: _.pick(node.attrs, node.tag == 'label' ? 'for' : 'name'),
                        xpath_info: expr,
                    },
                    node: {
                        tag: 'filter',
                        attrs: newAttrs,
                    }
                });
            } else{
                this._set({
                    type: type,
                    position: position,
                    target: {
                        tag: 'group//filter',
                        attrs: _.pick(node.attrs, node.tag == 'label' ? 'for' : 'name'),
                        xpath_info: expr,
                    },
                    node: {
                        tag: 'group',
                        attrs: {
                            name: 'editor_' + node.tag + '_' +  self._generateFieldString(5)
                        },
                    }
                });
            }
        },

        _addNewButton: function(data){
            var dialog = new FormButtonDialog(this, this.model_name).open();
            dialog.on('saved', this, function (result) {
                if (data.add_buttonbox) {
                    this.steps.push({type: 'buttonbox'});
                }
                this._set({
                    type: data.type,
                    target: {
                        tag: 'div',
                        attrs: {
                            class: 'oe_button_box',
                        }
                    },
                    position: 'inside',
                    node: {
                        tag: 'button',
                        field: result.field_id,
                        string: result.string,
                        attrs: {
                            class: 'oe_stat_button',
                            icon: result.icon,
                        }
                    },
                });
            });
        },

        _onAddFilterElement: function(type, node, expr, position, structure, newAttrs){
            var self = this;
            this._set({
                type: type,
                position: position,
                target: {
                    tag: node.tag,
                    attrs: _.pick(node.attrs, node.tag == 'label' ? 'for' : 'name'),
                    xpath_info: expr,
                },
                node: {
                    tag: 'filter',
                    attrs: newAttrs,
                },
            });
        },

        _onAddFormElement: function(type, node, expr, position, structure){
            var self = this;
            this._set({
                type: type,
                position: position,
                target: {
                    tag: node.tag,
                    attrs: _.pick(node.attrs, node.tag == 'label' ? 'for' : 'name'),
                    expr: expr,
                },
                node: {
                    tag: structure,
                    attrs: {
                        name: 'web_no_code_studio_' + structure + self._generateFieldString(5),
                    }
                }
            });
        },

        _onDefaultValueChange: function(e){
            var data = e.data;
            var model = this.relationalModel ? this.relationalModel : this.model_name;
            this._setDefaultValue(model, data.field_name, data.value)
            .guardedCatch(function () {
                if (data.on_fail) {
                    data.on_fail();
                }
            });
        },

        _setDefaultValue: function(model, field, value){
            return this._rpc({route: '/openeducat_web_no_code_studio/default_value/set', params: {
                model: model,
                field: field,
                value: value
            }});
        },

        _OnAddNewPage: function(type, node, expr, position){
            var self = this;
            this._set({
                type: type,
                target: {
                    tag: node.tag,
                    attrs: _.pick(node.attrs, node.tag == 'label' ? 'for' : 'name'),
                    xpath_info: expr,
                },
                position: position,
                node: {
                    tag: 'page',
                    attrs: {
                        string: 'New Page',
                        name: 'editor_page_' + self._generateFieldString(5),
                    }
                },
            });
        },

        _dataInvisible: function(e){
            this.show_invisible = e.data.show_invisible
            this._updateViewEditor();
        },

        _onViewChange: function(type, newAttrs){
            this._set({
                type: type,
                target: {
                    tag: this.view_type === 'list' ? 'tree' : this.view_type,
                    isSubviewAttr: true,
                },
                position: 'attributes',
                newAttrs: newAttrs,
            });
        },

        _generateRelationalFieldView: function(){
            var View = view_registry.get(this.mainViewType);
            var view = new View(this.fieldsView, _.extend({}, this.relationViewParams));

            var fields_view = view.fieldsView;
            _.each(this.relationalPath, function (step) {
                var x2mField = fields_view.fieldsInfo[step.view_type][step.isRelationField];
                fields_view = x2mField.views[step.relationViewType];
            });
            fields_view.model = this.relationalModel;
            return fields_view;
        },

        _calculateExprParent: function(arch, node){
            return this._calculateExpr(arch, node, [], 1);
        },

        _calculateExpr: function(parent, node, expr, index){
            var self = this,
                data;
            expr.push({
                'tag': parent.tag,
                'index': index,
            });

            if (parent === node) {
                return expr;
            } else {
                var current_indices = {};
                _.each(parent.children, function (child) {

                    current_indices[child.tag] = current_indices[child.tag] ? current_indices[child.tag] + 1 : 1;
                    var values = self._calculateExpr(child, node, expr, current_indices[child.tag]);
                    if (values) {
                        data = values;
                    } else {
                        expr.pop();
                    }
                });
            }
            return data;
        },

        _onRemoveField: function(type, node, expr){
            if(!this.isRelationField){
                var expr_attrs = node.tag == 'label' ? ['for'] : ['name']
                var parent = this._calculateParent(this.view.arch, node, expr_attrs);
                var is_root = !this._calculateParent(this.view.arch, parent, expr_attrs);
                var is_group = parent.tag === 'group';
                if (parent.children.length === 1 && !is_root && !is_group) {
                    node = parent;
                    if (node && _.isEmpty(_.pick(node.attrs, expr_attrs))) {
                        expr = this._calculateExprParent(this.view.arch, node);
                    }
                }
            }
             this._set({
                type: type,
                target: {
                    tag: node.tag,
                    attrs: _.pick(node.attrs, node.tag == 'label' ? ['for'] : ['name']),
                    xpath_info: expr,
                },
            });
        },

        _calculateParent: function(arch, node, expr_attrs) {
            var parent = arch;
            var self = this;
            var result;
            var xpathInfo = this._calculateExprParent(arch, node);
            _.each(parent.children, function (child) {
                var deepEqual = true;
                if (_.isEmpty(_.pick(child.attrs, child.tag == 'label' ? ['for'] : ['name']))) {
                    var childXpathInfo = self._calculateExprParent(arch, child);
                    _.each(xpathInfo, function (node, index) {
                        if (index >= childXpathInfo.length) {
                            deepEqual = false;
                        } else if (!_.isEqual(xpathInfo[index], childXpathInfo[index])) {
                            deepEqual = false;
                        }
                    });
                }
                if (deepEqual && child.attrs && child.attrs.name === node.attrs.name) {
                    result = parent;
                } else {
                    var res = self._calculateParent(child, node, expr_attrs);
                    if (res) {
                        result = res;
                    }
                }
            });
            return result;
        },

        _generateEditorSideBar: function(option){
            if (this.view_type == 'calendar' || this.view_type == 'graph'){
                var Mode = ['form', 'list', 'search'].includes(this.view_type) ? 'field' : 'view',
                model = this.relationalModel ? this.relationalModel : this.model_name;
            }else{
                if(this.view_type !== 'search'){
                    var Mode = ['form', 'list', 'search'].includes(this.view_type) ? 'field' : 'view',
                    allFields = this.viewEditor.state.getFieldNames(),
                    model = this.relationalModel ? this.relationalModel : this.model_name;
                }
            }
            option = _.defaults(option || {}, {
                mode: Mode,
                attrs: Mode === 'view' ? this.view.arch.attrs : {},
            });

            var options = {
                view_type: this.view_type,
                model_name: model,
                fields: this.fields,
                renamingAllowedFields: this.renamingAllowedFields,
                state: option,
                isRelationField: this.isRelationField,
                editorData: this.viewEditor.state && this.viewEditor.state.data || {},
                fieldsInfo: this.view.fieldsInfo ? this.view.fieldsInfo[this.view_type] : false,
                defaultOrder: this.view.arch.attrs ?  this.view.arch.attrs.default_order || false : false,
            };

            //TODO Fix Pick And Omit
            if (['list', 'form', 'kanban'].includes(this.view_type)) {
                options.fields_included = _.pick(this.fields, allFields);
                options.fields_excluded = _.omit(this.fields, allFields);
            } else if (this.view_type === 'search') {
                options.fields_excluded = this.fields;
                options.fields_included = [];
            }

            return new ViewEditorSideBarWidget(this, options);
        },

    });

    return AbstractViewEditorManager;
});
