odoo.define('openeducat_web_no_code_studio.ReportElementsEdit', function (require) {
"use strict";

var { ColorpickerDialog } = require('web.Colorpicker');
var config = require('web.config');
var core = require('web.core');
var utils = require('web.utils');
var fieldRegistry = require('web.field_registry');
var fieldRegistryOwl = require('web.field_registry_owl');
const FieldWrapper = require('web.FieldWrapper');
var ModelFieldSelector = require('web.ModelFieldSelector');
var StandaloneFieldManagerMixin = require('web.StandaloneFieldManagerMixin');
const { WidgetAdapterMixin } = require('web.OwlCompatibility');
//const wysiwygLoader = require('web_editor.loader');
//var Wysiwyg = require('web_editor.wysiwyg');
//var SummernoteManager = require('web_editor.rte.summernote');
var wysiwygLoader = require('web_editor.loader');
var DomainSelectorDialog = require('web.DomainSelectorDialog');
var Domain = require("web.Domain");
var Widget = require('web.Widget');
var py = window.py;
var Registry = require('web.Registry');
var qweb = core.qweb;
var registry = new Registry();

var MainComponent = Widget.extend(WidgetAdapterMixin, StandaloneFieldManagerMixin, {
    events: {
        'change input': function (e) {
            e.stopPropagation();
        },
    },
    custom_events: _.extend({}, Widget.prototype.custom_events, {
        field_changed: '_onFieldChange',
        field_chain_changed: '_onFieldChange',
    }),

    init: function (parent, params) {
        this._super.apply(this, arguments);
        StandaloneFieldManagerMixin.init.call(this);
        this.models = params.models;
        this.state = params.state || {};
        this.node = params.node;
        this.context = params.context;
        this.componentsList = params.componentsList;
        this.directiveFields = {};
        this.directiveRecordId = '';
        this.fieldSelector = {};

    },

    willStart: function () {
        var self = this;

        var directiveModel = [];
        _.each(this.directiveFields, function (options, directiveKey) {
            var value = options.value;
            if (!value) {
                value = self.node.attrs[options.attributeName || directiveKey];
            }

            if (options.type === 'related') {
                directiveModel.push({
                    name: directiveKey,
                    type: 'char',
                    value: options.freecode ? value : self._splitRelatedValue(value).chain.join('.'),
                });
            } else {
                directiveModel.push(_.extend({}, {
                    name: directiveKey,
                    value: value,
                }, options));
            }
        });

        var defDirective = this.model.makeRecord('ir.model.fields', directiveModel)
            .then(function (recordId) {
                self.directiveRecordId = recordId;

                _.each(self.directiveFields, function (options, directiveKey) {
                    if (options.type === 'related') {
                        self.createFieldSelector(directiveKey, options);
                    } else {
                        self.createField(directiveKey, options);
                    }
                });
            });
        var defParent = this._super.apply(this, arguments);
        return Promise.all([defDirective, defParent]);
    },

    destroy: function () {
        this._super.apply(this, arguments);
        WidgetAdapterMixin.destroy.call(this);
    },

    on_attach_callback: function () {
        WidgetAdapterMixin.on_attach_callback.call(this);
    },

    on_detach_callback: function () {
        WidgetAdapterMixin.on_detach_callback.call(this);
    },

    _getContextKeys: function (node) {
        var self = this;
        var contextOrder = node.contextOrder || [];

        var keys = _.compact(_.map(node.context, function (relation, key) {
            return {
                name: key,
                string: self.models[relation] ? key + ' (' + self.models[relation] + ')' : key + ' (' + relation + ')',
                relation: relation,
                type: self.models[relation] ? key[key.length-1] === 's' ? 'one2many' : 'many2one' : relation,
                store: true,
                related: true,
                searchable: true,
                order: -contextOrder.indexOf(key),
            };
        }));
        keys.sort(function (a, b) {
            return a.order - b.order;
        });
        return keys;
    },

    createField: function (directiveKey, options) {
        var directiveRecord = this.model.get(this.directiveRecordId);

        options = _.extend({
            mode: 'edit',
            attrs: _.extend({
                quick_create: false, can_create: false
            }, options)
        }, options);

        var field = directiveRecord.fields[directiveKey];
        var FieldClass = fieldRegistryOwl.getAny([options.Widget, field.type]);
        if (FieldClass) {
            this.fieldSelector[directiveKey] = new FieldWrapper(this, FieldClass, {
                fieldName: directiveKey,
                record: directiveRecord,
                options,
            });
        } else {
            FieldClass = fieldRegistry.getAny([options.Widget, field.type]);
            this.fieldSelector[directiveKey] = new FieldClass(
                this, directiveKey, directiveRecord, options);
        }
    },

    createFieldSelector: function (directiveKey, options) {
        var directiveRecord = this.model.get(this.directiveRecordId);

        var split = this._splitRelatedValue(directiveRecord.data[directiveKey]);
        if (split.chain[0] === 'undefined') {
            return this.createField(directiveKey);
        }

        if (options.freecode && split.rest) {
            var InputField = fieldRegistry.get('input');
            this.fieldSelector[directiveKey] = new InputField(
                this, directiveKey,
                directiveRecord,
                _.extend({mode: 'edit', attrs: options}, options));
            return;
        }

        var availableKeys = this._getContextKeys(this.node);
        if (options.loop) {
            availableKeys = _.filter(availableKeys, function (relation) {
                return relation.type === 'one2many' || relation.type === 'many2one';
            });
        }
        this.fieldSelector[directiveKey] = new ModelFieldSelector(this, 'record_fake_model', split.chain,
            _.extend({
                readonly: options.mode === 'readonly',
                searchable: false,
                fields: availableKeys,
                filters: {searchable: false},
                filter: options.filter || function () {
                    return true;
                },
                followRelations: options.followRelations || function (field) {
                    return field.type === 'many2one';
                },
            }, options));
    },

    _splitRelatedValue: function (value) {
        var chain = [];
        var rest = value || '';
        if (typeof value === "string") {
            try {
                value = py.extract(value);
            } catch (e) {
                return {
                    chain: [],
                    rest: value,
                };
            }
        }
        if (value) {
            if (value.isOperator) {
                if (value.params.values[0].isField) {
                    chain = value.params.values[0].expr.split('.');
                    rest = value.expr.slice(chain.length);
                } else {
                    rest = value.expr;
                }
            }
            if (value.isCall) {
                rest = (value.params.object.length ? '.' : '') + value.params.method + '(' + value.params.args.join(', ') + ')';
                chain = value.params.object;
            }
            if (value.isField) {
                rest = '';
                chain = value.expr.split('.');
            }
        }
        return {
            chain: chain,
            rest: rest,
        };
    },

    _tSetAttributes: function (newAttrs) {
        var self = this;
        var node = this.node;
        var op = [];
        _.each(newAttrs, function (val, set) {
            if (value === self.directiveFields[set].value) { return; }
            op.push({
                content: '<attribute name="t-value">' + value + '</attribute>',
                position: "attributes",
                view_id: +node.attrs['data-oe-id'],
                xpath: node.attrs['data-oe-xpath'] + "//t[@t-set='" + set + "']"
            });
        });
        if (!op.length) { return; }
        this.trigger_up('update_report', {
            node: node,
            operation: {
                inheritance: op,
            },
        });
    },

    _onChangeAttr: function (attributeName, toAdd, toRemove) {
        var attribute = '<attribute name="' + attributeName + '" separator="' + (attributeName === 'class' ? ' ' : ';') + '"';
        if (toAdd) {
            attribute += ' add="' + toAdd + '"';
        }
        if (toRemove) {
            attribute += ' remove="' + toRemove + '"';
        }
        attribute += '/>';

        this.trigger_up('update_report', {
            node: this.node,
            operation: {
                inheritance: [{
                    content: attribute,
                    position: "attributes",
                    view_id: +this.node.attrs['data-oe-id'],
                    xpath: this.node.attrs['data-oe-xpath']
                }],
            },
        });
    },

    _onUpdateView: function (newAttrs) {
        this.trigger_up('update_report', {
            node: this.node,
            operation: {
                type: 'attributes',
                newAttrs: newAttrs,
            },
        });
    },

    _onFieldChange: function (e) {
        var self = this;
        e.stopPropagation();  // TODO: is it really useful on an OdooEvent

        e.data.dataPointID = this.directiveRecordId;

        var always = function () {
            var newAttrs = {};
            _.each(self.fieldSelector, function (fieldType, directiveKey) {
                var directiveTarget = self.fieldSelector[directiveKey];
                var target = e.target;
                if (directiveTarget instanceof owl.Component) {
                    directiveTarget = directiveTarget.componentRef.comp;
                    target = e.data.__originalComponent
                }
                if (!e.data.forceChange && target !== directiveTarget) { return; }
                var data = self.model.get(self.directiveRecordId).data;
                var fieldValue = data[directiveKey];
                if (e.data.chain) {
                    fieldValue = e.data.chain.join('.');
                }
                if (fieldValue.res_ids) {
                    fieldValue = fieldValue.res_ids.slice();
                }
                newAttrs[directiveKey] = fieldValue;
            });

            if (e.data.chain) {
                e.data.dataPointID = self.directiveRecordId;
                e.data.changes = newAttrs;
            }

            self._onUpdateView(newAttrs);
        };

        StandaloneFieldManagerMixin._onFieldChanged.call(this, e).then(always, always);
    },
});

var loadColors;
var layout_editable = MainComponent.extend({
    name: 'layout',
    template : 'openeducat_web_no_code_studio.report_data_attr_main_layout',
    events : _.extend({}, MainComponent.prototype.events, {
        "change .web_no_code_studio_margin > div > input": "_onChangeMargin",
        "change .web_no_code_studio_width>input": "_onChangeWidth",
        "click .web_no_code_studio_font_size .dropdown-item-text": "_onChangeFont",
        "change .web_no_code_studio_table_style > select": "_onChangeTable",
        "click .web_no_code_studio_text_decoration button": "_onChangeTextDec",
        "click .web_no_code_studio_text_alignment button": "_onChangeTextAlign",
        "change .web_no_code_studio_classes>input": "_onChangeClasses",
        "click .font_changer_picker": "_onCustomColor",
    }),

    init: function (parent, params) {
        var self = this;
        this._super.apply(this, arguments);
        this.debug = config.isDebug();
        this.isTable = params.node.tag === 'table';
        this.isNodeText = _.contains(this.componentsList, 'text');
        this.allClasses = params.node.attrs.class || "";
        this.classesArray =(params.node.attrs.class || "").split(' ');
        this.stylesArray =(params.node.attrs.style || "").split(';');

        var fontSizeRegExp= new RegExp(/^\s*(h[123456]{1})|(small)|(display-[1234]{1})\s*$/gim);
        var backgroundColorRegExp= new RegExp(/^\s*background\-color\s*:/gi);
        var colorRegExp= new RegExp(/^\s*color\s*:/gi);
        var widthRegExp= new RegExp(/^\s*width\s*:/gi);
        var colClassRegex = /\bcol((-(sm|md|lg|xl))?-(\d{1,2}|auto))?\b/;

        this.setupParams(fontSizeRegExp, backgroundColorRegExp, colorRegExp, widthRegExp, colClassRegex, params.node.tag);

        this.allClasses = params.node.attrs.class || "";
//        new SummernoteManager(this);
    },

    willStart: async function () {
        await this._super();
        await this._rpc({
            model: 'ir.ui.view',
            method: 'render_public_asset',
            args: ['web_editor.colorpicker', {}],
        }).then(function (template) {
            return qweb.add_template('<templates>' + template + '</templates>');
        });
    },

    setupParams: function(fontSizeRegExp, backgroundColorRegExp, colorRegExp, widthRegExp, colClassRegex, tag){
        var self = this;

        ["margin-top", "margin-bottom", "margin-left", "margin-right"].forEach( function(value){
            self[value] = self._findMarginValue(value);
        });

        this["background-color-class"] = _.find(this.classesArray, function(item) {
            return !item.indexOf('bg-');
        });
        this["font-color-class"] = _.find(this.classesArray, function(item) {
            return !item.indexOf('text-');
        });
        this.tableStyle = _.find(this.classesArray, function(item) {
            return !item.indexOf('table-');
        });
        this["background-color"] = _.find(this.stylesArray, function(item) {
            return backgroundColorRegExp.test(item);
        });
        this.color = _.find(this.stylesArray, function(item) {
            return colorRegExp.test(item);
        });
        this.displayWidth = !(tag === 'div' && _.find(this.classesArray, function(item) {
            return colClassRegex.test(item);
        }));
        this.originalWidth =  _.find(this.stylesArray, function(item) {
            return widthRegExp.test(item);
        });
        if (this.originalWidth) {
            this.width = this.originalWidth.replace(/\D+/g,'');
        }

        this.fontSize = _.find(this.classesArray, function(item) {
            return fontSizeRegExp.test(item);
        });

        ['italic', 'bold', 'underline'].forEach( function(value){
            self[value] = _.contains(self.classesArray, `o_${value}`);
        })

        this.alignment = _.intersection(this.classesArray, ['text-left', 'text-center', 'text-right'])[0];
        this.displayAlignment = !_.contains(['inline', 'float'], this.node.$nodes.css('display'));
    },

    _findMarginValue: function(marginName) {
        if (this.node.attrs.style) {
            var margin = this.node.attrs.style
                .split(';')
                .map(function(item) { return item.trim(); })
                .filter(function(item){ return !item.indexOf(marginName); });
            if (margin.length) {
                var marginValue = margin[0].split(':')[1].trim().replace('px','');
                return parseInt(marginValue, 10);
            }
        }
    },

    _onColorChange: function (value, type) {
        var isClass = /^(text|bg)-/ .test(value);
        if (isClass) {
            this._onChangeAttr("class", value, type === "background" ? this["background-color-class"] : this["font-color-class"]);
        } else {
            var attributeName = type === "background" ? 'background-color' : 'color';
            this._onChangeAttr("style", attributeName + ':' + value, this[attributeName]);
        }
    },

    _onChangeClasses: function (e) {
        e.preventDefault();
        var newAttrs = { class : e.target.value };
        this.trigger_up('update_report', {
            node: this.node,
            operation: {
                type: 'attributes',
                newAttrs: newAttrs,
            },
        });
    },

    _onCustomColor: function (e) {
        var self = this;
        e.preventDefault();
        const colorpicker = new ColorpickerDialog(this, {
            defaultColor: 'rgb(255, 0, 0)',
        });
        colorpicker.on('colorpicker:saved', this, (ev) => {
            var color = ev.data.cssColor;
            var $button = $('<button/>');
            $button.attr('data-color', color);
            $button.attr('data-value', color);
            $button.css('background-color', color);
            $(e.target).closest('.dropdown-item').find('.web_no_code_studio_custom_colors').append($button);
            $button.mousedown();
            self._onColorChange(color, $(e.currentTarget).data('font'));
        });
        colorpicker.open();
    },

    _onChangeFont: function (e) {
        e.preventDefault();
        this._onChangeAttr('class', $(e.currentTarget).data('value'), this.fontSize);
    },

    _onChangeTable: function (e) {
        e.preventDefault();
        this._onChangeAttr("class", e.target.value, this.tableStyle);
    },

    _onChangeMargin: function (e) {
        e.preventDefault();
        var toRemove, toAdd;
        if (e.target.value !== "") {
            toAdd = e.target.dataset.margin + ':' + e.target.value + 'px';
        }
        if (this[e.target.dataset.margin]) {
            toRemove = e.target.dataset.margin + ':' + this[e.target.dataset.margin] + 'px';
        }
        this._onChangeAttr("style", toAdd, toRemove);
    },

    _onChangeTextAlign : function(e) {
        e.preventDefault();
        var data = $(e.currentTarget).data();
        var toAdd = this.alignment !== data.property ? data.property : null;
        this._onChangeAttr("class", toAdd, this.alignment);
    },

    _onChangeTextDec : function(e) {
        e.preventDefault();
        var data = $(e.target).closest("button").data();
        this._onChangeAttr("class",
            !this[data.property] && ("o_" + data.property),
            this[data.property] && ("o_" + data.property));
    },

    _onChangeWidth: function(e) {
        e.preventDefault();
        var addDisplayInlineBlock = "";
        var hasDisplay = _.any((this.node.attrs.style || '').split(';'), function (item) {
            return _.str.startsWith(item, 'display');
        });
        if (this.node.tag.toLowerCase() === 'span' && !hasDisplay) {
            addDisplayInlineBlock = ";display:inline-block";
        }
        this._onChangeAttr("style", e.target.value && ("width:" + e.target.value + "px" + addDisplayInlineBlock), this.originalWidth);
    }
});

var t_field = MainComponent.extend({
    name: 'tfield',
    template : 'openeducat_web_no_code_studio.report_data_attr_data_t_field',
    selector: '[t-field]',
    init: function () {
        this._super.apply(this, arguments);
        this.directiveFields['t-field'] = {
            type: 'related',
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            var $expr = self.$('.web_no_code_studio_tfield_fieldexpression');
            return self.fieldSelector['t-field'].appendTo($expr);
        });
    },
});

var t_if = MainComponent.extend({
    name: 'tif',
    template : 'openeducat_web_no_code_studio.report_data_attr_t_if',
    selector: '',
    events: _.extend({}, MainComponent.prototype.events, {
        "click .field_domain_dialog_button": "_onDialogEditButtonClick",
    }),
    init: function () {
        this._super.apply(this, arguments);
        this.directiveFields['t-if'] = {
            type: 'char',
        };
    },

    start: function () {
        var self = this;
        try {
            Domain.prototype.conditionToDomain(this.node.attrs['t-if'] || '');
        } catch (e) {
            this.$('.field_domain_dialog_button').hide();
        }
        return this._super.apply(this, arguments).then(function () {
            return self.fieldSelector['t-if'].appendTo(self.$('.web_no_code_studio_tif_ifexpression'));
        });
    },

    _onDialogEditButtonClick: function (e) {
        e.preventDefault();
        var self = this;
        var availableKeys = this._getContextKeys(this.node);
        var defaultDoc = _.findWhere(availableKeys, {relation: this.context.docs, type: 'many2one'});
        defaultDoc = defaultDoc && defaultDoc.name || _.first(availableKeys).name;
        var value = Domain.prototype.conditionToDomain(this.node.attrs['t-if'] || '');
        var dialog = new DomainSelectorDialog(this, 'record_fake_model', value, {
            readonly: this.mode === "readonly",
            debugMode: config.isDebug(),
            fields: availableKeys,
            default: [[defaultDoc, '!=', false]],
            operators: ["=", "!=", ">", "<", ">=", "<=", "in", "not in", "set", "not set"],
        }).open();
        dialog.on("domain_selected", this, function (e) {
            var condition = Domain.prototype.domainToCondition(e.data.domain);
            self.$('input').val(condition === 'True' ? '' : condition).trigger('change');
        });
    },

    _onFieldChange: function (e) {
        if (e.target.name === "t-if") {
            return this._super.apply(this, arguments);
        }
        e.stopPropagation();
    }
});

var t_else = MainComponent.extend({
    name: 'telse',
    template : 'openeducat_web_no_code_studio.report_data_attr_data_t_else',
    selector: '[t-else]',
    insertAsLastChildOfPrevious: true,
    init: function () {
        this._super.apply(this, arguments);
        if(this.node.parent){
            if(this.node.parent.children[this.node.parent.children.indexOf(this.node) - 1]){
                this.tIf = this.node.parent.children[this.node.parent.children.indexOf(this.node) - 1].attrs['t-if'];
            }
        }
        this.directiveFields['t-else'] = {
            type: 'boolean',
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return self.fieldSelector['t-else'].appendTo(self.$('.web_no_code_studio_telse_elseexpression'));
        });
    },

    _onUpdateView: function (newAttrs) {
        this.trigger_up('update_report', {
            node: this.node,
            operation: {
                type: 'attributes',
                newAttrs: {
                    't-else': newAttrs['t-else'] ? 'else' : null,
                },
            },
        });
    },

});

var t_esc = MainComponent.extend({
    name: 'tesc',
    template : 'openeducat_web_no_code_studio.report_data_attr_data_t_esc',
    selector: '[t-esc]',
    init: function () {
        this._super.apply(this, arguments);
        this.directiveFields['t-esc'] = {
            type: 'related',
            freecode: true,
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return self.fieldSelector['t-esc'].appendTo(self.$('.web_no_code_studio_tesc_escexpression'));
        });
    },
});

var t_set = MainComponent.extend({
    name: 'tset',
    template : 'openeducat_web_no_code_studio.report_data_attr_data_t_set',
    selector: '[t-set]',
    init: function () {
        this._super.apply(this, arguments);

        this.directiveFields['t-set'] = {
            type: 'char',
        };
        this.directiveFields['t-value'] = {
            type: 'related',
            freecode: true,
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return Promise.all([
                self.fieldSelector['t-set'].appendTo(self.$('.web_no_code_studio_tset_setexpression')),
                self.fieldSelector['t-value'].appendTo(self.$('.web_no_code_studio_tset_valueexpression'))
            ]);
        });
    },
});

var t_foreach = MainComponent.extend({
    name: 'tforeach',
    template : 'openeducat_web_no_code_studio.report_data_attr_data_t_foreach',
    debugSelector: '[t-foreach]',
    init: function () {
        this._super.apply(this, arguments);
        this.directiveFields['t-foreach'] = {
            type: 'related',
            freecode: true,
            loop: true,
        };
        this.directiveFields['t-as'] = {
            type: 'char',
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return Promise.all([
                self.fieldSelector['t-as'].appendTo(self.$('.web_no_code_studio_tas_asexpression')),
                self.fieldSelector['t-foreach'].appendTo(self.$('.web_no_code_studio_tforeach_foreachexpression'))
            ]);
        });
    },
});

var block_total = MainComponent.extend({
    name: 'blockTotal',
    template : 'openeducat_web_no_code_studio.block_total',
    selector: '.o_report_block_total',
    blacklist: 't, tr, td, th, small, span',
    init: function () {
        this._super.apply(this, arguments);
        this.directiveFields.total_amount_untaxed = {
            type: 'related',
            value: this.node.children[2].attrs['t-value'],
            filter: function (field) {
                return _.contains(['many2one', 'float', 'monetary'], field.type);
            },
            followRelations: function (field) {
                return field.type === 'many2one';
            },
        };
        this.directiveFields.total_currency_id = {
            type: 'related',
            value: this.node.children[0].attrs['t-value'],
            filter: function (field) {
                return field.type === 'many2one';
            },
            followRelations: function (field) {
                return field.type === 'many2one' && field.relation !== 'res.currency';
            },
        };
        this.directiveFields.total_amount_total = {
            type: 'related',
            value: this.node.children[1].attrs['t-value'],
            filter: function (field) {
                return _.contains(['many2one', 'float', 'monetary'], field.type);
            },
            followRelations: function (field) {
                return field.type === 'many2one';
            },
        };
        this.directiveFields.total_amount_by_groups = {
            type: 'related',
            value: this.node.children[3].attrs['t-value'],
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return Promise.all([
                self.fieldSelector.total_currency_id.appendTo(self.$('.web_no_code_studio_report_currency_id')),
                self.fieldSelector.total_amount_untaxed.appendTo(self.$('.web_no_code_studio_report_amount_untaxed')),
                self.fieldSelector.total_amount_total.appendTo(self.$('.web_no_code_studio_report_amount_total')),
                self.fieldSelector.total_amount_by_groups.appendTo(self.$('.web_no_code_studio_report_amount_by_groups'))
            ]);
        });
    },

    _onUpdateView: function (newAttrs) {
        this._tSetAttributes(newAttrs);
    },
});

var column = MainComponent.extend({
    name: 'column',
    template : 'openeducat_web_no_code_studio.report_data_attr_data_column',
    selector: 'div[class*=col-]',
    init: function () {
        this._super.apply(this, arguments);

        this.classes = (this.node.attrs.class || "").split(' ');
        this.sizeClass = _.find(this.classes, function (item) {
            return item.indexOf('col-') !== -1;
        }) || '';
        this.offsetClass = _.find(this.classes, function (item) {
            return item.indexOf('offset-') !== -1;
        }) || '';
        this.size = +this.sizeClass.split('col-')[1];
        this.offset = +this.offsetClass.split('offset-')[1];
        this.directiveFields.size = {
            type: 'integer',
            value: this.size,
        };
        this.directiveFields.offset = {
            type: 'integer',
            value: this.offset,
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return Promise.all([
                self.fieldSelector.size.prependTo(self.$('.web_no_code_studio_size')),
                self.fieldSelector.offset.prependTo(self.$('.web_no_code_studio_offset'))
            ]);
        });
    },

    _onUpdateView: function (newAttrs) {
        if ('size' in newAttrs && newAttrs.size >= 0) {
            this._onChangeAttr("class", 'col-' + newAttrs.size, this.sizeClass);
        } else if ('offset' in newAttrs && newAttrs.offset >= 0) {
            this._onChangeAttr("class", 'offset-' + newAttrs.offset, this.offsetClass);
        }
    },
});

var table = MainComponent.extend({
    selector: 'table.o_report_block_table',
    blacklist: 'thead, tbody, tfoot, tr, td[colspan="99"]',
});

var TextSelectorTags = 'span, p, h1, h2, h3, h4, h5, h6, blockquote, pre, small, u, i, b, font, strong, ul, li, dl, dt, ol, th, td';
var filter = ':not([t-field]):not(:has(t, [t-' + QWeb2.ACTIONS_PRECEDENCE.join('], [t-field], [t-') + ']))';
var text = MainComponent.extend({
    name: 'text',
    template : 'openeducat_web_no_code_studio.report_data_attr_data_text',
    selector: TextSelectorTags.split(',').join(filter + ',') + filter,
    blacklist: TextSelectorTags,
    custom_events: {
        wysiwyg_blur: '_onBlurWysiwygEditor',
    },
    init: function () {
        this._super.apply(this, arguments);
        this.$node = $(utils.json_node_to_xml(this.node));
        this.$node.find('*').add(this.$node).each(function () {
                var node = this;
                _.each(Array.prototype.slice.call(node.attributes), function (attr) {
                    if (!attr.name.indexOf('data-oe-')) {
                        node.removeAttribute(attr.name);
                    }
                });
            });
        this.directiveFields.text = {
            type: 'text',
            value: utils.xml_to_str(this.$node[0]).split('>').slice(1).join('>').split('</').slice(0, -1).join('</'),
        };
    },

    start: function () {
        var self = this;
        return this._super.apply(this, arguments)
            .then(function () {
                return self.fieldSelector.text.appendTo(self.$('.web_no_code_studio_text'));
            }).then(function () {
                return self._startWysiwygEditor();
            });
    },

    _onBlurWysiwygEditor: function () {
        var self = this;
        return this.wysiwyg.save().then(function (result) {
            if (result.isDirty) {
                self._onUpdateView({text: result.html});
            }
        });
    },

    _startWysiwygEditor: function () {
        var self = this;
        const options = {
            lang: "odoo",
            recordInfo: {context: this.context},
            value: this.directiveFields.text.value,
            resizable: true,
            toolbarTemplate: 'web_no_code_studio.report_sidebar.web_editor_toolbar',
        };
        this._createWysiwygInstance();
//        return this.wysiwyg.insertAfter(this.$textarea);
    },

    _createWysiwygInstance: async function () {
        const Wysiwyg = await wysiwygLoader.getWysiwygClass();
        this.wysiwyg = new Wysiwyg(this, this._getWysiwygOptions());
        this.$textarea = this.$('textarea:first').val(this.directiveFields.text.value);

        this.$textarea.after(this.$wysiwygWrapper);
        this.$textarea.hide();

        this.$textarea.off().on('input', function (e) { // to test simple
            e.preventDefault();
            e.stopImmediatePropagation();
            self.wysiwyg.setValue($(this).val());
            self.wysiwyg.trigger_up('wysiwyg_blur');
        });
        return this.wysiwyg.insertAfter(this.$textarea);
    },

    _getWysiwygOptions: function(){
        const options = {
            lang: "odoo",
            recordInfo: {context: this.context},
            value: this.directiveFields.text.value,
            resizable: true,
            toolbarTemplate: 'web_no_code_studio.report_sidebar.web_editor_toolbar',
        };
        return options;
    },

    _onUpdateView: function (newAttrs) {
        var node = this.node;
        var $node = this.$node.clone().html(newAttrs.text);
        var xml = utils.xml_to_str($node[0]).replace(/ xmlns="[^"]+"/, "");
        this.trigger_up('update_report', {
            node: node,
            operation: {
                inheritance: [{
                    content: xml,
                    position: "replace",
                    view_id: +node.attrs['data-oe-id'],
                    xpath: node.attrs['data-oe-xpath']
                }],
            },
        });
    },
});

var image = layout_editable.extend({
    name: 'image',
    template: 'openeducat_web_no_code_studio.report_data_attr_data_image',
    selector: 'img',
    init: function() {
        this._super.apply(this, arguments);
        this.directiveFields.src = {
            type: 'text', value: this.node.attrs.src
        };
    },

    start: function() {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return self.fieldSelector.src.appendTo(self.$('.web_no_code_studio_source'));
        });
    },
});

var groups = MainComponent.extend({
    name: 'groups',
    template: 'openeducat_web_no_code_studio.report_data_attr_data_groups',
    insertAsLastChildOfPrevious: true,

    init: function () {
        this._super.apply(this, arguments);

        var groups = this.node.attrs.editor_groups && JSON.parse(this.node.attrs.editor_groups);
        this.directiveFields.groups = {
            name: 'groups',
            fields: [{
                name: 'id',
                type: 'integer',
            }, {
                name: 'display_name',
                type: 'char',
            }],
            value: groups,
            relation: 'res.groups',
            type: 'many2many',
            Widget: 'many2many_tags',
        };
    },

    start: function() {
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            return self.fieldSelector.groups.appendTo(self.$('.web_no_code_studio_groups'));
        });
    },
});


registry
    .add('column', column)
    .add('groups', groups)
    .add('layout', layout_editable)
    .add('image', image)
    .add('table', table)
    .add('text', text)
    .add('total', block_total)
    .add('tEsc', t_esc)
    .add('tElse', t_else)
    .add('tField', t_field)
    .add('tForeach', t_foreach)
    .add('tIf', t_if)
    .add('tSet', t_set);

return {
    block_total: block_total,
    column: column,
    groups: groups,
    image: image,
    layout_editable: layout_editable,
    table: table,
    text: text,
    t_field: t_field,
    t_foreach: t_foreach,
    t_else: t_else,
    t_esc: t_esc,
    t_if: t_if,
    t_set: t_set,
    registry: registry,
};

});
