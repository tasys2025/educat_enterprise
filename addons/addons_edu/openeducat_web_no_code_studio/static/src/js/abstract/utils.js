/** @odoo-module */

export function extendEnv(env, extension) {
    const nextEnv = Object.create(env);
    const descrs = Object.getOwnPropertyDescriptors(extension);
    Object.defineProperties(nextEnv, descrs);
    return Object.freeze(nextEnv);
}
