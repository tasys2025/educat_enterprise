odoo.define('openeducat_web_no_code_studio.Elements', function(require){
    'use strict';

    var DraggableElement = require('openeducat_web_no_code_studio.DraggableElement');
    var NewFieldElement = require('openeducat_web_no_code_studio.NewFieldElement');
    var Registry = require('web.Registry');
    var web_no_code_studio_elements_registry = new Registry();

    var newFields = [],
        formElements = [],
        searchElements = [];

    var ExistingField = DraggableElement.extend({
        init: function (parent, name, field_description, type, store) {
            this._super(parent);
            this.structure = 'field';
            this.label = field_description;
            this.description = name;
            this.className = 'web_no_code_studio_field_type_' + type;
            this.type = type;
            this.store = store;
        },
        start: function () {
            this.$el.data('new_attrs',{
                name: this.description,
                label: this.label,
                type: this.type,
                store: this.store ? "true":"false",
            });
            this.$el.attr("title", this.label);
            return this._super.apply(this, arguments);
        },
    });

    formElements.push(
        DraggableElement.extend({
            structure: 'notebook',
            label: 'Tabs',
            type: 'tabs',
            className: 'web_no_code_studio_field_notebook',
        }),
        DraggableElement.extend({
            structure: 'group',
            label: 'Columns',
            type: 'columns',
            className: 'web_no_code_studio_field_group',
        })
    );

    searchElements.push(
        DraggableElement.extend({
            structure: 'filter',
            label: 'Filter',
            type: 'filter',
            node: {
                tag: 'filter'
            },
            new_attrs: {
                label: 'Filter',
            },
            className: 'web_no_code_studio_field_filter',
        }),
        DraggableElement.extend({
            structure: 'separator',
            label: 'Separator',
            type: 'separator',
            className: 'web_no_code_studio_field_separator',
        })
    );

    newFields.push(
        NewFieldElement.extend({
            type: 'char',
            label: 'Character',
            className: 'web_no_code_studio_field_char',
        }),
        NewFieldElement.extend({
            type: 'date',
            label: 'Date',
            className: 'web_no_code_studio_field_date',
        }),
        NewFieldElement.extend({
            type: 'datetime',
            label: 'Date And Time',
            className: 'web_no_code_studio_field_datetime',
        }),
        NewFieldElement.extend({
            type: 'selection',
            label: 'Selection',
            className: 'web_no_code_studio_field_selection',
        }),
        NewFieldElement.extend({
            type: 'monetary',
            label: 'Monetary',
            className: 'web_no_code_studio_field_monetary',
        }),
        NewFieldElement.extend({
            type: 'related',
            label: 'Related',
            className: 'web_no_code_studio_field_related',
        }),
        NewFieldElement.extend({
            type: 'binary',
            label: 'Binary',
            className: 'web_no_code_studio_field_binary',
        }),
        NewFieldElement.extend({
            type: 'many2many',
            label: 'Many To Many',
            className: 'web_no_code_studio_field_many2many',
        }),
        NewFieldElement.extend({
            type: 'one2many',
            label: 'One To Many',
            className: 'web_no_code_studio_field_one2many',
        }),
        NewFieldElement.extend({
            type: 'many2one',
            label: 'Many To One',
            className: 'web_no_code_studio_field_many2one',
        }),
        NewFieldElement.extend({
            type: 'text',
            label: 'Text',
            className: 'web_no_code_studio_field_text',
        }),
        NewFieldElement.extend({
            type: 'boolean',
            label: 'Boolean',
            className: 'web_no_code_studio_field_boolean',
        }),
        NewFieldElement.extend({
            type: 'integer',
            label: 'Integer',
            className: 'web_no_code_studio_field_integer',
        }),
        NewFieldElement.extend({
            type: 'float',
            label: 'Float',
            className: 'web_no_code_studio_field_float',
        }),
        NewFieldElement.extend({
            type: 'html',
            label: 'Html',
            className: 'web_no_code_studio_field_html',
        }),
    );

    web_no_code_studio_elements_registry
        .add('formElement', formElements)
        .add('searchElement', searchElements)
        .add('newField', newFields)
        .add('existingField',ExistingField)

    return web_no_code_studio_elements_registry;

});