odoo.define('openeducat_web_no_code_studio.EditTab', function(require){
    'use strict';

    var config = require('web.config');
    var core = require('web.core');
    var ajax = require('web.ajax');
    var Dialog = require('web.Dialog');
    var form_common = require('web.view_dialogs');
    var CreateNewMenu = require('openeducat_web_no_code_studio.CreateNewMenu');
    var _t = core._t;
    var bus = require('openeducat_web_no_code_studio.bus');
    const { useService } = require("@web/core/utils/hooks");

    var EditTabPopUp = Dialog.extend({
        template: 'openeducat_web_no_code_studio.EditMenu.tab',
        events: _.extend({}, Dialog.prototype.events, {
            'click button.menu_tab_edit': '_editTabMenu',
            'click button.menu_tab_remove': '_removeTabMenu',
        }),
        init: function(parent, menus, current_primary_menu, scrollToBottom){
            var options = {
                title: _t('Edit Menu'),
                size: 'medium',
                dialogClass: 'web_no_code_studio_menu_tab_popup',
                buttons: [{
                    text: _t("Confirm"),
                    classes: 'btn-primary',
                    click: this._onSaveButton.bind(this),
                }, {
                    text: _t("Cancel"),
                    close: true,
                }, {
                    icon: 'fa-plus-circle',
                    text: _t("New Menu"),
                    classes: 'btn-secondary js_add_tab_menu ms-auto',
                    click: this._onMenuAdd.bind(this),
                }],
            };
            this.current_primary_menu = current_primary_menu;
            this.base_menus = this._computeMenus(menus);
            this.scrollToBottom = scrollToBottom;
            this.can_delete = [];
            this.can_move = {};
            this._super(parent, options);
        },

        start: function () {
            this.$('.oe_menu_editor').nestedSortable({
                listType: 'ul',
                handle: 'div',
                items: 'li',
                maxLevels: 5,
                toleranceElement: '> div',
                forcePlaceholderSize: true,
                opacity: 0.6,
                placeholder: 'oe_menu_placeholder',
                tolerance: 'pointer',
                attribute: 'data-menu-id',
                expression: '()(.+)',
                //relocate: this.moveMenu.bind(this),
                rtl: _t.database.parameters.direction === "rtl",
            });
            this.opened().then(() => {
                if (this.scrollToBottom) {
                    this.$el.scrollTop(this.$el.prop('scrollHeight'));
                }
            });
            return this._super.apply(this, arguments);
        },

        _removeTabMenu: function(e){
            var $remove_menu = $(e.currentTarget).closest('[data-menu-id]');
            var menu_id = $remove_menu.data('menu-id') || 0;
            if (menu_id) {
                this.can_delete.push(menu_id);
            }
            $remove_menu.remove();
        },

        _computeMenus: function(menus){
            var self = this;
            var computeMenus = menus.childrenTree.filter(function (el) {
                return el['id'] ;
//                return el.id === self.current_primary_menu;
            });
            return computeMenus;
        },

        _refreshMenus: function(keepOpen, scrollToBottom){
            bus.trigger('refresh_menus', {
                keepOpen:keepOpen,
                scrollToBottom:scrollToBottom,
            });
        },

        _onMenuAdd: function(e){
            e.preventDefault();
            var self = this;
            var state = $.bbq.getState();
            new CreateNewMenu(this,this.current_primary_menu).open();
        },

        _editTabMenu: function (e) {
            var self = this;
            var menu_id = $(e.currentTarget).closest('[data-menu-id]').data('menu-id');
            new form_common.FormViewDialog(this, {
                res_model: 'ir.ui.menu',
                res_id: menu_id,
                on_saved: function () {
                    self._doChanges().then(function () {
                        self._refreshMenus(true);
                    });
                },
            }).open();

        },

        _onSaveButton: function(){
            var self = this;
            const $menus = this.$("[data-menu-id]");
            if (!$menus.length) {
                return Dialog.alert(self, _t('You cannot remove all the menu items of an app.\r\nTry uninstalling the app instead.'));
            }
            if (!_.isEmpty(this.can_move) || !_.isEmpty(this.can_delete)) {
                this._doChanges().then(function () {
                    //Todo: Reload Menu Data
                    self._refreshMenus();
                });
            } else {
                this.close();
            }
        },

        _doChanges: function () {
            return ajax.jsonRpc('/openeducat_web_no_code_studio/menu/edit','call',{
                    can_move: this.can_move,
                    can_delete: this.can_delete,
            });
        },
    });

    return EditTabPopUp;
});