odoo.define('openeducat_web_no_code_studio.ActionManager', function (require) {
"use strict";

//    var ActionManager = require('web.ActionManager');
    var AbstractAction = require('web.AbstractAction');
    var bus = require('openeducat_web_no_code_studio.bus');
    var core = require("web.core");

var ActionManager = AbstractAction.extend({
        init: function () {
            this._super.apply(this, arguments);
            this.editorId = undefined;
            this.env.bus.on('editor_toggled', this, this._editorChange);
            bus.on('editor_mode_started', this, this._editorModeStarted);
            bus.on('get_last_action', this, this.getEditorLastAction);
        },

        willStart: function() {
            var self = this;
            return $.when(ajax.loadLibs(this), this._super());
        },

        _editorChange: function(state){
            if(state){
                this.editorId = this.controllerStack.length - 1;
            }
        },

        _getControllerStackIndex: function (options) {
            if (options.editor_clear_breadcrumbs) {
                return this.editorId + 1;
            }
            return this._super.apply(this, arguments);
        },

        _editorModeStarted: function(type){
            this.editorViewType = type == 'search' ? false : type;
        },

        getEditorLastAction: function () {
            var self = this;
            var currentControllerID = _.last(this.controllerStack);
            var controller = currentControllerID ? this.controllers[currentControllerID] : null;
            var validController = controller ? this.actions[controller.actionID] : null;
            if(validController){
                if(validController.tag == "web_no_code_studio_action"){
                    var actionList = Object.keys(this.actions);
                    var actionIndex = _.findIndex(actionList, function(val){
                        return val == validController.controllerID;
                    });
                    var action = this.actions[actionList[actionList.length - 2]];
                    var index = _.findIndex(this.controllerStack, function(controllerID) {
                        var controller = self.controllers[controllerID];
                        return controller.actionID === action.jsID;
                    });
                    var options = {
                        additional_context: action.context,
                        index: index,
                        viewType: this.editorViewType,
                        target: 'main'
                    };
                    action.target = action.target ? action.target : 'current';
                    return this._executeAction(action, options);
                } else {
                    return validController;
                }
            }
            var editorIndex = this.editorId;
            var controllerID = this.controllerStack[editorIndex];
            var controller = this.controllers[controllerID];
            var action = this.actions[controller.actionID];

            var index = _.findIndex(this.controllerStack, function(controllerID) {
                var controller = self.controllers[controllerID];
                return controller.actionID === action.jsID;
            });

            this.editorId = undefined;

            var options = {
                additional_context: action.context,
                index: index,
                viewType: this.editorViewType,
            };
            if (this.editorViewType === 'form') {
                if (controller.widget) {
                    options.resID = controller.widget.exportState().currentId;
                }
            }
            return {action: action.id, options: options};
        },

        getCurrentEditorAction: function(){
            var controller = this.getCurrentEditorController();
            return controller ? this.actions[controller.actionID] : null;
        },

        getCurrentEditorController: function(){
            var controllerID = this.controllerStack[this.editorId];
            var controller = this.controllers[controllerID];
            return controller;
        },

    });
    core.action_registry.add("ActionManager", ActionManager);
    return ActionManager;
});
