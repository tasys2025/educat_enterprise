/** @odoo-module **/

const { hooks } = owl;
const { Component } = owl;
const ServicesMixin = require('web.ServicesMixin');
const Menu = require('@openeducat_web_no_code_studio/js/menu');
const { getBundle, loadBundle } = require('@web/core/assets');
import { loadWysiwyg } from "web_editor.loader";
var ajax = require('web.ajax');
var core = require("web.core");
var form_common = require('web.view_dialogs');
var bus = require('openeducat_web_no_code_studio.bus');
var CreateNewApp = require('openeducat_web_no_code_studio.CreateNewApp');
var ActionManager = require('openeducat_web_no_code_studio.ActionManager');
var ViewEditorSystrayMenu = require('openeducat_web_no_code_studio.systray_menu');

import session from 'web.session';
import framework from 'web.framework';
import { WebClient } from "@web/webclient/webclient";
import EditTab from "openeducat_web_no_code_studio.EditTab";
import { StudioNavBar } from '@openeducat_web_no_code_studio/js/menu';
import { useBus, useEffect, useService } from "@web/core/utils/hooks";
import { actionService } from "@web/webclient/actions/action_service";
import { computeAppsAndMenuItems } from "@web/webclient/menus/menu_helpers";
import { WebClientTheme } from "@openeducat_backend_theme/js/edu/web_client";
const { onMounted } = owl;

export class WebClientStudio extends WebClientTheme {
        setup() {
            this.menuService = useService("menu");
            var self = this;
            this.viewEditorOpen = false;
            super.setup();
            onMounted(() => {
                core.bus.on('click_web_no_code_studio', this, this._onClickViewEditor);
                bus.on('switch_editor_view', this, this._onSwitchViewEditor);
                bus.on('refresh_menus', this, this._onRefreshMenus);
                bus.on('switch_menu_action', this, this._onSwitchMenuAction);
                bus.on('edit_menu', this, this._onClickEditMenu);
                this.env.bus.on("app_menu_toggled", this, (toggle) => {
                    this.toggleAppMenu(toggle)
                });

            });
        }

        toggleAppMenu(toggle){
            var self = this
            $('.create_new_app').remove();
            const $html = $(core.qweb.render('openeducat_web_no_code_studio.searchMenu.template'));
            $('.o_apps_container').append($html);
            $html.on('click', function(e){
                new CreateNewApp(self).open();
            });
        }

        _onClickEditMenu(menu_id){
            new form_common.FormViewDialog(this, {
                res_model: 'ir.ui.menu',
                res_id: menu_id,
                on_saved: function () {
                    self._doChanges().then(function () {
                        self._refreshMenus(true);
                    });
                },
            }).open();
        }

        async _onRefreshMenus(data){
            data = data || {};
            bus.trigger('clear_cache');

            var allMenus = await this.menuService.reload();
            window.location.reload();
            // Todo: Refresh Menus
        }

        async _openViewEditor(){
            //$(".edit_tab_menu").removeClass("d-none");
            $(".o_menu_brand").css("pointer-events", "none");
            $(".full").css("pointer-events", "none");
            $(".o_navbar_apps_menu").css("pointer-events", "none");
            $(".o_menu_sections").children().css("pointer-events", "none");
            $(".o_menu_entry_lvl_1").css("pointer-events", "none");
            const xmlids = ['web_editor.assets_wysiwyg'];
            for (const xmlid of xmlids) {
                const assets = await getBundle(xmlid);
                await loadBundle(assets);
            }
            //Todo: Check If In Proper Place
            this.env.bus.trigger('editor_toggled', true);
            this.env.bus.trigger('some-event',true);
            const qs = $.deparam.querystring();
            if(this.ViewCanBeActivate){
                framework.blockUI();
                qs.editor = 'editor';
                session.user_context.editor = 1;
                this.viewEditorOpen = true;
                var controller = this.actionService.currentController;
                if(controller.props.type == 'kanban'){
                    controller.props.type = 'form'
                }
                this.ActionManager = controller;
                var params = {
                    action: controller.action,
                    controller: Object.assign({}, controller.getLocalState()),
                    viewType: controller.props.type,
                }
                await this._openViewEditorManager(params);
                framework.unblockUI();
            }else{
                delete session.user_context.editor;
                delete qs.editor;
            }
            const { protocol, host, pathname, hash } = window.location;
            const url = `${protocol}//${host}${pathname}?${$.param(qs)}${hash}`;
            window.history.pushState({ path: url }, '', url);
        }

        _openViewEditorManager(params){
            const actionParams = { additionalContext: params };
            return this.actionService.doAction('web_no_code_studio_action', actionParams);
        }

        _openViewEditorReport(ev){
            var params = _.extend({}, ev.data, {
                action: ev.data.action,
            });
            return this.do_action('web_no_code_studio_action',params);
        }

        _onSwitchViewEditor(ev){
            var action = this.actionService.currentController.action;
            var controller = this.actionService.currentController;
            var params = _.extend({}, ev.data, {
                action: ev.action,
            });
            if (controller.widget) {
                params.controllerState = controller.widget.exportState();
            }
            this._openViewEditorManager(params);
        }

        _onSwitchMenuAction(action,options){
           return this.env.services.action.doAction(action,{
                viewType: action.viewType,
                clearBreadcrumbs: true,
                additionalContext: action.context,
            })
        }

        async _toggleSystrayMenuActive(){
            var state = $.bbq.getState();
            var activate;
//            if((state.hasOwnProperty('home') && state.home == 'apps') || (Object.keys(state).length === 1 && Object.keys(state)[0] === "cids")){
            if(state.action == 'apps_menu'){
                this.ViewCanBeActivate = 'create_app';
            }else if(state.model == 'res.config.settings'){
                this.ViewCanBeActivate = false;
            }else{
                this.ViewCanBeActivate = true;
            }
//            await this._setSystrayMenu();
//            if(this.viewEditorSystrayMenu){
//                this.viewEditorSystrayMenu._activateToggle(this.ViewCanBeActivate);
//            }
        }

        async _setSystrayMenu(){
            this.viewEditorSystrayMenu = this.menuService.systray_menu.widgets.find(widget => widget instanceof ViewEditorSystrayMenu);
        }

        on_hashchange(event){
            this._super.apply(this, event);
            this._toggleSystrayMenuActive();
        }

        on_custom_clicked(){
            this._super.apply(this, arguments);
            this._toggleSystrayMenuActive();
        }

        _openMenu(action, options) {
            var self = this;
            if (this.viewEditorOpen) {
                // tag the action for the actionManager
                action.editor = true;
            }
            return this._super.apply(this, arguments).then(function () {
                if (self.viewEditorOpen) {
                }
            });
        }

        _closeViewEditor(){
            this._toggleSystrayMenuActive();
            var action = this.ActionManager.action;
            this.env.services.action.doAction(action.id, {
                viewType: action.viewType,
                clearBreadcrumbs: true,
                additionalContext: action.context,
            },)

            bus.trigger('get_last_action', false);
            const { protocol, host, pathname, hash } = window.location;
            const qs = $.deparam.querystring();
            delete qs.editor;
            const url = `${protocol}//${host}${pathname}?${$.param(qs)}${hash}`;
            window.history.pushState({ path: url }, '', url);
            $('.o_menu_brand').css('pointer-events','');
            $('.full').css('pointer-events','all');
            $(".o_navbar_apps_menu").css('pointer-events', '');
            $(".o_menu_sections").children().css('pointer-events', '');
            $(".o_menu_entry_lvl_1").css('pointer-events', '');
            //$(".edit_tab_menu").addClass("d-none");
            this._updateEditorSystrayMenu();
        }

        // On Clicking View Editor It Triggers Here
        async _onClickViewEditor(){
            await this._toggleSystrayMenuActive();
            if( this.ViewCanBeActivate == "create_app"){
                await this._startNewAppMenu();
            } else if(this.ViewCanBeActivate && this.viewEditorOpen == false){
                await this._openViewEditor();
            } else {
                await this._closeViewEditor();
            }
        }

        async _startNewAppMenu(){
            this.env.bus.trigger('apps_menu_started', this.ViewCanBeActivate);
        }

        async _updateEditorSystrayMenu(){
            var state = $.bbq.getState();
//            await this._setSystrayMenu();
            this.viewEditorOpen = false;
//            this.menuService.toggleViewEditor(false);
                this.env.bus.trigger('editor_toggled', false);
//            if(state.model == 'res.config.settings'){
//                this.viewEditorSystrayMenu._activateToggle(false);
//            }else{
//                this.viewEditorSystrayMenu._activateToggle(true);
//            }
            framework.unblockUI();
        }

    };
WebClientStudio.components = { ...WebClientTheme.components,Menu, StudioNavBar, ActionManager};

//});
