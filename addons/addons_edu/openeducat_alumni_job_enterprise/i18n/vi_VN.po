# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_alumni_job_enterprise
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 10:09+0000\n"
"PO-Revision-Date: 2022-12-12 10:09+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
msgid "<em class=\"font-weight-normal text-muted\">Created By:</em>"
msgstr "không"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
msgid "<small class=\"text-muted\">Alumni Job Post -</small>"
msgstr "<small class=\"text -muted\"> cựu sinh viên bài viết công việc -</small>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
msgid ""
"<strong>\n"
"                                                Employment Type:\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Loại việc làm:\n"
"                                            </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid ""
"<strong>\n"
"                                                Estimated New\n"
"                                                <br/>\n"
"                                                Employees:\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Ước tính mới\n"
"                                                <br/>\n"
"                                                Người lao động:\n"
"                                            </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
msgid ""
"<strong>\n"
"                                                Payable At:\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Phải trả:\n"
"                                            </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
msgid ""
"<strong>\n"
"                                                Skills:\n"
"                                            </strong>"
msgstr ""
"<strong>\n"
"                                                Kỹ năng:\n"
"                                            </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "<strong>Address:</strong>"
msgstr "<strong> Địa chỉ: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
msgid "<strong>Country:</strong>"
msgstr "<strong> Quốc gia: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
msgid "<strong>Description:</strong>"
msgstr "<strong> Mô tả: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid ""
"<strong>Description</strong>\n"
"                                        <span style=\"float: right;text-align: right;\">:</span>"
msgstr ""
"<strong> Mô tả </strong>\n"
"                                        <span style=\"float: right; text-align: right;\">: </span>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "<strong>End Date:</strong>"
msgstr "<strong> Ngày kết thúc: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "<strong>Job Position:</strong>"
msgstr "<strong> Vị trí công việc: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "<strong>Salary From:</strong>"
msgstr "<strong> tiền lương từ: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "<strong>Salary Upto:</strong>"
msgstr "<strong> tiền lương cho đến: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "<strong>Start Date:</strong>"
msgstr "<strong> Ngày bắt đầu: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
msgid "<strong>State:</strong>"
msgstr "<strong> State: </strong>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Address Detail"
msgstr "Địa chỉ chi tiết"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "All"
msgstr "All"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Alumni"
msgstr "Cựu sinh viên"

#. module: openeducat_alumni_job_enterprise
#: model:ir.ui.menu,name:openeducat_alumni_job_enterprise.menu_op_alumni_job
#: model:openeducat.portal.menu,name:openeducat_alumni_job_enterprise.poratl_menu_alumni_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_my_home_menu_alumni_job
msgid "Alumni Job"
msgstr "Công việc cựu sinh viên"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
msgid "Alumni Job List"
msgstr "Danh sách công việc của cựu sinh viên"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Alumni Job Post"
msgstr "Bài viết công việc của cựu sinh viên"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
msgid "Are you sure you want to delete this record ?"
msgstr "Bạn có chắc bạn muốn xóa bản ghi này không?"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
msgid "Close"
msgstr "Đóng"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
#, python-format
msgid "Created By"
msgstr "Được tạo bởi"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Created By Alumni"
msgstr "Được tạo bởi cựu sinh viên"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Created By Placement"
msgstr "Được tạo theo vị trí"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Created by:"
msgstr "Được tạo bởi:"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
msgid "Delete"
msgstr "Xóa bỏ"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
msgid "Edit"
msgstr "Chỉnh sửa"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Employment Type:"
msgstr "Loại việc làm:"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
#, python-format
msgid "End Date"
msgstr "Ngày cuối"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
msgid "Job Position"
msgstr "Vị trí công việc"

#. module: openeducat_alumni_job_enterprise
#: model:ir.actions.act_window,name:openeducat_alumni_job_enterprise.alumni_job_view_act
msgid "Job Post"
msgstr "Bài công việc"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Job description"
msgstr "Mô tả công việc"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
msgid "Job(s) Create by Others"
msgstr "(Các) công việc tạo ra bởi những người khác"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
msgid "Job(s) Create by You"
msgstr "(Các) công việc tạo ra bởi bạn"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "MM/DD/YYYY"
msgstr "Mm/dd/yyyy"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Monthly"
msgstr "Hàng tháng"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
#, python-format
msgid "Name"
msgstr "Tên"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "None"
msgstr "Không có"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
msgid "OK"
msgstr "ĐƯỢC RỒI"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Offer"
msgstr "Lời đề nghị"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.porta_alumni_list_details
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Other Information"
msgstr "Thông tin khác"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
#, python-format
msgid "Payable At"
msgstr "Phải trả"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Payable At Monthly"
msgstr "Phải trả hàng tháng"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Payable At Weekly"
msgstr "Phải trả hàng tuần"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Payable At Yearly"
msgstr "Phải trả hàng năm"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Payable At:"
msgstr "Phải trả:"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Placement"
msgstr "Vị trí"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Required Employe Number"
msgstr "Số lượng sử dụng được yêu cầu"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid ""
"SUBMIT\n"
"                                    <span class=\"fa fa-long-arrow-right\"/>"
msgstr ""
"GỬI ĐI\n"
"                                    <span class=\"fa fa-long-arrow-right\"/>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
msgid "Salary From"
msgstr "Tiền lương từ"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
msgid "Salary Upto"
msgstr "Mức lương cho đến"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in All"
msgstr "Tìm kiếm trong tất cả"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in Created By"
msgstr "Tìm kiếm trong Tạo bởi"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in End Date"
msgstr "Tìm kiếm trong Ngày kết thúc"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in Job Post"
msgstr "Tìm kiếm trong Tin tuyển dụng"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in Payable At"
msgstr "Tìm kiếm trong Khoản phải trả Tại"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in Salary From"
msgstr "Tìm kiếm trong Lương Từ"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in Salary Upto"
msgstr "Tìm kiếm trong Mức lương tối đa"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search in Start Date"
msgstr "Tìm kiếm trong Ngày bắt đầu"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Search<span class=\"nolabel\"> (in sequence)</span>"
msgstr "Tìm kiếm<span class=\"nolabel\"> (theo trình tự)</span>"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Select Country"
msgstr "Chọn quốc gia"

#. module: openeducat_alumni_job_enterprise
#. odoo-javascript
#: code:addons/openeducat_alumni_job_enterprise/static/src/xml/custom.xml:0
#, python-format
msgid "Select State"
msgstr "Chọn tiểu bang"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Select your option"
msgstr "Chọn tùy chọn của bạn"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Skills:"
msgstr "Kỹ năng:"

#. module: openeducat_alumni_job_enterprise
#: model:op.job.post,job_post:openeducat_alumni_job_enterprise.alumni_job_post_2
msgid "Software tester"
msgstr "Phần mềm thử nghiệm"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
#, python-format
msgid "Start Date"
msgstr "Ngày bắt đầu"

#. module: openeducat_alumni_job_enterprise
#. odoo-python
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#: code:addons/openeducat_alumni_job_enterprise/controllers/main.py:0
#, python-format
msgid "Status"
msgstr "Trạng thái"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.Alumni_posted_job_list
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job_list
msgid "There are no records."
msgstr "Không có hồ sơ."

#. module: openeducat_alumni_job_enterprise
#: model:op.job.post,job_post:openeducat_alumni_job_enterprise.alumni_job_post_1
msgid "Web developer"
msgstr "nhà phát triển web"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Weekly"
msgstr "Hàng tuần"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "Yearly"
msgstr "Hàng năm"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "city"
msgstr "thành phố"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "street"
msgstr "đường phố"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "street2"
msgstr "Street2"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_alumni_job_list_data
msgid "there is no data available for your account!!!"
msgstr "Không có dữ liệu có sẵn cho tài khoản của bạn !!!"

#. module: openeducat_alumni_job_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_alumni_job_enterprise.portal_student_alumni_job
msgid "zip"
msgstr "Mã Bưu Chính"
