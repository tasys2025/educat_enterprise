from odoo import models, fields, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import fiscalyear
import json
from odoo.tools import date_utils
import io
from odoo.exceptions import ValidationError

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class ReportTrialBalance(models.Model):
    _name = 'financial.trial.balance'
    _description = 'Financial Trial Balance'
    _inherit = 'report.openeducat_acc_pdf_reports.report_trialbalance'

    @api.model
    def _get_default_date_range(self):
        return self.env.company.date_range

    @api.onchange('date_range', 'financial_year')
    def onchange_date_range(self):
        if self.date_range:
            date = datetime.today()
            if self.date_range == 'today':
                self.date_from = date.strftime("%Y-%m-%d")
                self.date_to = date.strftime("%Y-%m-%d")

            if self.date_range == 'this_week':
                self.date_from = (date + relativedelta(days=-date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (self.date_from + relativedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_month':
                self.date_from = (date + relativedelta(day=1)).strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_quarter':
                currQuarter = int((date.month - 1) / 3 + 1)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'this_financial_year':
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    cur_year = fiscalyear.FiscalYear.current()
                    self.date_from = cur_year.start.date()
                    self.date_to = cur_year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    if date.month < 4:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    if date.month < 7:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

            if self.date_range == 'yesterday':
                self.date_from = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_week':
                date = (datetime.now() - relativedelta(days=7))
                day_today = date - timedelta(days=date.weekday())
                self.date_from = (day_today - timedelta(days=date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (day_today + timedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_month':
                self.date_from = (date - relativedelta(months=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_quarter':
                currQuarter = int((date.month - 1) / 3)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'last_financial_year':
                date = fiscalyear.FiscalDate.today()
                year = date.prev_fiscal_year
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

    financial_year = fields.Selection(
        [('april_march', '1 April to 31 March'),
         ('july_june', '1 july to 30 June'),
         ('january_december', '1 Jan to 31 Dec')],
        string='Financial Year', default=lambda self: self.env.company.financial_year,
        required=True)

    date_range = fields.Selection(
        [('today', 'Today'),
         ('this_week', 'This Week'),
         ('this_month', 'This Month'),
         ('this_quarter', 'This Quarter'),
         ('this_financial_year', 'This financial Year'),
         ('yesterday', 'Yesterday'),
         ('last_week', 'Last Week'),
         ('last_month', 'Last Month'),
         ('last_quarter', 'Last Quarter'),
         ('last_financial_year', 'Last Financial Year')],
        string='Date Range', default=_get_default_date_range
    )

    strict_range = fields.Boolean(
        string='Strict Range',
        default=lambda self: self.env.company.strict_range
    )

    target_moves = fields.Selection([('all', 'All Entries'),
                                     ('posted', 'All Posted Entries')],
                                    string='Target Moves', default='posted',
                                    required=True)
    display_accounts = fields.Selection(
        [('all', 'All'),
         ('not_zero', 'With balance is not equal to 0')],
        string='Display accounts', default='not_zero',
    )
    date_from = fields.Date(
        string='Start date',
    )
    date_to = fields.Date(
        string='End date',
    )
    account_ids = fields.Many2many(
        'account.account', string='Accounts'
    )
    analytic_ids = fields.Many2many(
        'account.analytic.account', string='Analytic Accounts'
    )
    journal_ids = fields.Many2many(
        'account.journal', string='Journals',
    )
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company
    )

    def process_filters(self):

        data = self.get_filters(default_filters={})
        filters = {}

        if data.get('date_from') > data.get('date_to'):
            raise ValidationError(_('From date must not be less than to date'))

        if not data.get('date_from') or not data.get('date_to'):
            raise ValidationError(_('From date and To dates '
                                    'are mandatory for this report'))

        if data.get('journal_ids', []):
            filters['journal_ids'] = self.env['account.journal'] \
                .browse(data.get('journal_ids', [])).mapped('id')
        else:
            filters['journal_ids'] = self.env['account.journal'].search([]).ids

        if data.get('account_ids', []):
            filters['account_ids'] = self.env['account.account'] \
                .browse(data.get('account_ids', []))

        if data.get('analytic_ids', []):
            filters['analytic_account_ids'] = self.env['account.analytic.account'] \
                .browse(data.get('analytic_ids', []))
        else:
            filters['analytic_account_ids'] = []

        if data.get('target_moves') == 'all':
            filters['state'] = 'all'
        else:
            filters['state'] = 'posted'

        if data.get('display_accounts') == 'all':
            filters['display_accounts'] = 'all'
        else:
            filters['display_accounts'] = 'not_zero'

        if data.get('strict_range'):
            filters['strict_range'] = data.get('strict_range')

        if data.get('date_from', False):
            filters['date_from'] = data.get('date_from')
        if data.get('date_to', False):
            filters['date_to'] = data.get('date_to')

        filters['journals_list'] = data.get('journals_list')
        filters['accounts_list'] = data.get('accounts_list')
        filters['analytics_list'] = data.get('analytics_list')
        filters['company_name'] = data.get('company_name')

        return filters

    def get_filters(self, default_filters={}):

        self.onchange_date_range()

        company_domain = [('company_id', '=', self.env.company.id)]

        journals = self.journal_ids if self.journal_ids \
            else self.env['account.journal'].search(company_domain)
        accounts = self.account_ids if self.account_ids \
            else self.env['account.account'].search(company_domain)
        analytics = self.analytic_ids if self.analytic_ids \
            else self.env['account.analytic.account'].search(company_domain)

        filter_dict = {
            'journal_ids': self.journal_ids.ids,
            'account_ids': self.account_ids.ids,
            'analytic_ids': self.analytic_ids.ids,
            'company_id': self.company_id and self.company_id.id or False,
            'date_from': self.date_from,
            'date_to': self.date_to,
            'display_accounts': self.display_accounts,
            'target_moves': self.target_moves,
            'strict_range': self.strict_range,
            'journals_list': [(j.id, j.name) for j in journals],
            'accounts_list': [(a.id, a.name) for a in accounts],
            'analytics_list': [(anl.id, anl.name) for anl in analytics],
            'company_name': self.company_id and self.company_id.name,
        }
        filter_dict.update(default_filters)
        return filter_dict

    @api.model_create_multi
    def create(self, vals):
        ret = super(ReportTrialBalance, self).create(vals)
        return ret

    def write(self, vals):

        if vals.get('date_range'):
            vals.update({'date_from': False, 'date_to': False})
        if vals.get('date_from') and vals.get('date_to'):
            vals.update({'date_range': False})

        if not vals.get('date_range') and not vals.get('date_from'):
            vals.update({'date_range': self._get_default_date_range()})

        if vals.get('display_accounts'):
            vals.update({'display_accounts': vals.get('display_accounts')})

        if vals.get('target_moves'):
            vals.update({'target_moves': vals.get('target_moves')})

        if vals.get('journal_ids'):
            vals.update({'journal_ids': vals.get('journal_ids')})
        if vals.get('journal_ids') == []:
            vals.update({'journal_ids': [(5,)]})

        if vals.get('account_ids'):
            vals.update({'account_ids': vals.get('account_ids')})
        if vals.get('account_ids') == []:
            vals.update({'account_ids': [(5,)]})

        if vals.get('analytic_ids'):
            vals.update({'analytic_ids': vals.get('analytic_ids')})
        if vals.get('analytic_ids') == []:
            vals.update({'analytic_ids': [(5,)]})

        ret = super(ReportTrialBalance, self).write(vals)
        return ret

    def get_report_values(self):
        display_account = self.display_accounts
        accounts = self.env['account.account'].search([])
        filters = self.process_filters()
        used_context = filters
        currency_id = self.company_id.currency_id.id

        report_values = self.with_context(used_context) \
            ._get_accounts(accounts, display_account)
        line = self.with_context(used_context) \
            .get_initial_balance(accounts, report_values)

        return filters, line, currency_id

    def get_initial_balance(self, accounts, data):
        init_balance_line = []
        for account in accounts:
            account_res = {}
            for dt in data:
                if account.code == dt['code']:
                    if account.include_initial_balance and \
                            self.strict_range and len(account.code) == len(dt['code']):
                        account_res['initial_balance'] = 0.0
                        account_res['initial_debit'] = 0.0
                        account_res['initial_credit'] = 0.0
                        account_res['code'] = account.code
                        account_res['credit'] = dt['credit']
                        account_res['debit'] = dt['debit']
                        account_res['balance'] = dt['balance']
                        account_res['ending_balance'] = \
                            account_res['initial_balance'] + dt['balance']
                        account_res['ending_credit'] = \
                            account_res['initial_credit'] + dt['credit']
                        account_res['ending_debit'] = \
                            account_res['initial_debit'] + dt['debit']
                        account_res['id'] = account.id
                        account_res['name'] = account.name
                        init_balance_line.append(account_res)

                    else:
                        account_res['initial_balance'] = dt['balance']
                        account_res['initial_debit'] = dt['debit']
                        account_res['initial_credit'] = dt['credit']
                        account_res['code'] = account.code
                        account_res['credit'] = 0
                        account_res['debit'] = 0
                        account_res['balance'] = 0
                        account_res['ending_balance'] = dt['balance']
                        account_res['ending_credit'] = dt['credit']
                        account_res['ending_debit'] = dt['debit']
                        account_res['id'] = account.id
                        account_res['name'] = account.name
                        init_balance_line.append(account_res)

                else:
                    continue
        return init_balance_line

    def action_xlsx(self):
        data = self.read()
        report = 'Trial Balance'
        name = '%s' % (report)

        return {
            'type': 'ir.actions.report',
            'data': {'model': 'financial.trial.balance',
                     'options': json.dumps(data[0], default=date_utils.json_default),
                     'output_format': 'xlsx',
                     'report_name': name,
                     },
            'report_type': 'xlsx'
        }

    def get_xlsx_report(self, data, response):

        record = self.env['financial.trial.balance'].browse(data.get('id', [])) or False
        filters, line, currency_id = record.get_report_values()
        currency_id = self.env.user.company_id.currency_id
        symbol = currency_id.symbol + '#,##0'

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        cell_format = workbook.add_format({'font_size': '12px', 'bold': True})
        cell_center = workbook.add_format({'align': 'center',
                                           'font_size': '10px', 'bold': True})
        cell_format_date = workbook.add_format({'font_size': '10px', })
        head = workbook.add_format({'align': 'center',
                                    'bold': True, 'font_size': '20px'})
        main_data = workbook.add_format({'font_size': '10px', 'bold': True})
        main_amt = workbook.add_format({'font_size': '10px',
                                        'num_format': symbol, 'bold': True})

        sheet.merge_range('B2:L3', 'Trial Balance', head)

        if filters['state']:
            sheet.write('B6', 'Target Moves:', cell_format)
            if filters['state'] == 'posted':
                sheet.write('C6', 'All Posted Entries', cell_format_date)
            else:
                sheet.write('C6', 'All Entries', cell_format_date)

        if filters['date_from']:
            date_from = filters['date_from'].strftime("%Y-%m-%d")
            sheet.write('B7', 'From:', cell_format)
            sheet.write('C7', date_from, cell_format_date)

        if filters['date_to']:
            date_to = filters['date_to'].strftime("%Y-%m-%d")
            sheet.write('B8', 'To:', cell_format)
            sheet.write('C8', date_to, cell_format_date)

        sheet.write(11, 1, _('Account'), cell_format)
        sheet.merge_range('C11:E11', _('Initial Balance'), cell_center)
        sheet.write(11, 2, _('Debit'), cell_format)
        sheet.write(11, 3, _('Credit'), cell_format)
        sheet.write(11, 4, _('Balance'), cell_format)
        sheet.merge_range('F11:H11', date_from + ' to ' + date_to, cell_center)
        sheet.write(11, 5, _('Debit'), cell_format)
        sheet.write(11, 6, _('Credit'), cell_format)
        sheet.write(11, 7, _('Balance'), cell_format)
        sheet.merge_range('I11:K11', _('Initial Balance'), cell_center)
        sheet.write(11, 8, _('Debit'), cell_format)
        sheet.write(11, 9, _('Credit'), cell_format)
        sheet.write(11, 10, _('Balance'), cell_format)

        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 12)
        sheet.set_column('D:D', 12)
        sheet.set_column('E:E', 12)
        sheet.set_column('F:F', 12)
        sheet.set_column('G:G', 12)
        sheet.set_column('H:H', 12)
        sheet.set_column('I:I', 12)
        sheet.set_column('J:J', 12)
        sheet.set_column('K:K', 12)

        row_pos = 13
        col_pos = 1

        for data in line:
            sheet.write(row_pos, col_pos, data['code'] + ' - ' + data['name'],
                        main_data)
            sheet.write(row_pos, col_pos + 1, float(data['initial_debit']), main_amt)
            sheet.write(row_pos, col_pos + 2, float(data['initial_credit']), main_amt)
            sheet.write(row_pos, col_pos + 3, float(data['initial_balance']), main_amt)
            sheet.write(row_pos, col_pos + 4, float(data['debit']), main_amt)
            sheet.write(row_pos, col_pos + 5, float(data['credit']), main_amt)
            sheet.write(row_pos, col_pos + 6, float(data['balance']), main_amt)
            sheet.write(row_pos, col_pos + 7, float(data['ending_debit']), main_amt)
            sheet.write(row_pos, col_pos + 8, float(data['ending_credit']), main_amt)
            sheet.write(row_pos, col_pos + 9, float(data['ending_balance']), main_amt)

            row_pos += 1

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()
