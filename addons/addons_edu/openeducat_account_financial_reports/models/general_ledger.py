from odoo import models, fields, api, _
from odoo.tools.misc import get_lang
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import fiscalyear
import json
from odoo.tools import date_utils
import io

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class ReportGeneralLedger(models.Model):
    _name = 'financial.general.ledger'
    _description = 'Financial General Ledger'
    _inherit = 'report.openeducat_acc_pdf_reports.report_general_ledger'

    @api.onchange('date_range', 'financial_year')
    def onchange_date_range(self):
        if self.date_range:
            date = datetime.today()
            if self.date_range == 'today':
                self.date_from = date.strftime("%Y-%m-%d")
                self.date_to = date.strftime("%Y-%m-%d")

            if self.date_range == 'this_week':
                self.date_from = (date + relativedelta(days=-date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (self.date_from + relativedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_month':
                self.date_from = (date + relativedelta(day=1)).strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_quarter':
                currQuarter = int((date.month - 1) / 3 + 1)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'this_financial_year':
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    cur_year = fiscalyear.FiscalYear.current()
                    self.date_from = cur_year.start.date()
                    self.date_to = cur_year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    if date.month < 4:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    if date.month < 7:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

            if self.date_range == 'yesterday':
                self.date_from = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_week':
                date = (datetime.now() - relativedelta(days=7))
                day_today = date - timedelta(days=date.weekday())
                self.date_from = (day_today - timedelta(days=date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (day_today + timedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_month':
                self.date_from = (date - relativedelta(months=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_quarter':
                currQuarter = int((date.month - 1) / 3)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'last_financial_year':
                date = fiscalyear.FiscalDate.today()
                year = date.prev_fiscal_year
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

    def _get_default_date_range(self):
        return self.env.company.date_range

    financial_year = fields.Selection([('april_march', '1 April to 31 March'),
                                       ('july_june', '1 july to 30 June'),
                                       ('january_december', '1 Jan to 31 Dec')],
                                      string='Financial Year',
                                      default=lambda self: self.env.
                                      company.financial_year,
                                      required=True)

    date_range = fields.Selection(
        [('today', 'Today'),
         ('this_week', 'This Week'),
         ('this_month', 'This Month'),
         ('this_quarter', 'This Quarter'),
         ('this_financial_year', 'This financial Year'),
         ('yesterday', 'Yesterday'),
         ('last_week', 'Last Week'),
         ('last_month', 'Last Month'),
         ('last_quarter', 'Last Quarter'),
         ('last_financial_year', 'Last Financial Year')],
        string='Date Range', default=_get_default_date_range)

    target_moves = fields.Selection([('all', 'All'), ('posted', 'Posted')],
                                    string='Target Moves', default='posted',
                                    required=False)

    display_accounts = fields.Selection([('all', 'All'),
                                         ('not_zero', 'With balance not equal to zero'),
                                         ('movement', 'Movement')],
                                        string='Display accounts', default='not_zero',
                                        required=False)

    sortby = fields.Selection([('sort_date', 'Date'),
                               ('sort_journal_partner', 'Journal & Partner')],
                              string='Sort by', required=True, default='sort_date')

    initial_balance = fields.Boolean(
        string='Include Initial Balance', default=False
    )

    strict_range = fields.Boolean(
        string='Strict Range',
        default=lambda self: self.env.company.strict_range
    )

    date_from = fields.Date(
        string='Start date',
    )
    date_to = fields.Date(
        string='End date',
    )
    analytic_ids = fields.Many2many('account.analytic.account',
                                    string='Analytic Accounts')
    journal_ids = fields.Many2many(
        'account.journal', string='Journals',
    )
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company
    )

    def process_filters(self):

        data = self.get_filters(default_filters={})

        filters = {}
        if data.get('journal_ids', []):
            filters['journal_ids'] = self.env['account.journal'] \
                .browse(data.get('journal_ids', [])).mapped('id')
        else:
            filters['journal_ids'] = self.env['account.journal'].search([]).ids

        if data.get('analytic_ids', []):
            filters['analytic_account_ids'] = self.env['account.analytic.account'] \
                .browse(data.get('analytic_ids', []))
        else:
            filters['analytic_account_ids'] = []

        if data.get('display_accounts') == 'all':
            filters['display_accounts'] = 'all'
        elif data.get('display_accounts') == 'With balance not equal to zero':
            filters['display_accounts'] = 'not_zero'
        else:
            filters['display_accounts'] = 'movement'

        if data.get('target_moves') == 'all':
            filters['state'] = 'all'
        else:
            filters['state'] = 'posted'

        if data.get('date_from', False):
            filters['date_from'] = data.get('date_from')
        if data.get('date_to', False):
            filters['date_to'] = data.get('date_to')

        if data.get('company_id'):
            filters['company_id'] = data.get('company_id')
        else:
            filters['company_id'] = ''

        if data.get('strict_range'):
            filters['strict_range'] = True
        else:
            filters['strict_range'] = False

        filters['journals_list'] = data.get('journals_list')
        filters['analytics_list'] = data.get('analytics_list')
        filters['company_name'] = data.get('company_name')

        return filters

    def get_filters(self, default_filters={}):

        self.onchange_date_range()

        company_domain = [('company_id', '=', self.env.company.id)]
        journals = self.journal_ids if self.journal_ids \
            else self.env['account.journal'].search(company_domain)
        analytics = self.analytic_ids if self.analytic_ids \
            else self.env['account.analytic.account'].search(company_domain)

        filter_dict = {
            'journal_ids': self.journal_ids.ids,
            'analytic_ids': self.analytic_ids.ids,
            'company_id': self.company_id and self.company_id.id or False,
            'target_moves': self.target_moves,
            'date_from': self.date_from,
            'date_to': self.date_to,
            'strict_range': self.strict_range,
            'display_accounts': self.display_accounts,
            'journals_list': [(j.id, j.name) for j in journals],
            'analytics_list': [(anl.id, anl.name) for anl in analytics],
            'company_name': self.company_id and self.company_id.name,
        }

        filter_dict.update(default_filters)

        return filter_dict

    def create(self, vals):
        res = super(ReportGeneralLedger, self).create(vals)
        return res

    def write(self, vals):

        if vals.get('date_range'):
            vals.update({'date_from': False, 'date_to': False})
        if vals.get('date_from') and vals.get('date_to'):
            vals.update({'date_range': False})

        if not vals.get('date_range') and not vals.get('date_from'):
            vals.update({'date_range': self._get_default_date_range()})

        if vals.get('journal_ids'):
            vals.update({'journal_ids': vals.get('journal_ids')})
        if vals.get('journal_ids') == []:
            vals.update({'journal_ids': [(5,)]})

        if vals.get('analytic_ids'):
            vals.update({'analytic_ids': vals.get('analytic_ids')})
        if vals.get('analytic_ids') == []:
            vals.update({'analytic_ids': [(5,)]})

        if vals.get('target_moves'):
            vals.update({'target_moves': vals.get('target_moves')})

        if vals.get('display_accounts'):
            vals.update({'display_accounts': vals.get('display_accounts')})

        res = super(ReportGeneralLedger, self).write(vals)
        return res

    def get_report_values(self):

        accounts = self.env['account.account'].search([])
        filters = self.process_filters()
        context = dict(filters, lang=get_lang(self.env).code)

        report_values = self.with_context(context, {}) \
            ._get_account_move_entry(accounts, self.initial_balance,
                                     self.sortby, self.display_accounts)

        return filters, report_values

    def _get_account_move_entry(self, accounts, initial_balance,
                                sortby, display_accounts):
        res = super(ReportGeneralLedger, self) \
            ._get_account_move_entry(accounts, initial_balance,
                                     sortby, display_accounts)
        for value in res:
            currency_id = self.company_id.currency_id
            value['currency_id'] = currency_id.id
        return res

    def action_xlsx(self):
        data = self.read()
        report = 'General Ledger'
        name = '%s' % (report)

        return {
            'type': 'ir.actions.report',
            'data': {'model': 'financial.general.ledger',
                     'options': json.dumps(data[0], default=date_utils.json_default),
                     'output_format': 'xlsx',
                     'report_name': name,
                     },
            'report_type': 'xlsx'
        }

    def get_xlsx_report(self, data, response):

        record = self.env['financial.general.ledger'].browse(data.get(
            'id', [])) or False
        filter_val, data = record.get_report_values()

        currency_id = self.env.user.company_id.currency_id
        symbol = currency_id.symbol + '#,##0'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        cell_format = workbook.add_format({'font_size': '12px', 'bold': True})
        cell_format_data = workbook.add_format({'font_size': '10px'})
        head = workbook.add_format({'align': 'center', 'bold': True,
                                    'font_size': '20px'})
        line_data = workbook.add_format({'font_size': '10px'})
        main_data = workbook.add_format({'font_size': '10px', 'bold': True})
        main_amt = workbook.add_format({'font_size': '10px',
                                        'num_format': symbol, 'bold': True})
        line_amt = workbook.add_format({'font_size': '10px',
                                        'num_format': symbol, })

        sheet.merge_range('B2:I3', 'General Ledger', head)

        if filter_val['state']:
            sheet.write('B6', 'Target Moves:', cell_format)
            if filter_val['state'] == 'posted':
                sheet.write('C6', 'All Posted Entries',
                            cell_format_data)
            else:
                sheet.write('C6', 'All Entries', cell_format_data)

        if filter_val['date_from']:
            date_from = filter_val['date_from'].strftime("%Y-%m-%d")
            sheet.write('B7', 'From:', cell_format)
            sheet.write('C7', date_from, cell_format_data)

        if filter_val['date_to']:
            date_to = filter_val['date_to'].strftime("%Y-%m-%d")
            sheet.write('B8', 'To:', cell_format)
            sheet.write('C8', date_to, cell_format_data)

        sheet.write(10, 1, _('Account'), cell_format)
        sheet.write(10, 2, _('Date'), cell_format)
        sheet.write(10, 3, _('Communication'), cell_format)
        sheet.write(10, 4, _('Partner'), cell_format)
        sheet.write(10, 5, _('Debit'), cell_format)
        sheet.write(10, 6, _('Credit'), cell_format)
        sheet.write(10, 7, _('Balance'), cell_format)

        sheet.set_column('B:B', 25)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 30)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 20)
        sheet.set_column('G:G', 20)
        sheet.set_column('H:H', 20)

        row_pos = 12
        col_pos = 1

        for value in data:
            sheet.write(row_pos, col_pos, value.get('name'), main_data)
            sheet.write(row_pos, col_pos + 4, float(value.get('debit')),
                        main_amt)
            sheet.write(row_pos, col_pos + 5, float(value.get('credit')),
                        main_amt)
            sheet.write(row_pos, col_pos + 6, float(value.get('balance')),
                        main_amt)

            if value['move_lines']:
                for move in value['move_lines']:
                    row_pos += 1
                    sheet.set_row(row_pos, 25)
                    sheet.write(row_pos, col_pos, '   ' * 2 +
                                move.get('move_name'), line_data)
                    if move.get('ldate'):
                        ldate = move['ldate'].strftime("%Y-%m-%d")
                        sheet.write(row_pos, col_pos + 1, ldate, line_data)
                    sheet.write(row_pos, col_pos + 2, move.get('lname'),
                                line_data)
                    sheet.write(row_pos, col_pos + 3, move.get('partner_name'),
                                line_data)
                    sheet.write(row_pos, col_pos + 4, float(move.get('debit')),
                                line_amt)
                    sheet.write(row_pos, col_pos + 5, float(move.get('credit')),
                                line_amt)
                    sheet.write(row_pos, col_pos + 6, float(move.get('balance')),
                                line_amt)

            row_pos += 1

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()
