from odoo import models
import re


class AccountTaxReportLine(models.Model):
    _inherit = "account.tax.report.line"

    def tax_report_financial_lines(self, line):
        res = self.env['account.tax.report.line']
        children = self.search([('parent_id', '=', line.id)], order='id ASC')
        for child in children:
            if child.children_line_ids:
                res += child.tax_report_financial_lines(child)
            elif child.code:
                res += child
                continue
            else:
                res += child.tax_report_financial_lines(child)
        res += self
        return res

    def tax_report_ordered_financial_lines(self, line):
        res = self
        children = self.search([('parent_id', '=', line.id)], order='id ASC')
        for child in children:
            if child.code:
                if child.children_line_ids:
                    res += child.tax_report_ordered_financial_lines(child)
                else:
                    res += child
                continue
            elif child.children_line_ids:
                res += child.tax_report_ordered_financial_lines(child)
            else:
                res += child.tax_report_ordered_financial_lines(child)
        return res

    def formula_calculate(self, formulas, values):
        temporary = {}
        vals_dic = {}
        line_id = self
        formula_string = ''
        for p_id, p_value in values.items():
            for form in formulas:
                if form == p_id:
                    if len(form) == len(p_id):
                        temporary[p_id] = p_value
                        if formula_string:
                            if form in line_id.formula:
                                pattern = re.compile(r'\b' + re.escape(form) + r'\b')
                                formula = str(temporary[form])
                                formula_string = pattern.sub(formula, formula_string)
                        else:
                            if form in line_id.formula:
                                pattern = re.compile(r'\b' + re.escape(form) + r'\b')
                                formula = str(temporary[form])
                                formula_string = pattern.sub(formula, line_id.formula)

        if line_id.parent_id.name == 'Revenue':
            for key, value in values.items():
                if key == 5:
                    for k, v in value.items():
                        vals_dic[line_id.formula] = v
                        return vals_dic

        vals = self.get_calculated_vals(formula_string)
        vals_dic[line_id.formula] = vals
        return vals_dic

    def get_calculated_vals(self, f):
        try:
            vals = eval(f)
        except Exception:
            vals = 0
        return vals
