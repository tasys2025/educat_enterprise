from odoo import models, fields, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import fiscalyear
import json
from odoo.tools import date_utils
import io

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class ReportAgedPartnerBalance(models.Model):
    _name = 'financial.aged.partner'
    _description = 'Financial Aged Partner'
    _inherit = 'report.openeducat_acc_pdf_reports.report_agedpartnerbalance'

    @api.onchange('date_range', 'financial_year')
    def onchange_date_range(self):
        if self.date_range:
            date = datetime.today()
            if self.date_range == 'today':
                self.date_from = date.strftime("%Y-%m-%d")
                self.date_to = date.strftime("%Y-%m-%d")

            if self.date_range == 'this_week':
                self.date_from = (date + relativedelta(days=-date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (self.date_from + relativedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_month':
                self.date_from = (date + relativedelta(day=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_quarter':
                currQuarter = int((date.month - 1) / 3 + 1)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'this_financial_year':
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    cur_year = fiscalyear.FiscalYear.current()
                    self.date_from = cur_year.start.date()
                    self.date_to = cur_year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    if date.month < 4:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    if date.month < 7:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

            if self.date_range == 'yesterday':
                self.date_from = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_week':
                date = (datetime.now() - relativedelta(days=7))
                day_today = date - timedelta(days=date.weekday())
                self.date_from = (day_today - timedelta(days=date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (day_today + timedelta(days=6)).strftime("%Y-%m-%d")

            if self.date_range == 'last_month':
                self.date_from = (date - relativedelta(months=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_quarter':
                currQuarter = int((date.month - 1) / 3)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'last_financial_year':
                date = fiscalyear.FiscalDate.today()
                year = date.prev_fiscal_year
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

    @api.model
    def _get_default_date_range(self):
        return self.env.company.date_range

    financial_year = fields.Selection(
        [('april_march', '1 April to 31 March'),
         ('july_june', '1 july to 30 June'),
         ('january_december', '1 Jan to 31 Dec')],
        string='Financial Year', default=lambda self: self.env.company.financial_year,
        required=True)

    date_range = fields.Selection(
        [('today', 'Today'),
         ('this_week', 'This Week'),
         ('this_month', 'This Month'),
         ('this_quarter', 'This Quarter'),
         ('this_financial_year', 'This financial Year'),
         ('yesterday', 'Yesterday'),
         ('last_week', 'Last Week'),
         ('last_month', 'Last Month'),
         ('last_quarter', 'Last Quarter'),
         ('last_financial_year', 'Last Financial Year')],
        string='Date Range', default=_get_default_date_range)

    type = fields.Selection(
        [('customer', 'Receivable Only'),
         ('supplier', 'Payable only')],
        string='Account Type', required=False
    )

    target_moves = fields.Selection(
        [('all', 'All'),
         ('posted', 'Posted')], string='Target Moves',
        default='posted', required=False
    )

    period_length = fields.Integer('Period Length', default=30)

    date_from = fields.Date(
        string='Start date',
    )
    date_to = fields.Date(
        string='End date',
    )

    partner_ids = fields.Many2many(
        'res.partner', string='Partners'
    )
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company
    )

    partner_category_ids = fields.Many2many(
        'res.partner.category', string='Partner Tag',
    )

    def write(self, vals):

        if vals.get('date_range'):
            vals.update({'date_from': False, 'date_to': False})
        if vals.get('date_from') and vals.get('date_to'):
            vals.update({'date_range': False})

        if not vals.get('date_range') and not vals.get('date_from'):
            vals.update({'date_range': self._get_default_date_range()})

        if vals.get('type'):
            vals.update({'type': vals.get('type')})

        if not vals.get('type'):
            vals.update({'type': False})

        if vals.get('target_moves'):
            vals.update({'target_moves': vals.get('target_moves')})

        if vals.get('partner_ids'):
            vals.update({'partner_ids': vals.get('partner_ids')})

        if vals.get('partner_ids') == []:
            vals.update({'partner_ids': [(5,)]})

        if vals.get('partner_category_ids'):
            vals.update({'partner_category_ids': vals.get('partner_category_ids')})

        if vals.get('partner_category_ids') == []:
            vals.update({'partner_category_ids': [(5,)]})

        res = super(ReportAgedPartnerBalance, self).write(vals)
        return res

    def process_filters(self):

        data = self.get_filters(default_filters={})

        filters = {}

        if data.get('partner_ids', []):
            filters['partners'] = self.env['res.partner']. \
                browse(data.get('partner_ids', []))
        else:
            filters['partners'] = []

        if data.get('partner_category_ids', []):
            filters['categories'] = self.env['res.partner.category']. \
                browse(data.get('partner_category_ids', []))
        else:
            filters['categories'] = []

        if data.get('date_from', False):
            filters['date_from'] = data.get('date_from')
        if data.get('date_to', False):
            filters['date_to'] = data.get('date_to')

        if data.get('company_id'):
            filters['company_id'] = data.get('company_id')
        else:
            filters['company_id'] = ''

        if data.get('target_moves') == 'all':
            filters['target_moves'] = 'all'
        else:
            filters['target_moves'] = 'posted'

        if data.get('type') == 'customer':
            filters['type'] = data.get('type')
        elif data.get('type') == 'supplier':
            filters['type'] = data.get('type')

        if data.get('period_length'):
            filters['period_length'] = data.get('period_length')

        filters['journals_list'] = data.get('journals_list')
        filters['partners_list'] = data.get('partners_list')
        filters['category_list'] = data.get('category_list')
        filters['company_name'] = data.get('company_name')

        return filters

    def get_filters(self, default_filters={}):

        self.onchange_date_range()

        partner_company_domain = [('parent_id', '=', False),
                                  '|',
                                  ('customer_rank', '>', 0),
                                  ('supplier_rank', '>', 0),
                                  '|',
                                  ('company_id', '=', self.env.company.id),
                                  ('company_id', '=', False)]

        partners = self.partner_ids if self.partner_ids else \
            self.env['res.partner'].search(partner_company_domain)
        categories = self.partner_category_ids if self.partner_category_ids \
            else self.env['res.partner.category'].search([])

        filter_dict = {
            'partner_ids': self.partner_ids.ids,
            'partner_category_ids': self.partner_category_ids.ids,
            'company_id': self.company_id and self.company_id.id or False,
            'date_from': self.date_from,
            'date_to': self.date_to,
            'type': self.type,
            'period_length': self.period_length,
            'target_moves': self.target_moves,
            'partners_list': [(p.id, p.name) for p in partners],
            'category_list': [(c.id, c.name) for c in categories],
            'company_name': self.company_id and self.company_id.name,
        }

        filter_dict.update(default_filters)
        return filter_dict

    def create(self, vals):
        res = super(ReportAgedPartnerBalance, self).create(vals)
        return res

    def get_account_type(self, data):

        if data.get('type') == 'customer':
            return ['receivable']
        elif data.get('type') == 'supplier':
            return ['payable']
        else:
            return ['payable', 'receivable']

    def get_report_values(self):

        filters = self.process_filters()

        data = {}
        data['form'] = {}
        account_type = self.get_account_type(filters)
        target_moves = filters.get('target_moves')
        currency_id = self.company_id.currency_id.id
        period_length = filters.get('period_length')

        if filters.get('date_from'):
            date_from = filters.get('date_from')
        else:
            date_from = datetime.today()

        date = date_from.strftime('%Y-%m-%d')
        start = date_from

        res = {}
        for i in range(5)[::-1]:
            stop = start - relativedelta(days=period_length - 1)
            res[str(i)] = {
                'name': (i != 0 and (str((5 - (i + 1)) * period_length) + '-' +
                                     str((5 - i) * period_length)) or
                         ('+' + str(4 * period_length))),
                'stop': start.strftime('%Y-%m-%d'),
                'start': (i != 0 and stop.
                          strftime('%Y-%m-%d') or False),
            }
            start = stop - relativedelta(days=1)

        data['form'].update(res)

        datas, total, lines = self._get_partner_move_lines(account_type, date,
                                                           target_moves, period_length)

        return [datas, total, lines, data, filters, currency_id]

    def _get_partner_move_lines(self, account_type, date_from,
                                target_move, period_length):

        res = super(ReportAgedPartnerBalance, self). \
            _get_partner_move_lines(account_type, date_from, target_move, period_length)

        for key, value in res[2].items():
            for line in value:
                current_line = line['line']
                line['line'] = {
                    'id': current_line.id,
                    'name': current_line.move_id.name,
                    'date': current_line.date,
                    'journal': current_line.journal_id.code,
                    'account_type': current_line.account_id.account_type,
                }
        return res

    def action_xlsx(self):
        data = self.read()
        report = 'Aged Partner Balance'
        name = '%s' % (report)

        return {
            'type': 'ir.actions.report',
            'data': {'model': 'financial.aged.partner',
                     'options': json.dumps(data[0], default=date_utils.json_default),
                     'output_format': 'xlsx',
                     'report_name': name,
                     },
            'report_type': 'xlsx'
        }

    def get_xlsx_report(self, data, response):

        record = self.env['financial.aged.partner'].browse(data.get('id', [])) or False
        datas, total, lines, data, filters, currency_id = record.get_report_values()
        currency_id = self.env.user.company_id.currency_id
        symbol = currency_id.symbol + '#,##0'

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        cell_format = workbook.add_format({'font_size': '12px', 'bold': True})
        cell_format_date = workbook.add_format({'font_size': '10px', })
        head = workbook.add_format({'align': 'center', 'bold': True,
                                    'font_size': '20px'})
        date_center = workbook.add_format({'align': 'center',
                                           'font_size': '10px'})
        line_data = workbook.add_format({'font_size': '10px'})
        main_data = workbook.add_format({'font_size': '10px', 'bold': True})
        main_amt = workbook.add_format({'font_size': '10px', 'num_format': symbol,
                                        'bold': True})
        line_amt = workbook.add_format({'font_size': '10px', 'num_format': symbol, })

        sheet.merge_range('B2:I3', 'Aged Partner Balance', head)

        if filters['target_moves']:
            sheet.write('B6', 'Target Moves:', cell_format)
            if filters['target_moves'] == 'posted':
                sheet.write('C6', 'All Posted Entries', cell_format_date)
            else:
                sheet.write('C6', 'All Entries', cell_format_date)

        if filters['date_from']:
            date_from = filters['date_from'].strftime("%Y-%m-%d")
            sheet.write('B7', 'From:', cell_format)
            sheet.write('C7', date_from, cell_format_date)

        if filters['date_to']:
            date_to = filters['date_to'].strftime("%Y-%m-%d")
            sheet.write('B8', 'To:', cell_format)
            sheet.write('C8', date_to, cell_format_date)

        sheet.write(10, 1, _('Partner'), cell_format)
        sheet.write(10, 2, _('Report Date'), cell_format)
        sheet.write(10, 3, _('Journal'), cell_format)
        sheet.write(10, 4, _('Account'), cell_format)
        sheet.write(10, 5, _('Not Due'), cell_format)
        sheet.write(10, 6, _(data['form']['4']['name']), cell_format)
        sheet.write(10, 7, _(data['form']['3']['name']), cell_format)
        sheet.write(10, 8, _(data['form']['2']['name']), cell_format)
        sheet.write(10, 9, _(data['form']['1']['name']), cell_format)
        sheet.write(10, 10, _(data['form']['0']['name']), cell_format)
        sheet.write(10, 11, _('Total'), cell_format)

        sheet.set_column('B:B', 20)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 10)

        row_pos = 12
        col_pos = 1

        for value in datas:
            sheet.write(row_pos, col_pos, value['name'], main_data)
            sheet.write(row_pos, col_pos + 4, float(value['direction']),
                        main_amt)
            sheet.write(row_pos, col_pos + 5, float(value['4']), main_amt)
            sheet.write(row_pos, col_pos + 6, float(value['3']), main_amt)
            sheet.write(row_pos, col_pos + 7, float(value['2']), main_amt)
            sheet.write(row_pos, col_pos + 8, float(value['1']), main_amt)
            sheet.write(row_pos, col_pos + 9, float(value['0']), main_amt)
            sheet.write(row_pos, col_pos + 10, float(value['total']), main_amt)

            if lines:
                for line in lines[value['partner_id']]:
                    row_pos += 1
                    sheet.write(row_pos, col_pos, line['line']['name'], date_center)
                    if line['line']['date']:
                        date = line['line']['date'].strftime("%Y-%m-%d")
                        sheet.write(row_pos, col_pos + 1, date, date_center)
                    sheet.write(row_pos, col_pos + 2, line['line']['journal'],
                                line_data)
                    sheet.write(row_pos, col_pos + 3, line['line']['account_type'],
                                line_data)
                    if line['period'] == 6:
                        sheet.write(row_pos, col_pos + 4, float(line['amount']),
                                    line_amt)
                    elif line['period'] == 5:
                        sheet.write(row_pos, col_pos + 5, float(line['amount']),
                                    line_amt)
                    elif line['period'] == 3:
                        sheet.write(row_pos, col_pos + 6, float(line['amount']),
                                    line_amt)
                    elif line['period'] == 2:
                        sheet.write(row_pos, col_pos + 7, float(line['amount']),
                                    line_amt)
                    elif line['period'] == 0:
                        sheet.write(row_pos, col_pos + 8, float(line['amount']),
                                    line_amt)
                    elif line['period'] == 1:
                        sheet.write(row_pos, col_pos + 9, float(line['amount']),
                                    line_amt)
                    else:
                        sheet.write(row_pos, col_pos + 10, float(line['amount']),
                                    line_amt)

            row_pos += 1

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()
