from odoo import models, fields, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import fiscalyear
import json
from odoo.tools import date_utils
import io

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class ReportPartnerLedger(models.Model):
    _name = 'financial.partner.ledger'
    _description = 'Financial Partner Ledger'
    _inherit = 'report.openeducat_acc_pdf_reports.report_partnerledger'

    @api.onchange('date_range', 'financial_year')
    def onchange_date_range(self):
        if self.date_range:
            date = datetime.today()
            if self.date_range == 'today':
                self.date_from = date.strftime("%Y-%m-%d")
                self.date_to = date.strftime("%Y-%m-%d")

            if self.date_range == 'this_week':
                self.date_from = (date + relativedelta(days=-date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (self.date_from + relativedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_month':
                self.date_from = (date + relativedelta(day=1)).strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'this_quarter':
                currQuarter = int((date.month - 1) / 3 + 1)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'this_financial_year':
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    cur_year = fiscalyear.FiscalYear.current()
                    self.date_from = cur_year.start.date()
                    self.date_to = cur_year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    if date.month < 4:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    if date.month < 7:
                        cur_year = fiscalyear.FiscalYear(datetime.now().year)
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()
                    else:
                        cur_year = fiscalyear.FiscalYear.current()
                        self.date_from = cur_year.start.date()
                        self.date_to = cur_year.end.date()

            if self.date_range == 'yesterday':
                self.date_from = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date - relativedelta(days=1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_week':
                date = (datetime.now() - relativedelta(days=7))
                day_today = date - timedelta(days=date.weekday())
                self.date_from = (day_today - timedelta(days=date.weekday())) \
                    .strftime("%Y-%m-%d")
                self.date_to = (day_today + timedelta(days=6)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_month':
                self.date_from = (date - relativedelta(months=1)) \
                    .strftime("%Y-%m-%d")
                self.date_to = (date + relativedelta(day=1, months=+1, days=-1)) \
                    .strftime("%Y-%m-%d")

            if self.date_range == 'last_quarter':
                currQuarter = int((date.month - 1) / 3)
                self.date_from = datetime(date.year, 3 * currQuarter - 2, 1)
                self.date_to = self.date_from + relativedelta(months=3, days=-1)

            if self.date_range == 'last_financial_year':
                date = fiscalyear.FiscalDate.today()
                year = date.prev_fiscal_year
                if self.financial_year == 'january_december':
                    fiscalyear.START_MONTH = 1
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'april_march':
                    fiscalyear.START_MONTH = 4
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

                if self.financial_year == 'july_june':
                    fiscalyear.START_MONTH = 7
                    self.date_from = year.start.date()
                    self.date_to = year.end.date()

    @api.model
    def _get_default_date_range(self):
        return self.env.company.date_range

    financial_year = fields.Selection(
        [('april_march', '1 April to 31 March'),
         ('july_june', '1 july to 30 June'),
         ('january_december', '1 Jan to 31 Dec')],
        string='Financial Year', default=lambda self: self.env.company.financial_year,
        required=True)

    date_range = fields.Selection(
        [('today', 'Today'),
         ('this_week', 'This Week'),
         ('this_month', 'This Month'),
         ('this_quarter', 'This Quarter'),
         ('this_financial_year', 'This financial Year'),
         ('yesterday', 'Yesterday'),
         ('last_week', 'Last Week'),
         ('last_month', 'Last Month'),
         ('last_quarter', 'Last Quarter'),
         ('last_financial_year', 'Last Financial Year')],
        string='Date Range', default=_get_default_date_range)

    type = fields.Selection(
        [('customer', 'Receivable Only'),
         ('supplier', 'Payable only')],
        string='Account Type', required=False
    )

    target_moves = fields.Selection(
        [('all', 'All'),
         ('posted', 'Posted')], string='Target Moves',
        default='posted', required=False
    )

    reconcile = fields.Boolean(string='Reconciled Entries', default=False)

    date_from = fields.Date(
        string='Start date',
    )
    date_to = fields.Date(
        string='End date',
    )

    partner_ids = fields.Many2many(
        'res.partner', string='Partners'
    )
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company
    )

    partner_category_ids = fields.Many2many(
        'res.partner.category', string='Partner Tag',
    )

    def create(self, vals):
        res = super(ReportPartnerLedger, self).create(vals)
        return res

    def write(self, vals):

        if vals.get('date_range'):
            vals.update({'date_from': False, 'date_to': False})
        if vals.get('date_from') and vals.get('date_to'):
            vals.update({'date_range': False})

        if vals.get('type'):
            vals.update({'type': vals.get('type')})
        if not vals.get('type'):
            vals.update({'type': False})

        if vals.get('partner_ids'):
            vals.update({'partner_ids': vals.get('partner_ids')})
        if vals.get('partner_ids') == []:
            vals.update({'partner_ids': [(5,)]})

        if vals.get('partner_category_ids'):
            vals.update({'partner_category_ids': vals.get('partner_category_ids')})
        if vals.get('partner_category_ids') == []:
            vals.update({'partner_category_ids': [(5,)]})

        res = super(ReportPartnerLedger, self).write(vals)
        return res

    def process_filters(self):

        data = self.get_filters(default_filters={})

        filters = {}

        if data.get('partner_ids', []):
            filters['partner_ids'] = self.env['res.partner']. \
                browse(data.get('partner_ids', []))
        else:
            filters['partner_ids'] = []

        if data.get('partner_category_ids', []):
            filters['partner_categories'] = self.env['res.partner.category']. \
                browse(data.get('partner_category_ids', []))
        else:
            filters['partner_categories'] = []

        if data.get('date_from', False):
            filters['date_from'] = data.get('date_from')
            filters['strict_range'] = True
        if data.get('date_to', False):
            filters['date_to'] = data.get('date_to')

        if data.get('company_id'):
            filters['company_id'] = data.get('company_id')
        else:
            filters['company_id'] = ''

        if data.get('target_moves') == 'all':
            filters['state'] = 'all'
        else:
            filters['state'] = 'posted'

        if data.get('type') == 'customer':
            filters['type'] = data.get('type')
        elif data.get('type') == 'supplier':
            filters['type'] = data.get('type')

        filters['partners_list'] = data.get('partners_list')
        filters['category_list'] = data.get('category_list')
        filters['company_name'] = data.get('company_name')

        return filters

    def get_filters(self, default_filters={}):

        self.onchange_date_range()

        partner_company_domain = [('parent_id', '=', False),
                                  '|',
                                  ('customer_rank', '>', 0),
                                  ('supplier_rank', '>', 0),
                                  '|',
                                  ('company_id', '=', self.env.company.id),
                                  ('company_id', '=', False)]

        partners = self.partner_ids if self.partner_ids else \
            self.env['res.partner'].search(partner_company_domain)
        categories = self.partner_category_ids if self.partner_category_ids \
            else self.env['res.partner.category'].search([])

        filter_dict = {
            'partner_ids': self.partner_ids.ids,
            'partner_category_ids': self.partner_category_ids.ids,
            'company_id': self.company_id and self.company_id.id or False,
            'date_from': self.date_from,
            'date_to': self.date_to,
            'type': self.type,
            'target_moves': self.target_moves,
            'partners_list': [(p.id, p.name) for p in partners],
            'category_list': [(c.id, c.name) for c in categories],
            'company_name': self.company_id and self.company_id.name,
        }

        filter_dict.update(default_filters)
        return filter_dict

    def lines(self, data, partner):

        lines_value = {}

        for part in partner:
            full_account = []
            currency = self.env['res.currency']
            query_get_data = self.env['account.move.line'] \
                .with_context(data['form'].get('used_context', {}))._query_get()
            reconcile_clause = "" if data['form'][
                'reconciled'] else ' AND "account_move_line".full_reconcile_id IS NULL '
            params = [part.id, tuple(data['computed']['move_state']),
                      tuple(data['computed']['account_ids'])] + query_get_data[2]
            query = """SELECT "account_move_line".id, "account_move_line".date, j.code,
                acc.code as a_code, acc.name as a_name, "account_move_line".ref,
                m.name as move_name, "account_move_line".name,
                "account_move_line".debit, "account_move_line".credit,
                "account_move_line".amount_currency, "account_move_line".currency_id,
                c.symbol AS currency_code
                FROM """ + query_get_data[0] + """
                LEFT JOIN account_journal j ON ("account_move_line".journal_id = j.id)
                LEFT JOIN account_account acc ON
                ("account_move_line".account_id = acc.id)
                LEFT JOIN res_currency c ON ("account_move_line".currency_id=c.id)
                LEFT JOIN account_move m ON (m.id="account_move_line".move_id)
                WHERE "account_move_line".partner_id = %s
                AND m.state IN %s
                AND "account_move_line".account_id IN %s AND
                """ + query_get_data[1] + reconcile_clause + """
                ORDER BY "account_move_line".date"""
            self.env.cr.execute(query, tuple(params))
            res = self.env.cr.dictfetchall()
            sum_value = 0.0
            total_balance = 0
            total_credit = 0
            total_debit = 0

            for r in res:
                r['date'] = r['date']
                r['displayed_name'] = '-'.join(
                    r[field_name] for field_name in ('move_name', 'ref', 'name')
                    if r[field_name] not in (None, '', '/')
                )
                sum_value += r['debit'] - r['credit']
                r['progress'] = sum_value
                r['currency_id'] = currency.browse(r.get('currency_id')).id
                full_account.append(r)
                total_balance = sum_value
                total_debit += r['debit']
                total_credit += r['credit']

            move_lines = {'partner': part.name, 'total_balance': total_balance,
                          'total_credit': total_credit, 'total_debit': total_debit,
                          'lines': full_account, 'currency_id': part.currency_id.id}
            lines_value[part.id] = move_lines
        return lines_value

    def get_report_values(self):
        filters = self.process_filters()

        data = {}
        data['form'] = {'reconciled': self.reconcile}

        if filters.get('type') == 'customer':
            data['form']['result_selection'] = filters.get('type')
        elif filters.get('type') == 'supplier':
            data['form']['result_selection'] = filters.get('type')

        data['form']['used_context'] = filters
        data['computed'] = {}
        obj_partner = self.env['res.partner']
        query_get_data = self.env['account.move.line'] \
            .with_context(data['form'].get('used_context', {}))._query_get()

        data['computed']['move_state'] = ['draft', 'posted']
        if data['form'].get('target_move', 'all') == 'posted':
            data['computed']['move_state'] = ['posted']
        result_selection = data['form'].get('result_selection')

        if result_selection == 'supplier':
            data['computed']['ACCOUNT_TYPE'] = ['liability_payable']
        elif result_selection == 'customer':
            data['computed']['ACCOUNT_TYPE'] = ['asset_receivable']
        else:
            data['computed']['ACCOUNT_TYPE'] = ['asset_receivable', 'liability_payable']

        self.env.cr.execute("""
            SELECT a.id
            FROM account_account a
            WHERE a.account_type IN %s
            AND NOT a.deprecated""", (tuple(data['computed']['ACCOUNT_TYPE']),))

        data['computed']['account_ids'] = [a for (a,) in self.env.cr.fetchall()]
        params = [tuple(data['computed']['move_state']),
                  tuple(data['computed']['account_ids'])] + query_get_data[2]
        reconcile_clause = "" if data['form']['reconciled'] \
            else ' AND "account_move_line".full_reconcile_id IS NULL '
        query = """
            SELECT DISTINCT "account_move_line".partner_id
            FROM """ + query_get_data[0] + """,
            account_account AS account, account_move AS am
            WHERE "account_move_line".partner_id IS NOT NULL
                AND "account_move_line".account_id = account.id
                AND am.id = "account_move_line".move_id
                AND am.state IN %s
                AND "account_move_line".account_id IN %s
                AND NOT account.deprecated
                AND """ + query_get_data[1] + reconcile_clause
        self.env.cr.execute(query, tuple(params))
        partner_ids = [res['partner_id'] for res in self.env.cr.dictfetchall()]
        partners = obj_partner.browse(partner_ids)
        partners = sorted(partners, key=lambda x: (x.ref or '', x.name or ''))
        lines = self.lines(data, partners)

        return filters, lines

    def action_xlsx(self):
        data = self.read()
        report = 'Partner Ledger'
        name = '%s' % (report)

        return {
            'type': 'ir.actions.report',
            'data': {'model': 'financial.partner.ledger',
                     'options': json.dumps(data[0], default=date_utils.json_default),
                     'output_format': 'xlsx',
                     'report_name': name,
                     },
            'report_type': 'xlsx'
        }

    def get_xlsx_report(self, data, response):

        record = \
            self.env['financial.partner.ledger'].browse(data.get('id', [])) or False
        filter_val, data = record.get_report_values()

        currency_id = self.env.user.company_id.currency_id
        symbol = currency_id.symbol + '#,##0'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        cell_format = workbook.add_format({'font_size': '12px', 'bold': True})
        cell_format_date = workbook.add_format({'font_size': '10px'})
        head = workbook.add_format({'align': 'center', 'bold': True,
                                    'font_size': '20px'})
        date_center = workbook.add_format({'align': 'center', 'font_size': '10px'})
        line_data = workbook.add_format({'font_size': '10px'})
        main_data = workbook.add_format({'font_size': '10px', 'bold': True})
        main_amt = workbook.add_format({'font_size': '10px', 'num_format': symbol,
                                        'bold': True})
        line_amt = workbook.add_format({'font_size': '10px', 'num_format': symbol, })

        sheet.merge_range('B2:I3', 'Partner Ledger', head)

        if filter_val['state']:
            sheet.write('B6', 'Target Moves:', cell_format)
            if filter_val['state'] == 'posted':
                sheet.write('C6', 'All Posted Entries', cell_format_date)
            else:
                sheet.write('C6', 'All Entries', cell_format_date)

        if filter_val['date_from']:
            date_from = filter_val['date_from'].strftime("%Y-%m-%d")
            sheet.write('B7', 'From:', cell_format)
            sheet.write('C7', date_from, cell_format_date)

        if filter_val['date_to']:
            date_to = filter_val['date_to'].strftime("%Y-%m-%d")
            sheet.write('B8', 'To:', cell_format)
            sheet.write('C8', date_to, cell_format_date)

        sheet.write(10, 1, _('Partner'), cell_format)
        sheet.write(10, 2, _('JRNL'), cell_format)
        sheet.write(10, 3, _('Account'), cell_format)
        sheet.write(10, 4, _('Move Name'), cell_format)
        sheet.write(10, 5, _('Debit'), cell_format)
        sheet.write(10, 6, _('Credit'), cell_format)
        sheet.write(10, 7, _('Balance'), cell_format)

        sheet.set_column('B:B', 20)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 10)

        row_pos = 12
        col_pos = 1

        for value in data:
            sheet.write(row_pos, col_pos, data[value]['partner'], main_data)
            sheet.write(row_pos, col_pos + 4, float(data[value]['total_debit']),
                        main_amt)
            sheet.write(row_pos, col_pos + 5, float(data[value]['total_credit']),
                        main_amt)
            sheet.write(row_pos, col_pos + 6, float(data[value]['total_balance']),
                        main_amt)

            if data[value]['lines']:
                for move in data[value]['lines']:
                    row_pos += 1
                    if move.get('date'):
                        date = move['date'].strftime("%Y-%m-%d")
                        sheet.write(row_pos, col_pos, date, date_center)
                    sheet.write(row_pos, col_pos + 1, move.get('code'), line_data)
                    sheet.write(row_pos, col_pos + 2, move.get('a_name'), line_data)
                    sheet.write(row_pos, col_pos + 3, move.get('move_name'), line_data)
                    sheet.write(row_pos, col_pos + 4, float(move.get('debit')),
                                line_amt)
                    sheet.write(row_pos, col_pos + 5, float(move.get('credit')),
                                line_amt)
                    sheet.write(row_pos, col_pos + 6, float(move.get('progress')),
                                line_amt)

            row_pos += 1

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()
