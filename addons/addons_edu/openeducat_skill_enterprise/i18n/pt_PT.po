
msgid ""
msgstr ""
"Project-Id-Version:Odoo Server 15.0"
"Report-Msgid-Bugs-To:"
"POT-Creation-Date:2022-09-23 10:44+0000"
"PO-Revision-Date:2022-09-23 10:44+0000"
"Last-Translator:"
"Language-Team:"
"MIME-Version:1.0"
"Content-Type:text/plain; charset=UTF-8"
"Content-Transfer-Encoding:"
"Plural-Forms:"

#. module: openeducat_skill_enterprise
#. openerp-web
#: code:addons/openeducat_skill_enterprise/static/src/xml/skill_template.xml:0
#, python-format
msgid "ADD"
msgstr "ADICIONAR"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_needaction
msgid "Action Needed"
msgstr "Ação necessária"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__active
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__active
msgid "Active"
msgstr "Ativo"

#. module: openeducat_skill_enterprise
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_assessment_analysis_root
msgid "Analysis"
msgstr "Análise"

#. module: openeducat_skill_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_skill_category_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_skill_category_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_name_form
msgid "Archived"
msgstr "Arquivado"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__user_id
msgid "Assessed By"
msgstr "Avaliado por"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_attachment_count
msgid "Attachment Count"
msgstr "Contagem de anexos"

#. module: openeducat_skill_enterprise
#. openerp-web
#: code:addons/openeducat_skill_enterprise/static/src/xml/skill_template.xml:0
#, python-format
msgid "CREATE A NEW ENTRY"
msgstr "CRIAR UMA NOVA ENTRADA"

#. module: openeducat_skill_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_form
msgid "Cancel"
msgstr "Cancelar"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields.selection,name:openeducat_skill_enterprise.selection__op_student_skill_assessment__state__cancel
msgid "Cancelled"
msgstr "Cancelado"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__code
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__code
msgid "Code"
msgstr "Código"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__company_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__company_id
msgid "Company"
msgstr "Companhia"

#. module: openeducat_skill_enterprise
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_configuration_root
msgid "Configuration"
msgstr "Configuração"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__create_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__create_uid
msgid "Created by"
msgstr "Criado por"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__create_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__create_date
msgid "Created on"
msgstr "Criado em"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__date
msgid "Date"
msgstr "Encontro"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__display_name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__display_name
msgid "Display Name"
msgstr "Nome em Exibição"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields.selection,name:openeducat_skill_enterprise.selection__op_student_skill_assessment__state__done
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_form
msgid "Done"
msgstr "Feito"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields.selection,name:openeducat_skill_enterprise.selection__op_student_skill_assessment__state__draft
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_form
msgid "Draft"
msgstr "Rascunho"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_follower_ids
msgid "Followers"
msgstr "Seguidores"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_partner_ids
msgid "Followers (Partners)"
msgstr "Seguidores (Parceiros)"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__has_message
msgid "Has Message"
msgstr "Tem mensagem"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__id
msgid "ID"
msgstr "EU IRIA"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__message_needaction
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__message_unread
msgid "If checked, new messages require your attention."
msgstr "Se marcada, novas mensagens requerem sua atenção."

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__message_has_error
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__message_has_sms_error
msgid "If checked, some messages have a delivery error."
msgstr "Se marcado, algumas mensagens têm um erro de entrega."

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_is_follower
msgid "Is Follower"
msgstr "É seguidor"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name____last_update
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type____last_update
msgid "Last Modified on"
msgstr "Última modificação em"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__write_uid
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__write_uid
msgid "Last Updated by"
msgstr "Última atualização por"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__write_date
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__write_date
msgid "Last Updated on"
msgstr "Última atualização em"

#. module: openeducat_skill_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.student_portal_skills_information
msgid "Level"
msgstr "Nível"

#. module: openeducat_skill_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_type_form
msgid "Levels"
msgstr "Níveis"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_main_attachment_id
msgid "Main Attachment"
msgstr "Anexo principal"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_has_error
msgid "Message Delivery error"
msgstr "Erro de entrega de mensagem"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_ids
msgid "Messages"
msgstr "Mensagens"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_skill_category__name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__student_skill_name_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__student_skill_level_name_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__name
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__name
msgid "Name"
msgstr "Nome"

#. module: openeducat_skill_enterprise
#: code:addons/openeducat_skill_enterprise/models/op_student_skill_assessment.py:0
#: code:addons/openeducat_skill_enterprise/models/op_student_skill_assessment.py:0
#: code:addons/openeducat_skill_enterprise/models/op_student_skill_assessment.py:0
#: code:addons/openeducat_skill_enterprise/models/op_student_skill_assessment.py:0
#, python-format
msgid "New"
msgstr "Novo"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_needaction_counter
msgid "Number of Actions"
msgstr "Número de ações"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_has_error_counter
msgid "Number of errors"
msgstr "Número de erros"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__message_needaction_counter
msgid "Number of messages which requires an action"
msgstr "Número de mensagens que requerem uma ação"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__message_has_error_counter
msgid "Number of messages with delivery error"
msgstr "Número de mensagens com erro de entrega"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__message_unread_counter
msgid "Number of unread messages"
msgstr "Número de mensagens não lidas"

#. module: openeducat_skill_enterprise
#. openerp-web
#: code:addons/openeducat_skill_enterprise/static/src/js/student_skills_widget.js:0
#, python-format
msgid "Other"
msgstr "Outro"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__progress
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__progress
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level_name__progress
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__progress
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.student_portal_skills_information
msgid "Progress"
msgstr "Progresso"

#. module: openeducat_skill_enterprise
#. openerp-web
#: code:addons/openeducat_skill_enterprise/static/src/xml/skill_template.xml:0
#, python-format
msgid "Resumé empty"
msgstr "Currículo vazio"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_has_sms_error
msgid "SMS Delivery error"
msgstr "Erro de entrega de SMS"

#. module: openeducat_skill_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_form
msgid "Schedule"
msgstr "Cronograma"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields.selection,name:openeducat_skill_enterprise.selection__op_student_skill_assessment__state__schedule
msgid "Scheduled"
msgstr "Agendado"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__self_assessed
msgid "Self Assessed"
msgstr "Autoavaliação"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_assessment_view
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_op_student_skill_assessment_view_root
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.student_portal_skills_information
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_view_form
msgid "Skill Assessment"
msgstr "Avaliação de habilidades"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_assessment_line_pivot_view
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_op_student_skill_assessment_line_pivot_view_sub_menu
msgid "Skill Assessment Analysis"
msgstr "Análise de avaliação de habilidades"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_assessment_line_view
msgid "Skill Assessment Line"
msgstr "Linha de avaliação de habilidades"

#. module: openeducat_skill_enterprise
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_op_student_skill_type_view_sub_menu
msgid "Skill Assessment Template"
msgstr "Modelo de avaliação de habilidades"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__student_skill_type_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__student_skill_type_id
msgid "Skill Assessment Type"
msgstr "Tipo de Avaliação de Habilidade"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__student_skill_level_id
msgid "Skill Level"
msgstr "Nível de habilidade"

#. module: openeducat_skill_enterprise
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_op_student_skill_level_view_sub_menu
msgid "Skill Levels"
msgstr "Níveis de habilidade"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_name_view
msgid "Skill Name"
msgstr "Nome da habilidade"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__student_skill_type_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_name__skill_category_type_id
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_skill_category_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_skill_category_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_skill_category_tree
msgid "Skill Type"
msgstr "Tipo de habilidade"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__student_skill_assessment_line
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__student_skills_id
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_op_student_skill_view_sub_menu
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.student_portal_skills_information
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_type_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_view_form
msgid "Skills"
msgstr "Habilidades"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_open_op_skill_category_view
#: model:ir.ui.menu,name:openeducat_skill_enterprise.menu_skill_category
msgid "Skills Category"
msgstr "Categoria de habilidades"

#. module: openeducat_skill_enterprise
#: model:ir.model,name:openeducat_skill_enterprise.model_op_skill_category
msgid "Skills Category Creation"
msgstr "Criação de categoria de habilidades"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__state
msgid "State"
msgstr "Estado"

#. module: openeducat_skill_enterprise
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__student_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_line__student_id
msgid "Student"
msgstr "Aluna"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_line_view
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_view
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__student_skill_id
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_line_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_line_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_line_tree
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_tree
msgid "Student Skill"
msgstr "Habilidade do Aluno"

#. module: openeducat_skill_enterprise
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill_assessment
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill_assessment_line
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__student_skill_assessment_id
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_pivot
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_tree
msgid "Student Skill Assessment"
msgstr "Avaliação de habilidades do aluno"

#. module: openeducat_skill_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_line_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_line_pivot
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_line_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_assessment_line_tree
msgid "Student Skill Assessment Line"
msgstr "Linha de avaliação de habilidades do aluno"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_level_view
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill_level
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment_line__student_skill_level_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__student_skill_level_line
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_level_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_level_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_level_tree
msgid "Student Skill Level"
msgstr "Nível de habilidade do aluno"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_level_name_view
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill_level_name
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_level_name_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_level_name_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_level_name_tree
msgid "Student Skill Level Name"
msgstr "Nome do Nível de Habilidade do Aluno"

#. module: openeducat_skill_enterprise
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill_line
msgid "Student Skill Line"
msgstr "Linha de Habilidade do Aluno"

#. module: openeducat_skill_enterprise
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill_name
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_name_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_name_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_name_tree
msgid "Student Skill Name"
msgstr "Nome da Habilidade do Aluno"

#. module: openeducat_skill_enterprise
#: model:ir.actions.act_window,name:openeducat_skill_enterprise.act_op_student_skill_type_view
msgid "Student Skill Template"
msgstr "Modelo de Habilidade do Aluno"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill__student_skill_type_id
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_level__student_skill_type_id
msgid "Student Skill Type"
msgstr "Tipo de habilidade do aluno"

#. module: openeducat_skill_enterprise
#: model:ir.model,name:openeducat_skill_enterprise.model_op_student_skill_type
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_type_form
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_type_search
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.view_op_student_skill_type_tree
msgid "Student Skill Types"
msgstr "Tipos de habilidades do aluno"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student__student_skill_line
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_type__student_skills_line
msgid "Student Skills"
msgstr "Habilidades do Aluno"

#. module: openeducat_skill_enterprise
#: model_terms:ir.ui.view,arch_db:openeducat_skill_enterprise.student_portal_skills_information
msgid "Type"
msgstr "Modelo"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_unread
msgid "Unread Messages"
msgstr "Mensagens não lidas"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__message_unread_counter
msgid "Unread Messages Counter"
msgstr "Contador de mensagens não lidas"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,field_description:openeducat_skill_enterprise.field_op_student_skill_assessment__website_message_ids
msgid "Website Messages"
msgstr "Mensagens do site"

#. module: openeducat_skill_enterprise
#: model:ir.model.fields,help:openeducat_skill_enterprise.field_op_student_skill_assessment__website_message_ids
msgid "Website communication history"
msgstr "Histórico de comunicação do site"

