from .test_automated_marketing_common import TestAutomatedMarketingCommon


class TestActivity(TestAutomatedMarketingCommon):

    def setUp(self):
        super(TestActivity, self).setUp()

    def test_case_activity(self):
        types = self.activity.search([])
        for activity in types:
            activity._get_graph_statistics()
            activity.execute()
            activity._compute_statistics_graph_data()
            activity.action_sent()
            activity.action_replied()
            activity.action_clicked()
            activity.action_bounced()
            activity._compute_activity_statistics()
            activity._activity_statistics()
            activity._compute_allowed_parent_ids()
            activity._compute_domain()


class TestCampaign(TestAutomatedMarketingCommon):

    def setUp(self):
        super(TestCampaign, self).setUp()

    def test_case_campaign(self):
        types = self.campaign.search([])
        for campaign in types:
            campaign.template_view_action()
            campaign._compute_require_sync()
            campaign._compute_mass_mailing_count()
            campaign._compute_total_participate()
            campaign._compute_state_participate()
            campaign._compute_test_participant()
            campaign.action_start_campaign()
            campaign.action_stop_campaign()
            campaign.action_view_tracker_statistics()
            campaign._compute_link_tracker_click_count()
            campaign.execute_activities()
            campaign.synch_participants()
            campaign.action_set_synchronized()
            campaign.action_update_participants()


class TestParticipant(TestAutomatedMarketingCommon):

    def setUp(self):
        super(TestParticipant, self).setUp()

    def test_case_campaign(self):
        types = self.participant.search([])
        for participant in types:
            participant._compute_resource_ref()
            participant._inverse_set_resource_ref()
            participant.check_completed()
            participant.action_set_completed()
            participant.action_set_running()
            participant.action_set_unlink()


class TestTrace(TestAutomatedMarketingCommon):

    def setUp(self):
        super(TestTrace, self).setUp()

    def test_case_campaign(self):
        types = self.trace.search([])
        for trace in types:
            trace.participant_action_cancel()
            trace.action_cancel()
            trace.action_execute()


class TestMailingTrace(TestAutomatedMarketingCommon):

    def setUp(self):
        super(TestMailingTrace, self).setUp()

    def test_case_campaign(self):
        types = self.mailing_trace.search([])
        for mailing_trace in types:
            mailing_trace.set_clicked()
            mailing_trace.set_opened()
            mailing_trace.set_replied()
            mailing_trace.set_bounced()
