import json
import logging
import threading

from ast import literal_eval
from datetime import timedelta, date, datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.fields import Datetime
from odoo.exceptions import AccessError
from odoo.osv import expression
_logger = logging.getLogger(__name__)


class MarketingActivity(models.Model):
    _name = 'marketing.activity'
    _description = 'Marketing Activity'
    _inherits = {'utm.source': 'utm_source_id'}

    activity_type = fields.Selection([('email', 'Email'),
                                      ('action', 'Server Action')],
                                     string='Activity Type', default='email',
                                     required=True)
    mailing_type = fields.Selection([('mail', 'Email')],
                                    string='Mailing Type', readonly=True, store=True)
    server_action_id = fields.Many2one('ir.actions.server',
                                       string='Server Action', readonly=False,
                                       store=True)
    campaign_id = fields.Many2one('marketing.campaign',
                                  string='Campaign', index=True, ondelete='cascade',
                                  required=True)
    mass_mailing_id = fields.Many2one('mailing.mailing',
                                      string='Marketing Template')
    utm_source_id = fields.Many2one('utm.source', 'Source', ondelete='cascade',
                                    required=True)
    utm_campaign_id = fields.Many2one('utm.campaign', string='UTM Campaign',
                                      related='campaign_id.utm_campaign_id',
                                      readonly=True)
    domain = fields.Char(string='Applied Filter', default='[]',
                         compute='_compute_domain', store=True,
                         recursive=True, readonly=True)
    activity_domain = fields.Char(string='Activity Filter', default='[]',
                                  help='Domain that applies to this '
                                       'activity and its child activities')

    interval_number = fields.Integer(string='Send after', default=1)
    interval_type = fields.Selection([
        ('hours', 'Hours'),
        ('days', 'Days'),
        ('weeks', 'Weeks'),
        ('months', 'Months')], string='Delay Type',
        default='hours', required=True)
    interval_standardized = fields.Integer('Send after (in hours)', store=True,
                                           readonly=True)

    validity_duration = fields.Boolean('Validity Duration')
    validity_duration_number = fields.Integer(string='Valid during', default=0)
    validity_duration_type = fields.Selection([
        ('hours', 'Hours'),
        ('days', 'Days'),
        ('weeks', 'Weeks'),
        ('months', 'Months')],
        default='hours', required=True)

    model_id = fields.Many2one('ir.model', related='campaign_id.model_id',
                               string='Model', readonly=True)
    model_name = fields.Char(related='model_id.model', string='Model Name',
                             readonly=True)
    parent_id = fields.Many2one('marketing.activity', string='Activity',
                                index=True, readonly=False, store=True,
                                ondelete='cascade')
    child_ids = fields.One2many('marketing.activity', 'parent_id',
                                string='Child Activities')
    allowed_parent_ids = fields.Many2many('marketing.activity',
                                          string='Allowed parents',
                                          help='All activities which can '
                                               'be the parent of this one',
                                          compute='_compute_allowed_parent_ids')

    trigger_type = fields.Selection([
        ('begin', 'beginning of campaign'),
        ('activity', 'another activity'),
        ('mail_open', 'Mail: opened'),
        ('mail_not_open', 'Mail: not opened'),
        ('mail_reply', 'Mail: replied'),
        ('mail_not_reply', 'Mail: not replied'),
        ('mail_click', 'Mail: clicked'),
        ('mail_not_click', 'Mail: not clicked'),
        ('mail_bounce', 'Mail: bounced')], default='begin', required=True)
    trigger_category = fields.Selection([('email', 'Mail')], )
    require_sync = fields.Boolean('Require trace sync', copy=False)
    trace_ids = fields.One2many('marketing.trace', 'activity_id', string='Traces',
                                copy=False)
    processed = fields.Integer(compute='_compute_activity_statistics')
    rejected = fields.Integer(compute='_compute_activity_statistics')
    total_sent = fields.Integer(compute='_compute_activity_statistics')
    total_click = fields.Integer(compute='_compute_activity_statistics')
    total_open = fields.Integer(compute='_compute_activity_statistics')
    total_reply = fields.Integer(compute='_compute_activity_statistics')
    total_bounce = fields.Integer(compute='_compute_activity_statistics')
    statistics_graph_data = fields.Char(compute='_compute_statistics_graph_data')

    @api.model_create_multi
    def create(self, values_list):
        for values in values_list:
            campaign_id = values.get('campaign_id')
            values['require_sync'] = self.env['marketing.campaign'].browse(
                campaign_id).state == 'running'
        return super(MarketingActivity, self).create(values_list)

    def write(self, values):
        if any(fields in values.keys() for fields in
               ('interval_number', 'interval_type')):
            values['require_sync'] = True
        return super(MarketingActivity, self).write(values)

    def _get_graph_statistics(self):
        past_date = (Datetime.from_string(Datetime.now()) + timedelta(
            days=-14)).strftime('%Y-%m-%d 00:00:00')
        base = date.today() + timedelta(days=-14)
        map_data = {}
        self.env.cr.execute("""
            SELECT
                activity.id AS activity_id,
                trace.schedule_date::date AS dt,
                count(*) AS total,
                trace.state
            FROM
                marketing_trace AS trace
            JOIN
                marketing_activity AS activity
                ON (activity.id = trace.activity_id)
            WHERE
                activity.id IN %s AND trace.schedule_date >= %s
            GROUP BY activity.id , dt, trace.state
            ORDER BY dt;
        """, (tuple(self.ids), past_date))
        for stat in self.env.cr.dictfetchall():
            map_data[(stat['activity_id'], stat['dt'], stat['state'])] = stat['total']
        graph_data = {}
        date_range = []
        for day in range(0, 15):
            date_range.append(base + timedelta(days=day))
        for activity in self:
            success = []
            rejected = []
            for i in date_range:
                x = i.strftime('%d %b')
                success.append({
                    'x': x,
                    'y': map_data.get((activity._origin.id, i, 'processed'), 0)
                })
                rejected.append({
                    'x': x,
                    'y': map_data.get((activity._origin.id, i, 'rejected'), 0)
                })
            graph_data[activity._origin.id] = [
                {'points': success, 'label': _('Success'), 'color': '#00a5a0'},
                {'points': rejected, 'label': _('Rejected'), 'color': '#d9534f'}
            ]
        return graph_data

    def execute_traces(self, traces):
        self.ensure_one()
        new_traces = self.env['marketing.trace']
        if self.validity_duration:
            duration = relativedelta(
                **{self.validity_duration_type: self.validity_duration_number})
            invalid_traces = \
                traces.filtered(
                    lambda trace: not trace.schedule_date
                    or trace.schedule_date + duration < datetime.now()
                )
            invalid_traces.action_cancel()
            traces = traces - invalid_traces
        if len(literal_eval(self.activity_domain)) > 1:
            rec_domain = expression.AND(
                [literal_eval(self.campaign_id.domain), literal_eval(self.activity_domain)])
        else:
            rec_domain = literal_eval(self.campaign_id.domain)
        if rec_domain:
            rec_valid = self.env[self.model_name].search(rec_domain)
            rec_ids_domain = set(rec_valid.ids)

            traces_allowed = traces.filtered(
                lambda trace: trace.res_id in rec_ids_domain or trace.is_test)
            traces_rejected = \
                traces.filtered(
                    lambda trace: trace.res_id not in rec_ids_domain
                    and not trace.is_test)
        else:
            traces_allowed = traces
            traces_rejected = self.env['marketing.trace']

        if traces_allowed:
            activity_method = getattr(self, '_execute_%s' % (self.activity_type))
            activity_method(traces_allowed)
            new_traces |= self._generate_children_traces(traces_allowed)
            traces.mapped('participant_id').check_completed()

        if traces_rejected:
            traces_rejected.write({
                'state': 'rejected',
                'state_msg': _(
                    'Rejected by activity filter or record deleted / archived')
            })
            traces_rejected.mapped('participant_id').check_completed()
        return new_traces

    def _execute_email(self, traces):
        res_ids = [res for res in set(traces.mapped('res_id'))]
        mailing = self.mass_mailing_id.with_context(
            default_marketing_activity_id=self.ids[0],
            active_ids=res_ids,
        )

        if not self.env.is_superuser() and not self.user_has_groups(
                'openeducat_automated_marketing.automated_marketing_user_group'):
            raise AccessError(
                _('To use this feature you should be an administrator '
                  'or belong to the Automated Marketing group.'))
        try:
            mailing.sudo().action_send_mail(res_ids)
        except Exception as e:
            _logger.warning(
                'Automated Marketing: activity <%s> encountered mass mailing issue %s',
                self.id, str(e), exc_info=True)
            traces.write({
                'state': 'error',
                'schedule_date': Datetime.now(),
                'state_msg': 'Exception in mass mailing: ',
            })
        else:
            failed_stats = self.env['mailing.trace'].sudo().search([
                ('marketing_trace_id', 'in', traces.ids),
                ('trace_status', 'in', ['error', 'bounce', 'cancel'])
            ])
            error_doc_ids = [
                stat.res_id for stat in failed_stats if stat.trace_status in (
                    'error', 'bounce')]
            cancel_doc_ids = [
                stat.res_id for stat in failed_stats if stat.trace_status == 'cancel']

            processed_traces = traces
            canceled_traces = traces.filtered(
                lambda trace: trace.res_id in cancel_doc_ids)
            error_traces = traces.filtered(lambda trace: trace.res_id in error_doc_ids)

            if canceled_traces:
                canceled_traces.write({
                    'state': 'canceled',
                    'schedule_date': Datetime.now(),
                    'state_msg': _('Email canceled')
                })
                processed_traces = processed_traces - canceled_traces
            if error_traces:
                error_traces.write({
                    'state': 'error',
                    'schedule_date': Datetime.now(),
                    'state_msg': _('Email failed')
                })
                processed_traces = processed_traces - error_traces
            if processed_traces:
                processed_traces.write({
                    'state': 'processed',
                    'schedule_date': Datetime.now(),
                })
        return True

    def _generate_children_traces(self, traces):
        child_traces = self.env['marketing.trace']
        for activity in self.child_ids:
            activity_offset = relativedelta(
                **{activity.interval_type: activity.interval_number})

            for trace in traces:
                vals = {
                    'parent_id': trace.id,
                    'participant_id': trace.participant_id.id,
                    'activity_id': activity.id
                }
                if activity.trigger_type in ['activity', 'mail_not_open',
                                             'mail_not_click', 'mail_not_reply']:
                    vals['schedule_date'] = Datetime.from_string(
                        trace.schedule_date) + activity_offset
                child_traces |= child_traces.create(vals)

        return child_traces

    def _execute_action(self, traces):
        if not self.server_action_id:
            return False

        traces_ok = self.env['marketing.trace']
        for trace in traces:
            action = self.server_action_id.with_context(
                active_model=self.model_name,
                active_ids=[trace.res_id],
                active_id=trace.res_id,
            )
            try:
                action.run()
            except Exception as e:
                _logger.warning(
                    'Automated Marketing: '
                    'activity <%s> encountered server action issue %s',
                    self.id, str(e), exc_info=True)
                trace.write({
                    'state': 'error',
                    'schedule_date': Datetime.now(),
                    'state_msg': _('Exception in server action: %s', str)(e),
                })
            else:
                traces_ok |= trace

        traces_ok.write({
            'state': 'processed',
            'schedule_date': Datetime.now(),
        })
        return True

    @api.depends('activity_type', 'trace_ids')
    def _compute_statistics_graph_data(self):
        if not self.ids:
            date_range = [date.today() - timedelta(days=d) for d in range(0, 15)]
            date_range.reverse()
            default_values = [{'x': date_item.strftime('%d %b'), 'y': 0} for date_item
                              in date_range]
            self.statistics_graph_data = json.dumps([
                {'points': default_values, 'label': _('Success'), 'color': '#21B799'},
                {'points': default_values, 'label': _('Rejected'), 'color': '#d9534f'}])
        else:
            activity_data = {activity._origin.id: {} for activity in self}
            for act_id, graph_data in self._get_graph_statistics().items():
                activity_data[act_id]['statistics_graph_data'] = json.dumps(graph_data)
            for activity in self:
                activity.update(activity_data[activity._origin.id])

    # action for marketing activity filtered template
    def _action_documents_filtered(self, view_filter):
        if not self.mass_mailing_id:  # Only available for mass mailing
            return False
        action = self.env["ir.actions.actions"]._for_xml_id(
            "openeducat_automated_marketing.marketing_participants_action_mail")
        participant_ids = self.trace_ids.filtered(
            lambda stat: stat[view_filter]).mapped("participant_id").ids
        action.update({
            'display_name': _('Participants of %s (%s)') % (self.name, view_filter),
            'domain': [('id', 'in', participant_ids)],
            'context': dict(self._context, create=False)
        })
        return action

    def action_sent(self):
        return self._action_documents_filtered('sent')

    def action_replied(self):
        return self._action_documents_filtered('replied')

    def action_clicked(self):
        return self._action_documents_filtered('clicked')

    def action_bounced(self):
        return self._action_documents_filtered('bounced')

    # compute activity statistics

    @api.depends('activity_type', 'trace_ids')
    def _compute_activity_statistics(self):
        self.update({
            'total_bounce': 0, 'total_reply': 0, 'total_sent': 0,
            'rejected': 0, 'total_click': 0, 'processed': 0, 'total_open': 0,
        })
        if self.ids:
            activity_data = {activity._origin.id: {} for activity in self}
            activity_record = self._activity_statistics()
            for record in activity_record:
                activity_data[record.pop('activity_id')].update(record)
            for activity in self:
                activity.update(activity_data[activity._origin.id])

    def _activity_statistics(self):
        self.env.cr.execute("""
            SELECT trace.activity_id,
                COUNT(stat.sent_datetime) AS total_sent,
                COUNT(stat.links_click_datetime) AS total_click,
                COUNT(stat.trace_status) FILTER (WHERE stat.trace_status = 'reply') AS total_reply,
                COUNT(stat.trace_status) FILTER (WHERE stat.trace_status in ('open', 'reply')) AS total_open,
                COUNT(stat.trace_status) FILTER (WHERE stat.trace_status = 'bounce') AS total_bounce,
                COUNT(trace.state) FILTER (WHERE trace.state = 'processed') AS processed,
                COUNT(trace.state) FILTER (WHERE trace.state = 'rejected') AS rejected
            FROM
                marketing_trace AS trace
            LEFT JOIN
                mailing_trace AS stat
                ON (stat.marketing_trace_id = trace.id)
            JOIN
                marketing_participant AS part
                ON (trace.participant_id = part.id)
            WHERE
                trace.activity_id IN %s
            GROUP BY
                trace.activity_id;
        """, (tuple(self.ids),)) # noqa
        data = self.env.cr.dictfetchall()
        return data

    @api.depends('trigger_type', 'campaign_id.marketing_activity_ids')
    def _compute_allowed_parent_ids(self):
        for activity in self:
            if activity.trigger_type == 'activity':
                activity.allowed_parent_ids = \
                    activity.campaign_id.marketing_activity_ids.filtered(
                        lambda parent_id: parent_id.ids != activity.ids)
            elif activity.trigger_category:
                activity.allowed_parent_ids = \
                    activity.campaign_id.marketing_activity_ids.filtered(
                        lambda parent_id: parent_id.ids != activity.ids
                        and parent_id.activity_type == activity.trigger_category)
            else:
                activity.allowed_parent_ids = False

    # compute applied domain for activity

    @api.depends('activity_domain', 'campaign_id.domain', 'parent_id.domain')
    def _compute_domain(self):
        for activity in self:
            activity_domain = expression.AND([literal_eval(activity.campaign_id.domain),
                                              literal_eval(activity.activity_domain)])
            parent_activity = activity.parent_id
            while parent_activity:
                activity_domain = expression.AND(
                    [activity_domain, literal_eval(parent_activity.activity_domain)])
                parent_activity = parent_activity.parent_id
            activity.domain = activity_domain

    def execute(self, domain=None):
        auto_commit = not getattr(threading.currentThread(), 'testing', False)
        trace_domain = [
            ('schedule_date', '<=', Datetime.now()),
            ('state', '=', 'scheduled'),
            ('activity_id', 'in', self.ids),
            ('participant_id.state', '=', 'running'),
        ]
        if domain:
            trace_domain += domain

        traces = self.env['marketing.trace'].search(trace_domain)
        trace_to_activities = dict()
        for trace in traces:
            if trace.activity_id not in trace_to_activities:
                trace_to_activities[trace.activity_id] = trace
            else:
                trace_to_activities[trace.activity_id] |= trace

        BATCH_SIZE = 500  # same batch size as the MailComposer
        for activity, traces in trace_to_activities.items():
            for traces_batch in (traces[i:i + BATCH_SIZE] for i in
                                 range(0, len(traces), BATCH_SIZE)):
                activity.execute_traces(traces_batch)
                if auto_commit:
                    self.env.cr.commit()
