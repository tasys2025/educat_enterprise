from odoo import fields, models


class MailingTrace(models.Model):
    _inherit = 'mailing.trace'

    marketing_trace_id = fields.Many2one('marketing.trace', string='Marketing Trace',
                                         index=True, ondelete='cascade')

    def set_clicked(self, domain=None):
        traces = super(MailingTrace, self).set_clicked(domain)
        mail_traces = traces.filtered(
            lambda trace: trace.marketing_trace_id
            and trace.marketing_trace_id.activity_type == 'email')
        for trace_id in mail_traces.marketing_trace_id:
            trace_id.process_event('mail_click')
        return traces

    def set_opened(self, domain=None):
        traces = super(MailingTrace, self).set_opened(domain)
        mail_traces = traces.filtered(
            lambda trace: trace.marketing_trace_id
            and trace.marketing_trace_id.activity_type == 'email')
        for trace_id in mail_traces.marketing_trace_id:
            trace_id.process_event('mail_open')
        return traces

    def set_replied(self, domain=None):
        traces = super(MailingTrace, self).set_replied(domain)
        mail_traces = traces.filtered(
            lambda trace: trace.marketing_trace_id
            and trace.marketing_trace_id.activity_type == 'email')
        for trace_id in mail_traces.marketing_trace_id:
            trace_id.process_event('mail_reply')
        return traces

    def set_bounced(self, domain=None):
        traces = super(MailingTrace, self).set_bounced(domain)
        mail_traces = traces.filtered(
            lambda trace: trace.marketing_trace_id
            and trace.marketing_trace_id.activity_type == 'email')
        for trace_id in mail_traces.marketing_trace_id:
            trace_id.process_event('mail_bounce')
        return traces
