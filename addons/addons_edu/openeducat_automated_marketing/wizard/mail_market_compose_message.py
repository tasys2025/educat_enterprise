# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    marketing_activity_id = fields.Many2one('marketing.activity', string='Marketing Activity')

    def get_mail_values(self, res_ids):
        res = super(MailComposeMessage, self).get_mail_values(res_ids)

        if self.composition_mode == 'mass_mail' and (self.mass_mailing_name or self.mass_mailing_id) and self.marketing_activity_id:

            mail_marketing_traces = self.env['marketing.trace'].search([('activity_id', '=', self.marketing_activity_id.id), ('res_id', 'in', res_ids)])
            last_marketing_traces= dict((mail_trace.res_id, mail_trace.id) for mail_trace in mail_marketing_traces)

            for res_id in res_ids:
                mail_values = res[res_id]
                mail_traces_values = mail_values.get('mailing_trace_ids')  # [(0, 0, stat_vals)]
                if mail_traces_values and len(mail_traces_values[0]) == 3:
                    mail_statistics_dict = mail_traces_values[0][2]
                    if last_marketing_traces.get(res_id):
                        mail_statistics_dict['marketing_trace_id'] = last_marketing_traces[res_id]

        return res
