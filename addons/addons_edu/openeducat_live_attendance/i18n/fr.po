# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_live_attendance
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 08:49+0000\n"
"PO-Revision-Date: 2022-12-14 08:49+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid ". Narrow your search to see more choices."
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Attendance"
msgstr ""

#. module: openeducat_live_attendance
#: model:ir.model,name:openeducat_live_attendance.model_op_attendance_sheet
msgid "Attendance Sheet"
msgstr "Feuille de présence"

#. module: openeducat_live_attendance
#: model:ir.model,name:openeducat_live_attendance.model_calendar_event
msgid "Calendar Event"
msgstr "Événement de calendrier"

#. module: openeducat_live_attendance
#: model:ir.model.fields,field_description:openeducat_live_attendance.field_op_attendance_sheet__date_time
msgid "Date Time"
msgstr "Date"

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Invitation Link"
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Invite people"
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "No user found that is not already a member of this channel."
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Password"
msgstr ""

#. module: openeducat_live_attendance
#: model:ir.model.fields,field_description:openeducat_live_attendance.field_calendar_event__register_id
#: model:ir.model.fields,field_description:openeducat_live_attendance.field_op_meeting__register_id
msgid "Register"
msgstr "S'inscrire"

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Save"
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Select Register:"
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Select Sheet:"
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Selected users:"
msgstr ""

#. module: openeducat_live_attendance
#: model:ir.model.fields,field_description:openeducat_live_attendance.field_calendar_event__sheet_id
#: model:ir.model.fields,field_description:openeducat_live_attendance.field_op_meeting__sheet_id
msgid "Sheet"
msgstr "Feuille"

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Showing"
msgstr ""

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "Type the name of a person"
msgstr ""

#. module: openeducat_live_attendance
#: model:ir.model.fields,field_description:openeducat_live_attendance.field_op_attendance_sheet__attendance_sheet_date
msgid "attendance sheet date"
msgstr "Date de la feuille de présence"

#. module: openeducat_live_attendance
#. openerp-web
#: code:addons/openeducat_live_attendance/static/src/xml/channel_invitation_form.xml:0
#, python-format
msgid "results out of"
msgstr ""
