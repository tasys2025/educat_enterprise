msgid ""
msgstr ""
"Project-Id-Version: openeducat11enttranslation\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-12 06:01+0000\n"
"PO-Revision-Date: 2018-03-07 06:49-0500\n"
"Last-Translator: aakash.kodwani <aakash@openeducat.org>\n"
"Language-Team: French, Canada\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: openeducat11enttranslation\n"
"X-Crowdin-Language: fr-CA\n"
"X-Crowdin-File: /v11_crowdin/openeducat_health_enterprise/i18n/openeducat_health_enterprise.pot\n"
"Language: fr_CA\n"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "A+ve"
msgstr "A + ve"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "A-ve"
msgstr "A-ve"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "AB+ve"
msgstr "AB + ve"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "AB-ve"
msgstr "AB-ve"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_regular_checkup
msgid "Any Regular Checkup Required?"
msgstr "N’importe quel infosanté requise ?"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "B+ve"
msgstr "B + ve"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "B-ve"
msgstr "B-ve"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_blood_group
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
msgid "Blood Group"
msgstr "Groupe sanguin"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_name
msgid "Checkup Detail"
msgstr "Bilan de santé en détail"

#. module: openeducat_health_enterprise
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_form
msgid "Checkup Details"
msgstr "Détails du bilan de santé"

#. module: openeducat_health_enterprise
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_form
msgid "Checkup Line"
msgstr "Bilan de santé ligne"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_health_line
msgid "Checkup Lines"
msgstr "Lignes du bilan de santé"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_recommendation
msgid "Checkup Recommendation"
msgstr "Bilan de santé recommandation"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_company_id
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_company_id
msgid "Company"
msgstr "Compagnie"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_create_uid
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_create_uid
msgid "Created by"
msgstr "Créé par"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_create_date
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_create_date
msgid "Created on"
msgstr "Créée le"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_date
msgid "Date"
msgstr "Date"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_display_name
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_display_name
msgid "Display Name"
msgstr "Nom d’affichage"

#. module: openeducat_health_enterprise
#: code:addons/openeducat_health_enterprise/models/health.py:59
#, python-format
msgid "Enter proper height and weight!"
msgstr "Entrez le poids et la taille adéquate !"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_eyeglasses_no
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
msgid "Eye Glasses"
msgstr "Lunettes"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_eyeglasses
msgid "Eye Glasses?"
msgstr "Lunettes ?"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_faculty_id
#: selection:op.health,type:0
msgid "Faculty"
msgstr "Corps professoral"

#. module: openeducat_health_enterprise
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
msgid "Group By..."
msgstr "Group By..."

#. module: openeducat_health_enterprise
#: model:ir.actions.act_window,name:openeducat_health_enterprise.act_open_op_health_view
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_health_id
#: model:ir.ui.menu,name:openeducat_health_enterprise.menu_op_health
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_form
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_tree
msgid "Health"
msgstr "Santé"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_faculty_health_faculty_lines
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_student_health_lines
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.health_faculty_form_view
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.health_student_form_view
msgid "Health Detail"
msgstr "Détail de la santé"

#. module: openeducat_health_enterprise
#: model:ir.model,name:openeducat_health_enterprise.model_op_health
msgid "Health Detail for Students and Faculties"
msgstr "Détail de la santé pour les étudiants et les facultés"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_height
msgid "Height(C.M.)"
msgstr "Height(C.M.)"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_id
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_id
msgid "ID"
msgstr "ID"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health___last_update
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line___last_update
msgid "Last Modified on"
msgstr "Dernière modification le"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_write_uid
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_write_uid
msgid "Last Updated by"
msgstr "Dernière mise à jour par"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_line_write_date
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_write_date
msgid "Last Updated on"
msgstr "Dernière mise à jour sur"

#. module: openeducat_health_enterprise
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
msgid "Major Disease"
msgstr "Maladie de major"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_major_diseases_note
msgid "Major Diseases"
msgstr "Principales maladies"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_major_diseases
msgid "Major Diseases?"
msgstr "Principales maladies ?"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "O+ve"
msgstr "O + ve"

#. module: openeducat_health_enterprise
#: selection:op.health,blood_group:0
msgid "O-ve"
msgstr "O-ve"

#. module: openeducat_health_enterprise
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_form
msgid "Other Details"
msgstr "Autres détails"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_physical_challenges_note
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
msgid "Physical Challenge"
msgstr "Défi physique"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_physical_challenges
msgid "Physical Challenge?"
msgstr "Défi physique ?"

#. module: openeducat_health_enterprise
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
msgid "Regular Checkup"
msgstr "Infosanté"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_student_id
#: selection:op.health,type:0
msgid "Student"
msgstr "Student"

#. module: openeducat_health_enterprise
#: model:ir.ui.view,arch_db:openeducat_health_enterprise.view_op_health_search
msgid "Student Name"
msgstr "Nom de l’étudiant"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_type
msgid "Type"
msgstr "Type de"

#. module: openeducat_health_enterprise
#: model:ir.model.fields,field_description:openeducat_health_enterprise.field_op_health_weight
msgid "Weight"
msgstr "Poids"

#. module: openeducat_health_enterprise
#: model:ir.model,name:openeducat_health_enterprise.model_op_faculty
msgid "op.faculty"
msgstr "op.Faculty"

#. module: openeducat_health_enterprise
#: model:ir.model,name:openeducat_health_enterprise.model_op_health_line
msgid "op.health.line"
msgstr "op.Health.Line"

#. module: openeducat_health_enterprise
#: model:ir.model,name:openeducat_health_enterprise.model_op_student
msgid "op.student"
msgstr "op.Student"

