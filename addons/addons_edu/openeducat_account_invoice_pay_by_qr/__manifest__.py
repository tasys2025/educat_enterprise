# Part of OpenEduCat. See LICENSE file for full copyright & licensing details.

##############################################################################
#
#    OpenEduCat Inc
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
##############################################################################

{
    'name': "OpenEduCat Account Invoice Pay by QR Code",
    'version': "16.0",
    'summary': """Account Invoice Pay by QR Code""",
    'description': """Invoice payment link by scanning the QR Code""",
    'author': 'OpenEduCat Inc',
    'website': 'http://www.openeducat.org',
    'category': 'Accounting',
    'depends': ['account', 'payment', 'openeducat_core_enterprise'],
    'data': [
        'security/account_security.xml',
        'report/invoice_inherit.xml',
    ],
    'demo': [
    ],
    'auto_install': False,
    'application': False,
    'license': 'Other proprietary',
}
