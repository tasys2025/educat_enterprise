# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* openeducat_lms_sale
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 16.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-13 05:35+0000\n"
"PO-Revision-Date: 2022-12-13 05:35+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: openeducat_lms_sale
#: model:ir.model.fields.selection,name:openeducat_lms_sale.selection__op_course__type__free
msgid "Free"
msgstr "Gratuito"

#. module: openeducat_lms_sale
#: model:ir.model,name:openeducat_lms_sale.model_op_course
msgid "LMS Course"
msgstr "Corso LMS"

#. module: openeducat_lms_sale
#: model:ir.model,name:openeducat_lms_sale.model_op_course_enrollment
msgid "LMS Course Enrollment"
msgstr "Iscrizione del corso LMS"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course_enrollment__order_id
msgid "Order"
msgstr "Ordine"

#. module: openeducat_lms_sale
#: model:ir.model.fields.selection,name:openeducat_lms_sale.selection__op_course__type__paid
msgid "Paid"
msgstr "Pagato"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course__price
msgid "Price"
msgstr "Prezzo"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course__product_id
msgid "Product"
msgstr "Prodotto"

#. module: openeducat_lms_sale
#: model:ir.model,name:openeducat_lms_sale.model_sale_order
msgid "Sales Order"
msgstr "Ordine di vendita"

#. module: openeducat_lms_sale
#: model:ir.model.fields,field_description:openeducat_lms_sale.field_op_course__type
msgid "Type"
msgstr "Tipo"
