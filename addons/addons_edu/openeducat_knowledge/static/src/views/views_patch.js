/** @odoo-module */

import { useService } from '@web/core/utils/hooks';
import { KanbanController } from "@web/views/kanban/kanban_controller";
import { kanbanView } from "@web/views/kanban/kanban_view";
import { ListController } from '@web/views/list/list_controller';
import { listView } from '@web/views/list/list_view';
import { patch } from "@web/core/utils/patch";
import { registry } from "@web/core/registry";

const InternalPatch = {

    setup() {
        this._super(...arguments);
        this.orm = useService('orm');
    },

    async createRecord() {
        const articleId = await this.orm.call('knowledge.article', 'article_create', [], {
            is_article_item: true,
            is_private: false,
            parent_id: this.props.context.active_id || false
        });
        this.actionService.doAction(
            await this.orm.call('knowledge.article', 'action_home_page', [articleId]),
            {}
        );
    },
};

export class KnowledgeInternalKanban extends KanbanController {}

patch(KnowledgeInternalKanban.prototype, 'knowledge_embedded_kanban_controller', InternalPatch);

registry.category("views").add('knowledge_article_view_kanban_internal', {
    ...kanbanView,
    Controller: KnowledgeInternalKanban,
});

export class KnowledgeInternalTree extends ListController {}

patch(KnowledgeInternalTree.prototype, 'knowledge_embedded_list_controller', InternalPatch);

registry.category("views").add('knowledge_article_view_tree_internal', {
    ...listView,
    Controller: KnowledgeInternalTree
});
