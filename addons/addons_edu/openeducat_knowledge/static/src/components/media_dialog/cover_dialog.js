/** @odoo-module **/

import { AutoResizeImage, ImageSelector } from '@web_editor/components/media_dialog/image_selector';
import { ConfirmationDialog } from '@web/core/confirmation_dialog/confirmation_dialog';
import { Dialog } from '@web/core/dialog/dialog';
import { UnsplashError } from '@web_unsplash/components/media_dialog/image_selector';
import { useService } from "@web/core/utils/hooks";

const { Component } = owl;

export class AutoResizeCover extends AutoResizeImage {
    setup() {
        super.setup();
        this.orm = useService("orm");
    }

    remove() {
        this.dialogs.add(ConfirmationDialog, {
            body: this.env._t("Are you sure you want to delete this cover ? It will be removed from every article it is used in."),
            confirm: async () => {
                const res = await this.orm.unlink(this.props.model,
                    [this.props.resId],
                );
                if (res) {
                    this.props.onRemoved(this.props.id, this.props.resId);
                }
            },
        });
    }
}

export class CoverSelector extends ImageSelector {
    setup() {
        super.setup();
        this.state.needle = this.props.searchTerm;
        this.searchUnsplash(this.state.needle);
    }

    get attachmentsDomain() {
        return ['&', ['res_model', '=', this.props.resModel], ['name', 'ilike', this.state.needle]];
    }

    onClickAttachment(attachment) {
        this.props.save(attachment.res_id);
    }

    onClickRecord(unsplashRecord) {
        this.uploadService.uploadUnsplashRecords(
            [unsplashRecord],
            {resModel: this.props.resModel, resId: this.props.resId},
            async (attachments) => this.onUploaded(attachments[0])
        );
    }

    onRemoved(attachmentId, coverId) {
        super.onRemoved(attachmentId);
        if (coverId === this.props.articleCoverId) {
            this.props.save(false);
        }
    }

    async onUploaded(attachment) {
        const data = await this.orm.call('knowledge.article', 'save_attachment_cover', [this.props.dataId, attachment.id])
        this.props.save(data);
    }

    get selectedAttachmentIds() {
        return [];
    }

    get selectedRecordIds() {
        return [];
    }
}

CoverSelector.defaultProps = {
    resModel: 'knowledge.article',
    orientation: 'landscape',
};
CoverSelector.attachmentsListTemplate = 'openeducat_knowledge.CoversListTemplate';
CoverSelector.components = {
    ...ImageSelector.components,
    AutoResizeCover,
    UnsplashError,
};

export class CoverDialog extends Component {
    setup() {
        this.size = 'xl';
        this.contentClass = 'o_select_media_dialog';
        this.title = this.env._t("Select a cover");
    }

    save(coverId) {
        this.props.save(coverId);
        this.props.close();
    }
}
CoverDialog.template = 'openeducat_knowledge.KnowledgeCoverDialog';
CoverDialog.components = {
    CoverSelector,
    Dialog
};
