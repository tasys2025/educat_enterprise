/** @odoo-module */

import { _t } from "web.core";
import { CalendarRenderer } from "@web/views/calendar/calendar_renderer";
import { GraphRenderer } from "@web/views/graph/graph_renderer";
import { KanbanRenderer } from "@web/views/kanban/kanban_renderer";
import { ListRenderer } from "@web/views/list/list_renderer";
import { patch } from "@web/core/utils/patch";
import { PivotRenderer } from "@web/views/pivot/pivot_renderer";
import { SelectCreateDialog } from "@web/views/view_dialogs/select_create_dialog";
import { useBus, useOwnedDialogs, useService } from "@web/core/utils/hooks";

const FavouriteMenuPatch = {
    setup() {
        this._super(...arguments);
        if (this.env.searchModel) {
            useBus(this.env.searchModel, 'insert_knowledge_view', this.insertKnowledgeView.bind(this));
            useBus(this.env.searchModel, 'insert_knowledge_link', this.insertKnowledgeLink.bind(this));
            this.orm = useService('orm');
            this.actionService = useService('action');
            this.addDialog = useOwnedDialogs();
        }
    },

    _getViewContext: function () {
        if (this.env.searchModel) {
            return this.env.searchModel.context;
        }
        return {};
    },

    _getViewState: function () {
        const state = {
            knowledge_embedded_view_framework: 'owl'
        };
        if (this.env.searchModel) {
            state.knowledge_search_model_state = JSON.stringify(this.env.searchModel.exportState());
        }
        return state;
    },

    insertKnowledgeView() {
        const config = this.env.config;
        if (config.actionType !== 'ir.actions.act_window') {
            return;
        }
        this.openArticleSelector(async id => {
            const context = Object.assign({}, this._getViewContext(), this._getViewState());
            await this.orm.call('knowledge.article', 'add_view_knowledge',
                [[id],
                config.actionId,
                config.viewType,
                config.getDisplayName(),
                context]
            );
            this.actionService.doAction('openeducat_knowledge.ir_actions_server_knowledge_home_page', {
                additionalContext: {
                    res_id: id
                }
            });
        });
    },

    insertKnowledgeLink() {
        const config = this.env.config;
        if (config.actionType !== 'ir.actions.act_window') {
            return;
        }
        this.openArticleSelector(async id => {
            const context = this._getViewContext();
            await this.orm.call('knowledge.article', 'add_view_link',
                [[id],
                config.actionId,
                config.viewType,
                config.getDisplayName(),
                context]
            );
            this.actionService.doAction('openeducat_knowledge.ir_actions_server_knowledge_home_page', {
                additionalContext: {
                    res_id: id
                }
            });
        });
    },

    openArticleSelector: function (onSelectCallback) {
        this.addDialog(SelectCreateDialog, {
            title: _t('Select an article'),
            noCreate: false,
            multiSelect: false,
            resModel: 'knowledge.article',
            context: {},
            domain: [
                ['user_has_write_access', '=', true]
            ],
            onSelected: resIds => {
                onSelectCallback(resIds[0]);
            },
            onCreateEdit: async () => {
                const articleId = await this.orm.call('knowledge.article', 'article_create', [], {
                    is_private: true
                });
                onSelectCallback(articleId);
            },
        });
    },
}

patch(CalendarRenderer.prototype, 'knowledge_calendar_favourite', FavouriteMenuPatch);
patch(GraphRenderer.prototype, 'knowledge_graph_favourite', FavouriteMenuPatch);
patch(KanbanRenderer.prototype, 'knowledge_kanban_favourite', FavouriteMenuPatch);
patch(ListRenderer.prototype, 'knowledge_list_favourite', FavouriteMenuPatch);
patch(PivotRenderer.prototype, 'knowledge_pivot_favourite', FavouriteMenuPatch);