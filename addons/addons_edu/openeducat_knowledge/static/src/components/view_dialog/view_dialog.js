/** @odoo-module */

import { _t } from "@web/core/l10n/translation";
import { Dialog } from "@web/core/dialog/dialog";
import { useWowlService } from "@web/legacy/utils";

const {
    Component,
    onMounted,
    onRendered,
    useRef,
    xml } = owl;

export class ViewDialog extends Component {
    setup () {
        this.input = useRef('input');
        onMounted(() => {
            window.setTimeout(() => {
                this.input.el.focus();
            }, 0);
        });
    }

    save () {
        this.props.save(this.input.el.value);
        this.props.close();
    }

    get placeholder () {
        if (this.props.viewType === 'kanban') {
            return _t('e.g. Buildings');
        }
        if (this.props.viewType === 'list') {
            return _t('e.g. Todos');
        }
    }

    get title () {
        if (this.props.viewType === 'list') {
            return _t('Insert a List View');
        }
        if (this.props.viewType === 'kanban') {
            return _t('Insert a Kanban View');
        }
        return _t('Embed a View');
    }

    onInputKeydown (event) {
        if (event.key === 'Enter') {
            this.save();
        }
    }
}

ViewDialog.template = 'openeducat_knowledge.ViewDialog';
ViewDialog.components = { Dialog };
ViewDialog.props = {
    defaultName: { type: String, optional: true },
    isNew: { type: Boolean, optional: true },
    viewType: { type: String },
    save: { type: Function },
    close: { type: Function, optional: true }
};

export class ViewDialogWrapper extends Component {
    setup () {
        this.dialogs = useWowlService('dialog');
        onRendered(() => {
            this.dialogs.add(ViewDialog, this.props);
        });
    }
}
ViewDialogWrapper.template = xml``;
ViewDialogWrapper.props = {
    defaultName: { type: String, optional: true },
    isNew: { type: Boolean, optional: true },
    viewType: { type: String },
    save: { type: Function },
    close: { type: Function, optional: true }
};
