/** @odoo-module */

import { _t } from "web.core";
import { AlertDialog } from "@web/core/confirmation_dialog/confirmation_dialog";
import { sprintf } from "@web/core/utils/strings";
import { useService } from "@web/core/utils/hooks";
import utils from "web.utils";

const { markup, Component } = owl;


export class File extends Component {
    setup() {
        super.setup();
        if (!this.props.readonly) {
            this.props.anchor.setAttribute('contenteditable', 'false');
        }
        this.dialogService = useService('dialog');
        //this.knowledgeCommandsService = useService('knowledgeCommandsService');
        this.rpcService = useService('rpc');
        this.uiService = useService('ui');
        //this.targetRecordInfo = this.knowledgeCommandsService.getCommandsRecordInfo();
        if (this.props.fileImage) {
            this.props.fileImage = markup(this.props.fileImage);
        }
    }

    async onClickDownload(ev) {
        const fileLink = this.props.anchor.querySelector('.o_knowledge_file_image > a');
        if (!fileLink || !fileLink.hasAttribute('href')) {
            return;
        }
        const title = fileLink.getAttribute('title');
        const href = fileLink.getAttribute('href');
        try {
            const response = await window.fetch(href);
            const blob = await response.blob();
            const file = new Blob([blob], {
                type: 'application/octet-stream'
            });
            const downloadLink = document.createElement('a');
            downloadLink.href = URL.createObjectURL(file);
            downloadLink.download = title;
            downloadLink.click();
        } catch {
            this.dialogService.add(AlertDialog, {
                body: sprintf(_t('Oops, the file %s could not be found. Please replace this file box by a new one to re-upload the file.'), title),
                title: _t('Missing File'),
                confirm: () => {},
                confirmLabel: _t('Ok'),
            });
        }
    }
}

File.template = "openeducat_knowledge.FileBehavior";
File.props = {
    readonly: { type: Boolean },
    anchor: { type: Element },
    wysiwyg: { type: Object, optional: true},
    root: { type: Element },
    record: { type: Object },
    fileName: { type: String, optional: true },
    fileExtension: { type: String, optional: true },
    fileImage: { type: String, optional: true },
};
