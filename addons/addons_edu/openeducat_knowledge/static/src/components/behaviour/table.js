/** @odoo-module */

const {
    useEffect,
    useState,
    Component,
    onMounted,
    onPatched,
    onWillPatch,
    onWillStart,} = owl;

const HEADINGS = [
    'H1',
    'H2',
    'H3',
    'H4',
    'H5',
    'H6',
];
let observerId = 0;

const fetchValidHeadings = (element) => {
    const templateHeadings = Array.from(element.querySelectorAll(
        HEADINGS.map((heading) => `.o_knowledge_behavior_type_template ${heading}`).join(',')
    ));

    return Array.from(element.querySelectorAll(HEADINGS.join(',')))
        .filter((heading) => heading.innerText.trim().replaceAll('\u200B', '').length > 0)
        .filter((heading) => !templateHeadings.includes(heading));
};

export class Table extends Component {
    setup () {
        super.setup();
        this.state = useState({
            toc: []
        });
        if (!this.props.readonly) {
            useEffect(() => {
                const onDrop = event => {
                    event.preventDefault();
                    event.stopPropagation();
                };
                const observer = this.setMutationObserver();
                this.props.anchor.addEventListener('drop', onDrop);
                return () => {
                    observer.disconnect();
                    this.props.anchor.removeEventListener('drop', onDrop);
                };
            });
            this.props.anchor.setAttribute('contenteditable', 'false');
            onWillStart(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onWillPatch(() => {
                this.editor.observerUnactive(`knowledge_behavior_id_${this.observerId}`);
                this.editor.idSet(this.props.anchor);
            });
            onMounted(() => {
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
            onPatched(() => {
                this.editor.idSet(this.props.anchor);
                this.editor.observerActive(`knowledge_behavior_id_${this.observerId}`);
            });
        }

        onWillStart(() => {
            this._updateTableOfContents();
        });
    }

    get editor () {
        return this.props.wysiwyg.odooEditor;
    }

    setMutationObserver () {
        const observer = new MutationObserver(mutationList => {
            const update = mutationList.find(mutation => {
                if (Array.from(mutation.addedNodes).find(node => HEADINGS.includes(node.tagName)) ||
                    Array.from(mutation.removedNodes).find(node => HEADINGS.includes(node.tagName))) {
                    // We just added/removed a header node -> update the ToC
                    return true;
                }

                if (this.editor.powerbox.isOpen) {
                    if (this.updateTimeout) {
                        window.clearTimeout(this.updateTimeout);
                    }
                    return false;
                }

                const target = mutation.target;
                const headerNode = this._findClosestHeader(target);

                return headerNode && headerNode.parentElement === this.props.root;
            });
            if (update) {
                this.delayedUpdateTableOfContents();
            }
        });
        observer.observe(this.props.root, {
            childList: true,
            attributes: false,
            subtree: true,
            characterData: true,
        });
        return observer;
    }

    delayedUpdateTableOfContents() {
        if (this.updateTimeout) {
            window.clearTimeout(this.updateTimeout);
        }
        this.updateTimeout = window.setTimeout(this._updateTableOfContents.bind(this), 500);
    }

    _findClosestHeader(node) {
        if (node && node.nodeType === Node.TEXT_NODE) {
            // we are modifying the text of a Node, check its parent
            node = node.parentElement;
        }

        if (node && node.nodeType === Node.ELEMENT_NODE) {
            if (!HEADINGS.includes(node.tagName)) {
                node = node.closest(HEADINGS.join(','));
            }

            if (node && HEADINGS.includes(node.tagName)) {
                return node;
            }
        }

        return undefined;
    }

    _updateTableOfContents () {

        let currentDepthByTag = {};
        let previousTag = undefined;
        let previousDepth = -1;
        let index = 0;

        this.state.toc = fetchValidHeadings(this.props.root).map(heading => {
            let depth = HEADINGS.indexOf(heading.tagName);
            if (depth !== previousDepth && heading.tagName === previousTag) {
                depth = previousDepth;
            } else if (depth > previousDepth) {
                if (heading.tagName !== previousTag && HEADINGS.indexOf(previousTag) < depth) {
                    depth = previousDepth + 1;
                } else {
                    depth = previousDepth;
                }
            } else if (depth < previousDepth) {
                if (currentDepthByTag.hasOwnProperty(heading.tagName)) {
                    depth = currentDepthByTag[heading.tagName];
                }
            }

            previousTag = heading.tagName;
            previousDepth = depth;

            // going back to 0 depth, wipe-out the 'currentDepthByTag'
            if (depth === 0) {
                currentDepthByTag = {};
            }
            currentDepthByTag[heading.tagName] = depth;

            return {
                depth: depth,
                index: index++,
                name: heading.innerText,
                tagName: heading.tagName,
            };
        });
    }

    _onTocLinkClick (event) {
        event.preventDefault();
        const headingIndex = parseInt(event.target.getAttribute('data-oe-nodeid'));
        const targetHeading = fetchValidHeadings(this.props.root)[headingIndex];
        if (targetHeading){
            targetHeading.scrollIntoView({
                behavior: 'smooth',
            });
            targetHeading.classList.add('o_knowledge_header_highlight');
            window.setTimeout(() => {
                targetHeading.classList.remove('o_knowledge_header_highlight');
            }, 2000);
        } else {
            this._updateTableOfContents();
        }
    }
}

Table.template = "openeducat_knowledge.TableOfContentBehavior";
Table.components = {};
Table.props = {
    readonly: { type: Boolean },
    anchor: { type: Element },
    wysiwyg: { type: Object, optional: true},
    record: { type: Object },
    root: { type: Element },
};
