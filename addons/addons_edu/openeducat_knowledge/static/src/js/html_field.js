/** @odoo-module */

import { HtmlField } from "@web_editor/js/backend/html_field";
import { KnowledgePlugin } from "@openeducat_knowledge/js/knowledge_plugin";
import { patch } from "@web/core/utils/patch";
import { templates } from "@web/core/assets";
import { useService } from "@web/core/utils/hooks";

import { BehaviourView } from '@openeducat_knowledge/components/behaviour/behaviour_view';
import { Link } from '@openeducat_knowledge/components/behaviour/link';
import { File } from '@openeducat_knowledge/components/behaviour/file';
import { Article } from '@openeducat_knowledge/components/behaviour/article';
import { Table } from '@openeducat_knowledge/components/behaviour/table';
import { Template } from '@openeducat_knowledge/components/behaviour/template';

const { App, onMounted, onPatched, onWillDestroy, onWillUnmount, markup } = owl;

const behaviorTypes = {
    o_knowledge_behavior_type_internal_view: {
        Behavior: BehaviourView
    },
    o_knowledge_behavior_type_file: {
        Behavior: File,
    },
    o_knowledge_behavior_type_view_link: {
        Behavior: Link
    },
    o_knowledge_behavior_type_articles_structure: {
        Behavior: Article
    },
    o_knowledge_behavior_type_toc: {
        Behavior: Table,
    },
    o_knowledge_behavior_type_template: {
        Behavior: Template,
    },
};

const HtmlFieldPatch = {
    setup() {
        this._super(...arguments);
        this.behaviorAnchors = new Set();
        this.bindedDelayedRefreshBehaviors = this.delayedRefreshBehaviors.bind(this);
        this.uiService = useService('ui');
        onWillUnmount(() => {
            if (!this.props.readonly) {
                this._removeRefreshBehaviorsListeners();
            }
        });
        onMounted(() => {
            if (this.props.readonly) {
                this.updateBehaviors();
            }
        });
        onPatched(() => {
            this.updateBehaviors();
        });
        onWillDestroy(() => {
            for (const anchor of Array.from(this.behaviorAnchors)) {
                if (anchor.oKnowledgeBehavior) {
                    anchor.oKnowledgeBehavior.destroy();
                    delete anchor.oKnowledgeBehavior;
                }
            }
        });
    },
    /**
     * @returns {Object}
     */
    get behaviorTypes() {
        return behaviorTypes;
    },
    /**
     * @returns {HTMLElement}
     */
    get injectorEl() {
        if (this.props.readonly && this.readonlyElementRef.el) {
            return this.readonlyElementRef.el;
        } else if (this.wysiwyg && this.wysiwyg.$editable) {
            return this.wysiwyg.$editable[0];
        }
        return null;
    },
    /**
     * @returns {integer}
     */
    delayedRefreshBehaviors() {
        return window.setTimeout(this.updateBehaviors.bind(this));
    },
    /**
     * @override
     * @param {Widget} wysiwyg
     */
    async startWysiwyg(wysiwyg) {
        await this._super(...arguments);
        this._addRefreshBehaviorsListeners();
        await this.updateBehaviors();
    },
    /**
     * @param {Array[Object]} behaviorsData
     */
    async updateBehaviors(behaviorsData = [], target = null) {
        const injectorEl = target || this.injectorEl;
        if (!injectorEl) {
            return;
        }
        if (!behaviorsData.length) {
            this._scanFieldForBehaviors(behaviorsData, injectorEl);
        }
        for (const behaviorData of behaviorsData) {
            const anchor = behaviorData.anchor;
            if (!document.body.contains(anchor)) {
                return;
            }
            const {Behavior} = this.behaviorTypes[behaviorData.behaviorType] || {};
            if (!Behavior) {
                return;
            }
            if (!anchor.oKnowledgeBehavior) {
                if (!this.props.readonly && this.wysiwyg && this.wysiwyg.odooEditor) {
                    this.wysiwyg.odooEditor.observerUnactive('injectBehavior');
                }
                const props = {
                    readonly: this.props.readonly,
                    anchor: anchor,
                    wysiwyg: this.wysiwyg,
                    record: this.props.record,
                    root: this.injectorEl
                };
                let behaviorProps = {};
                if (anchor.hasAttribute("data-behavior-props")) {
                    try {
                        behaviorProps = JSON.parse(decodeURIComponent(anchor.dataset.behaviorProps));
                    } catch {
                        behaviorProps = JSON.parse(anchor.dataset.behaviorProps);
                    }
                }
                for (const prop in behaviorProps) {
                    if (prop in Behavior.props) {
                        props[prop] = behaviorProps[prop];
                    }
                }
                const propNodes = anchor.querySelectorAll("[data-prop-name]");
                for (const node of propNodes) {
                    if (node.dataset.propName in Behavior.props) {
                        props[node.dataset.propName] = markup(node.innerHTML);
                    }
                }
                anchor.replaceChildren();
                if (!this.props.readonly && this.wysiwyg && this.wysiwyg.odooEditor) {
                    this.wysiwyg.odooEditor.observerActive('injectBehavior');
                }
                const config = (({env, dev, translatableAttributes, translateFn}) => {
                    return {env, dev, translatableAttributes, translateFn};
                })(this.__owl__.app);
                anchor.oKnowledgeBehavior = new App(Behavior, {
                    ...config,
                    templates: templates,
                    props,
                });
                //this.behaviorApps.add(anchor.oKnowledgeBehavior);
                await anchor.oKnowledgeBehavior.mount(anchor);
                if (!this.props.readonly && this.wysiwyg && this.wysiwyg.odooEditor) {
                    if (behaviorData.setCursor && anchor.oKnowledgeBehavior.root.component.setCursor) {
                        anchor.oKnowledgeBehavior.root.component.setCursor();
                    }
                    this.wysiwyg.odooEditor.historyStep();
                }
                await this.updateBehaviors([], anchor);
            }
        }
    },
    _addRefreshBehaviorsListeners() {
        if (this.wysiwyg.odooEditor) {
            this.wysiwyg.odooEditor.addEventListener('historyUndo', this.bindedDelayedRefreshBehaviors);
            this.wysiwyg.odooEditor.addEventListener('historyRedo', this.bindedDelayedRefreshBehaviors);
        }
        if (this.wysiwyg.$editable.length) {
            this.wysiwyg.$editable[0].addEventListener('paste', this.bindedDelayedRefreshBehaviors);
            this.wysiwyg.$editable[0].addEventListener('drop', this.bindedDelayedRefreshBehaviors);
            this.wysiwyg.$editable.on('refresh_behaviors', this._onRefreshBehaviors.bind(this));
        }
    },
    _onRefreshBehaviors(e, data = {}) {
        this.updateBehaviors("behaviorsData" in data ? data.behaviorsData : []);
    },
    _removeRefreshBehaviorsListeners() {
        if (this.wysiwyg.odooEditor) {
            this.wysiwyg.odooEditor.removeEventListener('historyUndo', this.bindedDelayedRefreshBehaviors);
            this.wysiwyg.odooEditor.removeEventListener('historyRedo', this.bindedDelayedRefreshBehaviors);
        }
        if (this.wysiwyg.$editable.length) {
            this.wysiwyg.$editable[0].removeEventListener('paste', this.bindedDelayedRefreshBehaviors);
            this.wysiwyg.$editable[0].removeEventListener('drop', this.bindedDelayedRefreshBehaviors);
            this.wysiwyg.$editable.off('refresh_behaviors');
        }
    },
    /**
     * @param {Array[Object]} behaviorsData
     * @param {HTMLElement} target
     */
    _scanFieldForBehaviors(behaviorsData, target) {
        const anchors = new Set();
        const types = new Set(Object.getOwnPropertyNames(this.behaviorTypes));
        const anchorNodes = target.querySelectorAll('.o_knowledge_behavior_anchor');
        const anchorNodesSet = new Set(anchorNodes);
        for (const anchorNode of anchorNodes) {
            const anchorSubNodes = anchorNode.querySelectorAll('.o_knowledge_behavior_anchor');
            for (const anchorSubNode of anchorSubNodes) {
                anchorNodesSet.delete(anchorSubNode);
            }
        }
        for (const anchor of Array.from(anchorNodesSet)) {
            const type = Array.from(anchor.classList).find(className => types.has(className));
            if (type) {
                behaviorsData.push({
                    anchor: anchor,
                    behaviorType: type,
                });
                anchors.add(anchor);
            }
        }
        const differenceAnchors = new Set([...this.behaviorAnchors].filter(anchor => !anchors.has(anchor)));
        differenceAnchors.forEach(anchor => {
            if (anchor.oKnowledgeBehavior) {
                anchor.oKnowledgeBehavior.destroy();
                delete anchor.oKnowledgeBehavior;
            }
        });
    },
};

const extractProps = HtmlField.extractProps;

HtmlField.extractProps = ({ attrs, field }) => {
    const props = extractProps({ attrs, field });
    props.wysiwygOptions.knowledgeCommands = attrs.options.knowledge_commands;
    props.wysiwygOptions.editorPlugins.push(KnowledgePlugin);
    return props;
};

patch(HtmlField.prototype, 'knowledge_html_field', HtmlFieldPatch);
