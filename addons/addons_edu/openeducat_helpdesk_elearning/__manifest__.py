{
    'name': "OpenEduCat Helpdesk E-Learning",
    'summary': """
        helpdesk e-learning
        """,
    'author': "OpenEduCat Inc",
    'category': 'eLearning',
    'version': '16.0.1.0',
    'website': 'http://www.openeducat.org',
    'license': 'Other proprietary',
    'depends': ['openeducat_core_enterprise',
                'openeducat_helpdesk_basic', 'openeducat_lms'],
    'data': [
        'views/elearning_views.xml',
    ],
}
