# -*- coding: utf-8 -*-

{
    'name': 'Project Template',
    'version': '16.0.1.0.0',
    'category': 'Services/Project',
    'description': '',
    'summary': 'Project Template',
    'sequence': '1',
    'author': 'AnhCT',
    'license': 'LGPL-3',
    'company': '',
    'maintainer': '',
    'support': '',
    'website': '',
    'depends': ['project'],
    'data': [
        'security/ir.model.access.csv',
        'views/project_template_views.xml',
        'views/project_views.xml',
        'views/menu.xml'
    ],
    'pre_init_hook': '',
    'installable': True,
    'application': True,
    'auto_install': False,
}
